
Json cdr's  with timescaledb

## Create table

```sql
CREATE extension if not exists timescaledb;
CREATE TABLE public.tbl_convy_oh_my_dev (
	"time" timestamptz NOT NULL,
	metric_name text NOT NULL,
	"data" jsonb NULL,
	value float8 NULL,
	CONSTRAINT pk_metric_name_time_tbl_convy_oh_my_dev PRIMARY KEY (metric_name, "time")
);

SELECT create_hypertable('tbl_convy_oh_my_dev', 'time');
SELECT set_chunk_time_interval('tbl_convy_oh_my_dev', INTERVAL '24 hours');

CREATE INDEX idxgin_tbl_convy_oh_my_dev ON tbl_convy_oh_my_dev USING GIN (data);




#CREATE INDEX idxgin_start_uepoch_idxgin_tbl_convy_oh_my_dev
  ON idxgin_tbl_convy_oh_my_dev(((data->'variables'->>'start_uepoch')::timestamptz))


```


## Dump part

```sh
PGPASSWORD=$POSTGRESQL_PASSWORD pg_dump -U postgres --dbname convy_cdrs --load-via-partition-root --no-owner --column-inserts --data-only  -t tbl_convy_oh_my_dev_p2023_02_20 > /tmp/tbl_convy_oh_my_dev_p2023_02_20_data_only.dump
```

single

PGPASSWORD=$POSTGRESQL_PASSWORD pg_dump -U postgres --dbname convy_cdrs --load-via-partition-root --no-owner --column-inserts --data-only  -t tbl_convy_oh_my_dev > /tmp/tbl_convy_oh_my_dev.dump


## Restore

```sql

SELECT timescaledb_pre_restore();
```

Restore in shell

```sh
PGPASSWORD=$POSTGRESQL_PASSWORD psql -U postgres --dbname convy_cdrs -f /tmp/tbl_convy_oh_my_dev_p2023_02_20_data_only.dump
```


```sql
SELECT timescaledb_post_restore();
```

## Count

```sql
select count(*)
from tbl_convy_oh_my_dev
WHERE time at time zone 'utc' >= '2023-02-20 00:00:00' and time at time zone 'utc' < '2023-02-21 00:00:00';

```


