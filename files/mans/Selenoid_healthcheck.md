Установка зависимостей

```sh
 apt-get install python3-pip
python3.8 -m pip install selenium python-dotenv
```

Создаем скрипт /docker-compose-local/SWARM/services/selenoid-chrome_2/docker-healthcheck.sh



```sh                                                                          
#!/usr/bin/env bash

if [[ -f "./selenium-check.py" ]];then
        python3.8 ./selenium-check.py
        if [[ $? == "0" ]];then
                echo "Alive"
        else
                echo "Bad health - kill main process"
                /usr/local/bin/docker-compose down || (echo "skip stop")
                /usr/local/bin/docker-compose up -d
        fi
fi
echo "Sleep 50"
sleep 50

````





Создаем скрипт /docker-compose-local/SWARM/services/selenoid-chrome_2/selenium-check.py                                                                                      

```py                                                                                                      
#!/usr/bin/env python3.8
from selenium import webdriver
import os
from selenium.common.exceptions import WebDriverException
import dotenv
import sys
dotenv.load_dotenv()
try:
    browser = webdriver.Remote(
        command_executor="http://%s:%s" % (os.getenv("SELENIUM_HOST", "192.168.100.108"), os.getenv("SELENIUM_PORT", "4445")),
    )
    browser.close()
except WebDriverException as error:
    print("Healtchcheck bad", file = sys.stdout)
    print(error)
    exit(1)
print("Healtchcheck finished", file = sys.stdout)
exit(0)
```


Создаем unit supervisor /etc/supervisor/conf.d/check_selenoid-chrome_2.conf


```conf
[supervisord]
nodaemon=true

[program:selenoid-chrome_2]
command=sh -c "while true;do cd /docker-compose-local/SWARM/services/selenoid-chrome_2 && bash docker-healthcheck.sh;done"
autostart=true
autorestart=true
stdout_logfile=/var/log/selenoid-chrome_2.log
stdout_logfile_maxbytes=0
stderr_logfile=/var/log/selenoid-chrome_2.log
stderr_logfile_maxbytes=0
startretries=140
startretries=140
stopwaitsecs=10
user=root

```

Делаем файлы исполняемыми
```
chmod +x /docker-compose-local/SWARM/services/selenoid-chrome_2/selenium-check.py     /docker-compose-local/SWARM/services/selenoid-chrome_2/docker-healthcheck.sh         
```

Перечитвываем конфиги и запускаем проверку
```

supervisorctl reread
supervisorctl update
```