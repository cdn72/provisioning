# From Node, image page: <https://hub.docker.com/_/node>
FROM node:16.18-alpine as node

# From php8.1.21, image page: <https://hub.docker.com/_/php>
FROM php:8.1.21-alpine

## Copy node and yarn
COPY --from=node /usr/local/bin /usr/local/bin
COPY --from=node /usr/local/lib /usr/local/lib
COPY --from=node /opt /opt

# Install composer, image page: <https://hub.docker.com/_/composer>
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Install roadrunner, image page: <https://hub.docker.com/r/spiralscout/roadrunner>
COPY --from=spiralscout/roadrunner:2.8.4 /usr/bin/rr /usr/bin/rr

# Caching composer and npm in volume
ENV COMPOSER_HOME="/tmp/composer"
ENV YARN_CACHE_FOLDER="/tmp/yarn"

ARG NPM_TOKEN
ARG UI_PACKAGE_REPO_HOST
ARG UI_PACKAGE_PROJECT_ID
ENV NPM_TOKEN=${NPM_TOKEN}
ENV UI_PACKAGE_REPO_HOST=${UI_PACKAGE_REPO_HOST}
ENV UI_PACKAGE_PROJECT_ID=${UI_PACKAGE_PROJECT_ID}

ARG APP_UID=10001

# Install common dependencies
RUN set -x \
    && apk update \
    # Permanent dependencies
    && apk add --no-cache \
        postgresql-libs \
        icu-libs \
        vim curl git \
        zip unzip \
        libpng-dev libzip-dev libpq-dev icu-dev libc6-compat \
        jpegoptim optipng pngquant gifsicle \
        mysql mysql-client \
    && apk add --no-cache \
        chromium \
        fontconfig \
        msttcorefonts-installer \
        && update-ms-fonts \
        fc-cache -f \
    # Temp depedencies
    && apk add --no-cache --virtual .build-deps \
        postgresql-dev \
        autoconf openssl \
        make g++

RUN set -x \
    # Install extensions
    && docker-php-ext-install \
        pdo \
        pdo_mysql \
        pdo_pgsql \
        intl \
        zip \
        gd \
        pcntl \
        exif \
        sockets \
    && pecl install redis \
    && docker-php-ext-enable redis

RUN set -x \
    # Install supercronic (for laravel task scheduling), project page: <https://github.com/aptible/supercronic>
    && wget -q "https://github.com/aptible/supercronic/releases/download/v0.1.12/supercronic-linux-amd64" \
        -O /usr/bin/supercronic \
    && chmod +x /usr/bin/supercronic \
    && mkdir -p /etc/supercronic \
    && echo '*/1 * * * * php /www/artisan schedule:run' > /etc/supercronic/laravel

RUN set -x \
    # Generate self-signed SSL key and certificate files
    && mkdir -p /etc/ssl/private \
    && openssl req -x509 -nodes -days 1095 -newkey rsa:2048 \
        -subj "/C=CA/ST=QC/O=Company, Inc./CN=mydomain.com" \
        -addext "subjectAltName=DNS:mydomain.com" \
        -keyout /etc/ssl/private/selfsigned.key \
        -out /etc/ssl/certs/selfsigned.crt \
    && chmod 644 /etc/ssl/private/selfsigned.key

RUN set -x \
    # enable opcache for CLI and JIT, docs: <https://www.php.net/manual/en/opcache.configuration.php#ini.opcache.jit>
    && echo -e "\nopcache.enable=1\nopcache.enable_cli=1\nopcache.jit_buffer_size=32M\nopcache.jit=1235\nopcache.revalidate_freq=0\n" >> \
        ${PHP_INI_DIR}/conf.d/docker-php-ext-opcache.ini \
    # adjust php memory limit
    && echo 'memory_limit = 4096M' >> ${PHP_INI_DIR}/conf.d/docker-php-ram-limit.ini

RUN set -x \
    # Clean up
    && docker-php-source delete \
    && apk del .build-deps \
    && rm -R /tmp/pear

RUN set -x \
    && adduser \
        --disabled-password \
        --shell "/sbin/nologin" \
        --uid "${APP_UID:-10001}" \
        --gecos "" \
        --home "/home/appuser" \
        "appuser"

WORKDIR /
# Yarn error workaround
RUN echo "" > .yarnrc \
    && chmod 777 .yarnrc \
    && mkdir /.config \
    && chmod -R 777 /.config \
    && mkdir /.yarn \
    && chmod -R 777 /.yarn
# Tinker error workaround
RUN mkdir -p /.config/psysh \
    && chmod -R 777 /.config/psysh

RUN set -x \
    # Create directory for application sources, roadrunner unix socket and yarn cache
    && mkdir -p /www /var/run/rr ${YARN_CACHE_FOLDER}/v6 \
    && chown -R appuser:appuser /www \
    && chmod -R 777 /www /var/run/rr ${YARN_CACHE_FOLDER}

USER appuser:appuser

RUN mkdir -p /www/frontend /www/vuepress

WORKDIR /www/frontend

# Create script with UI repo auth update
RUN set -x \
    && echo "npm config set \"//\${UI_PACKAGE_REPO_HOST}/api/v4/projects/:_authToken\" \"\${NPM_TOKEN}\"" > ~/ui_repo_auth.sh \
    && echo "npm config set \"//\${UI_PACKAGE_REPO_HOST}/api/v4/projects/\${UI_PACKAGE_PROJECT_ID}/packages/npm/:_authToken\" \"\${NPM_TOKEN}\"" >> ~/ui_repo_auth.sh
# Copy yarn.lock and package.json
COPY --chown=appuser:appuser ./frontend/package.json ./frontend/yarn.* ./frontend/.npmrc /www/frontend/
# Copy dir with extra shared files/dirs
COPY --chown=appuser:appuser ./.docker /www/.docker
# Install frontend dependencies
RUN set -x \
    && sh ~/ui_repo_auth.sh \
    && yarn install

WORKDIR /www/vuepress
# Copy yarn.lock and package.json
COPY --chown=appuser:appuser ./vuepress/package.json ./vuepress/yarn.* /www/vuepress/
# Install vuepress for docs generator
RUN yarn install

WORKDIR /www
# Install additional node dependencies inside root folder
COPY --chown=appuser:appuser ./package.json ./yarn.* /www/
RUN yarn install
# Copy composer.lock and composer.json
COPY --chown=appuser:appuser ./composer.* ./prepare-composer.js ./auth.* /www/
# Install backend dependencies (no scripts/autoload, will do later)
RUN yarn prepare-composer
RUN composer install --ansi --prefer-dist --no-scripts --no-autoloader

COPY --chown=appuser:appuser . /www/
RUN set -x \
    && composer dump-autoload --no-scripts \
    && chmod -R 777 ${COMPOSER_HOME}/cache

CMD ["tail", "-f", "/dev/null"]
