
export PHP_COMPOSER_VERSION=2.3.5 PHP_EXT_VER_SWOOLE_ASYNC=v4.5.5 PHP_EXT_VER_SWOOLE=v5.0.3 PHP_EXT_VER_APCU_BC=5.1.21 PHP_EXT_VER_APCU=5.1.22 PHP_EXT_VER_REDIS=5.3.7 PHP_EXT_VER_IMAGICK=3.7.0 IMAGICK_COMMIT=448c1cd0d58ba2838b9b6dff71c9b7e70a401b90
export PHP_EXTS \
        intl \
        imap \
        zip \
        pdo_mysql \
        xml \
        exif \
        bcmath \
        mysqli \
        pdo \
        json \
        ctype \
        tokenizer \
        mbstring \
        pcntl \
        pdo_pgsql \
        pgsql \
        sockets
export EXTRA_DEPS="gnupg2 wget lsb-release"
export DEBIAN_FRONTEND=noninteractive




part_1 () {
apt-get update; apt-get install -y \
${EXTRA_DEPS} \
&& sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' \
&& wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc |  apt-key add - \
&& apt-get update;   apt-get install -y postgresql-client-15 \
        msmtp \
        default-mysql-client \
        bash \
        git \
        curl \
        supervisor \
        nginx \
        openssh

}
part_1
      
part_2 () {
 apt-get install -y \
        libxml2-dev \
        libzip-dev \
libgmp-dev \
libpng-dev \
libjpeg62-turbo-dev \
libwebp-dev \
zlib1g-dev \
ghostscript \
nano \
libltdl7-dev \
libfreetype6-dev \
libxpm-dev \
libonig-dev \
libvpx-dev \
libxslt1-dev \
libmcrypt-dev \
libgmp10 \
libgmp-dev \
ffmpeg \
python3-dev \
screen \
rsync \
php8.1-dev \
krb5-dev \
jq \
chromium-browser \
fontconfig \
msttcorefonts-installer \
nodejs \
npm \
yarn \
imagemagick \
imagemagick-dev \
libpq-dev \
libstdc++6-dev \
zlib1g-dev \
libcurl4-openssl-dev \
libssl-dev \
libpcre3-dev \
libpcre2-dev \
zlib1g-dev \
        $PHPIZE_DEPS
echo ok
exit 1
}
part_2

update-ms-fonts && fc-cache -f \
&& docker-php-ext-configure zip \
&& docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
&& docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
&& for PHP_EXT in $PHP_EXTS;do echo "Install php ext begin  $PHP_EXT"; docker-php-ext-install -j$(nproc) $PHP_EXT ;echo "Install php ext finidhed  $PHP_EXT";done \
&& mkdir -p /usr/src/php/ext/ \
    && cd /usr/src/php/ext/ \
    && curl -L  https://github.com/phpredis/phpredis/archive/refs/tags/${PHP_EXT_VER_REDIS}.tar.gz -o /usr/src/php/ext/phpredis-${PHP_EXT_VER_REDIS}.tar.gz \
    && ls -la * \
    && tar -xvf phpredis-${PHP_EXT_VER_REDIS}.tar.gz \
    && rm -r /usr/src/php/ext/phpredis-${PHP_EXT_VER_REDIS}.tar.gz \
    && cd /usr/src/php/ext/ \
    && docker-php-ext-install phpredis-${PHP_EXT_VER_REDIS} \
    && pecl install apcu-$PHP_EXT_VER_APCU \
    && mkdir -p /usr/src/php/ext/ \
    && curl -L https://github.com/krakjoe/apcu/archive/refs/tags/v${PHP_EXT_VER_APCU_BC}.tar.gz -o /usr/src/php/ext/v${PHP_EXT_VER_APCU_BC}.tar.gz  \
    && cd /usr/src/php/ext/ \
    && tar -xf v${PHP_EXT_VER_APCU_BC}.tar.gz \
    && mv apcu-${PHP_EXT_VER_APCU_BC} apcu_bc \
    && cd /usr/src/php/ext/apcu_bc \
    && docker-php-ext-install apcu_bc \
    && echo "Install OK apcu_bc-$PHP_EXT_VER_APCU_BC" \
&& git clone https://github.com/Imagick/imagick /usr/src/php/ext/imagick \
    && cd /usr/src/php/ext/imagick  \
    && git checkout $PHP_EXT_VER_IMAGICK  \
    && phpize  \
    && ./configure  \
    && make  \
    && make install \
&& curl -sfL https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer  \
    && chmod +x /usr/local/bin/composer \
    && docker-php-source extract \
    && mkdir /usr/src/php/ext/swoole \
    && curl -sfL https://github.com/swoole/swoole-src/archive/${PHP_EXT_VER_SWOOLE}.tar.gz -o swoole.tar.gz \
    && tar xfz swoole.tar.gz --strip-components=1 -C /usr/src/php/ext/swoole \
    && docker-php-ext-configure swoole \
        --enable-mysqlnd \
        --enable-openssl \
        --enable-sockets \
        --enable-swoole-curl && docker-php-ext-install -j$(nproc) swoole \
    && curl -L https://gitlab.com/cdn72/extra-files/-/raw/main/files/php/docker-php-ext-enable-8.1  -o /usr/local/bin/docker-php-ext-enable \
    && chmod +x /usr/local/bin/docker-php-ext-enable \
    && docker-php-ext-configure gd \
        --with-freetype=/usr/include/freetype2/freetype/ \
        --with-webp=/usr/include/webp/ \
        --with-xpm=/usr/include/ \
        --with-jpeg=/usr/include/ && docker-php-ext-install -j$(nproc) gd \
    && echo "List extensions" \
    && ls -la /usr/local/lib/php/extensions/* \
    && echo "extension=imagick.so" > /usr/local/etc/php/conf.d/docker-php-ext-imagick.ini \
    && docker-php-ext-enable --ini-name docker-php-ext-opcache.ini opcache \
    && echo "----EXTENSION swoole----" && php --ri swoole && echo "----EXTENSION swoole----" \
    && docker-php-source delete \
    && apk del .build-deps \
    && ls -la /usr/local/etc/php/conf.d/ \
    && rm -rf /usr/src/php/ext/* /tmp/* /usr/src/* $HOME/.composer/*-old.phar


delete_deps() {
    cd /tmp \
    && apt-mark auto '.*' > /dev/null \
    && { [ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; } \
    && find /usr/local -type f -executable -exec ldd '{}' ';'   2>/dev/null \
        | grep -ve "not a dynamic executable" \
        | awk '/=>/ { print $(NF-1) }' \
        | sort -u \
        | xargs -r dpkg-query --search \
        | cut -d: -f1 \
        | sort -u   \
        | xargs -r apt-mark manual \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false
}



#########

    mkdir -p /usr/local/etc/php-fpm.d \
    && mkdir -p  /etc/supervisor.d/ \
       && echo 'Create supervisord worker conf'  \
       && { \
        echo '[supervisord]'; \
        echo 'nodaemon=true'; \
        echo ''; \
        echo ''; \
        echo '[program:php-fpm]'; \
        echo 'command=/usr/local/sbin/php-fpm -F --pid /var/run/php-fpm.pid'; \
        echo 'stdout_logfile=/dev/stdout'; \
        echo 'stdout_logfile_maxbytes=0'; \
        echo 'stderr_logfile=/dev/stderr'; \
        echo 'stderr_logfile_maxbytes=0'; \
        echo 'autorestart=false'; \
        echo 'startretries=0'; \
        echo ''; \
        echo '[program:nginx]'; \
        echo "command=nginx -g 'daemon off;'"; \
        echo 'stdout_logfile=/dev/stdout'; \
        echo 'stdout_logfile_maxbytes=0'; \
        echo 'stderr_logfile=/dev/stderr'; \
        echo 'stderr_logfile_maxbytes=0'; \
        echo 'autorestart=false'; \
        echo 'startretries=0'; \
          } | tee /etc/supervisor.d/worker.ini \
       && echo 'Change php.ini'  \
       && { \
        echo 'upload_max_filesize = 2000M'; \
        echo 'doc_root ='; \
        echo 'user_dir ='; \
        echo 'enable_dl = Off'; \
        echo 'allow_url_fopen = On'; \
        echo 'short_open_tag = On'; \
        echo 'post_max_size = 4000M'; \
        echo 'memory_limit = 18000M'; \
        echo 'file_uploads = On'; \
        echo 'max_input_vars = 2000'; \
        echo 'max_execution_time = 280'; \
        echo 'opcache.max_accelerated_files = 20000'; \
        echo 'opcache.memory_consumption = 256'; \
        echo 'opcache.interned_strings_buffer = 64'; \
        echo 'cgi.fix_pathinfo = 0'; \
        echo 'opcache.force_restart_timeout = 180'; \
          } | tee /usr/local/etc/php/conf.d/php.ini \
       && if [ -f "/usr/local/etcphp-fpm.d/www.conf" ];then rm /usr/local/etc/php-fpm.d/www.conf;else touch  /usr/local/etc/php-fpm.d/www.conf;fi\
       && echo 'Create www php-fpm pool'  \
       && { \
        echo '[global]'; \
        echo 'error_log = /proc/self/fd/2'; \
        echo '[www]'; \
        echo 'user = www-data'; \
        echo 'group = www-data'; \
        echo 'listen.owner = www-data'; \
        echo 'listen.group = www-data'; \
        echo 'listen = 9000'; \
        echo 'pm = dynamic'; \
        echo 'pm.max_children = 30'; \
        echo 'pm.start_servers = 19'; \
        echo 'pm.min_spare_servers = 19'; \
        echo 'pm.max_spare_servers = 20'; \
        echo 'pm.max_requests = 400'; \
        echo 'rlimit_files    = 131072'; \
        echo 'rlimit_core   = unlimited'; \
        echo 'clear_env = no'; \
        echo 'php_admin_flag[short_open_tag] = on'; \
        echo 'php_admin_value[date.timezone] = UTC'; \
        echo 'php_value[memory_limit] = 8000M'; \
        echo 'php_admin_value[memory_limit] = 8000M'; \
        echo 'php_value[max_file_uploads] = 550'; \
        echo 'php_value[upload_max_filesize] = 2000M'; \
        echo 'php_value[post_max_size] = 2000M'; \
        echo 'pm.status_path = /status'; \
        echo 'catch_workers_output = yes'; \
        echo 'php_flag[display_errors] = on'; \
        echo 'php_admin_value[error_reporting] = E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED'; \
        echo 'access.log = /proc/self/fd/2'; \
        echo 'php_admin_flag[log_errors] = on'; \
          } | tee /usr/local/etc/php-fpm.d/www.conf \
       && rm /usr/local/etc/php-fpm.conf \
       && echo 'Create php-fpm.conf'  \
       && { \
        echo '[global]'; \
        echo 'pid = /var/run/php-fpm.pid'; \
        echo 'rlimit_core = 0'; \
        echo 'events.mechanism = epoll'; \
        echo 'include=etc/php-fpm.d/*.conf'; \
          } | tee /usr/local/etc/php-fpm.conf \
       && cd /usr/local/etc/php-fpm.d/ \
       && rm docker.conf  www.conf.default  zz-docker.conf
ENV CONSUL_VERSION 1.14.4
ENV CONSUL_TEMPLATE_VERSION 0.31.0
RUN echo "Install consul" \
    && if [[ ! -d "/opt/hashicop" ]];then mkdir /opt/hashicop;fi \
    && if [[ ! -d "/var/log/consul" ]];then mkdir /var/log/consul;fi \
    && curl -L https://gitlab.com/cdn72/extra-files/-/raw/main/files/consul/consul_${CONSUL_VERSION:-1.14.4}_linux_amd64.zip  -o /tmp/consul_${CONSUL_VERSION:-1.14.4}_linux_amd64.zip \
    && curl -L https://gitlab.com/cdn72/extra-files/-/raw/main/files/consul-template/consul-template_${CONSUL_TEMPLATE_VERSION:-0.31.0}_linux_amd64.zip  -o /tmp/consul-template_${CONSUL_TEMPLATE_VERSION:-0.31.0}_linux_amd64.zip \
    && unzip  /tmp/consul_${CONSUL_VERSION:-1.14.4}_linux_amd64.zip -d /opt/hashicop \
    && unzip /tmp/consul-template_${CONSUL_TEMPLATE_VERSION:-0.31.0}_linux_amd64.zip -d /opt/hashicop \
    && chmod +x  /opt/hashicop/* -v \
    && rm -rv /tmp/consul*.zip \
    && ln -s /opt/hashicop/consul-template /usr/local/bin/consul-template \
    && ln -s /opt/hashicop/consul /usr/local/bin/consul \
    && consul-template -v \
    && consul -v
ARG DOCKER_IMAGE_VERSION
ENV DOCKER_IMAGE_VERSION $DOCKER_IMAGE_VERSION
ADD ./files/docker-entrypoint.sh /docker-entrypoint.sh
ADD ./files/initbash_profile.sh /usr/sbin/initbash_profile
ADD ./files/bashrc_alias.sh /usr/sbin/bashrc_alias.sh
RUN rm -rf /etc/nginx/* -v 
ADD ./files/configs/nginx-backend /etc/nginx
RUN /bin/bash -C "/usr/sbin/initbash_profile" \
    && mkdir -p /var/run /var/run/nginx  /var/www  /var/lib/nginx/body /var/tmp/nginx/fastcgi \
    && chown -R www-data:www-data /var/lib/nginx  /var/tmp/nginx /var/log/nginx \
    && php -r 'print_r(gd_info());' \
    && php -i | grep imap \
    && yarn --version \
    && node --version \
    && npm --version \
    && nginx -t \
    && ls -la /etc/nginx/* \
    && chmod +x /docker-entrypoint.sh 
RUN curl -L "https://github.com/aptible/supercronic/releases/download/v0.1.12/supercronic-linux-amd64" \
    -o /usr/bin/supercronic && \
    chmod +x /usr/bin/supercronic
RUN du -sh /usr/lib/* | grep M
EXPOSE 80 8000 
WORKDIR /var/www
ENTRYPOINT ["/docker-entrypoint.sh"]
