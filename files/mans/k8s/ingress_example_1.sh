#!/usr/bin/env bash
cat <<'OEF'> /tmp/ingress-nginx-config.yaml
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: ingress-nginx-controller
  namespace: ingress-nginx
data:
  http2: "True"
  map-hash-bucket-size: "256"
  log-format-escape-json: "true"
  log-format-stream : '{"timestamp": "$time_iso8601" , "remoteAddr": "$remote_addr", "upstreamAddr": "$upstream_addr"}'
  log-format-upstream: '{"timestamp": "$time_iso8601", "requestID": "$req_id", "proxyUpstreamName":
    "$proxy_upstream_name", "proxyAlternativeUpstreamName": "$proxy_alternative_upstream_name","upstreamStatus":
    "$upstream_status", "upstreamAddr": "$upstream_addr","httpRequest":{"requestMethod":
    "$request_method", "requestUrl": "$host$request_uri", "Url": "$uri","status": $status,"requestSize":
    "$request_length", "responseSize": "$upstream_response_length", "userAgent": "$http_user_agent",
    "remoteIp": "$remote_addr", "referer": "$http_referer", "latency": "$upstream_response_time s",
    "protocol":"$server_protocol"}}'
  proxy-connect-timeout: "300"
  proxy-read-timeout: "300"
  client-max-body-size: "1500m"
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: tcp-services
  namespace: ingress-nginx
data:
  25432: "default/10.99.231.57:4445"
OEF

cat <<OEF > /tmp/ingress-nginx-config-svc.yaml
apiVersion: v1
kind: Service
metadata:
  name: ingress-nginx-controller
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx-controller
    app.kubernetes.io/part-of: ingress-nginx-controller
spec:
  type: LoadBalancer
  ports:
    - name: http
      port: 80
      targetPort: 80
      protocol: TCP
    - name: https
      port: 443
      targetPort: 443
      protocol: TCP
    - name: proxied-tcp-14445
      port: 14445
      targetPort: 14445
      protocol: TCP
  selector:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
OEF


cat <<OEF > /tmp/selenoid-1.services.linux2be.com.yaml
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: selenoid-1.services.linux2be.com
  annotations:
    nginx.ingress.kubernetes.io/configuration-snippet: |
        add_header   Set-Cookie stikyconnect=\$uri";
        add_header   Set-Cookie stikyconnect=\$host";
    nginx.ingress.kubernetes.io/use-regex: "true"
    nginx.ingress.kubernetes.io/affinity: "cookie" 
    nginx.ingress.kubernetes.io/affinity-mode: persistent
    nginx.ingress.kubernetes.io/session-cookie-name: stikyconnect2
    nginx.ingress.kubernetes.io/session-cookie-path: "$stikyconnect"
    nginx.ingress.kubernetes.io/log-format: '{"nginx.time": "$time_local",
      "nginx.host":"$host",
      "nginx.remote_addr":"$remote_addr",
      "nginx.upstream_addr":"$upstream_addr",
      "nginx.upstream_status":"$upstream_status",
      "nginx.upstream_response_time":"$upstream_response_time",
      "nginx.user_agent":"$http_user_agent"}'
spec:
  ingressClassName: nginx
  rules:
    - host: selenoid-1.services.linux2be.com
      http:
        paths:

        - path: /info
          pathType: Prefix
          backend:
            service:
              name: selenoid-chrome-stack-1-headless
              port:
                number: 14444
        - path: /
          pathType: Prefix
          backend:
            service:
              name: selenoid-chrome-stack-1-headless
              port:
                number: 4445
  tls:
    - hosts:
      - "selenoid-1.services.linux2be.com"
      secretName: wc.services.linux2be.com
OEF


cat <<OEF > /tmp/selenoid-1.services.linux2be.com.yaml.old
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: selenoid-1.services.linux2be.com
  annotations:
    nginx.ingress.kubernetes.io/server-snippet: |
      location  /test {
        rewrite /test https://itc-life permanent;
      }
    nginx.ingress.kubernetes.io/stream-snippet: |
        server {
           listen 14445;
           proxy_pass 10.106.10.140:5432;
        }
    nginx.ingress.kubernetes.io/server-snippet: |
      location  /test {
        rewrite /test https://itc-life permanent;
      }
    nginx.ingress.kubernetes.io/configuration-snippet: |
        add_header          Set-Cookie checkc=\$request";
    nginx.ingress.kubernetes.io/use-regex: "true"
    #nginx.ingress.kubernetes.io/affinity: "cookie" 
    #nginx.ingress.kubernetes.io/use-regex: "true"
    #nginx.ingress.kubernetes.io/affinity-mode: persistent
    #nginx.ingress.kubernetes.io/session-cookie-name: checkc
    #nginx.ingress.kubernetes.io/session-cookie-path: "$request"

    nginx.ingress.kubernetes.io/configuration-snippet: |  
        add_header Set-Cookie "Path=\$1; HttpOnly; Secure";
    #nginx.ingress.kubernetes.io/upstream-hash-by:  $request_uri
    nginx.ingress.kubernetes.io/affinity-mode: "persistent"
    nginx.ingress.kubernetes.io/affinity: cookie
    #nginx.ingress.kubernetes.io/use-regex: "true"
    
    #nginx.ingress.kubernetes.io/enable-real-ip: "0.0.0.0/0"
    #nginx.ingress.kubernetes.io/upstream-hash-by-subset: "true"
    nginx.ingress.kubernetes.io/upstream-hash-by: "$request_uri"
    nginx.ingress.kubernetes.io/upstream-hash-by-subset-size: "3"
    #nginx.ingress.kubernetes.io/limit-connections: "1500"
    nginx.ingress.kubernetes.io/enable-access-log: "true"
    nginx.ingress.kubernetes.io/server-names-hash-max-size: "2056"
    nginx.ingress.kubernetes.io/log-format: json
    nginx.ingress.kubernetes.io/proxy-http-version: "1.1"
    nginx.ingress.kubernetes.io/proxy-connect-timeout: "3600"
    nginx.ingress.kubernetes.io/proxy-read-timeout: "3600"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "3600"
    nginx.ingress.kubernetes.io/send-timeout: "3600"
    nginx.ingress.kubernetes.io/redirect-to-https: "false"
    #nginx.ingress.kubernetes.io/whitelist-source-range: "10.0.0.0/24,172.10.0.1,192.168.1.0/24"
    nginx.ingress.kubernetes.io/proxy-body-size: 18m
    nginx.ingress.kubernetes.io/connection-proxy-header: "keep-alive"
    nginx.ingress.kubernetes.io/session-cookie-expires: "172800"
    nginx.ingress.kubernetes.io/session-cookie-max-age: "172800"
    nginx.ingress.kubernetes.io/log-format: '{"nginx.time": "$time_local",
      "nginx.host":"$host",
      "nginx.remote_addr":"$remote_addr",
      "nginx.upstream_addr":"$upstream_addr",
      "nginx.upstream_status":"$upstream_status",
      "nginx.upstream_response_time":"$upstream_response_time",
      "nginx.user_agent":"$http_user_agent"}'
spec:
  ingressClassName: nginx
  rules:
    - host: selenoid-1.services.linux2be.com
      http:
        paths:

        - path: /info
          pathType: Prefix
          backend:
            service:
              name: selenoid-chrome-stack-1
              port:
                number: 14444
        - path: /
          pathType: Prefix
          backend:
            service:
              name: selenoid-chrome-stack-1
              port:
                number: 4445
  tls:
    - hosts:
      - "selenoid-1.services.linux2be.com"
      secretName: wc.services.linux2be.com
OEF
kubectl apply -f /tmp/ingress-nginx-config.yaml
kubectl apply -f /tmp/ingress-nginx-config-svc.yaml
kubectl apply -f /tmp/selenoid-1.services.linux2be.com.yaml