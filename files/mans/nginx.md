# nginx-logs-headers.conf

```conf
underscores_in_headers on;
lua_need_request_body on;
set $req_body "";
set $req_headers "";
set $resp_body_log "";
set $response_body "";
set $resp_headers "";
set $resp_body_log_2 "";


rewrite_by_lua_block {
    local req_headers = "Headers: ";
    ngx.var.req_body = ngx.req.get_body_data();
    local h, err = ngx.req.get_headers()
    for k, v in pairs(h) do
       req_headers = req_headers .. k .. ": " .. v .. "\n";
    end
    ngx.var.req_headers = req_headers;
    local rh = ngx.resp.get_headers()
    for k, v in pairs(rh) do
       ngx.var.resp_headers = ngx.var.resp_headers .. k.."="..v.." "
    end
}

body_filter_by_lua '
   local resp_body_log = string.sub(ngx.arg[1], 1, -1)
   ngx.ctx.buffered = (ngx.ctx.buffered or "") .. resp_body_log
   if ngx.arg[2] then
     ngx.var.resp_body_log = ngx.ctx.buffered;
   end

   local i_r = ngx.re.match(ngx.var.resp_body_log, ".*(\\r|changeDatePicker|noscript).*")

   if i_r == nil then
      return
   else
      ngx.var.resp_body_log = "truncted";
   end
';



```

nginx-logs.conf

```conf
map $uri $loggable_csv_combined_xml {
    /api/xml  1;
    default 0;
}

map $uri $loggable_csv_combined_cdr {
    /api/cdr  1;
    default 0;
}

log_format json_combined escape=json
  '{'
                   '"nginx.time":"$time_local",'
                   '"nginx.host":"$host",'
                   '"nginx.http_host":"$http_host",'
                   '"nginx.remote_addr":"$remote_addr",'
                   '"nginx.http_x_forwarded_for":"$http_x_forwarded_for",'
                   '"nginx.request_method":"$request_method",'
                   '"nginx.request":"$request",'
                   '"nginx.request_length":"$request_length",'
                   '"nginx.status":"$status",'
                   '"nginx.upstream_status":"$upstream_status",'
                   '"nginx.body_bytes_sent":"$body_bytes_sent",'
                   '"nginx.http_referer":"$http_referer",'
                   '"nginx.request_time":"$request_time",'
                   '"nginx.upstream_response_time":"$upstream_response_time",'
                   '"nginx.upstream_http_x_cache":"$upstream_http_x_cache",'
                   '"nginx.uri":"$uri",'
                   '"nginx.upstream_addr":"$upstream_addr",'
                   '"nginx.upstream_response_length":"$upstream_response_length",'
                   '"nginx.server_name":"$server_name",'
                   '"nginx.upstream_cache_status":"$upstream_cache_status",'
                   '"nginx.user_agent":"$http_user_agent",'
                   '"nginx.request_uri":"$scheme://$host$request_uri",'
                   '"nginx.req_headers":"$req_headers",'
                   '"nginx.request_body":"$request_body",'
                   '"nginx.resp_headers":"$resp_headers",'
                   '"nginx.resp_body":"$resp_body_log"'

  '}';


#log_format csv_combined_full escape=json '"$time_local"|"$host"|"$http_host"|"$remote_addr"|"$http_x_forwarded_for"|"$request_method"|"$request"|"$request_length"|"$status"|"$upstream_status"|"$body_bytes_sent"|"$http_referer"|"$request_time"|"$upstream_response_time"|"$upstream_http_x_cache"|"$uri"|"$upstream_addr"|"$upstream_response_length"|"$server_name"|"$upstream_cache_status"|"$http_user_agent"|"$scheme://$host$request_uri"|"$request_body"|"$req_headers"|"$resp_body_log"';
#
#log_format csv_combined_small escape=json '"$time_local"|"$http_x_forwarded_for"|"$upstream_response_time"|"$uri"|"$resp_body_log"|"nginx_log_format_csv"';

###remote_sending
#access_log syslog:server=rsyslog-forwarder:30010,facility=local7,tag=nginxaccess,severity=info json_combined;
#error_log syslog:server=rsyslog-forwarder:30010,facility=local7,tag=nginxerror,severity=error warn;
#access_log /dev/stdout compression;
error_log /dev/stdout;
access_log /dev/stdout json_combined;
#access_log /nginx_access.log json_combined;
#access_log /var/log/nginx_access_csv_combined_full_xml.log csv_combined_full if=$loggable_csv_combined_xml; 
#access_log /var/log/nginx_access_csv_combined_small_xml.log csv_combined_small if=$loggable_csv_combined_xml;
#access_log /var/log/nginx_access_csv_combined_full_cdr.log csv_combined_full if=$loggable_csv_combined_cdr; 
#access_log /var/log/nginx_access_csv_combined_small_cdr.log csv_combined_small if=$loggable_csv_combined_cdr; 
#access_log /var/log/nginx_access_json_combined.log json_combined if=$loggable_csv_combined_xml; 
#default|json|none
```