#!/usr/bin/env bash
get_ip () {
    local  pod_network=$(nslookup ${TEST_HOST_ROUTE:-google.com} | grep Address | grep -ve ':53\|#53' | awk '{ print $2 }' | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | head -n 1)
    local  ipaddr=$(ip route get "${pod_network}" | head -n 1 | awk  '{ print $NF }'  | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" )
    printf "{\"ip\": \"${ipaddr}\"}"
}
main() {
    get_ip
}
main