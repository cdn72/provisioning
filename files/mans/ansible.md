## Description

Ansible — система управления конфигурациями, написанная на языке программирования Python, с использованием декларативного языка разметки для описания конфигураций. Используется для автоматизации настройки и развертывания программного обеспечения. Обычно используется для управления Linux-узлами, но Windows также поддерживается. Поддерживает работу с сетевыми устройствами, на которых установлен Python версии 2.4 и выше по SSH или WinRM соединению.

Автор платформы — Michael DeHaan, ранее разработавший серверную систему развертывания ПО Cobbler (англ.) и соавтор фреймворка удаленного администрирования Func. Система Ansible входит в состав большинства дистрибутивов Linux. Есть пакеты для Solaris, FreeBSD и MacOS. Компания Ansible, Inc осуществляла коммерческую поддержку и сопровождение Ansible. 16 октября 2015 года Red Hat, Inc объявила о поглощении Ansible, Inc.

Словом «Ansible» названа вымышленная система мгновенной гиперпространственной связи. Эта система была в мире Игра Эндера Орсона С. Карда, само слово придумано Урсулой Ле Гуин в романе Мир Роканнона (1966).

## Install

**ubuntu**

```sh
sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible -y
```


## Configure 

Edit file /etc/ansible.cfg

Replace /home/ansible with you dir == >> /home/username

```ini
[defaults]
deprecation_warnings = False
private_key_file = /home/ansible/.ssh/id_rsa
remote_tmp = /tmp/ansible-
inventory = /etc/ansible/hosts
log_path = /tmp/.ansible.log
forks = 50
ask_pass = False
sudo_flags = -H -S
remote_port = 22
gathering = implicit
gather_timeout = 20
command_warnings = False
roles_path = ~/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles
host_key_checking = False
remote_user = root

[inventory]
enable_plugins = host_list, script, yaml

[ssh_connection]
retries=3
pipelining=True
ssh_args = -C -o ControlMaster=auto -o ControlPersist=60s

```

