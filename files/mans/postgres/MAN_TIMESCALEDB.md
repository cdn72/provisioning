# Timescaledb examplse 

```sql
CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;

CREATE TABLE vehicle_traffic (
  status                TEXT not null,
  avg_measured_time     INT not null,
  avg_speed             INT not null,
  ext_id                INT not null,
  median_measured_time  INT not null,
  time                  TIMESTAMP not null,
  vehicle_count         INT not null,
  id                    INT not null,
  report_id             INT not null
);


SELECT create_hypertable('vehicle_traffic', 'time',
    chunk_time_interval => interval '1 day');
```

curl -LO iot.ee.surrey.ac.uk:8080/datasets/traffic/traffic_feb_june/citypulse_traffic_raw_data_surrey_feb_jun_2014.tar.gz
tar -xvf citypulse_traffic_raw_data_surrey_feb_jun_2014.tar.gz
cd traffic_feb_june



#!/usr/bin/env bash
for i in *.csv; do
    echo "item: $i"
    time psql \
        "postgres://testtimescale1:testtimescale1@127.0.0.1:5432/testtimescale1?sslmode=disable" \
        -c "\COPY vehicle_traffic FROM $i CSV HEADER"
done



# MANS
https://aiven-io.medium.com/timescaledb-101-the-why-what-and-how-9c0eb08a7c0b