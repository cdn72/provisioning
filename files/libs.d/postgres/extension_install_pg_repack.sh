apt update ; apt install -y wget gcc make build-essential libxml2-dev libgeos-dev libproj-dev libgdal-dev libssl-dev cmake git unzip zlib1g-dev liblz4-dev libreadline-dev
git clone -b ${APP_VERSION_PG_REPACK:-ver_1.4.8} https://github.com/reorg/pg_repack.git /tmp/${APP_VERSION_PG_REPACK:-ver_1.4.8} \
    && cd /tmp/${APP_VERSION_PG_REPACK:-ver_1.4.8} \
    && make && make install