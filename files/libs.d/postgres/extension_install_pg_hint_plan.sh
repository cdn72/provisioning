###USAGE

#LOAD 'pg_hint_plan';
#SET pg_hint_plan.enable_hint=ON;

##
#yugabyte=# SET pg_hint_plan.debug_print TO on;
#yugabyte=# \set SHOW_CONTEXT always
#yugabyte=# SET client_min_messages TO log;


### MANS
#https://github.com/ossc-db/pg_hint_plan
#https://docs.yugabyte.com/preview/explore/query-1-performance/pg-hint-plan/


### Install
apt update ; apt install -y wget gcc make build-essential libxml2-dev libgeos-dev libproj-dev libgdal-dev libssl-dev cmake git unzip zlib1g-dev liblz4-dev libreadline-dev
git clone -b ${APP_VERSION_PG_HINT_PLAN:-REL14_1_4_0} https://github.com/ossc-db/pg_hint_plan.git /tmp/${APP_VERSION_PG_HINT_PLAN:-REL14_1_4_0} \
    && cd /tmp/${APP_VERSION_PG_HINT_PLAN:-REL14_1_4_0} \
    && make && make install


