common_init(){
    if [[ "${exec_prefix}" == "root" ]];then
        export exec_prefix=""
    else
        export exec_prefix="sudo"
    fi
}

fix_perms(){
    echo "exec funct ${FUNCNAME}"
    if [[ -d "${APP_DIR_CURRENT}" ]];then
        screen -dmSL ${FUNCNAME}_find sh -c "find ${APP_DIR_CURRENT:-/var/www/html/${APP_HTTP_HOST}}  \! -user ${DOCKER_IMAGE_RUN_USER:-www-data} -exec chown -h -v ${DOCKER_IMAGE_RUN_USER:-www-data}:${DOCKER_IMAGE_RUN_USER:-www-data} {} +;find ${APP_DIR_CURRENT:-/var/www/html/${APP_HTTP_HOST}}  \! -group ${DOCKER_IMAGE_RUN_USER:-www-data} -exec chown -h -v ${DOCKER_IMAGE_RUN_USER:-www-data}:${DOCKER_IMAGE_RUN_USER:-www-data} {} +;"
    fi
}

app_fix_perms() {
    export APP_DIRS="${APP_DIR_CURRENT} ${APP_DIR_SHARED}"
    for APP_DIR in ${APP_DIRS};do
    if [[ -d "${APP_DIR}" ]];then
        find ${APP_DIR}  \! -user ${DOCKER_IMAGE_RUN_USER:-www-data} -exec chown -h  ${DOCKER_IMAGE_RUN_USER:-www-data} {} +;
        find ${APP_DIR}  \! -group ${DOCKER_IMAGE_RUN_USER:-www-data} -exec chown -h  :${DOCKER_IMAGE_RUN_USER:-www-data} {} +;
    fi
done
}


am_i_root() {
    if [[ "$(id -u)" = "0" ]]; then
        true
    else
        false
    fi
}


if [[ -f "/opt/public_libs/files/libs.d/apps/initbash_profile.sh" ]]; then
    bash /opt/public_libs/files/libs.d/apps/initbash_profile.sh || ( echo "skip load addtional profile" )
fi


if [ -f "/opt/public_libs/files/libs.d/apps/php.sh" ]; then
    . /opt/public_libs/files/libs.d/apps/php.sh
fi


get_function_name() {
    echo "Exec ${FUNCNAME[1]}"
}

get_total_memory() {
    echo $(($(grep MemTotal /proc/meminfo | awk '{print $2}') / 1024))
}

app_prepare_run_adb () {
    export EMULATOR_HOST=$(grep -o "^EMULATOR_HOST=.*" ${APP_DIR_CURRENT}/.env | awk -F\= '{print $2}')
    if [[ -n ${EMULATOR_HOST} ]];then
        if [[ $(curl --max-time 2 -s -o /dev/null -I -w "%{http_code}" ${EMULATOR_HOST} == "000") ]];then
            echo "Spawn adb connect ${EMULATOR_HOST}"
            if am_i_root;then
                echo "Run as regular user=${DOCKER_IMAGE_RUN_USER:-www-data} from root"
                su -s /bin/bash -c "adb connect ${EMULATOR_HOST}" ${DOCKER_IMAGE_RUN_USER:-www-data}
            else
                echo "Run from regular"
                adb connect ${EMULATOR_HOST}
            fi
        else
            echo "Skip spawn adb connect"
        fi
    else
        echo "EMULATOR_HOST not defined"
    fi
}

send_message_chat() {

local TEXT_HEADER=$1
local TEXT_ATTACHMENT=$2
local LOG_FILE=$3

local DATE_EXEC="$(date "+%d %b %Y %H:%M")"

if [[ "${TEXT_ATTACHMENT}" == "" ]];then
    TEXT_ATTACHMENT="No additinal info"
fi

check_telegram=$(echo "${WEBHOOK_URL:-${notify_url}}" | grep -q telegram ; echo $? )
if [[ "${check_telegram}" == "0" ]]
then
    echo "Send to Telegram"
    TEXT_ATTACHMENT_ENCODED=$(echo -ne "${TEXT_ATTACHMENT}" | hexdump -v -e '/1 "%02x"' | sed 's/\(..\)/%\1/g')
    curl -s -X GET "${WEBHOOK_URL:-${notify_url}}&text=${TEXT_ATTACHMENT_ENCODED}"  > /dev/null
    if [[ -f "${LOG_FILE}" ]];then
        echo "Exec curl -F document=@${LOG_FILE} $(echo ${WEBHOOK_URL:-${notify_url}} | sed 's/sendMessage/sendDocument/') > /dev/null"
        sleep 5 && curl -F document=@${LOG_FILE} $(echo ${WEBHOOK_URL:-${notify_url}} | sed 's/sendMessage/sendDocument/') > /dev/null
    fi

else 
    echo "Send to rocket_chat(slack)";
    curl -q -X POST --connect-timeout 3 -H 'Content-Type: application/json' --data "{\"text\":\"${TEXT_HEADER}\", \"attachments\": [{\"title\": \"Additional_info\",\"color\": \"#A52A2A\",\"text\": \"${TEXT_ATTACHMENT}\"}]}" ${WEBHOOK_URL:-${ROCKET_CHAT_URL}} > /dev/null
fi

}