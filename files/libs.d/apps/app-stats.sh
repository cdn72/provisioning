#!/usr/bin/env bash

octane_restart(){
	su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan octane:reload && php artisan octane:stop " ${DOCKER_IMAGE_RUN_USER:-www-data};
    su -s /bin/bash -c "supervisorctl restart octane && supervisorctl status octane"
}

queue_restart () {
	sh -c "cd ${APP_DIR_CURRENT} ; supervisorctl stop dispatcher ; php artisan queue:restart ; supervisorctl start dispatcher"
}


artisan_tinker () {
	su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan tinker" ${DOCKER_IMAGE_RUN_USER:-www-data} 
}

artisan_config_cache () {
	su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan config:cache" ${DOCKER_IMAGE_RUN_USER:-www-data} 
}


artisan_route_cache () {
	su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan cache:clear;/usr/local/bin/php artisan route:clear;/usr/local/bin/php artisan route:cache;/usr/local/bin/php artisan config:cache;/usr/local/bin/php artisan optimize;" ${DOCKER_IMAGE_RUN_USER:-www-data} 
}

app_prepare_app-stats (){
    #Simlink
	echo "exec funct ${FUNCNAME}"
	if [[ ! -d "${APP_DIR_SHARED}/bootstrap/cache" ]];then mkdir -p "${APP_DIR_SHARED}/bootstrap/cache" -m 777;fi
	if [[ -f ${APP_DIR_SHARED}/.env ]];then
	 ln -sf "${APP_DIR_SHARED}"/.env "${APP_DIR_CURRENT}"/.env
	fi
	if [[  ! -L "${APP_DIR_CURRENT}/storage" ]]; then
	 	mv "${APP_DIR_CURRENT}"/storage "${APP_DIR_CURRENT}"/storage.old
	 	ln -sf "${APP_DIR_SHARED}"/storage "${APP_DIR_CURRENT}"/storage
	fi
	if [[  ! -L "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
		mv "${APP_DIR_CURRENT}"/bootstrap/cache "${APP_DIR_CURRENT}"/bootstrap/cache.old
		ln -sf "${APP_DIR_SHARED}"/bootstrap/cache "${APP_DIR_CURRENT}"/bootstrap/cache;
	fi

    #Crons
    if [[ ! -d "/etc/supercronic" ]] ;then mkdir /etc/supercronic; fi
    echo "*/1 * * * * php ${APP_DIR_CURRENT}/artisan schedule:run" > /etc/supercronic/laravel
    chown -v ${DOCKER_IMAGE_RUN_USER:-www-data}  /etc/supercronic/laravel 
  
    artisan_config_cache
	if [[ "${APP_FUNCTION}" == "backend" ]];then
		artisan_route_cache
		cd "${APP_DIR_CURRENT}" && php artisan octane:stop || echo "Skip stop octane"
		cd "${APP_DIR_CURRENT}" && php artisan octane:reload || echo "Skip reload octane"
	fi

if [[ "${APP_FUNCTION}" == "cron" ]];then
	
cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/cron_second.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:cron_second]
command=sh -c "while true;do bash /opt/public_libs/files/libs.d/apps/app-stats/cron_second.sh;done"
autostart=true
autorestart=true
stdout_logfile=${APP_DIR_SHARED}/storage/logs/cron_second.log
stdout_logfile_maxbytes=0
stderr_logfile=${APP_DIR_SHARED}/storage/logs/cron_second.log
stderr_logfile_maxbytes=0
startretries=140
startretries=140
stopwaitsecs=10
user=root
OEF
fi

}

