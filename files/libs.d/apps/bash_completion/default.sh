common_init(){
    if [[ "${exec_prefix}" == "root" ]];then
        export exec_prefix=""
    else
        export exec_prefix="sudo"
    fi
}

fix_perms(){
    echo "exec funct ${FUNCNAME}"
    if [[ -d "/var/www/html" ]];then
        screen -dmSL ${FUNCNAME}_find sh -c "find ${APP_DIR_CURRENT:-/var/www/html/${APP_HTTP_HOST}}  -user root -exec chown -h -v ${DOCKER_IMAGE_RUN_USER:-www-data}:www-data {} +;find ${APP_DIR_CURRENT:-/var/www/html/${APP_HTTP_HOST}}  -group root -exec chown -h -v ${DOCKER_IMAGE_RUN_USER:-www-data}:www-data {} +;"
    fi
}

app_fix_perms() {
    export APP_DIRS="${APP_DIR_CURRENT} ${APP_DIR_SHARED}"
    for APP_DIR in ${APP_DIRS};do
    if [[ -d "${APP_DIR}" ]];then
        find ${APP_DIR}  \! -user ${DOCKER_IMAGE_RUN_USER:-www-data} -exec chown -h  ${DOCKER_IMAGE_RUN_USER:-www-data} {} +;
        find ${APP_DIR}  \! -group ${DOCKER_IMAGE_RUN_USER:-www-data} -exec chown -h  :${DOCKER_IMAGE_RUN_USER:-www-data} {} +;
    fi
done
}
