supercronic_minute_laravel() {
    if [[ ! -d "/etc/supercronic" ]] ;then mkdir /etc/supercronic; fi
    echo "*/1 * * * * php ${APP_DIR_CURRENT}/artisan schedule:run" > /etc/supercronic/laravel
    chown -v ${DOCKER_IMAGE_RUN_USER:-www-data}  /etc/supercronic/laravel 
}