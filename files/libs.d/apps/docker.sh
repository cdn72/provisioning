docker_get_container_ip(){
   ## help "docker_get_container_ip haproxy_mysql_haproxy | grep 'docker_mysql_container_ip' | awk '{ print $2}'"
   local container_prefix=$1
   export docker_mysql_container_name=$(docker ps --filter name=${container_prefix} --filter status=running -q)
   export docker_mysql_container_id=$(docker inspect ${docker_mysql_container_name} | jq .[].Id)
   #printf "docker_mysql_container_id %s$docker_mysql_container_id\n" 
   export docker_mysql_container_ip=$(docker network inspect docker_gwbridge \
   | jq  ".[].Containers."$docker_mysql_container_id".IPv4Address" \
   | sed 's~[^\.[:alnum:]/]\+~~g' | cut -d'/' -f1)
   printf "%s$docker_mysql_container_ip\n"
}