#!/usr/bin/env bash
apache2_configs_init_sms_cashback(){
    if [[ ! -d "${APACHE2_DIR_BACKEND:-/etc/apache2/conf.d}" ]];then
        mkdir -p ${APACHE2_DIR_BACKEND:-/etc/apache2/conf.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${APACHE2_DIR_BACKEND:-/etc/apache2/conf.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${APACHE2_DIR_BACKEND:-/etc/apache2/conf.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${APACHE2_DIR_BACKEND:-/etc/apache2/conf.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${APACHE2_DIR_BACKEND:-/etc/apache2/conf.d}"/"${APP_HTTP_HOST}".conf
###apache2 BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}

<VirtualHost *:80>
    ServerName ${APP_HTTP_HOST}

    VirtualDocumentRoot "${APP_DIR_CURRENT}/build" 
    <Directory ${APP_DIR_CURRENT}/build>
        AllowOverride All
        Options MultiViews Indexes Includes FollowSymLinks
        Require all granted
    </Directory>

</VirtualHost>

<VirtualHost *:443>
    ServerName ${APP_HTTP_HOST}
    VirtualDocumentRoot "${APP_DIR_CURRENT}/build" 
    <Directory ${APP_DIR_CURRENT}/build>
        AllowOverride All
        Options MultiViews Indexes Includes FollowSymLinks
        Require all granted
    </Directory>
    SSLProxyEngine on
    SSLEngine on
    SSLCertificateFile    /etc/letsencrypt/live/${APP_HTTP_HOST}/cert.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/${APP_HTTP_HOST}/privkey.pem
    SSLCertificateChainFile /etc/letsencrypt/live/${APP_HTTP_HOST}/fullchain.pem
</VirtualHost>

###apache2 BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF
        fi
chown -R ${APACHE2_RUN_USER:-www-data}:${APACHE2_RUN_USER:-www-data}  /etc/apache2/        
}