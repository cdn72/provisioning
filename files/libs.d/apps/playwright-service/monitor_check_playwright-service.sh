#!/usr/bin/env bash

monitor_check_playwright-service () {
  if [[ -f "${APP_DIR_CURRENT}/utils/health_check.py" ]];then
    echo "Exec ${APP_DIR_CURRENT}/utils/health_check.py"
    ${APP_DIR_CURRENT}/utils/health_check.py
  else
    echo "File ${APP_DIR_CURRENT}/utils/health_check.py not exit. Skip run it."
  fi

}

main () {
  if grep -q "^HEALTHCHECK_ON=.*rue" ${APP_DIR_SHARED}/.env
  then
    monitor_check_playwright-service
  fi
  echo "Wait 90 sec" && sleep 90;
}

main