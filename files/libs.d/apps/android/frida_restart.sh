#!/usr/bin/env bash
export FRIDA_LOG_DIR="/data/log"
export FRIDA_LOG_FILE="${FRIDA_LOG_FILE:-${FRIDA_LOG_DIR}/frida-server-x86_64.log}"
export FRIDA_RESTART_LOG_FILE="${FRIDA_LOG_DIR}/frida-server-restart.log"
export APP_DIR_SHARED=${APP_DIR_SHARED:-/data}
export DATE=$(date +%Y-%m-%d__%H_%M_%S)

monitor_check_frida () {

    supervisorctl -c /etc/supervisor/supervisord.conf stop frida || (echo "Not running")
    sleep 1;
    if [[ -f "/docker-cmd.sh" ]];then
        . /docker-cmd.sh
        ${ANDROID_ADB_FILE} shell "su 0 killall frida-server" || echo "Skipped illall frida-server"
        ${ANDROID_ADB_FILE} shell "su 0 killall re.frida.helper" || echo "Skipped illall re.frida.helper"
        ${ANDROID_ADB_FILE} shell "pm clear com.google.android.apps.tachyon" || echo "skip clean app com.google.android.apps.tachyon"
        local frida_pids=$(${ANDROID_ADB_FILE} shell "su 0 ps -A | grep frida " | awk '{ print $2}')
        if [[ -n ${frida_pids} ]];then
            for frida_pid in ${frida_pids};do
                echo "Frida pid's finded. Force kill proc with pid ${frida_pid}"
                ${ANDROID_ADB_FILE} shell "su 0 kill -9 ${frida_pid}"
                local message="Frida health bad = Frida killed && restarted at ${DATE}. Wait ${FRIDA_WAIT_HEALTHCHECK:-30} sec"
            done
        else
            local message="Frida killer successed"
        fi
    fi
    truncate -s 0 ${FRIDA_LOG_FILE}
    #sleep 50;
    supervisorctl -c /etc/supervisor/supervisord.conf start frida
    sleep 1;
    echo "${message}"
    echo "${message}" >> ${FRIDA_RESTART_LOG_FILE}
    #sleep ${FRIDA_WAIT_HEALTHCHECK:-30};
}

main () {
    if [[ ! -d "${FRIDA_LOG_DIR}" ]];then mkdir -p ${FRIDA_LOG_DIR};fi

    if [[ "${MONITOR_FRIDA:-1}" == "1" ]];then
            monitor_check_frida
    else
        echo "MONITOR_FRIDA disabled via env MONITOR_FRIDA !=1 . ${DATE}"
    fi
    sleep 10; 
}
main