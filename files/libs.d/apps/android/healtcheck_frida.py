import frida
import sys
import time
import os
import adbutils
from xmlrpc.client import ServerProxy
import subprocess
import time

EMULATOR_CHECK_PACKAGE = os.getenv('EMULATOR_CHECK_PACKAGE', 'com.google.android.apps.tachyon')
EMULATOR_HOST = os.getenv('EMULATOR_HOST', '127.0.0.1:25555')
HEALTHCHECK_SLEEP_TIME = os.getenv('HEALTHCHECK_SLEEP_TIME', 60)
SUPERVISOR_SERVER_URI = os.getenv('SUPERVISOR_SERVER_URI', '127.0.0.1:9001')
SUPERVISOR_PROC_NAME_EMULATOR = "emulator_" + os.getenv('ANDROID_ADV_NAME', 'Pixel_2_API_29')
SUPERVISOR_PROC_NAME_FRIDA = os.getenv('SUPERVISOR_PROC_NAME_FRIDA', 'frida')

def supervisord_restart():
    server = ServerProxy('http://' + SUPERVISOR_SERVER_URI + '/RPC2')
    state = server.supervisor.getState()
    supervisor_list = []
    supervisor_list.append(SUPERVISOR_PROC_NAME_FRIDA)

    for sp in supervisor_list:
        try:
            print("Try stop " + sp)
            state_stop = server.supervisor.stopProcess(sp)

        except Exception as e:
            pass

            print(f"Skip stop {sp} ... {e}")
            
    for sp in supervisor_list:        
        try:
            state_start = server.supervisor.startProcess(sp)

        except Exception as e:
            pass

            print(f"Skip start {sp} ... {e}")
            

def adb_connect():
    
    p = subprocess.Popen("adb connect " + EMULATOR_HOST , stdout=subprocess.PIPE, shell=True)
    print(p.communicate())

def adb_disconnect():
    p = subprocess.Popen("adb disconnect " + EMULATOR_HOST , stdout=subprocess.PIPE, shell=True)
    print(p.communicate())

def connect_to_device():
    session = None
    pid = None
    start_time = time.time()
    while time.time() - start_time < 30:
        try:
            device = frida.get_device(EMULATOR_HOST, timeout=10)
            processes = device.enumerate_processes()
            for process in processes:
                if process.name == EMULATOR_CHECK_PACKAGE:
                    pid = process.pid
                    break
            if pid is None:
                pid = device.spawn([EMULATOR_CHECK_PACKAGE])
                device.resume(pid)
            session = device.attach(pid)
            break
        except Exception as e:
            print(f"Connection failed, retrying... {e}")
            time.sleep(5)
    if session is None:
        print("Could not connect to Frida within 60 seconds....")
        
    return session


def script_load():
    script_start_time = time.time()
    
    while time.time() - script_start_time < 60:
        try:
            adb_connect()
            session = connect_to_device()
            script = session.create_script("""
                    console.log('Frida script loaded successfully');
                """)
            script.load()
            script.unload()
            adb_disconnect()
            return True
        except Exception as e:
            print(f"Script loading failed, retrying... {e}")
            adb_disconnect()
            time.sleep(5)
    supervisord_restart()
    return False

if __name__ == "__main__":
    print(f"Exec healthcheck. EMULATOR_HOST={EMULATOR_HOST}")
    
    if script_load():
        print("Sleep for " + str(HEALTHCHECK_SLEEP_TIME))
        time.sleep(HEALTHCHECK_SLEEP_TIME)
        sys.exit(0)    
    print("Could not load script within 60 seconds")
    supervisord_restart()
    #
    time.sleep(HEALTHCHECK_SLEEP_TIME)
       
    




