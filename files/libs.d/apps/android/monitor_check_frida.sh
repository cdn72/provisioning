#!/usr/bin/env bash
export FRIDA_LOG_DIR="/data/log"
export FRIDA_LOG_FILE="${FRIDA_LOG_FILE:-${FRIDA_LOG_DIR}/frida-server-x86_64.log}"
export FRIDA_RESTART_LOG_FILE="${FRIDA_LOG_DIR}/frida-server-restart.log"
export APP_DIR_SHARED=${APP_DIR_SHARED:-/data}
export DATE=$(date +%Y-%m-%d__%H_%M_%S)

monitor_check_frida () {

if [[ -s ${FRIDA_LOG_FILE} ]]; then
    
    supervisorctl -c /etc/supervisor/supervisord.conf stop frida || (echo "Not running")
    sleep 1;
    if [[ -f "/docker-cmd.sh" ]];then
        . /docker-cmd.sh
        ${ANDROID_ADB_FILE} shell "su 0 killall frida-server"
        ${ANDROID_ADB_FILE} shell "su 0 killall re.frida.helper"
        local frida_pids=$(${ANDROID_ADB_FILE} shell "su 0 ps -A | grep frida " | awk '{ print $2}')
        if [[ -n ${frida_pids} ]];then
            echo "Frida pid's finded. Force kill proc with pid ${frida_pids}"
            ${ANDROID_ADB_FILE} shell "su 0 kill -9 ${frida_pids}"
            local message="Frida health bad = Frida killed && restarted at ${DATE}. Wait ${FRIDA_WAIT_HEALTHCHECK:-30} sec"
        else
            local message="Frida health bad = Frida restarted at ${DATE}. Wait ${FRIDA_WAIT_HEALTHCHECK:-30} sec"
        fi
    fi
    truncate -s 0 ${FRIDA_LOG_FILE}
    supervisorctl -c /etc/supervisor/supervisord.conf start frida
    sleep 1;
    echo "${message}"
    echo "${message}" >> ${FRIDA_RESTART_LOG_FILE}
    sleep ${FRIDA_WAIT_HEALTHCHECK:-30};
else
    echo "Frida health ok = File ${FRIDA_LOG_FILE} is empty. Skip restart frida. ${DATE}"
    echo "Frida health ok = File ${FRIDA_LOG_FILE} is empty. Skip restart frida. ${DATE}" >> ${FRIDA_RESTART_LOG_FILE}
fi

}

main () {
    if [[ ! -d "${FRIDA_LOG_DIR}" ]];then mkdir -p ${FRIDA_LOG_DIR};fi

    if [[ "${MONITOR_FRIDA:-1}" == "1" ]];then
            monitor_check_frida
    else
        echo "MONITOR_FRIDA disabled via env MONITOR_FRIDA !=1 . ${DATE}"
    fi
    sleep 10; 
}
main