#!/bin/bash

adb_clear_app() {
    IP_ADDRESS=$(echo $EMULATOR_HOST)
    adb connect $IP_ADDRESS
    adb shell pm clear com.google.android.apps.tachyon
}

main () {
    adb_clear_app
}
main