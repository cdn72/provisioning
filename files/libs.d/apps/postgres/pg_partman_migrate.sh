#!/usr/bin/env bash

####-----------------------------####
###Usage
####-----------------------------####

###curl -q -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/apps/postgres/pg_partman_migrate.sh -o /tmp/pg_partman_migrate.sh; chmod +x /tmp/pg_partman_migrate.sh
###/tmp/pg_partman_migrate.sh \
##--pg_prepare=true \
##--pg_prepare_only=true \
##--pg_tables="tbl_test" \
##--pg_con_str_from="postgresql://PG_USER:PG_PASSWORD@pg_souce_host:5432/pg_dbname" \
##--pg_con_str_into="postgresql://PG_USER:PG_PASSWORD@pg_target_host:5432/pg_dbname" \
##--pg_load_start_date="2022-07-30" \
##--pg_load_end_date="2022-12-05" \
##--pg_p_start_partition="2022-07-01"

for i in "$@"
do
case $i in
		--pg_tables=*) # --pg_tables=tbl_convy_cloud
		PG_TABLES="${i#*=}"
		shift # past argument=value
		;;
		--pg_prepare=*) # --pg_prepare=false|true
		PG_PREPARE="${i#*=}"
		shift # past argument=value
		;;
		--pg_prepare_only=*) # --pg_prepare_only=false|true
		PG_PREPARE_ONLY="${i#*=}"
		shift # past argument=value
		;;
		--pg_con_str_from=*) # --pg_con_str_from="postgresql://PG_USER:PG_PASSWORD@pg_soucer_host:25432/pg_dbname"
		PG_CON_STR_FROM="${i#*=}"
		shift # past argument=value
		;;  
		--pg_con_str_into=*) # --pg_con_str_into="postgresql://PG_USER:PG_PASSWORD@pg_target_host:5432/pg_dbname"
		PG_CON_STR_INTO="${i#*=}"
		shift # past argument=value
		;;
		--pg_load_start_date=*) # --pg_load_start_date="2022-01-21"
		PG_LOAD_START_DATE="${i#*=}"
		shift # past argument=value
		;;
		--pg_load_end_date=*) # --pg_load_start_date="2022-12-31"
		PG_LOAD_END_DATE="${i#*=}"
		shift # past argument=value
		;;
		--pg_p_start_partition=*) # --pg_p_start_partition="2022-01-01"
		PG_P_START_PARTITION="${i#*=}"
		shift # past argument=value
		;;
		--pg_p_premake=*) # --pg_p_premake="200"
		PG_P_PREMAKE="${i#*=}"
		shift # past argument=value
		;;
		--default)
		DEFAULT=YES
		shift # past argument with no value
		;;
		*)
			# unknown option
		;;
esac
done

echo "PG_TABLES=${PG_TABLES}"
echo "PG_P_PREMAKE=${PG_P_PREMAKE}"

pg_prepare() {
	local pg_con_str_from=${1}
	local pg_con_str_into=${2}
	local pg_table=${3}
	local pg_start_date="${4}"
	local pg_end_date="${5}"
	local pg_prepare_status="${6}"
	local p_start_partition="${7}"
	local PG_DB_HOST=$(echo "${pg_con_str_into}" | awk -F "@" '{print $2}' | awk -F ":" '{print $1}')
	local PG_DB_PORT=$(echo "${pg_con_str_into}" | awk -F "@" '{print $2}' | awk -F ":" '{print $2}' | awk -F'/' '{ print $1 }')
	local PG_DB_USER=$(echo "${pg_con_str_into}" | awk -F "//" '{print $2}'  | awk -F":" '{print $1}' )
	local PG_DB_PASSWORD=$(echo "${pg_con_str_into}" | awk -F ":" '{print $3}' | awk -F"@" '{print $1}')
	local PG_DB_NAME=$(echo "${pg_con_str_into}" | awk -F "/" '{print $NF}')

	echo "Check for pgloader installed"
	if which pgloader > /dev/null;then 
		echo "Ok. Pgloader exist"
	else
		echo "Error. Pgloader not exist"
		exit 1
	fi
	echo "pg_prepare_status=${pg_prepare_status}.p_start_partition=${p_start_partition}"
if [[ "${pg_prepare_status}" == "true" ]];then
cat <<OEF> /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_1.load
LOAD database
    FROM ${pg_con_str_from}
    INTO ${pg_con_str_into}

    MATERIALIZE VIEWS 
        _temp_${pg_table}_s1 AS \$\$ SELECT TIMESTAMPTZ '2022-01-21 02:11:49+00' as time,text '0ca40802-16ea-49c3-ad80-94ff339866c1.cdr.json' as metric_name,JSONB '{"temp_${pg_table}_s1":"temp_${pg_table}_s1"}' as data, double precision '1' as value \$\$
    INCLUDING ONLY TABLE NAMES MATCHING '_temp_${pg_table}_s1' in schema 'public'

    BEFORE LOAD DO
        \$\$  CREATE SCHEMA IF NOT EXISTS partman; \$\$,
        \$\$  CREATE EXTENSION IF NOT EXISTS pg_partman SCHEMA partman; \$\$
    AFTER LOAD DO
    	\$\$ DROP TABLE IF EXISTS _temp_${pg_table}_s1; \$\$;

OEF

cat <<OEF> /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_2.load
LOAD database
    FROM ${pg_con_str_from}
    INTO ${pg_con_str_into}

    MATERIALIZE VIEWS 
        _temp_${pg_table}_s2 AS \$\$ SELECT TIMESTAMPTZ '2022-01-21 02:11:49+00' as time,text '0ca40802-16ea-49c3-ad80-94ff339866c1.cdr.json' as metric_name,JSONB '{"temp_${pg_table}_s1":"temp_${pg_table}_s1"}' as data, double precision '1' as value \$\$
    INCLUDING ONLY TABLE NAMES MATCHING '_temp_${pg_table}_s2' in schema 'public'

    BEFORE LOAD DO
        \$\$  CREATE ROLE partman WITH LOGIN; \$\$

    AFTER LOAD DO
    	\$\$ DROP TABLE IF EXISTS _temp_${pg_table}_s2; \$\$;

OEF

cat <<OEF> /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_3.load
LOAD database
    FROM ${pg_con_str_from}
    INTO ${pg_con_str_into}

    MATERIALIZE VIEWS 
        _temp_${pg_table}_s3 AS \$\$ SELECT TIMESTAMPTZ '2022-01-21 02:11:49+00' as time,text '0ca40802-16ea-49c3-ad80-94ff339866c1.cdr.json' as metric_name,JSONB '{"temp_${pg_table}_s1":"temp_${pg_table}_s1"}' as data, double precision '1' as value \$\$

    INCLUDING ONLY TABLE NAMES MATCHING '_temp_${pg_table}_s3' in schema 'public'

    BEFORE LOAD DO
        \$\$  GRANT ALL ON SCHEMA partman TO partman; \$\$ ,
        \$\$  GRANT ALL ON ALL TABLES IN SCHEMA partman TO partman; \$\$ ,
        \$\$  GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA partman TO partman; \$\$ ,
        \$\$  GRANT EXECUTE ON ALL PROCEDURES IN SCHEMA partman TO partman; \$\$ ,
        \$\$  GRANT ALL ON SCHEMA public TO partman; \$\$,
        \$\$  GRANT TEMPORARY ON DATABASE ${PG_DB_NAME} to partman; \$\$,
        \$\$  GRANT CREATE ON DATABASE  ${PG_DB_NAME} TO partman; \$\$
    AFTER LOAD DO
    	\$\$ DROP TABLE IF EXISTS _temp_${pg_table}_s3; \$\$;
OEF


cat <<OEF> /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_4.load
LOAD database
    FROM ${pg_con_str_from}
    INTO ${pg_con_str_into}

    MATERIALIZE VIEWS 
        _temp_${pg_table}_s4 AS \$\$ SELECT TIMESTAMPTZ '2022-01-21 02:11:49+00' as time,text '0ca40802-16ea-49c3-ad80-94ff339866c1.cdr.json' as metric_name,JSONB '{"temp_${pg_table}_s1":"temp_${pg_table}_s1"}' as data, double precision '1' as value \$\$

    INCLUDING ONLY TABLE NAMES MATCHING '_temp_${pg_table}_s4' in schema 'public'

    BEFORE LOAD DO
    	\$\$  SET timezone = 'UTC'; \$\$,
        \$\$  create table IF NOT EXISTS "${pg_table}" 
        	(
         		time TIMESTAMPTZ NOT NULL,
        	  	metric_name text not null,
        		data JSONB,
        	    value double precision,
        	    CONSTRAINT pk_metric_name_time_${pg_table} PRIMARY KEY (metric_name, time)
            )
            partition by range (time); 
 		\$\$

    AFTER LOAD DO
    	\$\$ DROP TABLE IF EXISTS _temp_${pg_table}_s4; \$\$;

OEF


cat <<OEF> /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_5.load
LOAD database
    FROM ${pg_con_str_from}
    INTO ${pg_con_str_into}

    MATERIALIZE VIEWS 
        _temp_${pg_table}_s5 AS \$\$ SELECT TIMESTAMPTZ '2022-01-21 02:11:49+00' as time,text '0ca40802-16ea-49c3-ad80-94ff339866c1.cdr.json' as metric_name,JSONB '{"temp_${pg_table}_s1":"temp_${pg_table}_s1"}' as data, double precision '1' as value \$\$

    INCLUDING ONLY TABLE NAMES MATCHING '_temp_${pg_table}_s5' in schema 'public'

    BEFORE LOAD DO
 		\$\$  CREATE INDEX  IF NOT EXISTS ${pg_table}_idx_data ON ${pg_table} USING GIN (data); \$\$

    AFTER LOAD DO
    	\$\$ DROP TABLE IF EXISTS _temp_${pg_table}_s5; \$\$;

OEF


cat <<OEF> /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_6.load
LOAD database
    FROM ${pg_con_str_from}
    INTO ${pg_con_str_into}

    MATERIALIZE VIEWS 
        _temp_${pg_table}_s6 AS \$\$ SELECT TIMESTAMPTZ '2022-01-21 02:11:49+00' as time,text '0ca40802-16ea-49c3-ad80-94ff339866c1.cdr.json' as metric_name,JSONB '{"temp_${pg_table}_s1":"temp_${pg_table}_s1"}' as data, double precision '1' as value \$\$

    INCLUDING ONLY TABLE NAMES MATCHING '_temp_${pg_table}_s6' in schema 'public'
    BEFORE LOAD DO
    	\$\$  SET timezone = 'UTC'; \$\$,
        \$\$  SELECT partman.create_parent('public.${pg_table}', 'time', 'native', 'daily',p_start_partition := '${p_start_partition}', p_premake => ${PG_P_PREMAKE:-10} );\$\$

    AFTER LOAD DO
    	\$\$ DROP TABLE IF EXISTS _temp_${pg_table}_s6; \$\$;

OEF

cat <<OEF> /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_7.load
LOAD database
    FROM ${pg_con_str_from}
    INTO ${pg_con_str_into}

    MATERIALIZE VIEWS 
        _temp_${pg_table}_s7 AS \$\$ SELECT TIMESTAMPTZ '2022-01-21 02:11:49+00' as time,text '0ca40802-16ea-49c3-ad80-94ff339866c1.cdr.json' as metric_name,JSONB '{"temp_${pg_table}_s1":"temp_${pg_table}_s1"}' as data, double precision '1' as value \$\$

    INCLUDING ONLY TABLE NAMES MATCHING '_temp_${pg_table}_s7' in schema 'public'

    BEFORE LOAD DO
    	\$\$  SET timezone = 'UTC'; \$\$,
    	\$\$  CREATE EXTENSION IF NOT EXISTS pg_cron; \$\$,
    	\$\$ update partman.part_config set infinite_time_partitions = true, premake=${PG_P_PREMAKE:-10} where parent_table in ('public.${pg_table}'); \$\$,
    	\$\$ SELECT partman.run_maintenance('public.${pg_table}'); \$\$,
    	\$\$ SELECT partman.run_maintenance(p_analyze := false); \$\$

    AFTER LOAD DO
    	\$\$ DROP TABLE IF EXISTS _temp_${pg_table}_s7; \$\$;

OEF

	echo "Start pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_1.load"
	if ! pgloader --verbose /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_1.load;then
		echo "Error during create shema and extension partman"
		exit 1
	fi

	set -e
	echo "Start pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_2.load"
	if ! pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_2.load;then
		echo "Error create role partman. Already exist ?"
	fi
	set +e

	echo "Start pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_3.load"
	if ! pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_3.load;then
		echo "Error during grant priv partman"
		exit 1
	fi
	echo "Start pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_4.load"
	if ! pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_4.load;then
		echo "Error working with table ${pg_table}"
		exit 1
	fi

	#echo "Start pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_5.load"
	#if ! pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_5.load;then
	#	echo "Error working with table ${pg_table}"
	#	exit 1
	#fi
	
	set -e
	echo "Start pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_6.load"
	if ! pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_6.load;then
		echo "Error create partman.create_parent on pg_table=${pg_table} with p_start_partition=${p_start_partition} . Already exist ?"
	fi
	set +e

	echo "Start pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_7.load"
	if ! pgloader --verbose  /tmp/${pg_table}_${pg_start_date}_${pg_end_date}_prepare_7.load;then
		echo "Error update partman.part_config for ${pg_table}"
		exit 1
	fi


fi

}

pg_load() {
	local pg_con_str_from=${1}
	local pg_con_str_into=${2}
	local pg_table=${3}
	local pg_start_date="${4}"
	local pg_end_date="${5}"
	local pg_prepare_status="${6}"
	local p_start_partition="${7}"
	local PG_DB_HOST=$(echo "${pg_con_str_into}" | awk -F "@" '{print $2}' | awk -F ":" '{print $1}')
	local PG_DB_PORT=$(echo "${pg_con_str_into}" | awk -F "@" '{print $2}' | awk -F ":" '{print $2}' | awk -F'/' '{ print $1 }')
	local PG_DB_USER=$(echo "${pg_con_str_into}" | awk -F "//" '{print $2}'  | awk -F":" '{print $1}' )
	local PG_DB_PASSWORD=$(echo "${pg_con_str_into}" | awk -F ":" '{print $3}' | awk -F"@" '{print $1}')
	local PG_DB_NAME=$(echo "${pg_con_str_into}" | awk -F "/" '{print $NF}')


cat <<OEF> /tmp/${pg_table}_${pg_start_date}_${pg_end_date}.load
LOAD database
    FROM ${pg_con_str_from}
    INTO ${pg_con_str_into}

    MATERIALIZE VIEWS 
        temp_${pg_table}_${pg_start_date//-/_}_load AS \$\$ SELECT * FROM ${pg_table} WHERE time >= '${pg_start_date}' and time < '${pg_end_date}' \$\$
    INCLUDING ONLY TABLE NAMES MATCHING 'temp_${pg_table}_${pg_start_date//-/_}_load' in schema 'public'
    BEFORE LOAD DO
    	\$\$ DROP TABLE IF EXISTS temp_${pg_table}_${pg_start_date//-/_}_load; \$\$
    AFTER LOAD DO
        \$\$ INSERT INTO ${pg_table}
                (time,
                metric_name,
                data,
                value)
            SELECT
                time,
                metric_name,
                data,
                value
            FROM temp_${pg_table}_${pg_start_date//-/_}_load
            ON CONFLICT DO NOTHING; \$\$,
        \$\$ DROP TABLE IF EXISTS temp_${pg_table}_${pg_start_date//-/_}_load; \$\$;
OEF

	echo "Start pgloader --dry-run /tmp/${pg_table}_${pg_start_date}_${pg_end_date}.load"
	pgloader --dry-run /tmp/${pg_table}_${pg_start_date}_${pg_end_date}.load
	if [[ "${?}" != "0" ]];then
		local message="Error test \"pgloader --dry-run /tmp/${pg_table}_${pg_start_date}_${pg_end_date}.load\""
	else
		echo "!!! Start pgloader --verbose /tmp/${pg_table}_${pg_start_date}_${pg_end_date}.load !!!"
		pgloader --verbose /tmp/${pg_table}_${pg_start_date}_${pg_end_date}.load
		if [[ "${?}" != "0" ]];then
			echo "!!! Error pgloader --verbose /tmp/${pg_table}_${pg_start_date}_${pg_end_date}.load !!!"
			exit 1
		fi
	fi
}

main () {

if [[ -n "${PG_TABLES}" ]];then
	for PG_TABLE in ${PG_TABLES};do
		echo "Start pg_prepare ${PG_CON_STR_FROM} ${PG_CON_STR_INTO} ${PG_TABLE} ${PG_LOAD_START_DATE} ${PG_LOAD_END_DATE} ${PG_PREPARE} ${PG_P_START_PARTITION} at $(date)"
		pg_prepare ${PG_CON_STR_FROM} ${PG_CON_STR_INTO} ${PG_TABLE} ${PG_LOAD_START_DATE} ${PG_LOAD_END_DATE} ${PG_PREPARE} ${PG_P_START_PARTITION}
		echo "Ended pg_prepare ${PG_CON_STR_FROM} ${PG_CON_STR_INTO} ${PG_TABLE} ${PG_LOAD_START_DATE} ${PG_LOAD_END_DATE} ${PG_PREPARE} ${PG_P_START_PARTITION} at $(date)"

		if [[ "${PG_PREPARE_ONLY}" != "true" ]];then
			local d_start=${PG_LOAD_START_DATE}
			until [[ ${d_start} > ${PG_LOAD_END_DATE} ]]; do
				local d_end=$(date -I -d "${d_start} + 1 day")
				local message="Started pg_load ${PG_CON_STR_FROM} ${PG_CON_STR_INTO} ${PG_TABLE} ${d_start} ${d_end} ${PG_PREPARE} ${PG_P_START_PARTITION} at $(date)"
				echo $message && echo $message >> /tmp/${PG_TABLE}.log
				pg_load ${PG_CON_STR_FROM} ${PG_CON_STR_INTO} ${PG_TABLE} ${d_start} ${d_end} ${PG_PREPARE} ${PG_P_START_PARTITION}
				local message="Ended pg_load ${PG_CON_STR_FROM} ${PG_CON_STR_INTO} ${PG_TABLE} ${d_start} ${d_end} ${PG_PREPARE} ${PG_P_START_PARTITION} at $(date)"
				echo $message && echo $message >> /tmp/${PG_TABLE}.log
				echo "" >> /tmp/${PG_TABLE}.log
				local d_start=${d_end}
done
		else
			echo "Skip load data in ${PG_TABLE}. PG_PREPARE_ONLY=${PG_PREPARE_ONLY}"
		fi
done
else
	echo "Error. Var PG_TABLES empty"
	exit 1
fi
	rm -rf /tmp/*.load
}

main



### Errors
# duplicate key value violates unique constraint "part_config_parent_table_pkey" = > DELETE FROM partman.part_config WHERE parent_table = 'public.table_name';
#