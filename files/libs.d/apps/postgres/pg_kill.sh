#!/usr/bin/env bash

POSTGRES_HOST_CI=""
POSTGRES_PORT_CI=""
POSTGRES_DBUSER_CI=""
POSTGRES_DB_PASSWORD_CI=""
POSTGRES_DBNAME_CI=""

PG_KILL_OLD=$(PGPASSWORD=${POSTGRES_DB_PASSWORD_CI} psql \
        --host=${POSTGRES_HOST_CI} \
        --port=${POSTGRES_PORT_CI} \
        --username=${POSTGRES_DBUSER_CI} \
        --dbname=${POSTGRES_DBNAME_CI} \
        --tuples-only \
        --field-separator ' ' \
        --pset footer=off \
        --no-align \
        --quiet \
        --set ON_ERROR_STOP=on \
        <<EOF
 select pg_terminate_backend(pid)
from (
  SELECT
      pid
  FROM pg_stat_activity
     WHERE query <> '<insufficient privilege>'
     AND state <> 'idle'
     AND pid <> pg_backend_pid()
     AND query_start < now() - interval '230 second'
     ORDER BY query_start DESC) t;
EOF
  )

echo "Time=$(date) PG_KILL_OLD=${PG_KILL_OLD}"