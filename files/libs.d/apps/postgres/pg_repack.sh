#!/usr/bin/env bash

##Example run 
#curl -q -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/apps/postgres/pg_repack.sh -o pg_repack.sh && nohup bash pg_repack.sh --db_name=convypg --db_n_dead_tup_min=100 &
##curl -q -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/apps/postgres/pg_repack.sh -o pg_repack.sh &&  bash pg_repack.sh --db_name=convypg --db_n_dead_tup_min=100
##curl -q -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/apps/postgres/pg_repack.sh -o pg_repack.sh &&  bash pg_repack.sh --db_name=homer_data --db_n_dead_tup_min=0  --db_user=root --db_password=homerSeven
for i in "$@"
do
case $i in
		--db_name=*) # db_name=convypg
		PG_DB_NAME="${i#*=}"
		shift # past argument=value
		;;
		--db_n_dead_tup_min=*) # db_n_dead_tup_min=1
		PG_DB_N_DEAD_TUP_MIN="${i#*=}"
		shift # past argument=value
		;;
		--db_user=*) # db_user=postgres
		PG_DB_USER="${i#*=}"
		shift # past argument=value
		;;  
		--db_password=*) # db_password=postgrespassword
		PG_DB_PASSWORD="${i#*=}"
		shift # past argument=value
		;;  
		--default)
		DEFAULT=YES
		shift # past argument with no value
		;;
		*)
			# unknown option
		;;
esac
done


pg_show_db_size(){
	local db_name="${1}"
	echo "Get db size PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} psql  --username ${PG_DB_USER:-postgres} --dbname ${PG_DB_NAME} -qAt --command  \"SELECT pg_size_pretty(pg_database_size('${PG_DB_NAME}'));\""
	db_size=$(PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} psql  --username ${PG_DB_USER:-postgres} --dbname ${PG_DB_NAME} -qAt --command  "SELECT pg_size_pretty(pg_database_size('${PG_DB_NAME}'));")
	echo "${db_size}"
}

prepare () {
	if [[ ! -f "${LOG_NAME}" ]];then
		touch "${LOG_NAME}"
	fi
}

extension_create() {
	echo "Try Create extension pg_repack"
	local pg_extension_check=$(PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} psql -t  --username ${PG_DB_USER:-postgres} --dbname ${PG_DB_NAME:-convypg} -c "SELECT name FROM pg_available_extensions where name = 'pg_repack';")
	if [[ "${pg_extension_check}" =~ "pg_repack" ]];then
		echo "Extension pg_repack exist in system";
	else
        if [ -f "/etc/alpine-release" ]; then  
        OS_PACKAGE_MANAGER=alpine && echo "Install for $OS_PACKAGE_MANAGER disto" && apk add coreutils clang clang-dev musl-dev wget gcc make cmake git unzip libxml2-dev readline-dev geos-dev proj-dev gdal-dev libressl-dev zlib-dev
          else
        OS_PACKAGE_MANAGER=deb &&  echo "Install for $OS_PACKAGE_MANAGER disto" && apt update ; apt install -y wget gcc make cmake git unzip  libxml2-dev libreadline-dev  libgeos-dev  libproj-dev build-essential  libgdal-dev libssl-dev  zlib1g-dev liblz4-dev
        fi 
        if [[ -d "/tmp/${APP_VERSION_PG_REPACK:-ver_1.4.8}" ]];then rm -rf /tmp/${APP_VERSION_PG_REPACK:-ver_1.4.8} ;fi
		git clone -b ${APP_VERSION_PG_REPACK:-ver_1.4.8} https://github.com/reorg/pg_repack.git /tmp/${APP_VERSION_PG_REPACK:-ver_1.4.8} 
			cd /tmp/${APP_VERSION_PG_REPACK:-ver_1.4.8} \
			&& make && make install
			local pg_extension_check=$(PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} psql -t  --username ${PG_DB_USER:-postgres} --dbname ${PG_DB_NAME:-convypg} -c "SELECT name FROM pg_available_extensions where name = 'pg_repack';")
			if [[ "${pg_extension_check}" =~ "pg_repack" ]];then
			echo "Extension pg_repack installed succesfull in system";
		else
			echo "Error installing extension pg_repack"
			exit 1
		fi
	fi
	PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} psql --username ${PG_DB_USER:-postgres} --dbname ${PG_DB_NAME} -c "CREATE EXTENSION IF NOT EXISTS pg_repack;"
	if [[ "$?" != "0" ]];then echo "Error Create extension pg_repack" && exit 1;fi
}

extension_remove() {
	PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} psql --username ${PG_DB_USER:-postgres} --dbname ${PG_DB_NAME} -c "DROP EXTENSION IF EXISTS pg_repack CASCADE;"

}
pg_show_table_size(){
	local table_name="${1}"
	table_size=$(PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} psql  --username ${PG_DB_USER:-postgres} --dbname ${PG_DB_NAME} -qAt --command  "select pg_total_relation_size(quote_ident(table_name))  from information_schema.tables where table_schema = 'public' and table_name = '${table_name}';" | numfmt --from=iec --to-unit=1M)
	echo "${table_size}"
}



pg_count_table_n_dead_tup(){
	local table_name="${1}"
	n_dead_tup=$(PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} psql  --username ${PG_DB_USER:-postgres} --dbname ${PG_DB_NAME} -qAt --command  "SELECT n_dead_tup FROM pg_stat_user_tables where relname = '${table_name}';")
	echo "${n_dead_tup}"
}


pg_repack_run() {

	echo "Started Repack in dbname=${PG_DB_NAME} dbsize=$(pg_show_db_size ${PG_DB_NAME}) Time=$(date +%F__%R)" >> ${LOG_NAME}
if [[ -n "${PG_PARTITIONAL_TABLES}" ]];then
	for PG_TABLE_NAME in ${PG_PARTITIONAL_TABLES}
	do
		local pg_repack_options_2="--table ${PG_TABLE_NAME} ${PG_DB_NAME}"
		local pg_show_table_n_dead_tup=$(pg_count_table_n_dead_tup ${PG_TABLE_NAME})
		local table_type="partitional"

		if [ "${pg_show_table_n_dead_tup}" -ge "${PG_DB_N_DEAD_TUP_MIN}" ];then
			local pg_show_table_size_old=$(pg_show_table_size ${PG_TABLE_NAME})
			echo "Repack table ${PG_TABLE_NAME} in ${PG_DB_NAME}  with options ${pg_repack_options_main} ${pg_repack_options_2}. pg_show_table_n_dead_tup=${pg_show_table_n_dead_tup}. Started at $(date)";
			PGOPTIONS="-c max_parallel_maintenance_workers=6 -c idle_in_transaction_session_timeout=2h" PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} pg_repack  ${pg_repack_options_main}  ${pg_repack_options_2}
			local pg_show_table_size_current=$(pg_show_table_size ${PG_TABLE_NAME})
			local pg_show_table_size_after_repack=$(expr ${pg_show_table_size_old} - ${pg_show_table_size_current} )
			local m_report="Repack ${table_type} table finished. \
				db_name=${PG_DB_NAME}. \
				table_name=${PG_TABLE_NAME}. \
				pg_show_table_size_old=${pg_show_table_size_old}MB. \
				pg_show_table_size_current=${pg_show_table_size_current}MB. \
				Reduced size=${pg_show_table_size_after_repack}MB. \
				pg_show_table_n_dead_tup_before=${pg_show_table_n_dead_tup}. \
				Time=$(date +%F__%R)"
			printf "${m_report}\n" | tr -d '\t'
			printf "${m_report}\n" | tr -d '\t' >> ${LOG_NAME}
		else
			local pg_show_table_size_current=$(pg_show_table_size ${PG_TABLE_NAME})
			local m_report="Repack ${table_type} table skipped. \
					db_name=${PG_DB_NAME}. \
					table_name=${PG_TABLE_NAME}. \
					pg_show_table_size_current=${pg_show_table_size_current}MB. \
					pg_show_table_n_dead_tup=${pg_show_table_n_dead_tup}. \
					Time=$(date +%F__%R)"
			printf "${m_report}\n" | tr -d '\t'
			printf "${m_report}\n" | tr -d '\t' >> ${LOG_NAME}
		fi
done
fi

if [[ -n "${PG_SIMPLE_TABLES}" ]];then
	for PG_TABLE_NAME in ${PG_SIMPLE_TABLES}
	do
		local pg_repack_options_2="--table ${PG_TABLE_NAME} ${PG_DB_NAME}"
		local pg_show_table_n_dead_tup=$(pg_count_table_n_dead_tup ${PG_TABLE_NAME})
		local table_type="simple"
		
		if [ "${pg_show_table_n_dead_tup}" -ge "${PG_DB_N_DEAD_TUP_MIN}" ];then
			local pg_show_table_size_old=$(pg_show_table_size ${PG_TABLE_NAME})
			echo "Repack table ${PG_TABLE_NAME} in ${PG_DB_NAME}  with options ${pg_repack_options_main} ${pg_repack_options_2}. pg_show_table_n_dead_tup=${pg_show_table_n_dead_tup}. Started at $(date)";
			PGOPTIONS="-c max_parallel_maintenance_workers=6 -c idle_in_transaction_session_timeout=2h" PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} pg_repack  ${pg_repack_options_main}  ${pg_repack_options_2}
			local pg_show_table_size_current=$(pg_show_table_size ${PG_TABLE_NAME})
			local pg_show_table_size_after_repack=$(expr ${pg_show_table_size_old} - ${pg_show_table_size_current} )
			local m_report="Repack ${table_type} table finished. \
				db_name=${PG_DB_NAME}. \
				table_name=${PG_TABLE_NAME}. \
				pg_show_table_size_old=${pg_show_table_size_old}MB. \
				pg_show_table_size_current=${pg_show_table_size_current}MB. \
				Reduced size=${pg_show_table_size_after_repack}MB. \
				pg_show_table_n_dead_tup_before=${pg_show_table_n_dead_tup}. \
				Time=$(date +%F__%R)"
			printf "${m_report}\n" | tr -d '\t'
			printf "${m_report}\n" | tr -d '\t' >> ${LOG_NAME}
		else
			local pg_show_table_size_current=$(pg_show_table_size ${PG_TABLE_NAME})
			local m_report="Repack ${table_type} table skipped. \
					db_name=${PG_DB_NAME}. \
					table_name=${PG_TABLE_NAME}. \
					pg_show_table_size_current=${pg_show_table_size_current}MB. \
					pg_show_table_n_dead_tup=${pg_show_table_n_dead_tup}. \
					Time=$(date +%F__%R)"
			printf "${m_report}\n" | tr -d '\t'
			printf "${m_report}\n" | tr -d '\t' >> ${LOG_NAME}
		fi
done
fi
	export dbsize_after=$(pg_show_db_size ${PG_DB_NAME})
	local final_message="Ended Repack in dbname=${PG_DB_NAME}  db_size_before=${db_size_before} dbsize_after=${dbsize_after} Time=$(date +%F__%R)"
	echo "${final_message}"
	echo "${final_message}" >> ${LOG_NAME}
}

main () {
	is_app_running=$(ps aux  | grep 'pg_repack -U' | grep -ve 'grep\|curl')
	if [[ -n "${is_app_running}" ]];then
		echo "Error. pg_repack already running. Exit"
		exit 1
	fi
  export PG_QUERY_SHOW_SIMPLE_TABLES="SELECT i.table_name FROM information_schema.tables as i WHERE NOT EXISTS (select relname from pg_class as p where relpartbound is not null and p.relname =  i.table_name )  and NOT EXISTS(select relname from pg_class as p where oid in (select partrelid from pg_partitioned_table) and p.relname =  i.table_name ) and table_type = 'BASE TABLE' AND table_schema NOT IN ('pg_catalog', 'information_schema');"
  
  export PG_QUERY_SHOW_PARTITIONAL_CHILDREN_TABLES="SELECT c.relname FROM pg_class AS c WHERE  EXISTS (SELECT 1 FROM pg_inherits AS i WHERE i.inhrelid = c.oid) AND c.relkind IN ( 'r');"
  export PG_QUERY_SHOW_PARTITIONAL_PARENT_TABLES="SELECT c.relname FROM pg_class AS c WHERE  NOT EXISTS (SELECT 1 FROM pg_inherits AS i WHERE i.inhrelid = c.oid) AND c.relkind IN ( 'p');"
  
  export PG_PARTITIONAL_TABLES=$(PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} psql  --username ${PG_DB_USER:-postgres} --dbname ${PG_DB_NAME} -qAt --command  "${PG_QUERY_SHOW_PARTITIONAL_CHILDREN_TABLES}")
  export PG_SIMPLE_TABLES=$(PGPASSWORD=${PG_DB_PASSWORD:-${POSTGRESQL_PASSWORD}} psql  --username ${PG_DB_USER:-postgres} --dbname ${PG_DB_NAME} -qAt --command  "${PG_QUERY_SHOW_SIMPLE_TABLES}")
  
  export CUR_DATA=$(date +%F__%R)
  export LOG_NAME="/tmp/repack_${PG_DB_NAME}_${CUR_DATA}.log"
  
  
  export db_size_before=$(pg_show_db_size ${PG_DB_NAME})
  export pg_repack_options_main="-U ${PG_DB_USER:-postgres}  --no-order -j 6 --no-superuser-check --no-kill-backend --wait-timeout=80"
  printf "=====pg_repack started======\n"
  printf "============================\n"
  printf "============================\n"
  printf "===========ENVS=============\n"
  printf "pg_repack_options_main=${pg_repack_options_main}\n"
  printf "CUR_DATA=${CUR_DATA}\n"
  printf "LOG_NAME=${LOG_NAME}\n"
  printf "PG_DB_N_DEAD_TUP_MIN=${PG_DB_N_DEAD_TUP_MIN}\n"
  printf "PG_DB_NAME=${PG_DB_NAME}\n"
  printf "db_size_curent=${db_size_before}\n"
  sleep 5;
	prepare
	extension_create
	pg_repack_run
	extension_remove
}

main