queue_restart () {
    sh -c "cd ${APP_DIR_CURRENT} ; supervisorctl stop dispatcher ; php artisan queue:restart ; supervisorctl start dispatcher"
}

artisan_tinker () {
    su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan tinker" ${DOCKER_IMAGE_RUN_USER:-www-data} 
}

artisan_config_cache () {
    su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan config:cache" ${DOCKER_IMAGE_RUN_USER:-www-data} 
}

artisan_route_cache () {
    su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan cache:clear;/usr/local/bin/php artisan route:clear;/usr/local/bin/php artisan route:cache;/usr/local/bin/php artisan config:cache;/usr/local/bin/php artisan optimize;" ${DOCKER_IMAGE_RUN_USER:-www-data} 
}

app_prepare_deployment-ui (){

    echo "exec funct ${FUNCNAME}"
    if [[ ! -d "${APP_DIR_SHARED}/bootstrap/cache" ]];then mkdir -p "${APP_DIR_SHARED}/bootstrap/cache" -m 777;fi
    if [[ -f ${APP_DIR_SHARED}/.env ]];then
     ln -sf "${APP_DIR_SHARED}"/.env "${APP_DIR_CURRENT}"/.env
    fi
    if [[  ! -L "${APP_DIR_CURRENT}/storage" ]]; then
         mv "${APP_DIR_CURRENT}"/storage "${APP_DIR_CURRENT}"/storage.old
         ln -sf "${APP_DIR_SHARED}"/storage "${APP_DIR_CURRENT}"/storage
    fi
    if [[  ! -L "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        mv "${APP_DIR_CURRENT}"/bootstrap/cache "${APP_DIR_CURRENT}"/bootstrap/cache.old
        ln -sf "${APP_DIR_SHARED}"/bootstrap/cache "${APP_DIR_CURRENT}"/bootstrap/cache;
    fi
    artisan_config_cache
}

nginx_config_generate_backend_deployment-ui () {

if [[ ! -f "${nginx_main_dir:-/etc/nginx}/nginx_backend_${APP_NAME}_additional.conf" ]];then
    touch ${nginx_main_dir:-/etc/nginx}/nginx_backend_${APP_NAME}_additional.conf
fi

if [[ ! -d "${nginx_dir_backend:-/etc/nginx/backend.d}" ]];then
    mkdir -p ${nginx_dir_backend:-/etc/nginx/backend.d}
fi


cat <<OEF> ${nginx_main_dir:-/etc/nginx}/nginx_backend_${APP_NAME}_additional.conf

location ~ /\.git {
    deny all;
} 
set \$realip \$remote_addr;
if (\$http_x_forwarded_for ~ "^(\d+\.\d+\.\d+\.\d+)") {
    set \$realip \$1;
}
OEF


grep -q "${APP_HTTP_HOST}" "${nginx_dir_backend:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF>> ${nginx_dir_backend:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        ###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
        server {
            listen 80;
            server_name 
            ${APP_HTTP_HOST}
            ;
            root ${APP_DIR_CURRENT}/public;
            index index.php index.html index.htm;     
            include ${nginx_main_dir:-/etc/nginx}/nginx_backend_${APP_NAME}_additional.conf;
            location / {
                try_files \$uri \$uri/ /index.php?\$query_string;
            } 
            lingering_time 60;
            location ~ \.php$ {
                try_files \$uri /index.php =404;
                fastcgi_split_path_info ^(.+\.php)(/.+)\$;
                fastcgi_pass ${NGINX_PHP_BACKEND:-127.0.0.1:9000};
                fastcgi_index index.php;
                fastcgi_read_timeout 500;
                fastcgi_send_timeout 500;
                fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
                fastcgi_param  PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/node/bin:/bin;
                include fastcgi_params;

            }
        }
        ###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF

}