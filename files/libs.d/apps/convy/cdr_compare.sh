#!/usr/bin/env bash

#days_for_restore="2023-01-09 2023-01-18 2023-01-20 2023-01-25 2023-01-29 2023-02-02 2023-02-03 2023-02-04 2023-02-06 2023-02-12"
days_for_restore="2023-01-09"
#d="2023-02-13"
#d_end="2023-02-13"

#cdr_extra_flags="-R"
cdr_extra_flags="-R"

cdr_compre_run() {
local d=${1}
local d_end=${2}
echo "d=${d} d_end=${d_end}"

until [[ $d > $d_end ]]; do 
    echo "Cdr compare for $d begin at $(date)"
#    docker exec --env d=$d --env cdr_extra_flags=$cdr_extra_flags -i -u 0 $(docker ps --filter name=loader_cdr --filter status=running -q) sh -c 'cd /var/www/html/${APP_HTTP_HOST}/current && php artisan convy:cdr-load-db $d $d ${cdr_extra_flags}'
    echo "Cdr compare for $d finished at $(date)"
    d=$(date -I -d "$d + 1 day")
done
}

main() {
	echo "Run cdr compare with cdr_extra_flags=${cdr_extra_flags}"
for day_for_restore in ${days_for_restore}
do
	echo  "Run cdr_compre_run ${day_for_restore} ${day_for_restore}"
	cdr_compre_run ${day_for_restore} ${day_for_restore}
done
}
main