#!/usr/bin/env bash
export CONVY_WORKERS=$(docker ps --filter name=convy_worker --filter status=running -q)
export convy_dispatcher=$(docker ps --filter name=convy_dispatcher --filter status=running -q) 
for CONVY_WORKER in $CONVY_WORKERS;do
	docker exec -i -u 0 $CONVY_WORKER sh -c 'cd ${APP_DIR_CURRENT}/bootstrap/cache && rm -rfv config.php services.php packages.php'
	docker exec -i -u 0 $CONVY_WORKER sh -c 'cd ${APP_DIR_CURRENT} && php artisan cache:clear;php artisan route:clear;php artisan route:cache;php artisan config:cache;php artisan optimize;find /var/www/html/${APP_HTTP_HOST}  \! -user www-data -exec chown -h -v www-data:www-data {} +;find /var/www/html/${APP_HTTP_HOST}  \! -group www-data -exec chown -h -v www-data:www-data {} +;'
	docker exec -i -u 0 $CONVY_WORKER sh -c '. /opt/public_libs/files/libs.d/apps/convy.sh && convy_octane_restart'
done
docker exec -i -u 0 ${convy_dispatcher} sh -c 'cd ${APP_DIR_CURRENT}/bootstrap/cache && rm -rfv config.php services.php packages.php'
docker exec -i -u 0 ${convy_dispatcher} sh -c 'cd ${APP_DIR_CURRENT} && php artisan cache:clear;php artisan route:cache;php artisan route:clear;php artisan config:cache;php artisan optimize;find /var/www/html/${APP_HTTP_HOST}  \! -user www-data -exec chown -h -v www-data:www-data {} +;find /var/www/html/${APP_HTTP_HOST}  \! -group www-data -exec chown -h -v www-data:www-data {} +;'
docker exec -i -u 0 ${convy_dispatcher} sh -c '. /opt/public_libs/files/libs.d/apps/convy.sh && convy_queue_restart'