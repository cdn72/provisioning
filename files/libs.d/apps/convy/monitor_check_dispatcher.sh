#!/usr/bin/env bash

monitor_check_dispatcher () {

  env_file="${APP_DIR_SHARED}/.env"
  prom_log_file="${APP_DIR_SHARED}/storage/logs/dispatcher_check.log"
  prom_http_host=$(grep -o "^PROM_HTTP_HOST.*" ${APP_DIR_SHARED}/.env | awk -F= '{ print $2 }' | sed 's/["]//g' )
  prom_auth_user=$(grep -o "^PROM_AUTH_USER.*" ${APP_DIR_SHARED}/.env | awk -F= '{ print $2 }' | sed 's/["]//g' )
  prom_auth_pass=$(grep -o "^PROM_AUTH_PASS.*" ${APP_DIR_SHARED}/.env | awk -F= '{ print $2 }' | sed 's/["]//g' )
  prom_value_compare=$(grep -o "^PROM_VALUE_COMPARE.*" ${APP_DIR_SHARED}/.env | awk -F= '{ print $2 }' | sed 's/["]//g' )
  prom_data_horison_status=$(curl --user ${prom_auth_user}:${prom_auth_pass} -fs --data-urlencode "query=app_horizon_status{instance=\"${APP_HTTP_HOST}:443\"}"  \
    "https://${prom_http_host}/api/v1/query" | jq -r '.data.result[] | .value[1] ')


if [[ "${prom_data_horison_status}" == "1" ]]; then 
  prom_data_workload=$(curl --user ${prom_auth_user}:${prom_auth_pass} -fs --data-urlencode "query=app_horizon_current_workload{instance=\"${APP_HTTP_HOST}:443\"}" \
    "https://${prom_http_host}/api/v1/query" | jq -r '.data.result[] | [.metric.queue,.value[1] ] | @csv' )
  echo "${prom_data_workload}" > /tmp/horison.txt

while IFS="," read -r queue_name queue_quantity
do
  echo "Convy queue_name = ${queue_name}" \
  echo "Convy queue_quantity = ${queue_quantity}" \
  echo "Compare queue_quantity=$(echo "${queue_quantity}" | sed 's/["]//g') with prom_value_compare=${prom_value_compare}"
  if [[ $(echo "${queue_quantity}" | sed 's/["]//g') -gt "${prom_value_compare}" ]];then
    echo "queue_name ${queue_name} problem. Current quantity = ${queue_quantity}"
    convy_dispatcher_restart ${queue_name} ${queue_quantity}
    echo "Sleep for ${dispatcher_wait_after_restart:-30m}"
    sleep ${dispatcher_wait_after_restart:-30m}
    break
  else
    echo "Prom_value_compare=${prom_value_compare} less than current queue_quantity=${queue_quantity}. queue_name ${queue_name}  status ok. "
  fi
done < <(tail -n +2 /tmp/horison.txt)

else
  echo "Skip dispatcher check as prom_data_horison_status != 1. APP_VERSION=${APP_VERSION}" >> ${prom_log_file}
fi 
}


convy_dispatcher_restart () {
  echo "Try restart dispatcher"
  local message="queue_name $1 problem. Current queue_quantity=$2. Convy_dispatcher_restarted at $(date +%Y_%m_%d__%H:%Sh). APP_VERSION=${APP_VERSION}"
  cd ${APP_DIR_CURRENT:-${dir_app}/current} ; supervisorctl stop dispatcher ; php artisan queue:restart ; supervisorctl start dispatcher
  echo "${message}" \ 
  echo "${message}" >> ${prom_log_file}
}


main () {
  if grep -q "^DISPATCHER_MONITOR=.*rue" ${APP_DIR_SHARED}/.env
  then
    monitor_check_dispatcher
  fi
  echo "Wait 30 sec" && sleep 30;
}

main