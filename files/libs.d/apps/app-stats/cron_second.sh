#!/usr/bin/env bash
cron_second () {
  su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan short-schedule:run" ${DOCKER_IMAGE_RUN_USER:-www-data};
}


main () {
  cron_second
}

main