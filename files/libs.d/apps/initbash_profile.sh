#!/usr/bin/env bash
if [[ -f "/root/.bashrc" && -f "/opt/public_libs/files/libs.d/shell/.bashrc" ]]; then
	cat /opt/public_libs/files/libs.d/shell/.bashrc > /root/.bashrc
fi

if [[ -d "/home/${DOCKER_IMAGE_RUN_USER:-www-data}" && -f "/opt/public_libs/files/libs.d/shell/.bashrc" ]];then
	cat /opt/public_libs/files/libs.d/shell/.bashrc > /home/"${DOCKER_IMAGE_RUN_USER:-www-data}"/.bashrc
	chown "${DOCKER_IMAGE_RUN_USER:-www-data}":"${DOCKER_IMAGE_RUN_USER:-www-data}" /home/"${DOCKER_IMAGE_RUN_USER:-www-data}"/.bashrc
fi