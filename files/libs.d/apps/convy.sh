#!/usr/bin/env bash
export dir_app="/var/www/html/${APP_HTTP_HOST:-${APP_HTTP_HOST:-${CONVY_HTTP_HOST}}}"

convy_app_perms_fix(){
if [[ -d "${APP_DIR_CURRENT}" ]];then
   find ${APP_DIR_CURRENT}  \! -user "${DOCKER_IMAGE_RUN_USER:-www-data}" -exec chown -h  ${DOCKER_IMAGE_RUN_USER:-www-data}:"${DOCKER_IMAGE_RUN_USER:-www-data}" {} +;
   find ${APP_DIR_CURRENT}  \! -group "${DOCKER_IMAGE_RUN_USER:-www-data}" -exec chown -h  ${DOCKER_IMAGE_RUN_USER:-www-data}:"${DOCKER_IMAGE_RUN_USER:-www-data}" {} +;
fi
if [[ -d "${APP_DIR_SHARED}" ]];then
   find ${APP_DIR_SHARED}  \! -user "${DOCKER_IMAGE_RUN_USER:-www-data}" -exec chown -h  ${DOCKER_IMAGE_RUN_USER:-www-data}:"${DOCKER_IMAGE_RUN_USER:-www-data}" {} +;
   find ${APP_DIR_SHARED}  \! -group "${DOCKER_IMAGE_RUN_USER:-www-data}" -exec chown -h  ${DOCKER_IMAGE_RUN_USER:-www-data}:"${DOCKER_IMAGE_RUN_USER:-www-data}" {} +;
fi
}

convy_fix_perms(){
	echo "exec funct ${FUNCNAME}"
	cd ${APP_DIR_CURRENT} && php artisan storage:link
	convy_app_perms_fix
}


convy_fix_docs_generate () {
	echo "exec funct ${FUNCNAME}"
	if [[ -d "${APP_DIR_CURRENT}" ]];then
		if [[ -f "${APP_DIR_CURRENT}/storage/app/scribe/openapi.yaml" ]];then
			mv -f ${APP_DIR_CURRENT}/storage/app/scribe/openapi.yaml "${APP_DIR_CURRENT}"/storage/app/scribe/openapi.yaml.old
		fi
		screen -dmSL "${FUNCNAME:-docs_generate}" sh -c "cd ${APP_DIR_CURRENT} && php artisan docs:generate && touch docs_generated_ok && . /opt/public_libs/files/libs.d/apps/main.sh && app_fix_perms"
		echo "Docs generated"
	fi

}

convy_clean_cache(){
	echo "Exec convy_clean_cache"
local check_files="config.php services.php packages.php"
for check_file in ${check_files};
do
	if [[ -f "${APP_DIR_CURRENT}/bootstrap/cache/${check_file}" ]];then
		rm ${APP_DIR_CURRENT}/bootstrap/cache/${check_file}
	else
		echo "File ${APP_DIR_CURRENT}/bootstrap/cache/${check_file} not exist"
	fi
done
   cd "${APP_DIR_CURRENT}" 
   php artisan cache:clear
   php artisan route:cache
   php artisan config:cache
   php artisan optimize
   convy_app_perms_fix

}

convy_php_artisan_update_run(){
	echo "exec funct ${FUNCNAME}"
	if [[ -d "${APP_DIR_CURRENT}" ]];then
		screen -dmSL "${FUNCNAME}" sh -c "cd ${APP_DIR_CURRENT} && php artisan update:run; echo \"exec funct ${FUNCNAME} finished\""
	fi
}

convy_worker_configs_init(){
	if  grep -q 'OCTANE_SERVER=roadrunner' "${APP_DIR_CURRENT}/.env" && grep -q  'roadrunner' "${APP_DIR_CURRENT}/composer.json"; then
		cat <<OEF> "${APP_DIR_CURRENT}"/.rr.yaml
http:
  address: 0.0.0.0:8000
  pool:
    num_workers: ${OCTANE_WORKERS:-auto}
    max_jobs: ${OCTANE_MAX_REQUESTS:-1500}
    supervisor:
      exec_ttl: 30s
  uploads:
    forbid: [ ".exe", ".sh" ]

logs:
   mode: production
   level: ${ROADRUNNER_LOG_LEVEL:-debug}
   output: stdout
   encoding: json		
OEF
		OCTANE_EXTRA_PARAMS="--server=roadrunner --host=0.0.0.0  --port=8000 --workers=${OCTANE_WORKERS:-auto} --task-workers=${OCTANE_TASK_WORKERS:-auto} --max-requests=${OCTANE_MAX_REQUESTS:-1500} --rr-config=${APP_DIR_CURRENT}/.rr.yaml --rpc-port=${OCTANE_RPC_PORT:-16001}"
	else
		OCTANE_EXTRA_PARAMS="--server=swoole --host=0.0.0.0  --port=8000 --workers=${OCTANE_WORKERS:-auto} --task-workers=${OCTANE_TASK_WORKERS:-auto} --max-requests=${OCTANE_MAX_REQUESTS:-1500}"
	fi

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/worker.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true


[program:php-fpm]
command=/usr/local/sbin/php-fpm -F --pid /var/run/php-fpm.pid
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
autorestart=false
startretries=0
stopwaitsecs=10
stopsignal=SIGINT

[program:nginx]
command=nginx -g 'daemon off;'
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
autorestart=false
startretries=0
stopwaitsecs=10
stopsignal=SIGINT
OEF


if [[ ! -f "${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/websockets.${SUPERVISOR_CONF_EXT:-conf}" ]];then
cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/websockets.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:websockets]
command=php ${APP_DIR_CURRENT}/artisan websockets:serve --port=6001
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
user=${DOCKER_IMAGE_RUN_USER:-www-data}
startretries=140
stopwaitsecs=10
OEF
fi

	if [[ -f "${APP_DIR_SHARED}/p0f.fp" ]];then
   cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/p0f.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:p0f]
command=/usr/bin/p0f -s /var/run/p0f.sock -f ${APP_DIR_SHARED}/p0f.fp
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=10
user=root
OEF

fi
	if [[ -f "${APP_DIR_SHARED}/octane" ]];then
      cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/octane.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:octane]
directory=${APP_DIR_CURRENT}
command=php artisan octane:start ${OCTANE_EXTRA_PARAMS}
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=10
user=${DOCKER_IMAGE_RUN_USER:-www-data}
OEF

	if [ ! -f "${APP_DIR_CURRENT}/storage/logs/swoole_http.log" ];then
		touch "${APP_DIR_CURRENT}"/storage/logs/swoole_http.log
		chown ${DOCKER_IMAGE_RUN_USER:-www-data}:"${DOCKER_IMAGE_RUN_USER:-www-data}" "${APP_DIR_CURRENT}"/storage/logs/swoole_http.log
	else
		chown ${DOCKER_IMAGE_RUN_USER:-www-data}:"${DOCKER_IMAGE_RUN_USER:-www-data}" "${APP_DIR_CURRENT}"/storage/logs/swoole_http.log
	fi

else
	echo "File ${APP_DIR_SHARED}/octane not exist. Skip run octane"
	if [[ -f "${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/octane.${SUPERVISOR_CONF_EXT:-conf}" ]];then
		rm "${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/octane.${SUPERVISOR_CONF_EXT:-conf}"
	fi
	
fi

}
convy_dispatcher_configs_init(){

	echo "exec funct ${FUNCNAME}"
	if [[ -f "${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/worker.${SUPERVISOR_CONF_EXT:-conf}" ]];then
		rm ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/worker.${SUPERVISOR_CONF_EXT:-conf}
	fi

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/nginx.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:nginx]
command=nginx -g 'daemon off;'
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stderr_logfile=/dev/stderr
stdout_logfile_maxbytes=0
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=20
OEF

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/dispatcher.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:dispatcher]
command=php ${APP_DIR_CURRENT}/artisan horizon
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
user=${DOCKER_IMAGE_RUN_USER:-www-data}
startretries=140
stopwaitsecs=100
OEF

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/websockets.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:websockets]
command=php ${APP_DIR_CURRENT}/artisan websockets:serve --port=6001
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
user=${DOCKER_IMAGE_RUN_USER:-www-data}
startretries=140
stopwaitsecs=10
OEF

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/monitor_check_dispatcher.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:monitor_check_dispatcher]
command=sh -c "while true;do bash /opt/public_libs/files/libs.d/apps/convy/monitor_check_dispatcher.sh;done"
autostart=true
autorestart=true
stdout_logfile=${APP_DIR_SHARED}/storage/logs/monitor_check_dispatcher.log
stdout_logfile_maxbytes=0
stderr_logfile=${APP_DIR_SHARED}/storage/logs/monitor_check_dispatcher.log
stderr_logfile_maxbytes=0
startretries=140
startretries=140
stopwaitsecs=10
user=root
OEF

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/cron.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:crond]
command=/usr/sbin/crond -f -l 0 -d 0 
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=10
user=root
OEF


if [[ -f "${APP_DIR_SHARED}/p0f.fp.dispatcher" ]];then

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/p0f.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:p0f]
command=/usr/bin/p0f -s /var/run/p0f.sock -f ${APP_DIR_SHARED}/p0f.fp.dispatcher
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=10
user=root
OEF

fi

if [[ ! -d "/etc/periodic/everyminute" ]];then
 	mkdir -p /etc/periodic/everyminute
fi

cat <<OEF> /etc/periodic/everyminute/schedule
#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
su -s /bin/bash -c 'cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan schedule:run' ${DOCKER_IMAGE_RUN_USER:-www-data}
OEF

if [[ -f "/etc/periodic/everyminute/schedule" ]];then
	chmod 0755 /etc/periodic/everyminute/schedule
fi

echo "* * * * * run-parts /etc/periodic/everyminute " > /var/spool/cron/crontabs/root && chown -R -v  root /etc/periodic/

}

convy_init(){
	echo "exec funct ${FUNCNAME}"
	if [[ -f ${APP_DIR_SHARED}/.env ]];then
	 ln -sf "${APP_DIR_SHARED}"/.env "${APP_DIR_CURRENT}"/.env
	fi
	if [[ -f ${APP_DIR_SHARED}/auth.json ]];then
		ln -sf "${APP_DIR_SHARED}"/auth.json "${APP_DIR_CURRENT}"/auth.json;
	fi
	if [[  ! -L "${APP_DIR_CURRENT}/storage" ]]; then
	 	mv "${APP_DIR_CURRENT}"/storage "${APP_DIR_CURRENT}"/storage.old
	 	ln -sf "${APP_DIR_SHARED}"/storage "${APP_DIR_CURRENT}"/storage
	fi
	if [[  ! -L "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
		if [[ -d "${APP_DIR_CURRENT}/bootstrap/cache" ]] ;then mv "${APP_DIR_CURRENT}"/bootstrap/cache "${APP_DIR_CURRENT}"/bootstrap/cache.old;fi
		ln -sf "${APP_DIR_SHARED}"/bootstrap/cache "${APP_DIR_CURRENT}"/bootstrap/cache;
	fi
	convy_clean_cache;
	if [[ "${ENV_WORKER_FUNCTION}" == "dispatcher" ]];then
		cd "${APP_DIR_CURRENT}";
		convy_php_artisan_update_run;
		convy_fix_docs_generate;
		cd "${APP_DIR_CURRENT}";
		if [[ -n "${APP_VERSION}" ]];then
			grep -q "VUE_APP_VERSION" "${APP_DIR_SHARED}"/.env && echo exists || sed -i "\$aVUE_APP_VERSION=${APP_VERSION}" "${APP_DIR_SHARED}"/.env;
			sed -i  "s/VUE_APP_VERSION=.*$/VUE_APP_VERSION=${APP_VERSION}/g"  "${APP_DIR_SHARED}"/.env;
		fi
		echo "" > ${APP_DIR_SHARED}/storage/logs/monitor_check_dispatcher.log || echo "Skip truncate file ${APP_DIR_SHARED}/storage/logs/monitor_check_dispatcher.log"
		php artisan tracked-processes:start

	fi

	if [[ "${ENV_WORKER_FUNCTION}" == "worker" ]];then
		cd "${APP_DIR_CURRENT}";
		cd "${APP_DIR_CURRENT}" && php artisan octane:stop || echo "Skip stop octane"
		cd "${APP_DIR_CURRENT}" && php artisan octane:reload || echo "Skip reload octane"
	fi
	convy_clean_cache

}

nginx_config_generate_backend_convy () {

if [[ ! -f "${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf" ]];then
    touch ${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf
fi

if [[ ! -d "${nginx_dir_backend:-/etc/nginx/backend.d}" ]];then
	mkdir -p ${nginx_dir_backend:-/etc/nginx/backend.d}
fi

if [[ -f "${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf" ]];then
    cat <<OEF> ${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf
location ~ /\.git {
    deny all;
} 
#set_real_ip_from 0.0.0.0/0;
#real_ip_header X-Forwarded-For;
set \$realip \$remote_addr;
  if (\$http_x_forwarded_for ~ "^(\d+\.\d+\.\d+\.\d+)") {
    set \$realip \$1;
  }

OEF

fi
if [[ -f "${APP_DIR_SHARED}/octane" ]];then
    cat <<OEF>> ${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf
location /api/xml {
    proxy_pass   http://127.0.0.1:8000/api/xml;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}  

location /api/cdr {
    proxy_pass   http://127.0.0.1:8000/api/cdr;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
} 
location /api/sms/receive {
    proxy_pass   http://127.0.0.1:8000/api/sms/receive;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}
location /api/dlr/report {
    proxy_pass   http://127.0.0.1:8000/api/dlr/report;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}  
location /api/conversion/report {
    proxy_pass   http://127.0.0.1:8000/api/conversion/report;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}
location /api/conversion/event {
    proxy_pass   http://127.0.0.1:8000/api/conversion/event;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}
location /api/bread/ {
    proxy_pass   http://127.0.0.1:8000/api/bread/;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}
OEF
fi


for app_http_host in $(echo "${APP_HTTP_HOST:-${CONVY_HTTP_HOST}}" | tr "," "\n")
do
	echo "app_http_host is ${app_http_host}"
	if [[ -n "${app_http_host/[ ]*\n/}" ]]
	then
		echo " Генерируем nginx config ${app_http_host} для nginx_backend_convy"
		if [[ ! -f "${nginx_dir_backend:-/etc/nginx/backend.d}/${app_http_host}.conf" ]]
		then
			touch "${nginx_dir_backend:-/etc/nginx/backend.d}"/"${app_http_host}".conf
		fi
		grep -q "${NGINX_CONVY_HOST_HTTP}" "${nginx_dir_backend:-/etc/nginx/backend.d}"/"${app_http_host}".conf ||  /bin/cat <<OEF>> ${nginx_dir_backend:-/etc/nginx/backend.d}/"${app_http_host}".conf
		###NGINX BACKEND CONFIG BEGIN for ${app_http_host}
		server {
			listen 80;
			server_name 
			${app_http_host}
			www.${app_http_host}
			;
			root ${APP_DIR_CURRENT}/public;
			index index.php index.html index.htm;     
			include ${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf;
			location / {
				try_files \$uri \$uri/ /index.php?\$query_string;
			} 
			lingering_time 60;
			location ~ \.php$ {
				try_files \$uri /index.php =404;
				fastcgi_split_path_info ^(.+\.php)(/.+)$;
				fastcgi_pass ${NGINX_PHP_BACKEND:-127.0.0.1:9000};
				fastcgi_index index.php;
				fastcgi_read_timeout 500;
				fastcgi_send_timeout 500;
				fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
				fastcgi_param  PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/node/bin:/bin;
				include fastcgi_params;

			}
		}
		###NGINX BACKEND CONFIG END for ${app_http_host}
OEF

		fi
done

}

convy_ccd(){
	if [[ -d ${APP_DIR_CURRENT} ]];then
		cd ${APP_DIR_CURRENT}
	else
		echo "dir ${APP_DIR_CURRENT} not exist"
	fi
}

convy_octane_restart(){
	su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan octane:reload && php artisan octane:stop " ${DOCKER_IMAGE_RUN_USER:-www-data};
    su -s /bin/bash -c "supervisorctl restart octane && supervisorctl status octane"
}




convy_tinker(){
	su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan tinker" ${DOCKER_IMAGE_RUN_USER:-www-data} 
}

convy_rebuild_worker(){
	sh -c "cd ${APP_DIR_CURRENT}/frontend ;  yarn build ; cd ${APP_DIR_CURRENT}/ ; php artisan config:clear; php artisan  cache:clear; php artisan optimize"
	convy_fix_perms
}

convy_rebuild_dispatcher(){
	sh -c "cd ${APP_DIR_CURRENT}/frontend ;  yarn build ; cd ${APP_DIR_CURRENT}/ ; php artisan config:clear; php artisan  cache:clear; php artisan optimize; supervisorctl restart all;cd ${APP_DIR_CURRENT};php artisan config:cache;if [ -f ${APP_DIR_CURRENT}/storage/app/scribe/openapi.yaml ];then mv -f ${APP_DIR_CURRENT}/storage/app/scribe/openapi.yaml ${APP_DIR_CURRENT}/storage/app/scribe/openapi.yaml.old;fi;php artisan docs:generate"
	convy_fix_perms
}


convy_queue_restart(){
	sh -c "cd ${APP_DIR_CURRENT} ; supervisorctl stop dispatcher ; php artisan queue:restart ; supervisorctl start dispatcher"
}

convy_sms_generator_update_start () {
   echo "exec ${APP_DIR_CURRENT} && php artisan sms-generator:update-start. Status=begin";
   cd ${APP_DIR_CURRENT} && php artisan sms-generator:update-start;
   echo "exec ${APP_DIR_CURRENT} && php artisan sms-generator:update-start. Status=finished";
}


convy_sms_generator_update_finish () {
   echo "exec ${APP_DIR_CURRENT} && php artisan sms-generator:update-finish. Status=begin";
   cd ${APP_DIR_CURRENT} && php artisan sms-generator:update-finish;
   echo "exec ${APP_DIR_CURRENT} && php artisan sms-generator:update-finish. Status=finished";
}


convy_clean_failed_jobs(){
	su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && /usr/local/bin/php artisan horizon:flush-failed" ${DOCKER_IMAGE_RUN_USER:-www-data} 
}
convy_load_sql_pg() {
	local file_to_load=${1}
	if [[ ! -f "${file_to_load}" ]];then
		echo "Error. File ${file_to_load} not exist."
		exit 1
	fi
	if [[ ! -f "${APP_DIR_CURRENT}/.env" ]];then echo "Error. File ${APP_DIR_CURRENT}/.env not exist" && exit 1;fi
	local pg_host=$(awk -F "=" '{if(/^DB_HOST_PG14=/) print $2}' < "${APP_DIR_CURRENT}/.env")
	local pg_port=$(awk -F "=" '{if(/^DB_PORT_PG14=/) print $2}' < "${APP_DIR_CURRENT}/.env")
	local pg_db_name=$(awk -F "=" '{if(/^DB_DATABASE_PG14=/) print $2}' < "${APP_DIR_CURRENT}/.env")
	local pg_db_user=$(awk -F "=" '{if(/^DB_USERNAME_PG14=/) print $2}' < "${APP_DIR_CURRENT}/.env")
	local pg_db_password=$(awk -F "=" '{if(/^DB_PASSWORD_PG14=/) print $2}' < "${APP_DIR_CURRENT}/.env")
	PGPASSWORD=${pg_db_password} psql --host=${pg_host} --port=${pg_port} --username=${pg_db_user} --dbname=${pg_db_name} --command "SELECT 1;" >/dev/null
	if [[ $? != "0" ]];then
		local message="Error during connection to database. Command psql --host=${pg_host} --port=${pg_port} --username=${pg_db_user} --dbname=${pg_db_name}"
		print "${message}"
	else
		echo "Start load file ${file_to_load} in database. Exec command psql --host=${pg_host} --port=${pg_port} --username=${pg_db_user} --dbname=${pg_db_name} -f ${file_to_load}"
		PGPASSWORD=${pg_db_password} psql --host=${pg_host} --port=${pg_port} --username=${pg_db_user} --dbname=${pg_db_name} -f ${file_to_load}
		if [[ $? != "0" ]];then
			local message="Error during load file ${file_to_load} in database. psql --host=${pg_host} --port=${pg_port} --username=${pg_db_user} --dbname=${pg_db_name} -f ${file_to_load}"
			print "${message}"
		fi
	fi
}