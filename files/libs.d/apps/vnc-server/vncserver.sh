#!/usr/bin/env bash
export USER=$(whoami)
export PASSWD_PATH="${HOME}/.vnc/passwd"

export    DISPLAY=:1 \
    HOME=${ARG_HOME:-/home/headless} \
    NO_VNC_PORT="6901" \
    REFRESHED_AT=${ARG_REFRESHED_AT} \
    STARTUPDIR=/dockerstartup \
    VERSION_STICKER=${ARG_VERSION_STICKER} \
    VNC_BLACKLIST_THRESHOLD=${ARG_VNC_BLACKLIST_THRESHOLD:-20} \
    VNC_BLACKLIST_TIMEOUT=${ARG_VNC_BLACKLIST_TIMEOUT:-0} \
    VNC_COL_DEPTH=24 \
    VNC_PORT="5901" \
    VNC_PW=${ARG_VNC_PW:-headless} \
    VNC_RESOLUTION=${ARG_VNC_RESOLUTION:-1360x768} \
    VNC_VIEW_ONLY=false
mkdir $STARTUPDIR
if [[ "${VNC_VIEW_ONLY}" == "true" ]]; then
    echo "Start VNC server in view only mode"
    ### create random pw to prevent access
    echo $(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 20) | vncpasswd -f > "${PASSWD_PATH}"
fi

echo "${VNC_PW:-$(pwgen -1 -c 14)}" | vncpasswd -f >> "${PASSWD_PATH}"
chmod 600 "${PASSWD_PATH}"
echo "USER=$USER"
vncserver ${DISPLAY} -depth ${VNC_COL_DEPTH:-24} -geometry ${VNC_RESOLUTION:-1280x720} \
    -BlacklistTimeout ${VNC_BLACKLIST_TIMEOUT:-20} \
    -BlacklistThreshold ${VNC_BLACKLIST_THRESHOLD:-20} &> "${STARTUPDIR:-/tmp}"/no_vnc_startup.log