#!/usr/bin/env bash

consul_agent_start(){
    if [[ -z "${APP_ROLE:-${DOCKER_ROLE}}" ]];then
        export APP_ROLE=${ENV_WORKER_FUNCTION:-unknown_docker_role};
    fi
    export consul_net=$(nslookup ${DISCOVERY_SERVICE_HOST:-consul.server} | grep Address | grep -ve ':53\|#53' | awk '{ print $2 }' | head -n 1)
    export ipaddr=$(ip route get "${consul_net}" | head -n 1 | awk  '{ print $5 }'  | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" )
    export hostname=$(echo "${APP_ROLE}_$(hostname)_${APP_NAME:-unknown_app_name}_${ipaddr}" | sed "s/\./-/g")
    export consul_node=$( echo "${APP_NAME:-unknown_app_name}-${APP_HTTP_HOST:-unknown_http_host}-${APP_VERSION:-unknown_app_version}-${APP_ROLE:-unknown_app_role}-${ipaddr}" | sed "s/\./-/g" )
    export tags=$(echo "[\"${APP_NAME:-unknown_app_name}\",\"${APP_HTTP_HOST:-unknown_http_host}\",\"${APP_VERSION:-unknown_app_version}\",\"${APP_ROLE:-unknown_app_role}\"]")

    if [[ ! -d "${CONSUL_AGENT_DIR:-/opt/consul_agent}" ]];then mkdir -p "${CONSUL_AGENT_DIR:-/opt/consul_agent}";fi
        echo "{ \"enable_script_checks\" : true, \"services\": [ { \"name\": \"${APP_NAME:-unknown_app_name}-${APP_ROLE}\", \"tags\": ${tags}", \"address\": \""$ipaddr"\", \"port\": "${APP_SERVICE_PORT:-80}", \"check\": [ { \"id\": \"Container health status\", \"name\": \"Service healtcheck\", \"args\": [\"/docker-healtcheck.sh\"], \"interval\": \"10s\", \"timeout\": \"3s\" } ] } ] }"" > "${CONSUL_AGENT_DIR:-/opt/consul_agent}"/"${APP_NAME:-unknown_app_name}"-"${APP_ROLE:-unknown_app_role}".json
    if [[ $(ps aux | grep "consul.*agent" | grep -ve 'grep\|consul_agent' ) == "" ]];then
        echo "Start consul agent"
        consul agent -retry-join "${DISCOVERY_SERVICE_HOST:-consul.server}" -client 0.0.0.0 -bind "$ipaddr" -node "${consul_node}" -data-dir "${CONSUL_AGENT_DIR:-/opt/consul_agent}" -log-level err -config-file "${CONSUL_AGENT_DIR:-/opt/consul_agent}"/"${APP_NAME:-unknown_app_name}"-"${APP_ROLE:-unknown_app_role}".json 2>&1 & disown;
    else
        echo "consul agent already running Status=$(ps aux | grep "consul.*agent" | grep -ve 'grep')"
  fi

}
