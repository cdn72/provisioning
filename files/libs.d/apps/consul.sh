#!/usr/bin/env bash

urldecode() {
    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
}

consul_server_remove_critical(){
  consul_get_server_info
  consul_services_critical=$(curl --header "$consul_toker_header" -qs ${consul_url}/v1/health/state/critical? | jq .)
  for consul_crit in $(echo $consul_services_critical | jq '.[].Node' | sed 's/"//g');
  do
    consul_node_crit_node=$(echo $consul_crit)
    consul_node_crit_id=$($(command -v consul) catalog nodes | grep ${consul_node_crit_node} | awk '{print $2}')
    consul_node_crit_dc=$($(command -v consul) catalog nodes | grep ${consul_node_crit_node} | awk '{print $NF}')
    echo "Exec remove  $consul_node_crit_node with id $consul_node_crit_id";
cat <<OEF> /tmp/${consul_node_crit_dc}_${consul_node_crit_node}_${consul_node_crit_id}.json
{
  "Datacenter": "${consul_node_crit_dc}",
  "Node": "${consul_node_crit_node}",
  "ServiceID": "${consul_node_crit_node}"
}
OEF
  #consul_node_crit_node_urldecoded=$(urldecode ${consul_node_crit_node})
  #echo "Exec curl --location -v --show-error --max-time \"${CURL_MAX_TIME:=5}\" --header \"$consul_toker_header\" --request PUT \"$consul_url/v1/agent/service/deregister/${consul_node_crit_node_urldecoded}\"";
  echo "exec $(command -v consul) force-leave ${consul_node_crit_node}"
  $(command -v consul) force-leave ${consul_node_crit_node}
  #curl --location -v --show-error --max-time "${CURL_MAX_TIME:=5}" --header "$consul_toker_header" --request PUT -d /tmp/${consul_node_crit_dc}_${consul_node_crit_node}_${consul_node_crit_id}.json "$consul_url/v1/agent/service/deregister/"
  #curl --location -v --show-error --max-time "${CURL_MAX_TIME:=5}" --header "$consul_toker_header" --request PUT "$consul_url/v1/agent/service/deregister/${consul_node_crit_node_urldecoded}"
done
###
##https://gist.github.com/cdown/1163649
###
if [ -f "/tmp/${consul_node_crit_dc}_${consul_node_crit_node}_${consul_node_crit_id}.json" ];then 
  rm /tmp/${consul_node_crit_dc}_${consul_node_crit_node}_${consul_node_crit_id}.json
fi
}


consul_get_server_info () {
  if [[ -n "${SERVICE_REGISTER_IP_CUSTOM}"  && "${SERVICE_DISCOVERY_MECHANISM}" == "external" ]];then
      ip_addr=${SERVICE_REGISTER_IP_CUSTOM};
  else 
      consul_net=$(nslookup ${DISCOVERY_SERVICE_HOST:-consul.server} | grep Address | grep -ve ':53\|#53' | awk '{ print $2 }' | head -n 1)
      ipaddr=$(ip route get "${consul_net}" | head -n 1 | awk  '{ print $5 }'  | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" )

  fi

  consul_toker_header="${SERVICE_DISCOVERY_AUTH_PASSWORD}"
  if [[ -n ${SERVICE_DISCOVERY_SERVER_URL} ]];then
    consul_url="${SERVICE_DISCOVERY_SERVER_URL}"
    consul_leader="$(curl --header "$consul_toker_header" -qs ${consul_url}/v1/status/leader | sed  's/:8300//' | sed 's/"//g')"
  else
    consul_url="http://consul.server:8500"
    consul_leader="$(curl --header "$consul_toker_header" -qs ${consul_url}/v1/status/leader | sed  's/:8300//' | sed 's/"//g')"
  fi
  
  
}


consul_exporter_info_get () {
  if [[ -n "${SERVICE_REGISTER_IP_CUSTOM}"  && "${SERVICE_DISCOVERY_MECHANISM}" == "external" ]];then
      ip_addr=${SERVICE_REGISTER_IP_CUSTOM};
  else 
      consul_net=$(nslookup ${DISCOVERY_SERVICE_HOST:-consul.server} | grep Address | grep -ve ':53\|#53' | awk '{ print $2 }' | head -n 1)
      ipaddr=$(ip route get "${consul_net}" | head -n 1 | awk  '{ print $5 }'  | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" )
  fi

  if [[ -f "/rootfs/etc/hostname" ]];then
    v_hostname=$(cat /rootfs/etc/hostname | sed 's|[\._]|-|g' )-$(curl -m 2 -4 -qs https://ifconfig.io | sed 's|[\._]|-|g' || echo '--')
  else
    v_hostname=$(cat /etc/hostname | sed 's|[\._]|-|g' )-$(curl -m 2 -4 -qs https://ifconfig.io | sed 's|[\._]|-|g' || echo '--')
  fi

  app_env="${APP_ENV}"
  name="${APP_FUNCTION}"
  tags="prometheus,${APP_FUNCTION}"
  node_name="${v_hostname}_${name}_$(echo $ip_addr | sed 's|\.|-|g')"
  app_port="${APP_PORT}"
  consul_toker_header="${SERVICE_DISCOVERY_AUTH_PASSWORD}"
  if [[ -n ${SERVICE_DISCOVERY_SERVER_URL} ]];then
    consul_url="${SERVICE_DISCOVERY_SERVER_URL}"
  else
    consul_url="http://consul.server:8500"
  fi
  tags=$(echo ${tags} | sed 's/,/\",\"/g' | sed 's/\(^\|$\)/"/g')
  tags="[\"$app_env\",$tags]"
  check_data="{\"DeregisterCriticalServiceAfter\": \"111190m\",\"tcp\": \"${ip_addr}:${app_port}\", \"Interval\": \"10s\",\"Timeout\": \"5s\"}"
  payload="{\"ID\":\"$node_name\",\"Name\":\"$name\",\"Address\":\"$ip_addr\",\"Port\":$app_port, \"Check\":$check_data ,\"Tags\":$tags}"
  consul_leader="$(curl -qs ${consul_url}/v1/status/leader | sed  's/:8300//' | sed 's/"//g')"
  consul_services_critical=$(curl -qs ${consul_url}/v1/health/state/critical? | jq .)
}


consul_register_exporter () {
  consul_exporter_info_get
  echo "Try Load ${payload} to $consul_url/v1/agent/service/register"
  curl --location --silent --show-error --max-time "${CURL_MAX_TIME:=5}" --header "$consul_toker_header" --request PUT --data "$payload" "$consul_url/v1/agent/service/register"
  echo "${FUNCTNAME[0]} finished"
}

consul_deregister_exporter () {
  consul_exporter_info_get
  curl --location --silent --show-error --max-time "${CURL_MAX_TIME:=5}" --header "$consul_toker_header" --request PUT "$consul_url/v1/agent/service/deregister/$node_name"
  echo "${FUNCTNAME[0]} finished"
}


