DROP TABLE IF EXISTS session;
CREATE TABLE session (
	  sess_id varchar(128) NOT NULL,
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  changed TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  ip varchar(40) NOT NULL,
	  vars TEXT NOT NULL,
	  PRIMARY KEY (sess_id)
);
DROP INDEX if  exists idx_session_changed;
CREATE INDEX if not exists idx_session_changed ON session (changed);

DROP TABLE IF EXISTS system;
CREATE TABLE system (
	  name varchar(64) NOT NULL,
	  value TEXT,
	  PRIMARY KEY (name)
);

DROP TABLE IF EXISTS users;
CREATE TABLE users (
	  user_id SERIAL NOT NULL,
	  username varchar(128)  NOT NULL UNIQUE,
	  mail_host varchar(128) NOT NULL,
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  last_login TIMESTAMP DEFAULT NULL,
	  language varchar(5) DEFAULT NULL,
	  preferences TEXT,
	  PRIMARY KEY (user_id)
);


DROP TABLE IF EXISTS cache;
CREATE TABLE cache (
	  user_id BIGINT NOT NULL,
	  cache_key varchar(128)  NOT NULL,
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  expires TIMESTAMP DEFAULT NULL,
	  data TEXT NOT NULL,
	  CONSTRAINT user_id_fk_cache FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
);
DROP INDEX if exists idx_cache_expires;
DROP INDEX if exists idx_cache_user_id_cache_key;
CREATE INDEX if not exists idx_cache_expires ON cache (expires);
CREATE INDEX if not exists idx_cache_user_id_cache_key ON cache (user_id,cache_key);




DROP TABLE IF EXISTS cache_index;
CREATE TABLE cache_index (
	  user_id BIGINT NOT NULL,
	  mailbox varchar(255)  NOT NULL,
	  expires TIMESTAMP DEFAULT NULL,
	  valid SMALLINT NOT NULL DEFAULT '0',
	  data TEXT NOT NULL,
	  PRIMARY KEY (user_id,mailbox),
	  CONSTRAINT user_id_fk_cache_index FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
);
DROP INDEX if exists idx_cache_index_expires;
CREATE INDEX if not exists idx_cache_index_expires ON cache_index (user_id,mailbox);


DROP TABLE IF EXISTS cache_messages;
CREATE TABLE cache_messages (
	  user_id BIGINT NOT NULL,
	  mailbox varchar(255)  NOT NULL,
	  uid BIGINT NOT NULL DEFAULT '0',
	  expires TIMESTAMP DEFAULT NULL,
	  data TEXT NOT NULL,
	  flags INT NOT NULL DEFAULT '0',
	  PRIMARY KEY (user_id,mailbox,uid),
	  CONSTRAINT user_id_fk_cache_messages FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
);
DROP INDEX if exists idx_cache_messages_expires;
CREATE INDEX if not exists idx_cache_messages_expires ON cache_messages (expires);


DROP TABLE IF EXISTS cache_shared;
CREATE TABLE cache_shared (
	  cache_key varchar(255)  NOT NULL,
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  expires TIMESTAMP DEFAULT NULL,
	  data TEXT NOT NULL
);

DROP INDEX if exists idx_cache_shared_expires;
CREATE INDEX if not exists idx_cache_shared_expires ON cache_shared (expires);
DROP INDEX if exists idx_cache_shared_expires;
CREATE INDEX if not exists idx_cache_shared_cache_key ON cache_shared (cache_key);


DROP TABLE IF EXISTS cache_thread;
CREATE TABLE cache_thread (
	  user_id BIGINT NOT NULL,
	  mailbox varchar(255)  NOT NULL,
	  expires TIMESTAMP DEFAULT NULL,
	  data TEXT NOT NULL,
	  PRIMARY KEY (user_id,mailbox),
	  CONSTRAINT user_id_fk_cache_thread FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
);
DROP INDEX if exists idx_cache_thread_expires;
CREATE INDEX if not exists idx_cache_thread_expires ON cache_thread (expires);


DROP TABLE IF EXISTS contactgroupmembers;
CREATE TABLE contactgroupmembers (
	  contactgroup_id BIGINT NOT NULL,
	  contact_id BIGINT NOT NULL,
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  PRIMARY KEY (contactgroup_id,contact_id)
);

DROP INDEX if exists idx_contactgroupmembers_contact_id;
CREATE INDEX if not exists idx_contactgroupmembers_contact_id ON contactgroupmembers (contact_id);
#CONSTRAINT contactgroup_id_fk_contactgroups FOREIGN KEY (contactgroup_id) REFERENCES contactgroups (contactgroup_id) ON DELETE CASCADE ON UPDATE CASCADE,
#CONSTRAINT contact_id_fk_contacts FOREIGN KEY (contact_id) REFERENCES contacts (contact_id) ON DELETE CASCADE ON UPDATE CASCADE
#



DROP TABLE IF EXISTS contactgroups;
CREATE TABLE contactgroups (
	  contactgroup_id SERIAL NOT NULL,
	  user_id BIGINT NOT NULL,
	  changed TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  del SMALLINT NOT NULL DEFAULT '0',
	  name varchar(128) NOT NULL DEFAULT '',
	  PRIMARY KEY (contactgroup_id),
	  CONSTRAINT user_id_fk_contactgroups FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP INDEX if exists idx_contactgroups_user_id_del;
CREATE INDEX if not exists idx_contactgroups_user_id_del ON contactgroups (user_id,del);


DROP TABLE IF EXISTS contacts;
CREATE TABLE contacts (
	  contact_id SERIAL NOT NULL,
	  changed TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  del SMALLINT NOT NULL DEFAULT '0',
	  name varchar(128) NOT NULL DEFAULT '',
	  email text NOT NULL,
	  firstname varchar(128) NOT NULL DEFAULT '',
	  surname varchar(128) NOT NULL DEFAULT '',
	  vcard TEXT,
	  words text,
	  user_id BIGINT NOT NULL,
	  PRIMARY KEY (contact_id),
	  CONSTRAINT user_id_fk_contacts FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
);
DROP INDEX if exists idx_contacts_user_id_del;
CREATE INDEX if not exists idx_contacts_user_id_del ON contacts (user_id,del);


DROP TABLE IF EXISTS dictionary;
CREATE TABLE dictionary (
	  user_id BIGINT DEFAULT NULL,
	  language varchar(5) NOT NULL,
	  data TEXT NOT NULL,
	  CONSTRAINT uniqueness UNIQUE (user_id,language),
	  CONSTRAINT user_id_fk_dictionary FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
);


DROP TABLE IF EXISTS identities;
CREATE TABLE identities (
	  identity_id SERIAL NOT NULL,
	  user_id BIGINT NOT NULL,
	  changed TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  del SMALLINT NOT NULL DEFAULT '0',
	  standard SMALLINT NOT NULL DEFAULT '0',
	  name varchar(128) NOT NULL,
	  organization varchar(128) NOT NULL DEFAULT '',
	  email varchar(128) NOT NULL,
	  reply_to varchar(128) NOT NULL DEFAULT '',
	  bcc varchar(128) NOT NULL DEFAULT '',
	  signature TEXT,
	  html_signature SMALLINT NOT NULL DEFAULT '0',
	  PRIMARY KEY (identity_id),
	  CONSTRAINT user_id_fk_identities FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
);
DROP INDEX if exists idx_identities_user_id_del;
DROP INDEX if exists idx_identities_email_del;
CREATE INDEX if not exists idx_identities_user_id_del ON identities (user_id,del);
CREATE INDEX if not exists idx_identities_email_del ON identities (email,del);


DROP TABLE IF EXISTS searches;
CREATE TABLE searches (
	  search_id SERIAL NOT NULL,
	  user_id BIGINT NOT NULL,
	  type INT NOT NULL DEFAULT '0',
	  name varchar(128) NOT NULL,
	  data text,
	  PRIMARY KEY (search_id),
	  CONSTRAINT searches_uniqueness UNIQUE (user_id,type,name),
	  CONSTRAINT user_id_fk_searches FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
);













