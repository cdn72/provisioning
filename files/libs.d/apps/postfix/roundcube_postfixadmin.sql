CREATE DATABASE mail /*!40100 DEFAULT CHARACTER SET latin1 */;
GRANT ALL ON mail.* TO mail@localhost IDENTIFIED BY 'mail';
USE mail;

DROP TABLE IF EXISTS admin;

CREATE TABLE admin (
	  username varchar(255) NOT NULL,
	  password varchar(255) NOT NULL,
	  superadmin tinyint(1) NOT NULL DEFAULT '0',
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active tinyint(1) NOT NULL DEFAULT '1',
	  PRIMARY KEY (username)
)

--
-- Table structure for table alias
--

DROP TABLE IF EXISTS alias;


CREATE TABLE alias (
	  address varchar(255) NOT NULL,
	  goto text NOT NULL,
	  domain varchar(255) NOT NULL,
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active tinyint(1) NOT NULL DEFAULT '1',
	  PRIMARY KEY (address),
	  KEY domain (domain)
)


--
-- Table structure for table alias_domain
--

DROP TABLE IF EXISTS alias_domain;


CREATE TABLE alias_domain (
	  alias_domain varchar(255) NOT NULL,
	  target_domain varchar(255) NOT NULL,
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active tinyint(1) NOT NULL DEFAULT '1',
	  PRIMARY KEY (alias_domain),
	  KEY active (active),
	  KEY target_domain (target_domain)
)


--
-- Table structure for table config
--

DROP TABLE IF EXISTS config;


CREATE TABLE config (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  name varchar(20) NOT NULL DEFAULT '',
	  value varchar(20) NOT NULL DEFAULT '',
	  PRIMARY KEY (id),
	  UNIQUE KEY name (name)
)


--
-- Table structure for table domain
--

DROP TABLE IF EXISTS domain;


CREATE TABLE domain (
	  domain varchar(255) NOT NULL,
	  description varchar(255) CHARACTER SET utf8 NOT NULL,
	  aliases int(10) NOT NULL DEFAULT '0',
	  mailboxes int(10) NOT NULL DEFAULT '0',
	  maxquota bigint(20) NOT NULL DEFAULT '0',
	  quota bigint(20) NOT NULL DEFAULT '0',
	  transport varchar(255) NOT NULL,
	  backupmx tinyint(1) NOT NULL DEFAULT '0',
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active tinyint(1) NOT NULL DEFAULT '1',
	  PRIMARY KEY (domain)
)


--
-- Table structure for table domain_admins
--

DROP TABLE IF EXISTS domain_admins;


CREATE TABLE domain_admins (
	  username varchar(255) NOT NULL,
	  domain varchar(255) NOT NULL,
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active tinyint(1) NOT NULL DEFAULT '1',
	  KEY username (username)
)


LOCK TABLES domain_admins WRITE;

INSERT INTO domain_admins VALUES ('admin@example.com','ALL','2016-03-06 16:10:41',1);

UNLOCK TABLES;

--
-- Table structure for table fetchmail
--

DROP TABLE IF EXISTS fetchmail;


CREATE TABLE fetchmail (
	  id int(11) unsigned NOT NULL AUTO_INCREMENT,
	  domain varchar(255) DEFAULT '',
	  mailbox varchar(255) NOT NULL,
	  src_server varchar(255) NOT NULL,
	  src_auth enum('password','kerberos_v5','kerberos','kerberos_v4','gssapi','cram-md5','otp','ntlm','msn','ssh','any') DEFAULT NULL,
	  src_user varchar(255) NOT NULL,
	  src_password varchar(255) NOT NULL,
	  src_folder varchar(255) NOT NULL,
	  poll_time int(11) unsigned NOT NULL DEFAULT '10',
	  fetchall tinyint(1) unsigned NOT NULL DEFAULT '0',
	  keep tinyint(1) unsigned NOT NULL DEFAULT '0',
	  protocol enum('POP3','IMAP','POP2','ETRN','AUTO') DEFAULT NULL,
	  usessl tinyint(1) unsigned NOT NULL DEFAULT '0',
	  sslcertck tinyint(1) NOT NULL DEFAULT '0',
	  sslcertpath varchar(255) CHARACTER SET utf8 DEFAULT '',
	  sslfingerprint varchar(255) DEFAULT '',
	  extra_options text,
	  returned_text text,
	  mda varchar(255) NOT NULL,
	  date timestamp NOT NULL DEFAULT '2023-04-21 00:00:00',
	  created timestamp NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  active tinyint(1) NOT NULL DEFAULT '0',
	  PRIMARY KEY (id)
)


--
-- Table structure for table log
--

DROP TABLE IF EXISTS log;


CREATE TABLE log (
	  timestamp datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  username varchar(255) NOT NULL,
	  domain varchar(255) NOT NULL,
	  action varchar(255) NOT NULL,
	  data text NOT NULL,
	  KEY timestamp (timestamp),
	  KEY domain_timestamp (domain,timestamp)
)


--
-- Table structure for table mailbox
--

DROP TABLE IF EXISTS mailbox;


CREATE TABLE mailbox (
	  username varchar(255) NOT NULL,
	  password varchar(255) NOT NULL,
	  name varchar(255) CHARACTER SET utf8 NOT NULL,
	  maildir varchar(255) NOT NULL,
	  quota bigint(20) NOT NULL DEFAULT '0',
	  local_part varchar(255) NOT NULL,
	  domain varchar(255) NOT NULL,
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active tinyint(1) NOT NULL DEFAULT '1',
	  PRIMARY KEY (username),
	  KEY domain (domain)
)


--
-- Table structure for table quota
--

DROP TABLE IF EXISTS quota;


CREATE TABLE quota (
	  username varchar(255) NOT NULL,
	  path varchar(100) NOT NULL,
	  current bigint(20) DEFAULT NULL,
	  PRIMARY KEY (username,path)
)


--
-- Table structure for table quota2
--

DROP TABLE IF EXISTS quota2;


CREATE TABLE quota2 (
	  username varchar(100) NOT NULL,
	  bytes bigint(20) NOT NULL DEFAULT '0',
	  messages int(11) NOT NULL DEFAULT '0',
	  PRIMARY KEY (username)
)


--
-- Table structure for table vacation
--

DROP TABLE IF EXISTS vacation;


CREATE TABLE vacation (
	  email varchar(255) NOT NULL,
	  subject varchar(255) CHARACTER SET utf8 NOT NULL,
	  body text CHARACTER SET utf8 NOT NULL,
	  activefrom timestamp NOT NULL DEFAULT '2023-04-21 00:00:00',
	  activeuntil timestamp NOT NULL DEFAULT '2023-04-21 00:00:00',
	  cache text NOT NULL,
	  domain varchar(255) NOT NULL,
	  interval_time int(11) NOT NULL DEFAULT '0',
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  active tinyint(1) NOT NULL DEFAULT '1',
	  PRIMARY KEY (email),
	  KEY email (email)
)


--
-- Table structure for table vacation_notification
--

DROP TABLE IF EXISTS vacation_notification;


CREATE TABLE vacation_notification (
	  on_vacation varchar(255) CHARACTER SET latin1 NOT NULL,
	  notified varchar(255) CHARACTER SET latin1 NOT NULL,
	  notified_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (on_vacation,notified),
	  CONSTRAINT vacation_notification_pkey FOREIGN KEY (on_vacation) REFERENCES vacation (email) ON DELETE CASCADE
)











CREATE DATABASE roundcube /*!40100 DEFAULT CHARACTER SET latin1 */;
GRANT ALL ON roundcube.* TO roundcube@localhost IDENTIFIED BY 'roundcube';
USE roundcube;

DROP TABLE IF EXISTS cache;


CREATE TABLE cache (
	  user_id int(10) unsigned NOT NULL,
	  cache_key varchar(128) CHARACTER SET ascii NOT NULL,
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  expires datetime DEFAULT NULL,
	  data longtext NOT NULL,
	  KEY expires_index (expires),
	  KEY user_cache_index (user_id,cache_key),
	  CONSTRAINT user_id_fk_cache FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
)


--
-- Table structure for table cache_index
--

DROP TABLE IF EXISTS cache_index;


CREATE TABLE cache_index (
	  user_id int(10) unsigned NOT NULL,
	  mailbox varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
	  expires datetime DEFAULT NULL,
	  valid tinyint(1) NOT NULL DEFAULT '0',
	  data longtext NOT NULL,
	  PRIMARY KEY (user_id,mailbox),
	  KEY expires_index (expires),
	  CONSTRAINT user_id_fk_cache_index FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
)


--
-- Table structure for table cache_messages
--

DROP TABLE IF EXISTS cache_messages;


CREATE TABLE cache_messages (
	  user_id int(10) unsigned NOT NULL,
	  mailbox varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
	  uid int(11) unsigned NOT NULL DEFAULT '0',
	  expires datetime DEFAULT NULL,
	  data longtext NOT NULL,
	  flags int(11) NOT NULL DEFAULT '0',
	  PRIMARY KEY (user_id,mailbox,uid),
	  KEY expires_index (expires),
	  CONSTRAINT user_id_fk_cache_messages FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
)


--
-- Table structure for table cache_shared
--

DROP TABLE IF EXISTS cache_shared;


CREATE TABLE cache_shared (
	  cache_key varchar(255) CHARACTER SET ascii NOT NULL,
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  expires datetime DEFAULT NULL,
	  data longtext NOT NULL,
	  KEY expires_index (expires),
	  KEY cache_key_index (cache_key)
)


--
-- Table structure for table cache_thread
--

DROP TABLE IF EXISTS cache_thread;


CREATE TABLE cache_thread (
	  user_id int(10) unsigned NOT NULL,
	  mailbox varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
	  expires datetime DEFAULT NULL,
	  data longtext NOT NULL,
	  PRIMARY KEY (user_id,mailbox),
	  KEY expires_index (expires),
	  CONSTRAINT user_id_fk_cache_thread FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
)


--
-- Table structure for table contactgroupmembers
--

DROP TABLE IF EXISTS contactgroupmembers;


CREATE TABLE contactgroupmembers (
	  contactgroup_id int(10) unsigned NOT NULL,
	  contact_id int(10) unsigned NOT NULL,
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  PRIMARY KEY (contactgroup_id,contact_id),
	  KEY contactgroupmembers_contact_index (contact_id),
	  CONSTRAINT contactgroup_id_fk_contactgroups FOREIGN KEY (contactgroup_id) REFERENCES contactgroups (contactgroup_id) ON DELETE CASCADE ON UPDATE CASCADE,
	  CONSTRAINT contact_id_fk_contacts FOREIGN KEY (contact_id) REFERENCES contacts (contact_id) ON DELETE CASCADE ON UPDATE CASCADE
)


--
-- Table structure for table contactgroups
--

DROP TABLE IF EXISTS contactgroups;


CREATE TABLE contactgroups (
	  contactgroup_id int(10) unsigned NOT NULL AUTO_INCREMENT,
	  user_id int(10) unsigned NOT NULL,
	  changed datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  del tinyint(1) NOT NULL DEFAULT '0',
	  name varchar(128) NOT NULL DEFAULT '',
	  PRIMARY KEY (contactgroup_id),
	  KEY contactgroups_user_index (user_id,del),
	  CONSTRAINT user_id_fk_contactgroups FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
)


--
-- Table structure for table contacts
--

DROP TABLE IF EXISTS contacts;


CREATE TABLE contacts (
	  contact_id int(10) unsigned NOT NULL AUTO_INCREMENT,
	  changed datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  del tinyint(1) NOT NULL DEFAULT '0',
	  name varchar(128) NOT NULL DEFAULT '',
	  email text NOT NULL,
	  firstname varchar(128) NOT NULL DEFAULT '',
	  surname varchar(128) NOT NULL DEFAULT '',
	  vcard longtext,
	  words text,
	  user_id int(10) unsigned NOT NULL,
	  PRIMARY KEY (contact_id),
	  KEY user_contacts_index (user_id,del),
	  CONSTRAINT user_id_fk_contacts FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
)


--
-- Table structure for table dictionary
--

DROP TABLE IF EXISTS dictionary;


CREATE TABLE dictionary (
	  user_id int(10) unsigned DEFAULT NULL,
	  language varchar(5) NOT NULL,
	  data longtext NOT NULL,
	  UNIQUE KEY uniqueness (user_id,language),
	  CONSTRAINT user_id_fk_dictionary FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
)


--
-- Table structure for table identities
--

DROP TABLE IF EXISTS identities;


CREATE TABLE identities (
	  identity_id int(10) unsigned NOT NULL AUTO_INCREMENT,
	  user_id int(10) unsigned NOT NULL,
	  changed datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  del tinyint(1) NOT NULL DEFAULT '0',
	  standard tinyint(1) NOT NULL DEFAULT '0',
	  name varchar(128) NOT NULL,
	  organization varchar(128) NOT NULL DEFAULT '',
	  email varchar(128) NOT NULL,
	  reply-to varchar(128) NOT NULL DEFAULT '',
	  bcc varchar(128) NOT NULL DEFAULT '',
	  signature longtext,
	  html_signature tinyint(1) NOT NULL DEFAULT '0',
	  PRIMARY KEY (identity_id),
	  KEY user_identities_index (user_id,del),
	  KEY email_identities_index (email,del),
	  CONSTRAINT user_id_fk_identities FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
)


--
-- Table structure for table searches
--

DROP TABLE IF EXISTS searches;


CREATE TABLE searches (
	  search_id int(10) unsigned NOT NULL AUTO_INCREMENT,
	  user_id int(10) unsigned NOT NULL,
	  type int(3) NOT NULL DEFAULT '0',
	  name varchar(128) NOT NULL,
	  data text,
	  PRIMARY KEY (search_id),
	  UNIQUE KEY uniqueness (user_id,type,name),
	  CONSTRAINT user_id_fk_searches FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE
)


--
-- Table structure for table session
--

DROP TABLE IF EXISTS session;


CREATE TABLE session (
	  sess_id varchar(128) NOT NULL,
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  changed datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  ip varchar(40) NOT NULL,
	  vars mediumtext NOT NULL,
	  PRIMARY KEY (sess_id),
	  KEY changed_index (changed)
)


--
-- Table structure for table system
--

DROP TABLE IF EXISTS system;


CREATE TABLE system (
	  name varchar(64) NOT NULL,
	  value mediumtext,
	  PRIMARY KEY (name)
)


--
-- Table structure for table users
--

DROP TABLE IF EXISTS users;


CREATE TABLE users (
	  user_id int(10) unsigned NOT NULL AUTO_INCREMENT,
	  username varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
	  mail_host varchar(128) NOT NULL,
	  created datetime NOT NULL DEFAULT '2023-04-21 00:00:00',
	  last_login datetime DEFAULT NULL,
	  language varchar(5) DEFAULT NULL,
	  preferences longtext,
	  PRIMARY KEY (user_id),
	  UNIQUE KEY username (username,mail_host)
)











