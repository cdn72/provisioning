#!/usr/bin/env bash

######info dovecot
# Commonly used available substitutions (see http://wiki2.dovecot.org/Variables
# for full list):
#   %u = entire user@domain
#   %n = user part of user@domain
#   %d = domain part of user@domain


#telnet USER user1@linux2be.com"
export DEBIAN_FRONTEND=noninteractive
export DOMAIN="linux2be.com"
export MY_SERVER_IP=$(curl -q ifconfig.io -4)

export USER_1="user1"
export PASSWORD_1="password1"
export EMAIL_1="${USER_1}@${DOMAIN}"

export USER_2="user2"
export PASSWORD_2="password2"
export EMAIL_2="${USER_2}@${DOMAIN}"

export DATABASE="servermail"
export DB_USER="servermail"
export DB_USER_PASS="mypass2323dsdsdsdsds&dsdsworMMMd"

bootstrapdb_1(){

cat <<EOF > /tmp/mysql_1.sql
  DROP DATABASE IF  EXISTS $DATABASE;
  DROP USER IF EXISTS '$DB_USER'@'%';
  CREATE DATABASE IF NOT EXISTS $DATABASE;
  CREATE USER '$DB_USER'@'%' IDENTIFIED BY '$DB_USER_PASS';
  GRANT SELECT ON $DATABASE.* TO '$DB_USER'@'%';
  FLUSH PRIVILEGES;

  USE $DATABASE;

  CREATE TABLE IF NOT EXISTS domains (
        id int unsigned NOT NULL AUTO_INCREMENT,
        domain varchar(255) NOT NULL,
        PRIMARY KEY (id),
        UNIQUE KEY (domain)
  );

  CREATE TABLE accounts (
        id int unsigned NOT NULL AUTO_INCREMENT,
        username varchar(64) NOT NULL,
        domain varchar(255) NOT NULL,
        password varchar(255) NOT NULL,
        quota int unsigned DEFAULT '0',
        enabled boolean DEFAULT '0',
        sendonly boolean DEFAULT '0',
        PRIMARY KEY (id),
        UNIQUE KEY (username, domain),
        FOREIGN KEY (domain) REFERENCES domains (domain)
  );

    CREATE TABLE aliases (
        id int unsigned NOT NULL AUTO_INCREMENT,
        source_username varchar(64) NOT NULL,
        source_domain varchar(255) NOT NULL,
        destination_username varchar(64) NOT NULL,
        destination_domain varchar(255) NOT NULL,
        enabled boolean DEFAULT '0',
        PRIMARY KEY (id),
        UNIQUE KEY (source_username, source_domain, destination_username, destination_domain),
        FOREIGN KEY (source_domain) REFERENCES domains (domain)
    );

  CREATE TABLE tlspolicies (
        id int unsigned NOT NULL AUTO_INCREMENT,
        domain varchar(255) NOT NULL,
        policy enum('none', 'may', 'encrypt', 'dane', 'dane-only', 'fingerprint', 'verify', 'secure') NOT NULL,
        params varchar(255),
        PRIMARY KEY (id),
        UNIQUE KEY (domain)
);
EOF

mysql -uroot < /tmp/mysql_1.sql

}

bootstrapdb_2() {
  PASSWORD_INS_1=$(echo -e "${PASSWORD_1}\n${PASSWORD_1}"      | doveadm pw -s SHA512-CRYPT)
  PASSWORD_INS_2=$(echo -e "${PASSWORD_2}\n${PASSWORD_2}"  | doveadm pw -s SHA512-CRYPT)

  cat <<EOF>/tmp/bootstrapdb_2.sql
  insert into domains (domain) values ('$DOMAIN');
  insert into accounts (username, domain, password, quota, enabled, sendonly) values ('${USER_1}', '$DOMAIN', '$PASSWORD_INS_1', 2048, true, false);
  insert into accounts (username, domain, password, quota, enabled, sendonly) values ('${USER_2}', '$DOMAIN', '$PASSWORD_INS_2', 2048, true, false);
  insert into aliases (source_username, source_domain, destination_username, destination_domain, enabled) values ('root', '$DOMAIN', '${USER_1}', '$DOMAIN', true);
EOF

mysql -uroot -D $DATABASE  < /tmp/bootstrapdb_2.sql

}

installpkg(){
    dpkg-query --status $1 >/dev/null || apt install -y $1
}

run_delete() {
  systemctl stop postfix || echo skip
  systemctl stop dovecot  || echo skip
  old_dir=$(pwd)
  cd /tmp && apt-get --purge autoremove -y postfix*
  cd /tmp && apt-get --purge autoremove -y postfix-*
  cd /tmp && apt-get --purge autoremove -y dovecot-*
  r_dirs="/etc/postfix /etc/dovecot /var/lib/dovecot"
  for r_dir in $r_dirs
  do
    if [[ -d ${r_dir} ]];then
      rm -rf ${r_dir}
    else
      echo "skip remove ${r_dir}"
    fi
  done
  
}
run_install() {
  apt update
  #apt install -y mysql-server mysql-client
  #debconf-set-selections <<< "postfix postfix/main_mailer_type string 'No configuration'" # optional
  debconf-set-selections <<< "postfix postfix/mailname string $DOMAIN"
  debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
  apt install -y postfix postfix-mysql dovecot-core dovecot-imapd dovecot-pop3d dovecot-lmtpd dovecot-mysql postfix-pgsql
  systemctl stop postfix

}

config_postfix_1 () {
  if [[ -d "/etc/postfix" ]];then
    cd /etc/postfix;
    rm -rf sasl
    rm -rf master.cf main.cf.proto master.cf.proto
  fi

cat <<OEF> /etc/postfix/main.cf
##
## Network settings
##

mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 92.255.182.83/32
inet_interfaces = 127.0.0.1, ::1, $MY_SERVER_IP
myhostname = mail.${DOMAIN}

##
## Mail queue settings
##
maximal_queue_lifetime = 1h
bounce_queue_lifetime = 1h
maximal_backoff_time = 15m
minimal_backoff_time = 5m
queue_run_delay = 5m

##
## TLS settings
##

tls_preempt_cipherlist = yes
tls_ssl_options = NO_COMPRESSION
tls_high_cipherlist = EDH+CAMELLIA:EDH+aRSA:EECDH+aRSA+AESGCM:EECDH+aRSA+SHA256:EECDH:+CAMELLIA128:+AES128:+SSLv3:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!DSS:!RC4:!SEED:!IDEA:!ECDSA:kEDH:CAMELLIA128-SHA:AES128-SHA

### Outbound SMTP connections (Postfix as sender)

smtp_tls_security_level = dane
smtp_dns_support_level = dnssec
#smtp_tls_policy_maps = mysql:/etc/postfix/sql/tls-policy.cf
smtp_tls_session_cache_database = btree:\${data_directory}/smtp_scache
smtp_tls_protocols = !SSLv2, !SSLv3
smtp_tls_ciphers = high
smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt


### Inbound SMTP connections

smtpd_tls_security_level = may
smtpd_tls_protocols = !SSLv2, !SSLv3
smtpd_tls_ciphers = high
smtpd_tls_session_cache_database = btree:\${data_directory}/smtpd_scache

smtpd_tls_cert_file=/etc/letsencrypt/live/mail.${DOMAIN}/fullchain.pem
smtpd_tls_key_file=/etc/letsencrypt/live/mail.${DOMAIN}/privkey.pem


##
## Local mail delivery to Dovecot via LMTP
##

virtual_transport = lmtp:unix:private/dovecot-lmtp
virtual_transport = dovecot

##
## Spam filter and DKIM signatures via Rspamd
##

#smtpd_milters = inet:localhost:11332
#non_smtpd_milters = inet:localhost:11332
#milter_protocol = 6
#milter_mail_macros =  i {mail_addr} {client_addr} {client_name} {auth_authen}
#milter_default_action = accept

##
## Server Restrictions for clients, cecipients and relaying
## (concerning S2S-connections. Mailclient-connections are configured in submission-section in master.cf)
##

### Conditions in which Postfix works as a relay. (for mail user clients)
smtpd_relay_restrictions =      reject_non_fqdn_recipient
                                reject_unknown_recipient_domain
                                permit_mynetworks
                                reject_unauth_destination


### Conditions in which Postfix accepts e-mails as recipient (additional to relay conditions)
### check_recipient_access checks if an account is "sendonly"
smtpd_recipient_restrictions = check_recipient_access mysql:/etc/postfix/sql/recipient-access.cf


### Restrictions for all sending foreign servers ("SMTP clients")
smtpd_client_restrictions =     permit_mynetworks
                                check_client_access hash:/etc/postfix/without_ptr
                                reject_unknown_client_hostname


### Foreign mail servers must present a valid "HELO"
smtpd_helo_required = yes
smtpd_helo_restrictions =   permit_mynetworks
                            reject_invalid_helo_hostname
                            reject_non_fqdn_helo_hostname
                            reject_unknown_helo_hostname

# Block clients, which start sending too early
smtpd_data_restrictions = reject_unauth_pipelining


##
## Restrictions for MUAs (Mail user agents)
##

mua_relay_restrictions = reject_non_fqdn_recipient,reject_unknown_recipient_domain,permit_mynetworks,permit_sasl_authenticated,reject
mua_sender_restrictions = permit_mynetworks,reject_non_fqdn_sender,reject_sender_login_mismatch,permit_sasl_authenticated,reject
mua_client_restrictions = permit_mynetworks,permit_sasl_authenticated,reject


##
## Postscreen Filter
##

### Postscreen Whitelist / Blocklist
postscreen_access_list =        permit_mynetworks
                                cidr:/etc/postfix/postscreen_access
postscreen_blacklist_action = drop


# Drop connections if other server is sending too quickly
postscreen_greet_action = drop




##
## MySQL queries
##

virtual_alias_maps = mysql:/etc/postfix/sql/aliases.cf
virtual_mailbox_maps = mysql:/etc/postfix/sql/accounts.cf
virtual_mailbox_domains = mysql:/etc/postfix/sql/domains.cf
local_recipient_maps = \$virtual_mailbox_maps


##
## Miscellaneous
##

### Maximum mailbox size (0=unlimited - is already limited by Dovecot quota)
mailbox_size_limit = 0


message_size_limit = 52428800
biff = no
append_dot_mydomain = no
recipient_delimiter = +
strict_mailbox_ownership = no
meta_directory = /usr/share/postfix
compatibility_level=2
mydestination = localhost
virtual_uid_maps = static:150
virtual_gid_maps = static:8

OEF

cat <<'OEF'> /etc/postfix/master.cf
# ==========================================================================
# service type  private unpriv  chroot  wakeup  maxproc command + args
#               (yes)   (yes)   (no)    (never) (100)
# ==========================================================================
smtp      inet  n       -       y       -       1       postscreen
    -o smtpd_sasl_auth_enable=no
smtpd     pass  -       -       y       -       -       smtpd
dnsblog   unix  -       -       y       -       0       dnsblog
tlsproxy  unix  -       -       y       -       0       tlsproxy
submission inet n       -       y       -       -       smtpd
    -o syslog_name=postfix/submission
    -o smtpd_tls_security_level=encrypt
    -o smtpd_sasl_auth_enable=yes
    -o smtpd_sasl_type=dovecot
    -o smtpd_sasl_path=private/auth
    -o smtpd_sasl_security_options=noanonymous
    -o smtpd_client_restrictions=$mua_client_restrictions
    -o smtpd_sender_restrictions=$mua_sender_restrictions
    -o smtpd_relay_restrictions=$mua_relay_restrictions
    -o milter_macro_daemon_name=ORIGINATING
    -o smtpd_sender_login_maps=mysql:/etc/postfix/sql/sender-login-maps.cf
    -o smtpd_helo_required=no
    -o smtpd_helo_restrictions=
    -o cleanup_service_name=submission-header-cleanup
pickup    unix  n       -       y       60      1       pickup
cleanup   unix  n       -       y       -       0       cleanup
qmgr      unix  n       -       n       300     1       qmgr
tlsmgr    unix  -       -       y       1000?   1       tlsmgr
rewrite   unix  -       -       y       -       -       trivial-rewrite
bounce    unix  -       -       y       -       0       bounce
defer     unix  -       -       y       -       0       bounce
trace     unix  -       -       y       -       0       bounce
verify    unix  -       -       y       -       1       verify
flush     unix  n       -       y       1000?   0       flush
proxymap  unix  -       -       n       -       -       proxymap
proxywrite unix -       -       n       -       1       proxymap
smtp      unix  -       -       y       -       -       smtp
relay     unix  -       -       y       -       -       smtp
showq     unix  n       -       y       -       -       showq
error     unix  -       -       y       -       -       error
retry     unix  -       -       y       -       -       error
discard   unix  -       -       y       -       -       discard
local     unix  -       n       n       -       -       local
virtual   unix  -       n       n       -       -       virtual
lmtp      unix  -       -       y       -       -       lmtp
anvil     unix  -       -       y       -       1       anvil
scache    unix  -       -       y       -       1       scache
submission-header-cleanup unix n - n    -       0       cleanup
    -o header_checks=regexp:/etc/postfix/submission_header_cleanup
OEF


cat <<'OEF'>/etc/postfix/submission_header_cleanup
### Removes headers of MUAs for privacy reasons

/^Received:/            IGNORE
/^X-Originating-IP:/    IGNORE
/^X-Mailer:/            IGNORE
/^User-Agent:/          IGNORE
OEF

cat <<'OEF'>/etc/postfix/dynamicmaps.cf
#pgsql postfix-pgsql.so  dict_pgsql_open 
mysql   /usr/lib/postfix/postfix-mysql.so.1.0.1 dict_mysql_open
OEF

postfix-mysql

}

config_postfix_2 () {

if [[ ! -d "/etc/postfix/sql" ]];then mkdir -p /etc/postfix/sql;fi

  cat <<OEF> /etc/postfix/sql/recipient-access.cf
user = $DB_USER
password = $DB_USER_PASS
hosts = 127.0.0.1
dbname = $DATABASE
query = select if(sendonly = true, 'REJECT', 'OK') AS access from accounts where username = '%u' and domain = '%d' and enabled = true LIMIT 1;
OEF

  cat <<OEF> /etc/postfix/sql/accounts.cf
user = $DB_USER
password = $DB_USER_PASS
hosts = 127.0.0.1
dbname = $DATABASE
query = select 1 as found from accounts where username = '%u' and domain = '%d' and enabled = true LIMIT 1;
OEF

cat <<OEF> /etc/postfix/sql/aliases.cf
user = $DB_USER
password = $DB_USER_PASS
hosts = 127.0.0.1
dbname = $DATABASE
query = select concat(destination_username, '@', destination_domain) as destinations from aliases where source_username = '%u' and source_domain = '%d' and enabled = true;
OEF

cat <<OEF> /etc/postfix/sql/domains.cf
user = $DB_USER
password = $DB_USER_PASS
hosts = 127.0.0.1
dbname = $DATABASE
query = select 1 from domains where domain = '%s' LIMIT 1;
OEF

cat <<OEF> /etc/postfix/sql/sender-login-maps.cf
user = $DB_USER
password = $DB_USER_PASS
hosts = 127.0.0.1
dbname = $DATABASE
query = select concat(username, '@', domain) as 'owns' from accounts where username = '%u' AND domain = '%d' and enabled = true union select concat(destination_username, '@', destination_domain) AS 'owns' from aliases where source_username = '%u' and source_domain = '%d' and enabled = true;
OEF

cat <<OEF> /etc/postfix/sql/tls-policy.cf
user = $DB_USER
password = $DB_USER_PASS
hosts = 127.0.0.1
dbname = $DATABASE
query = SELECT policy, params FROM tlspolicies WHERE domain = '%s';
OEF

chmod -R 640 /etc/postfix/sql
systemctl daemon-reload
systemctl restart postfix
systemctl status postfix

#status=$(postmap -q $DOMAIN mysql:/etc/postfix/mysql-virtual-mailbox-domains.cf)
#status=$(postmap -q $EMAIL_1 mysql:/etc/postfix/mysql-virtual-mailbox-maps.cf)
#status=$(postmap -q $EMAIL_2 mysql:/etc/postfix/mysql-virtual-mailbox-maps.cf)
#status=$(postmap -q mail.$DOMAIN mysql:/etc/postfix/mysql-virtual-mailbox-domains.cf)
}





##Dovecot
config_dovecot_1() {

cp /etc/dovecot/dovecot.conf /etc/dovecot/dovecot.conf.orig
cp /etc/dovecot/conf.d/10-mail.conf /etc/dovecot/conf.d/10-mail.conf.orig
cp /etc/dovecot/conf.d/10-auth.conf /etc/dovecot/conf.d/10-auth.conf.orig
cp /etc/dovecot/dovecot-sql.conf.ext /etc/dovecot/dovecot-sql.conf.ext.orig
cp /etc/dovecot/conf.d/10-master.conf /etc/dovecot/conf.d/10-master.conf.orig
cp /etc/dovecot/conf.d/10-ssl.conf /etc/dovecot/conf.d/10-ssl.conf.orig

cat <<'OEF' > /etc/dovecot/dovecot.conf
!include_try /usr/share/dovecot/protocols.d/*.protocol
dict {
}
!include conf.d/*.conf
!include_try local.conf
OEF

cat <<'OEF'> /etc/dovecot/conf.d/10-mail.conf
namespace inbox {
  inbox = yes
}
protocol !indexer-worker {
}
mail_location = maildir:/var/mail/vhosts/%d/%n
mail_privileged_group = mail
OEF


cat <<OEF> /etc/dovecot/conf.d/10-auth.conf
auth_mechanisms = plain login
!include auth-sql.conf.ext
OEF

cat <<OEF> /etc/dovecot/conf.d/auth-sql.conf.ext

passdb {
  driver = sql
  args = /etc/dovecot/dovecot-sql.conf.ext
}

userdb {
  driver = sql
  args = /etc/dovecot/dovecot-sql.conf.ext
}

OEF

cat <<OEF> /etc/dovecot/dovecot-sql.conf.ext
driver = mysql
connect = host=127.0.0.1 dbname=$DATABASE user=$DB_USER password=$DB_USER_PASS
default_pass_scheme = SHA512-CRYPT
password_query = SELECT username AS user, domain, password FROM accounts WHERE username = '%n' AND domain = '%d' and enabled = true;
#user_query = SELECT concat('*:storage=', quota, 'M') AS quota_rule FROM accounts WHERE username = '%n' AND domain = '%d' AND sendonly = false;
#iterate_query = SELECT username, domain FROM accounts where sendonly = false;
OEF

cat <<OEF> /etc/dovecot/conf.d/10-master.conf
service imap-login {
  inet_listener imap {
    port = 143
  }
  inet_listener imaps {
    port = 993
    ssl = yes
  }
}

service pop3-login {
  inet_listener pop3 {
    port = 110
  }
  inet_listener pop3s {
    port = 995
    ssl = yes
  }
}

service lmtp {
  unix_listener /var/spool/postfix/private/dovecot-lmtp {
    mode = 0666
    user = postfix
    group = postfix
  }
  user=mail
}

service imap {
}

service pop3 {
}

service auth {
  unix_listener /var/spool/postfix/private/auth {
    mode = 0666
    user = postfix
    group = postfix
  }

  unix_listener auth-userdb {
    mode = 0600
    user = vmail
  }
  user = dovecot
}

service auth-worker {
  user = vmail
}

service dict {
  unix_listener dict {
  }
}

OEF

groupadd -g 5000 vmail 
useradd -g vmail -u 5000 vmail -d /var/mail
mkdir -p /var/mail/vhosts/"$DOMAIN"
chown -R vmail:vmail /var/mail/vhosts
chown -R vmail:dovecot /etc/dovecot
chmod -R o-rwx /etc/dovecot

if [[ -f "/etc/letsencrypt/live/mail.${DOMAIN}/fullchain.pem" ]]; then 
    cat <<OEF> /etc/dovecot/conf.d/10-ssl.conf
ssl = yes
ssl_cert = </etc/letsencrypt/live/mail.${DOMAIN}/fullchain.pem
ssl_key = </etc/letsencrypt/live/mail.${DOMAIN}/privkey.pem
ssl_client_ca_dir = /etc/ssl/certs
ssl_dh = </usr/share/dovecot/dh.pem
OEF
fi

systemctl restart dovecot

}

config_postfix_3() {
  touch /etc/postfix/without_ptr
  touch /etc/postfix/postscreen_access
  cat <<OEF> /etc/postfix/without_ptr
92.255.182.83 OK
${MY_SERVER_IP} OK
OEF

  cat <<OEF> /etc/postfix/postscreen_access
${MY_SERVER_IP} permit
92.255.182.83 permit
OEF

  postmap /etc/postfix/without_ptr
}


config_roundcube_1(){
  echo ""
}


postconfigure_1() {
  systemctl status dovecot
  systemctl status postfix
}


bootstrapdb_old() {
  cat <<OEF> /tmp/old.sql
  INSERT INTO $DATABASE.domains
      (id ,name)
  VALUES
      ('1', '$DOMAIN'),
      ('2', 'mail.$DOMAIN');

  INSERT INTO $DATABASE.accounts
    (id, domain_id, password , email)
  VALUES
    ('1', '1', TO_BASE64(UNHEX(SHA2('$PASSWORD_1', 512))), '$EMAIL_1'),
    ('2', '1', TO_BASE64(UNHEX(SHA2('$PASSWORD_2', 512))), '$EMAIL_2');  
OEF
}

main(){
  run_delete
  run_install
  bootstrapdb_1
  bootstrapdb_2
  #bootstrapdb_old
  config_postfix_1
  config_postfix_2
  config_postfix_3
  config_dovecot_1
  config_roundcube_1
  postconfigure_1

}

main



#MANS
#https://gist.github.com/ibqn/22b9c1726dae656f295a9060fb758d15?permalink_comment_id=3701331

#https://vegastack.com/tutorials/install-and-configure-postfix-and-dovecot/
#https://www.roundcubeforum.net/index.php?topic=29033.0