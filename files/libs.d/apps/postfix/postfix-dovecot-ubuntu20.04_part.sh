export DEBIAN_FRONTEND=noninteractive
export DOMAIN="linux2be.com"
export MY_SERVER_IP=$(curl -q ifconfig.io -4)

export USER_1="user1"
export PASSWORD_1="password1"
export EMAIL_1="${USER_1}@${DOMAIN}"

export USER_2="user2"
export PASSWORD_2="password2"
export EMAIL_2="${USER_2}@${DOMAIN}"

export DATABASE="servermail"
export DB_USER="servermail"
export DB_USER_PASS="mypass2323dsdsdsdsds&dsdsworMMMd"

cat <<OEF> /etc/dovecot/dovecot-sql.conf.ext
driver = mysql
connect = host=127.0.0.1 dbname=$DATABASE user=$DB_USER password=$DB_USER_PASS
default_pass_scheme = SHA512-CRYPT

user_query = \
SELECT '/var/vmail/%u/%n' as home, 'maildir:/var/vmail/%u/%n' as mail, \
5000 AS uid, 5000 AS gid, concat('dirsize:storage=', quota) AS quota \
FROM accounts WHERE  username = '%u'  AND enabled = true;

password_query = \
SELECT username as user, password, '/var/vmail/%d/%n' as userdb_home, \
'maildir:/var/vmail/%u/%n' as userdb_mail, 5000 as userdb_uid, 5000 as userdb_gid \
FROM accounts WHERE  domain = '%d' AND email = '%u' AND enabled = true;

iterate_query = \
SELECT username as user, domain FROM accounts

OEF

cat <<OEF> /etc/dovecot/dovecot.conf

auth_mechanisms = plain login
disable_plaintext_auth = yes
mail_gid = vmail
mail_uid = vmail
mail_location = maildir:/var/vmail/%u

namespace inbox {
  inbox = yes
  location =
  mailbox Drafts {
    special_use = \Drafts
  }
  mailbox Junk {
    special_use = \Junk
  }
  mailbox Sent {
    special_use = \Sent
  }
  mailbox "Sent Messages" {
    special_use = \Sent
  }
  mailbox Trash {
    special_use = \Trash
  }
  prefix =
}
postmaster_address = admin@${DOMAIN}
protocols = " imap lmtp pop3"
passdb {
  args = /etc/dovecot/dovecot-sql.conf.ext
  driver = sql
}
userdb {
  args = /etc/dovecot/dovecot-sql.conf.ext
  driver = sql
}
service auth {
  unix_listener /var/spool/postfix/private/auth {
    group = postfix
    mode = 0666
    user = postfix
  }
  unix_listener auth-userdb {
    group = mail
    mode = 0666
    user = vmail
  }
}
service auth-worker {
  #user = vmail
}

service dict {
  unix_listener dict {
  }
}
ssl = yes
ssl_cert = </etc/letsencrypt/live/mail.${DOMAIN}/fullchain.pem
ssl_key = </etc/letsencrypt/live/mail.${DOMAIN}/privkey.pem
ssl_client_ca_dir = /etc/ssl/certs
ssl_dh = </usr/share/dovecot/dh.pem
OEF



cat <<OEF> /etc/postfix/main.cf
##
## Network settings
##

mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 92.255.182.83/32
inet_interfaces = 127.0.0.1, ::1, $MY_SERVER_IP
myhostname = mail.${DOMAIN}

##
## Mail queue settings
##
maximal_queue_lifetime = 1h
bounce_queue_lifetime = 1h
maximal_backoff_time = 15m
minimal_backoff_time = 5m
queue_run_delay = 5m

##
## TLS settings
##

tls_preempt_cipherlist = yes
tls_ssl_options = NO_COMPRESSION
tls_high_cipherlist = EDH+CAMELLIA:EDH+aRSA:EECDH+aRSA+AESGCM:EECDH+aRSA+SHA256:EECDH:+CAMELLIA128:+AES128:+SSLv3:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!DSS:!RC4:!SEED:!IDEA:!ECDSA:kEDH:CAMELLIA128-SHA:AES128-SHA

### Outbound SMTP connections (Postfix as sender)

smtp_tls_security_level = dane
smtp_dns_support_level = dnssec
#smtp_tls_policy_maps = mysql:/etc/postfix/sql/tls-policy.cf
smtp_tls_session_cache_database = btree:\${data_directory}/smtp_scache
smtp_tls_protocols = !SSLv2, !SSLv3
smtp_tls_ciphers = high
smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt


### Inbound SMTP connections

smtpd_tls_security_level = may
smtpd_tls_protocols = !SSLv2, !SSLv3
smtpd_tls_ciphers = high
smtpd_tls_session_cache_database = btree:\${data_directory}/smtpd_scache

smtpd_tls_cert_file=/etc/letsencrypt/live/mail.${DOMAIN}/fullchain.pem
smtpd_tls_key_file=/etc/letsencrypt/live/mail.${DOMAIN}/privkey.pem


##
## Local mail delivery to Dovecot via LMTP
##

#virtual_transport = lmtp:unix:private/dovecot-lmtp
virtual_transport = dovecot
dovecot_destination_recipient_limit = 1
##
## Spam filter and DKIM signatures via Rspamd
##

#smtpd_milters = inet:localhost:11332
#non_smtpd_milters = inet:localhost:11332
#milter_protocol = 6
#milter_mail_macros =  i {mail_addr} {client_addr} {client_name} {auth_authen}
#milter_default_action = accept

##
## Server Restrictions for clients, cecipients and relaying
## (concerning S2S-connections. Mailclient-connections are configured in submission-section in master.cf)
##

### Conditions in which Postfix works as a relay. (for mail user clients)
smtpd_relay_restrictions =      reject_non_fqdn_recipient
                                reject_unknown_recipient_domain
                                permit_mynetworks
                                reject_unauth_destination


### Conditions in which Postfix accepts e-mails as recipient (additional to relay conditions)
### check_recipient_access checks if an account is "sendonly"
smtpd_recipient_restrictions = check_recipient_access mysql:/etc/postfix/sql/recipient-access.cf


### Restrictions for all sending foreign servers ("SMTP clients")
smtpd_client_restrictions =     permit_mynetworks
                                check_client_access hash:/etc/postfix/without_ptr
                                reject_unknown_client_hostname


### Foreign mail servers must present a valid "HELO"
smtpd_helo_required = yes
smtpd_helo_restrictions =   permit_mynetworks
                            reject_invalid_helo_hostname
                            reject_non_fqdn_helo_hostname
                            reject_unknown_helo_hostname

# Block clients, which start sending too early
smtpd_data_restrictions = reject_unauth_pipelining


##
## Restrictions for MUAs (Mail user agents)
##

mua_relay_restrictions = reject_non_fqdn_recipient,reject_unknown_recipient_domain,permit_mynetworks,permit_sasl_authenticated,reject
mua_sender_restrictions = permit_mynetworks,reject_non_fqdn_sender,reject_sender_login_mismatch,permit_sasl_authenticated,reject
mua_client_restrictions = permit_mynetworks,permit_sasl_authenticated,reject


##
## Postscreen Filter
##

### Postscreen Whitelist / Blocklist
postscreen_access_list =        permit_mynetworks
                                cidr:/etc/postfix/postscreen_access
postscreen_blacklist_action = drop


# Drop connections if other server is sending too quickly
postscreen_greet_action = drop




##
## MySQL queries
##
virtual_mailbox_base = /var/vmail
virtual_alias_maps = mysql:/etc/postfix/sql/aliases.cf
virtual_mailbox_maps = mysql:/etc/postfix/sql/accounts.cf
virtual_mailbox_domains = mysql:/etc/postfix/sql/domains.cf
virtual_uid_maps = static:150
virtual_gid_maps = static:8
local_recipient_maps = \$virtual_mailbox_maps


##
## Miscellaneous
##

mailbox_size_limit = 0
message_size_limit = 52428800
biff = no
append_dot_mydomain = no
recipient_delimiter = +
strict_mailbox_ownership = no
meta_directory = /usr/share/postfix
queue_directory = /var/spool/postfix
compatibility_level=2
mydestination = 


OEF
groupadd -g 5000 vmail 
useradd -g vmail -u 5000 vmail -d /var/vmail
mkdir -p /var/vmail/"$DOMAIN"
chown -R vmail:vmail /var/vmail
chown -R vmail:dovecot /etc/dovecot
chmod -R o-rwx /etc/dovecot
systemctl restart postfix
systemctl restart dovecot


  systemctl status dovecot
  systemctl status postfix