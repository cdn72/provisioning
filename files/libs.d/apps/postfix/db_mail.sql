DROP TABLE IF EXISTS admin;
CREATE TABLE admin (
	  username varchar(255) NOT NULL,
	  password varchar(255) NOT NULL,
	  superadmin SMALLINT NOT NULL DEFAULT '0',
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active SMALLINT NOT NULL DEFAULT '1',
	  PRIMARY KEY (username)
);


DROP TABLE IF EXISTS alias;
CREATE TABLE alias (
	  address varchar(255) NOT NULL,
	  goto text NOT NULL,
	  domain varchar(255) NOT NULL,
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active SMALLINT NOT NULL DEFAULT '1',
	  PRIMARY KEY (address)
);
CREATE INDEX if not exists idx_alias_domain ON alias (domain);


DROP TABLE IF EXISTS alias_domain;
CREATE TABLE alias_domain (
	  alias_domain varchar(255) NOT NULL,
	  target_domain varchar(255) NOT NULL,
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active SMALLINT NOT NULL DEFAULT '1',
	  PRIMARY KEY (alias_domain)
);
CREATE INDEX if not exists idx_alias_domain_active ON alias_domain (active);
CREATE INDEX if not exists idx_alias_domain_target_domain ON alias_domain (active);


DROP TABLE IF EXISTS config;
CREATE TABLE config (
	  id BIGINT NOT NULL PRIMARY KEY,
	  name CHAR(20) DEFAULT '' NOT NULL,
	  value CHAR(20) DEFAULT '' NOT NULL,
	  CONSTRAINT unique_name UNIQUE (name)
);


DROP TABLE IF EXISTS domain;
CREATE TABLE domain (
	  domain varchar(255) NOT NULL,
	  description text NOT NULL,
	  aliases BIGINT NOT NULL DEFAULT '0',
	  mailboxes BIGINT NOT NULL DEFAULT '0',
	  maxquota bigint NOT NULL DEFAULT '0',
	  quota bigint NOT NULL DEFAULT '0',
	  transport varchar(255) NOT NULL,
	  backupmx SMALLINT NOT NULL DEFAULT '0',
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active SMALLINT NOT NULL DEFAULT '1',
	  PRIMARY KEY (domain)
);


DROP TABLE IF EXISTS domain_admins;
CREATE TABLE domain_admins (
	  username varchar(255) NOT NULL,
	  domain varchar(255) NOT NULL,
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active SMALLINT NOT NULL DEFAULT '1',
	  PRIMARY KEY (username)
);
CREATE INDEX if not exists idx_domain_admins_username ON domain_admins (username);


DROP TABLE IF EXISTS fetchmail;
DROP TYPE enum_src_auth;
DROP TYPE enum_protocol;
CREATE TYPE enum_src_auth AS ENUM ('password','kerberos_v5','kerberos','kerberos_v4','gssapi','cram-md5','otp','ntlm','msn','ssh','any');
CREATE TYPE enum_protocol AS ENUM ('POP3','IMAP','POP2','ETRN','AUTO');

CREATE TABLE fetchmail (
	  id SERIAL PRIMARY KEY,
	  domain varchar(255) DEFAULT '',
	  mailbox varchar(255) NOT NULL,
	  src_server varchar(255) NOT NULL,
	  src_auth enum_src_auth DEFAULT NULL,
	  src_user varchar(255) NOT NULL,
	  src_password varchar(255) NOT NULL,
	  src_folder varchar(255) NOT NULL,
	  poll_time BIGINT NOT NULL DEFAULT '10',
	  fetchall INTEGER NOT NULL DEFAULT '0',
	  keep INTEGER NOT NULL DEFAULT '0',
	  protocol enum_protocol DEFAULT NULL,
	  usessl INTEGER NOT NULL DEFAULT '0',
	  sslcertck SMALLINT NOT NULL DEFAULT '0',
	  sslcertpath text DEFAULT '',
	  sslfingerprint varchar(255) DEFAULT '',
	  extra_options text,
	  returned_text text,
	  mda varchar(255) NOT NULL,
	  date timestamp NOT NULL DEFAULT '2023-04-21 00:00:00',
	  created timestamp NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  active SMALLINT NOT NULL DEFAULT '0'
);


DROP TABLE IF EXISTS log;
CREATE TABLE log (
	  timestamp TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  username varchar(255) NOT NULL,
	  domain varchar(255) NOT NULL,
	  action varchar(255) NOT NULL,
	  data text NOT NULL
);
DROP INDEX idx_log_timestamp;
DROP INDEX idx_log_domain_timestamp;
CREATE INDEX if not exists idx_log_timestamp ON log (timestamp);
CREATE INDEX if not exists idx_log_domain_timestamp ON log (domain,timestamp);


DROP TABLE IF EXISTS mailbox;
CREATE TABLE mailbox (
	  username varchar(255) NOT NULL,
	  password varchar(255) NOT NULL,
	  name varchar(255)  NOT NULL,
	  maildir varchar(255) NOT NULL,
	  quota bigint NOT NULL DEFAULT '0',
	  local_part varchar(255) NOT NULL,
	  domain varchar(255) NOT NULL,
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  active SMALLINT NOT NULL DEFAULT '1',
	  PRIMARY KEY (username)
);
DROP INDEX idx_mailbox_domain;
CREATE INDEX if not exists idx_mailbox_domain ON mailbox (domain);


DROP TABLE IF EXISTS quota;
CREATE TABLE quota (
	  username varchar(255) NOT NULL,
	  path varchar(100) NOT NULL,
	  current bigint DEFAULT NULL,
	  PRIMARY KEY (username,path)
);


DROP TABLE IF EXISTS quota2;
CREATE TABLE quota2 (
	  username varchar(100) NOT NULL,
	  bytes bigint NOT NULL DEFAULT '0',
	  messages BIGINT NOT NULL DEFAULT '0',
	  PRIMARY KEY (username)
);


DROP TABLE IF EXISTS vacation;
CREATE TABLE vacation (
	  email varchar(255) NOT NULL,
	  subject varchar(255)  NOT NULL,
	  body text  NOT NULL,
	  activefrom timestamp NOT NULL DEFAULT '2023-04-21 00:00:00',
	  activeuntil timestamp NOT NULL DEFAULT '2023-04-21 00:00:00',
	  cache text NOT NULL,
	  domain varchar(255) NOT NULL,
	  interval_time BIGINT NOT NULL DEFAULT '0',
	  created TIMESTAMP NOT NULL DEFAULT '2023-04-21 00:00:00',
	  modified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  active SMALLINT NOT NULL DEFAULT '1',
	  PRIMARY KEY (email)
);
DROP INDEX idx_vacation_email;
CREATE INDEX if not exists idx_vacation_email ON vacation (email);


DROP TABLE IF EXISTS vacation_notification;
CREATE TABLE vacation_notification (
	  on_vacation varchar(255)  NOT NULL,
	  notified varchar(255)  NOT NULL,
	  notified_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (on_vacation,notified),
	  CONSTRAINT vacation_notification_pkey FOREIGN KEY (on_vacation) REFERENCES vacation (email) ON DELETE CASCADE
);


INSERT INTO domain_admins VALUES ('admin@example.com','ALL','2023-04-21 00:00:00',1);