#!/usr/bin/env bash
nginx_run(){
    nginx -g 'daemon off;'
}

nginx_prepare(){
    if [[ ! -d "/etc/nginx/stream.d" ]];then
        mkdir -p /etc/nginx/stream.d
    fi
}

nginx_autoreload_inotifywait(){
    # inspired/copied from https://github.com/kubernetes/kubernetes/blob/master/examples/https-nginx/auto-reload-nginx.sh
while true
do
        inotifywait -e create -e modify -e delete -e move -r --exclude "\\.certbot\\.lock|\\.well-known" "/etc/nginx"
        echo "Changes noticed in /spcgeonode-certificates"

        echo "Waiting 5s for additionnal changes"
        sleep 5


        # Test nginx configuration
        nginx -t
        # If it passes, we reload
        if [ $? -eq 0 ]
        then
                echo "Configuration valid, we reload..."
                nginx -s reload
        else
                echo "Configuration not valid, we do not reload."
        fi
done
}

nginx_reload(){
    export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games"
    local config_test_nginx=$(/usr/sbin/nginx  -t > /dev/null ; echo "return_code=$?" | awk -F= '{print $NF}')
    if [[ "${config_test_nginx}" == "0" ]];then
        /usr/sbin/nginx -s reload
    fi
}


nginx_acl_gen_mysql(){
    ${exec_prefix} echo "Exec funct  ${FUNCNAME[0]}"
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d" ]];then
        mkdir -p "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d"
    fi
    if [ ! -f "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf" ];then 
        touch "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf"
    else
        : > "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf"
    fi

    for IP in $(echo "${NGINX_MYSQL_ACL_LIST_NETWORK:-0.0.0.0/0}" | tr -s " " "\012")
    do
        echo "allow ${IP};" >> "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf"
done
    echo "deny all;" >> ${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf

    cat ${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf

}

nginx_acl_gen_postgres(){
    ${exec_prefix} echo "Exec funct  ${FUNCNAME[0]}"
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d" ]];then
        mkdir -p "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d"
    fi
    if [ ! -f "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_postgres.conf" ];then 
        touch "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_postgres.conf"
    else
        : > "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_postgres.conf"
    fi

    for IP in $(echo "${NGINX_POSTGRESQL_ACL_LIST_NETWORK:-0.0.0.0/0}" | tr -s " " "\012")
    do
        echo "allow ${IP};" >> "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_postgres.conf"
done
    echo "deny all;" >> ${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_postgres.conf

    cat ${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_postgres.conf

}


nginx_config_dir_create(){

   if [[ -d "${acme_dir:-/var/www/letsencrypt}" ]]
   then
        mkdir -p "${acme_dir:-/var/www/letsencrypt}"
        if [[ ! -d "/var/tmp/nginx/fastcgi" ]];then
            mkdir -p /var/tmp/nginx/fastcgi
        fi
        chown --recursive www-data ${acme_dir:-/var/www/letsencrypt}
        chown --recursive www-data:www-data /var/tmp/nginx
   fi
}


nginx_config_gen_http_backend_digital_ms(){
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF>> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 80;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT}/build;
    index index.php index.html index.htm;

    location ~ /\.git {
        deny all;
    }      

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    } 
    lingering_time 60;
    location ~ \.php$ {
        try_files \$uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass ${NGINX_PHP_BACKEND:-127.0.0.1:9000};
        fastcgi_index index.php;
        fastcgi_read_timeout 500;
        fastcgi_send_timeout 500;
        fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
        fastcgi_param  PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/node/bin:/bin;
        include fastcgi_params;

    }
}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF

    fi
}

nginx_config_gen_http_backend_iprn(){
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF>> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 80;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT}/build;
    index index.php index.html index.htm;

    location ~ /\.git { deny all; }
    location ~ /\..env.example { deny all; }
    location ~ /\.env { deny all; }
    location ~ /\.gitlab-ci.yml{ deny all; }
    location ~ /\.npmrc { deny all; }

    location / {
      try_files \$uri \$uri/ /index.html;
    }

}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF

    fi
}

nginx_config_gen_http_backend_izzicall(){
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF>> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 80;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT}/public;
    index index.php index.html index.htm;

    location ~ /\.git {
        deny all;
    }      

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    } 
    lingering_time 60;
    location ~ \.php$ {
        try_files \$uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass ${NGINX_PHP_BACKEND:-127.0.0.1:9000};
        fastcgi_index index.php;
        fastcgi_read_timeout 500;
        fastcgi_send_timeout 500;
        fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
        fastcgi_param  PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/node/bin:/bin;
        include fastcgi_params;

    }
}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF

    fi
}

nginx_config_gen_http_backend_whitesparrow() {
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 80;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT}/out;
    index index.php index.html index.htm;


    location ~ /\.git { deny all; }
    location ~ /\..env.example { deny all; }
    location ~ /\.env { deny all; }
    location ~ /\.gitlab-ci.yml{ deny all; }
    location ~ /\.npmrc { deny all; }    

    location / {
      try_files \$uri \$uri/ /index.html;
    }
}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF
fi
}


nginx_config_gen_http_backend_website-foxcom() {
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 8000;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT}/dist;
    index index.html index.htm;

    location ~ /\.git {
        deny all;
    }      

    location / {
      try_files \$uri \$uri/ /index.html;
      if (!-e \$request_filename){
          rewrite ^(.*)\$ /\$1.html break;
      }
      if (!-e \$request_filename){
          rewrite ^(.*)$ /\$1.php break;
      }
      if (\$request_uri ~ "\.((html)|(php))\$"){
          rewrite ^/(.*)\.([^.]+)\$ /\$1 redirect;
      }
    }
}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF
fi
}

nginx_config_gen_http_backend_smsgic-website() {
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 80;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT}/dist/client;
    index index.php index.html index.htm;

    location ~ /\.git { deny all; }
    location ~ /\..env.example { deny all; }
    location ~ /\.env { deny all; }
    location ~ /\.gitlab-ci.yml{ deny all; }
    location ~ /\.npmrc { deny all; }    

    location / {
      try_files \$uri \$uri/ /index.html;
    }
}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF
fi
}


nginx_init_redis_proxy() {
    cat <<OEF >/etc/nginx/stream.d/redis.conf
server {
    listen 6379 so_keepalive=on;
    proxy_pass ${REDIS_HOST:-10.28.0.1}:6379;
    access_log off;
}
OEF
}

nginx_config_gen_http_backend_smsgic-backend(){
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF>> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 80;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT}/public;
    index index.php index.html index.htm;


    location ~ /\.git { deny all; }
    location ~ /\..env.example { deny all; }
    location ~ /\.env { deny all; }
    location ~ /\.gitlab-ci.yml{ deny all; }
    location ~ /\.npmrc { deny all; }

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    } 
    lingering_time 60;
    location ~ \.php$ {
        try_files \$uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass ${NGINX_PHP_BACKEND:-127.0.0.1:9000};
        fastcgi_index index.php;
        fastcgi_read_timeout 500;
        fastcgi_send_timeout 500;
        fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
        fastcgi_param  PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/node/bin:/bin;
        include fastcgi_params;

    }
}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF

    fi
}

nginx_config_gen_http_backend_smsgic-admin() {
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 80;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT}/dist;
    index index.php index.html index.htm;

    location ~ /\.git { deny all; }
    location ~ /\..env.example { deny all; }
    location ~ /\.env { deny all; }
    location ~ /\.gitlab-ci.yml{ deny all; }
    location ~ /\.npmrc { deny all; }

    location / {
      try_files \$uri \$uri/ /index.html;
    }
}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF
fi
}
nginx_config_gen_http_backend_smsgic-portal() {
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 80;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT}/dist;
    index index.php index.html index.htm;

    location ~ /\.git { deny all; }
    location ~ /\..env.example { deny all; }
    location ~ /\.env { deny all; }
    location ~ /\.gitlab-ci.yml{ deny all; }
    location ~ /\.npmrc { deny all; }

    location / {
      try_files \$uri \$uri/ /index.html;
    }
}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF
fi
}


nginx_config_gen_http_backend_mgc-website(){
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF>> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 80;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT}/public;
    index index.php index.html index.htm;


    location ~ /\.git { deny all; }
    location ~ /\..env.example { deny all; }
    location ~ /\.env { deny all; }
    location ~ /\.gitlab-ci.yml { deny all; }
    location ~ /\.npmrc { deny all; }

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    } 
    lingering_time 60;
    location ~ \.php$ {
        try_files \$uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass ${NGINX_PHP_BACKEND:-127.0.0.1:9000};
        fastcgi_index index.php;
        fastcgi_read_timeout 500;
        fastcgi_send_timeout 500;
        fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
        fastcgi_param  PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/node/bin:/bin;
        include fastcgi_params;

    }
}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF

    fi
}

nginx_config_gen_http_backend_nordic-website() {
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 80;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT};
    index index.php index.html index.htm;

    location ~ /\.git { deny all; }
    location ~ /\..env.example { deny all; }
    location ~ /\.env { deny all; }
    location ~ /\.gitlab-ci.yml{ deny all; }
    location ~ /\.npmrc { deny all; }

    location / {
      try_files \$uri \$uri/ /index.html;
    }
}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF
fi
}


nginx_config_gen_http_backend_sms-raffles-website() {
    if [[ ! -d "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}" ]];then
        mkdir -p ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}
    fi
    if [[ -n "${APP_HTTP_HOST/[ ]*\n/}" ]];
    then
        if [[ ! -f "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/${APP_HTTP_HOST}.conf" ]];then
            touch ${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}/"${APP_HTTP_HOST}".conf
        fi
        grep -q "${APP_HTTP_HOST}" "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${NGINX_CONFIG_DIR_ROOT_BACKEND:-/etc/nginx/backend.d}"/"${APP_HTTP_HOST}".conf
###NGINX BACKEND CONFIG BEGIN for ${APP_HTTP_HOST}
server {
    listen 80;
    server_name 
    ${APP_HTTP_HOST}
    ;
    root ${APP_DIR_CURRENT}/dist/client;
    index index.php index.html index.htm;

    location ~ /\.git { deny all; }
    location ~ /\..env.example { deny all; }
    location ~ /\.env { deny all; }
    location ~ /\.gitlab-ci.yml{ deny all; }
    location ~ /\.npmrc { deny all; }
    location ~ /public/static/numbers.json { deny all; }
    location ~ /public/static/text.json { deny all; }

    location / {
      try_files \$uri \$uri/ /index.html;
    }
}
###NGINX BACKEND CONFIG END for ${APP_HTTP_HOST}
OEF
fi
}