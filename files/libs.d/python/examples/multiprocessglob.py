#!/usr/bin/env python3.11
import multiprocessing
import os
import glob
import logging
import sys
import psycopg2
from psycopg2 import OperationalError, errorcodes, errors,sql
from psycopg2.errorcodes import UNIQUE_VIOLATION

import multiprocessing
from multiprocessing import Pool
import glob
import re
import gitlab
import json
import time
import datetime
from datetime import datetime, timedelta
from time import gmtime, strftime
from progress.bar import IncrementalBar
import tqdm
from tqdm.contrib.concurrent import process_map
import sys
from requests import get
import pycurl
import csv
import threading
import numpy as np
import pandas
from io import StringIO
from sqlalchemy import create_engine
import traceback
from typing import Iterator, Optional
from typing import Iterator, Dict, Any
from pathlib import Path
import fcntl


CDR_FILES_DIR = os.getenv('CDR_FILES_DIR', '/mnt/hdd/freeswitch/json_cdr_oh_my')

def process(file):
    #print(str(file))
    pass # do stuff to a file

startTime_0 = time.time()

p = multiprocessing.Pool(processes=50)
for f in sorted(glob.glob(CDR_FILES_DIR + "/" + "**/" + "*" + "cdr.json", 
        recursive=True),key=os.path.getmtime, reverse=True):
    # launch a process for each file (ish).
    # The result will be approximately one process per CPU core available.
    p.apply_async(process, [f]) 

p.close()
p.join() # Wait for all child processes to close.
executionTime_0 = (time.time() - startTime_0)

print('Time execution for prepare: ' + str(executionTime_0))


#Time execution for prepare: 131.72637820243835 = 100
#Time execution for prepare: 144.81919598579407 = 50
#
#Time execution for create csv: 38.59341788291931
#Total files cdr's for load finded = 2 872 376