# SuperFastPython.com
# create a large number of data files concurrently with asyncio
from os import makedirs
from os.path import join
from random import random
import aiofiles
import asyncio
 
# save data to a file
async def save_file(filepath, data):
    # open the file
    async with aiofiles.open(filepath, 'w') as handle:
        # save the data
        await handle.write(data)
 
# generate a csv file with v=10 values per line and l=5k lines
def generate_file(identifier, n_values=10, n_lines=5000):
    # generate many lines of data
    data = list()
    for i in range(n_lines):
        # generate a list of numeric values as strings
        line = [str(identifier+i+j) for j in range(n_values)]
        #print(line)
        # convert list to string with values separated by commas
        data.append(','.join(line))
    # convert list of lines to a string separated by new lines
    return '\n'.join(data)
 
# generate and save a file
async def generate_and_save(path, identifier, semaphore):
    # acquire the semaphore
    async with semaphore:
        # generate data
        data = generate_file(identifier)
        # create a unique filename
        filepath = join(path, f'data-{identifier:04d}.csv')
        # save data file to disk
        save_file(filepath, data)
        # report progress
        print(f'.saved {filepath}')
        with open(filepath,'r') as f:
            data = json.load(f)
            print(data)
 
# generate many data files in a directory
async def main(path='/tmp/test', n_files=50):
    # create a local directory to save files
    makedirs(path, exist_ok=True)
    # create a semaphore to limit concurrent file creations
    semaphore = asyncio.Semaphore(50)
    # create one coroutine for each file to rename
    tasks = [generate_and_save(path, i, semaphore) for i in range(n_files)]
    # execute and wait for all coroutines
    await asyncio.gather(*tasks)
 
# entry point
if __name__ == '__main__':
    # execute the asyncio run loop
    asyncio.run(main())

#main-> generate_and_save- > generate_file - > save_file