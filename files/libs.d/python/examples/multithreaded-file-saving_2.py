#!/usr/bin/env python3.11
import os
from os.path import join
import glob
import logging
import psycopg2
from psycopg2 import OperationalError, errorcodes, errors,sql
from psycopg2.errorcodes import UNIQUE_VIOLATION
import multiprocessing
from multiprocessing import Pool
import re
import gitlab
import json
import time
import datetime
from datetime import datetime, timedelta
from time import gmtime, strftime
from progress.bar import IncrementalBar
import tqdm
from tqdm.contrib.concurrent import process_map
import sys
from requests import get
import pycurl
import csv

import threading
import numpy as np
import pandas
from io import StringIO
from sqlalchemy import create_engine
import traceback
from typing import Iterator, Optional
from typing import Iterator, Dict, Any
from pathlib import Path
import fcntl
from concurrent.futures import ProcessPoolExecutor,ThreadPoolExecutor
# Import telegram send
print("Import telegram")
FILE_DEST = 'telegram_send.py'
FILE_SRC = 'https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/python/telegram_send.py'
with open(FILE_DEST, 'wb') as f:
    c = pycurl.Curl()
    c.setopt(c.URL, FILE_SRC)
    c.setopt(c.WRITEDATA, f)
    c.perform()
    c.close()
#from telegram_send import telegram_send

POSTGRES_DB_HOST = os.getenv('POSTGRES_DB_HOST', '10.28.0.1')
#POSTGRES_DB_HOST = '10.38.0.1'
POSTGRES_DB_PORT = os.getenv('POSTGRES_DB_PORT', '15433')
POSTGRES_DB_NAME = os.getenv('POSTGRES_DB_NAME', 'demo')
POSTGRES_DB_USER = os.getenv('POSTGRES_DB_USER', 'demo')
POSTGRES_DB_PASSWORD = os.getenv(
    'POSTGRES_DB_PASSWORD', 'demo')
POSTGRES_TABLE_NAME = os.getenv('POSTGRES_TABLE_NAME', 'tbl_cdr_demo')
POOL_SIZE = os.getenv('POOL_SIZE', 4)
CDR_FILES_DIR = os.getenv('CDR_FILES_DIR', '/mnt/hdd/freeswitch/json_cdr_oh_my')
#CDR_FILES_DIR = os.getenv('CDR_FILES_DIR', '/mnt/hdd/freeswitch/json_cdr_oh_my/test')
WEBHOOK_HTTP_HOST = os.getenv('WEBHOOK_HTTP_HOST', 'demowhlistener.services.linux2be.com/job_cdr')
TELEGRAM_BOT_TOKEN = os.getenv('TELEGRAM_BOT_TOKEN', 'dsdsdsdsdgggf3423')
TELEGRAM_CHANNEL_ID = os.getenv('TELEGRAM_CHANNEL_ID', '-2323232323232')


pool = Pool(processes=int(POOL_SIZE))
 

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger('requests').setLevel(logging.CRITICAL)
logger = logging.getLogger(__name__)

# save data to a file
def save_file(filepath, data):
    # open the file
    with open(filepath, 'w') as handle:
        # save the data
        handle.write(data)
 
# generate a csv file with v=10 values per line and l=5k lines
def generate_file(identifier, n_values=10, n_lines=5000):
    # generate many lines of data
    data = list()
    for i in range(n_lines):
        # generate a list of numeric values as strings
        line = [str(identifier+i+j) for j in range(n_values)]
        # convert list to string with values separated by commas
        data.append(','.join(line))
    # convert list of lines to a string separated by new lines
    return '\n'.join(data)
 
# generate and save a file
def generate_and_save(path, identifier):
    # generate data
    data = generate_file(identifier)
    # create a unique filename
    filepath = join(path, f'data-{identifier:04d}.csv')
    # save data file to disk
    save_file(filepath, data)
    # report progress
    print(f'.saved {filepath}', flush=True)
 
def split_list(lst, chunk_size):
    for i in range(0, len(lst), chunk_size):
        yield lst[i:i+chunk_size]


def csv_create(cdr_files,identifier):
    relevant_path = CDR_FILES_DIR
    #print(cdr_files)
    for file in cdr_files:
        try:
            file_name = re.sub(relevant_path + "/", '', str(file), 2)
            head, tail = os.path.split(file_name)
            with open(file,'r') as f:
                metric_name = str(tail)
                data = json.load(f)
                value = "1"
                cdr_start_epoch = data['variables']['start_epoch']
                cdr_start_date = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(int(cdr_start_epoch)))
                send_time = cdr_start_date
                f.close()
                target_file=time.strftime("%Y-%m-%d-%H:%M", time.gmtime(int(cdr_start_epoch))) + "00"
                target_file_full_path=csv_store_dir+ '/cdr_load_file_' +  pg_table_name  + '_' + str(target_file) + '.csv'
                with open(target_file_full_path, 'a') as fileObj:
                    fcntl.flock(fileObj, fcntl.LOCK_EX)
                    writerObj = csv.writer(fileObj, delimiter = "\t",quotechar='\'', quoting=csv.QUOTE_MINIMAL)
                    writerObj.writerow([send_time, str(metric_name), json.dumps(data, skipkeys=True, sort_keys=True), str(value)])
                    fcntl.flock(fileObj, fcntl.LOCK_UN)                    
        except Exception as e:
            print(e)
            #print("Exception on : {error_f}. Error data = {error_m}".format(error_f=file,error_m=e))
            #logger.exception("Exception on : {error_f}. Error data = {error_m}".format(error_f=file,error_m=e))
            raise
def app_exec_clean(csv_store_dir):
    for f in Path(csv_store_dir).glob('*'):
        try:
            f.unlink()
        except OSError as e:
            print("Error: %s : %s" % (f, e.strerror))

def csv_create(cdr_files):
    relevant_path = CDR_FILES_DIR
    print(cdr_files)
    for file in cdr_files:
        try:
            file_name = re.sub(relevant_path + "/", '', str(file), 2)
            head, tail = os.path.split(file_name)
            with open(file,'r') as f:
                metric_name = str(tail)
                data = json.load(f)
                value = "1"
                cdr_start_epoch = data['variables']['start_epoch']
                cdr_start_date = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(int(cdr_start_epoch)))
                send_time = cdr_start_date
                f.close()
                target_file=time.strftime("%Y-%m-%d-%H", time.gmtime(int(cdr_start_epoch))) + "00"
                target_file_full_path=csv_store_dir+ '/cdr_load_file_' +  pg_table_name  + '_' + str(target_file) + '.csv'

                with open(target_file_full_path, 'a') as fileObj:
                    fcntl.flock(fileObj, fcntl.LOCK_EX)
                    writerObj = csv.writer(fileObj, delimiter = "\t",quotechar='\'', quoting=csv.QUOTE_MINIMAL)
                    writerObj.writerow([send_time, str(metric_name), json.dumps(data, skipkeys=True, sort_keys=True), str(value)])
                    fcntl.flock(fileObj, fcntl.LOCK_UN)
        except Exception as e:
            print("Exception on : {}".format(file))
            logger.exception("Exception on : {}".format(file))
            raise

def csv_create_single(file):
    #print("Filename =" + str(file))
    #quit()
    try:
        file_name = re.sub(relevant_path + "/", '', str(file), 2)
        head, tail = os.path.split(file_name)
        with open(file,'r') as f:
            metric_name = str(tail)
            data = json.load(f)
            value = "1"
            cdr_start_epoch = data['variables']['start_epoch']
            cdr_start_date = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(int(cdr_start_epoch)))
            send_time = cdr_start_date
            f.close()
            target_file=time.strftime("%Y-%m-%d-%H", time.gmtime(int(cdr_start_epoch))) + "00"
            target_file_full_path=csv_store_dir+ '/cdr_load_file_' +  pg_table_name  + '_' + str(target_file) + '.csv'
            #print("write " + str(file) + " to file " + target_file_full_path )
            with open(target_file_full_path, 'a') as fileObj:
                fcntl.flock(fileObj, fcntl.LOCK_EX)
                writerObj = csv.writer(fileObj, delimiter = "\t",quotechar='\'', quoting=csv.QUOTE_MINIMAL)
                writerObj.writerow([send_time, str(metric_name), json.dumps(data, skipkeys=True, sort_keys=True), str(value)])
                fcntl.flock(fileObj, fcntl.LOCK_UN)
                fileObj.close()
                
    except Exception as e:
        print("Exception on : {}".format(file))
        logger.exception("Exception on : {}".format(file))
        raise
            

# generate many data files in a directory
if __name__ == "__main__":
    startTime_0 = time.time()
    n_files=50
    startTime_0 = time.time()
    pg_table_name = POSTGRES_TABLE_NAME
    relevant_path = CDR_FILES_DIR
    print("relevant_path= " + CDR_FILES_DIR)
    csv_store_dir="/tmp/csv_store_dir"
    app_exec_clean(csv_store_dir)
    os.makedirs(csv_store_dir, exist_ok=True)
    executionTime_0 = (time.time() - startTime_0)

    ##BLOCK_1
    startTime_1 = time.time()
    cdr_files = sorted(glob.glob(CDR_FILES_DIR + "/" + "**/" + "*" + "cdr.json", 
        recursive=True),key=os.path.getmtime, reverse=False)
    total_files_cdr = len(cdr_files)
    chunk_size = 3
    #for chunk in split_list(cdr_files, chunk_size):
        #print("chunk")
    print("Total files cdr's finded for load finded = " + str(total_files_cdr))
    #print(cdr_files)
    with Pool(4) as p:
        p.map(csv_create_single, cdr_files)
    #with ProcessPoolExecutor(8) as exe:
    #        _ = [exe.submit(csv_create, cdr_files, i) for i in range(5)]
        #print(chunk)
    #quit()
    # use as many threads as possible, default: min(32, os.cpu_count()+4)
    #with ThreadPoolExecutor() as executor:
    #    result = executor.map(csv_create2, cdr_files)
    logging.info('Took %s seconds', (time.time() - startTime_0))