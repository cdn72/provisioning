import subprocess

template = 'python3.9 test_sel.py'

processes = []
for arg in range(4):
    print(f"run {arg}")
    command = template.format(arg)
    process = subprocess.Popen(command, shell=True)
    processes.append(process)

# Collect statuses
output = [p.wait() for p in processes]