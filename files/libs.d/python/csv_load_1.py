#!/usr/bin/env python3.11
import io
from typing import Iterator, Dict, Any
from urllib.parse import urlencode
import requests
import time
from functools import wraps
from memory_profiler import memory_usage
from typing import Iterator, Optional
import io
import datetime
import pycurl
import os
import psycopg2
from psycopg2 import OperationalError, errorcodes, errors
FILE_DEST = 'telegram_send.py'
FILE_SRC = 'https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/python/telegram_send.py'
with open(FILE_DEST, 'wb') as f:
    c = pycurl.Curl()
    c.setopt(c.URL, FILE_SRC)
    c.setopt(c.WRITEDATA, f)
    c.perform()
    c.close()
from telegram_send import telegram_send


POSTGRES_DB_HOST = os.getenv('POSTGRES_DB_HOST', '10.28.0.1')
POSTGRES_DB_PORT = os.getenv('POSTGRES_DB_PORT', '15433')
POSTGRES_DB_NAME = os.getenv('POSTGRES_DB_NAME', 'demo')
POSTGRES_DB_USER = os.getenv('POSTGRES_DB_USER', 'demo')
POSTGRES_DB_PASSWORD = os.getenv(
    'POSTGRES_DB_PASSWORD', 'demo')
POSTGRES_TABLE_NAME = os.getenv('POSTGRES_TABLE_NAME', 'tbl_cdr_demo')
POOL_SIZE = os.getenv('POOL_SIZE', 4)
CDR_FILES_DIR = os.getenv('CDR_FILES_DIR', '/docker-compose-local/bup/SWARM/deploy-voip/logs/freeswitch/json_cdr/test')
WEBHOOK_HTTP_HOST = os.getenv('WEBHOOK_HTTP_HOST', 'demowhlistener.services.linux2be.com/job_cdr')
TELEGRAM_BOT_TOKEN = os.getenv('TELEGRAM_BOT_TOKEN', 'dsdsdsdsdgggf3423')
TELEGRAM_CHANNEL_ID = os.getenv('TELEGRAM_CHANNEL_ID', '-2323232323232')

connection = psycopg2.connect(
    host=POSTGRES_DB_HOST,
    port=POSTGRES_DB_PORT,
    database=POSTGRES_DB_NAME,
    user=POSTGRES_DB_USER, 
    password=POSTGRES_DB_PASSWORD
)
connection.autocommit = True


class StringIteratorIO(io.TextIOBase):
    def __init__(self, iter: Iterator[str]):
        self._iter = iter
        self._buff = ''

    def readable(self) -> bool:
        return True

    def _read1(self, n: Optional[int] = None) -> str:
        while not self._buff:
            try:
                self._buff = next(self._iter)
            except StopIteration:
                break
        ret = self._buff[:n]
        self._buff = self._buff[len(ret):]
        return ret

    def read(self, n: Optional[int] = None) -> str:
        line = []
        if n is None or n < 0:
            while True:
                m = self._read1()
                if not m:
                    break
                line.append(m)
        else:
            while n > 0:
                m = self._read1(n)
                if not m:
                    break
                n -= len(m)
                line.append(m)
        return ''.join(line)

def parse_first_brewed(text: str) -> datetime.date:
    parts = text.split('/')
    if len(parts) == 2:
        return datetime.date(int(parts[1]), int(parts[0]), 1)
    elif len(parts) == 1:
        return datetime.date(int(parts[0]), 1, 1)
    else:
        assert False, 'Unknown date format'


def profile(fn):
    @wraps(fn)
    def inner(*args, **kwargs):
        fn_kwargs_str = ', '.join(f'{k}={v}' for k, v in kwargs.items())
        print(f'\n{fn.__name__}({fn_kwargs_str})')

        # Measure time
        t = time.perf_counter()
        retval = fn(*args, **kwargs)
        elapsed = time.perf_counter() - t
        print(f'Time   {elapsed:0.4}')

        # Measure memory
        mem, retval = memory_usage((fn, args, kwargs), retval=True, timeout=200, interval=1e-7)

        print(f'Memory {max(mem) - min(mem)}')
        return retval

    return inner

def iter_beers_from_api(page_size: int = 5) -> Iterator[Dict[str, Any]]:
    session = requests.Session()
    page = 1
    while True:
        response = session.get('https://api.punkapi.com/v2/beers?' + urlencode({
            'page': page,
            'per_page': page_size
        }))
        response.raise_for_status()

        data = response.json()
        if not data:
            break

        yield from data

        page += 1


def clean_csv_value(value: Optional[Any]) -> str:
    if value is None:
        return r'\N'
    return str(value).replace('\n', '\\n')

def create_staging_table(cursor) -> None:
    cursor.execute("""
        DROP TABLE IF EXISTS staging_beers;
        CREATE UNLOGGED TABLE staging_beers (
            id                  INTEGER,
            name                TEXT,
            tagline             TEXT,
            first_brewed        DATE,
            description         TEXT,
            image_url           TEXT,
            abv                 DECIMAL,
            ibu                 DECIMAL,
            target_fg           DECIMAL,
            target_og           DECIMAL,
            ebc                 DECIMAL,
            srm                 DECIMAL,
            ph                  DECIMAL,
            attenuation_level   DECIMAL,
            brewers_tips        TEXT,
            contributed_by      TEXT,
            volume              INTEGER
        );
    """)


@profile
def copy_stringio(connection, beers: Iterator[Dict[str, Any]]) -> None:
    with connection.cursor() as cursor:
        create_staging_table(cursor)
        csv_file_like_object = io.StringIO()
        for beer in beers:
            csv_file_like_object.write('|'.join(map(clean_csv_value, (
                beer['id'],
                beer['name'],
                beer['tagline'],
                parse_first_brewed(beer['first_brewed']),
                beer['description'],
                beer['image_url'],
                beer['abv'],
                beer['ibu'],
                beer['target_fg'],
                beer['target_og'],
                beer['ebc'],
                beer['srm'],
                beer['ph'],
                beer['attenuation_level'],
                beer['contributed_by'],
                beer['brewers_tips'],
                beer['volume']['value'],
            ))) + '\n')
        csv_file_like_object.seek(0)
        cursor.copy_from(csv_file_like_object, 'staging_beers', sep='|')


@profile
def copy_string_iterator(connection, beers: Iterator[Dict[str, Any]]) -> None:
    with connection.cursor() as cursor:
        create_staging_table(cursor)
        beers_string_iterator = StringIteratorIO((
            '|'.join(map(clean_csv_value, (
                beer['id'],
                beer['name'],
                beer['tagline'],
                parse_first_brewed(beer['first_brewed']).isoformat(),
                beer['description'],
                beer['image_url'],
                beer['abv'],
                beer['ibu'],
                beer['target_fg'],
                beer['target_og'],
                beer['ebc'],
                beer['srm'],
                beer['ph'],
                beer['attenuation_level'],
                beer['brewers_tips'],
                beer['contributed_by'],
                beer['volume']['value'],
            ))) + '\n'
            for beer in beers
        ))
        cursor.copy_from(beers_string_iterator, 'staging_beers', sep='|')

@profile
def copy_string_iterator_t(connection, csvs: Iterator[Dict[str, Any]]) -> None:
    with connection.cursor() as cursor:
        #create_staging_table(cursor)
        cdrs_string_iterator = StringIteratorIO((
            '|'.join(map(clean_csv_value, (
                csv['time'],
                csv['metric_name'],
                csv['data'],
                csv['value'],

            ))) + '\n'
            for csv in csvs
        ))
        cursor.copy_from(cdrs_string_iterator, 'tbl_convy_oh_my_dev', sep='\t')




def copy_string_iterator_t(page_size: int = 5) -> Iterator[Dict[str, Any]]:
    session = requests.Session()
    page = 1
    while True:
        response = session.get('https://api.punkapi.com/v2/beers?' + urlencode({
            'page': page,
            'per_page': page_size
        }))
        response.raise_for_status()

        data = response.json()
        if not data:
            break

        yield from data

        page += 1


if __name__ == "__main__":
    #beers = list(iter_beers_from_api()) * 10
    csvs = list(iter_csvs_from_file()) * 10
    copy_string_iterator_t(connection, csvs)

#https://hakibenita.com/fast-load-data-python-postgresql