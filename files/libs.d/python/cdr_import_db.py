#!/usr/bin/env python3.9
import multiprocessing
import os
import glob
import logging
import sys
import psycopg2
import multiprocessing
from multiprocessing import Pool
import glob
import re
import gitlab
import json
import time
import datetime
from datetime import datetime, timedelta
from time import gmtime, strftime
from progress.bar import IncrementalBar
import tqdm
import sys
from requests import get
import pycurl
FILE_DEST = 'telegram_send.py'
FILE_SRC = 'https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/python/telegram_send.py'
with open(FILE_DEST, 'wb') as f:
    c = pycurl.Curl()
    c.setopt(c.URL, FILE_SRC)
    c.setopt(c.WRITEDATA, f)
    c.perform()
    c.close()
from telegram_send import telegram_send

POSTGRES_DB_HOST = os.getenv('POSTGRES_DB_HOST', '10.28.0.1')
POSTGRES_DB_PORT = os.getenv('POSTGRES_DB_PORT', '15433')
POSTGRES_DB_NAME = os.getenv('POSTGRES_DB_NAME', 'demo')
POSTGRES_DB_USER = os.getenv('POSTGRES_DB_USER', 'demo')
POSTGRES_DB_PASSWORD = os.getenv(
    'POSTGRES_DB_PASSWORD', 'demo')
POSTGRES_TABLE_NAME = os.getenv('POSTGRES_TABLE_NAME', 'tbl_cdr_demo')
POOL_SIZE = os.getenv('POOL_SIZE', 4)
CDR_FILES_DIR = os.getenv('CDR_FILES_DIR', '/docker-compose-local/bup/SWARM/deploy-voip/logs/freeswitch/json_cdr/test')
WEBHOOK_HTTP_HOST = os.getenv('WEBHOOK_HTTP_HOST', 'demowhlistener.services.linux2be.com/job_cdr')
TELEGRAM_BOT_TOKEN = os.getenv('TELEGRAM_BOT_TOKEN', 'dsdsdsdsdgggf3423')
TELEGRAM_CHANNEL_ID = os.getenv('TELEGRAM_CHANNEL_ID', '-2323232323232')


# define logger
class MyFormatter(logging.Formatter):
    def format(self, record):
        datefmt = "%Y-%m-%d %H:%M:%S"
        if record.levelno == logging.DEBUG:
            if record.__dict__.get("host"):
                return logging.Formatter(fmt="{asctime} [{levelname:<5}] ({funcName}:{lineno}) {host}: {message}",
                                         datefmt=datefmt, style="{").format(record)
            else:
                return logging.Formatter(fmt="{asctime} [{levelname:<5}] ({funcName}:{lineno}): {message}",
                                         datefmt=datefmt, style="{").format(record)
        if record.__dict__.get("host"):
            return logging.Formatter(fmt="{asctime} [{levelname:<5}] {host}: {message}", datefmt=datefmt,
                                     style="{").format(record)
        else:
            return logging.Formatter(fmt="{asctime} [{levelname:<5}]: {message}", datefmt=datefmt, style="{").format(
                record)


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
stream_handler = logging.StreamHandler()
debug_formatter = MyFormatter()
stream_handler.setFormatter(debug_formatter)
logger.addHandler(stream_handler)


def prepare_postgresqldb(postgresql_TABLE_NAME):
    try:
        con = psycopg2.connect(host=POSTGRES_DB_HOST, port=POSTGRES_DB_PORT, database=POSTGRES_DB_NAME,
                               user=POSTGRES_DB_USER, password=POSTGRES_DB_PASSWORD
                               )
        con.autocommit = True
    except:
        print('Cant connect to db', file=sys.stderr)
    with con:
        try:
            cur = con.cursor()
            query_1 = "CREATE TABLE IF NOT EXISTS %s (time TIMESTAMPTZ NOT NULL,metric_name text not null,data JSONB,value double precision,PRIMARY KEY (metric_name, time),partition by range (time) )" % postgresql_TABLE_NAME
            cur.execute(query_1, )

        except:
            print('TABLE ' + postgresql_TABLE_NAME + " exist", file=sys.stderr)
            pass
        try:
            cur = con.cursor()
            query_2 = "CREATE INDEX CONCURRENTLY  IF NOT EXISTS %s_idx_data ON %s USING GIN (data)" % postgresql_TABLE_NAME, postgresql_TABLE_NAME
            cur.execute(query_2, )
        except:
            print('INDEX ON '  + postgresql_TABLE_NAME + 'with name ' + postgresql_TABLE_NAME + '_idx_data' + " exist", file=sys.stderr)
            pass
        try:
            postgresql_p_start_partition = datetime.today().date().replace(month=1, day=1).strftime("%Y"+"-%m"+"-%d")
            cur = con.cursor()
            query_3 = "SELECT partman.create_parent('public.%s', 'time', 'native', 'daily',p_start_partition := '%s' )" % postgresql_TABLE_NAME, postgresql_p_start_partition
            cur.execute(query_3, )
        except:
            print('Partition on  ' + postgresql_TABLE_NAME + " exist", file=sys.stderr)
            pass                        


def send_postgresql(send_time, metric_name, data, value, pg_table_name):
    # print("Send " + str(data))
    json_data = json.dumps(data, skipkeys=True, sort_keys=True, indent=4)
    try:
        con = psycopg2.connect(host=POSTGRES_DB_HOST, port=POSTGRES_DB_PORT, database=POSTGRES_DB_NAME,
                               user=POSTGRES_DB_USER, password=POSTGRES_DB_PASSWORD
                               )
        con.autocommit = True
    except:
        print('Cant connect to db', file=sys.stderr)
    pg_timestamp = send_time
    with con:
        try:
            cur = con.cursor()
            query = "INSERT INTO %s (time, metric_name,data,value) VALUES (%%s, %%s, %%s, %%s)" % pg_table_name
            cur.execute(query, (pg_timestamp, metric_name, json_data, value))
        except Exception:
            pass
            print(" Record exist ", file=sys.stderr)


def get_postgresql_cdr_count(pg_table_name, limit=1):
    try:
        con = psycopg2.connect(host=POSTGRES_DB_HOST, port=POSTGRES_DB_PORT, database=POSTGRES_DB_NAME,
                               user=POSTGRES_DB_USER, password=POSTGRES_DB_PASSWORD
                               )
        con.autocommit = True
    except:
        print('Cant connect to db', file=sys.stderr)
    with con:
        try:
            cur = con.cursor()
            print("exec count in " + str(pg_table_name))
            query = "select count(*) from %s" % pg_table_name
            cur.execute(query, dict(limit=limit))
            pg_data = cur.fetchone()[0]
        except Exception:
            logging.exception("select from postgres db failed on {}".format(asu_config.get('db_name')))
            print(" error get records from db ", file=sys.stderr)
            pg_data = "error get records from db"
    return pg_data

def process(file):
    try:
        file_name = re.sub(relevant_path + "/", '', str(file), 1)
        f = open(file)
        data = json.load(f)
        cdr_start_epoch = data['variables']['start_epoch']
        cdr_start_date = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(int(cdr_start_epoch)))
        f.close()
        send_time = cdr_start_date
        head, tail = os.path.split(file_name)
        metric_name = str(tail)
        data = data
        value = "1"
        send_postgresql(send_time, metric_name, data, value, pg_table_name)
    except Exception as e:
        pass
        logger.exception("Exception on : {}".format(file))


if __name__ == "__main__":
    pg_table_name = POSTGRES_TABLE_NAME
    relevant_path = CDR_FILES_DIR
    included_extensions = 'cdr.json'

    cdr_files = sorted(glob.glob(relevant_path + "/" + "**/" + "*" + included_extensions, recursive=True),
                       key=os.path.getmtime, reverse=True)
    total_files = len(cdr_files)
    bar = IncrementalBar('Countdown', max=len(cdr_files))
    print("Begin import files from " + str(relevant_path) + " to database " + POSTGRES_DB_NAME + " into table " + str(
        pg_table_name) + ". Total files = " + str(total_files))
    prepare_postgresqldb(pg_table_name)
    pool = Pool(processes=int(POOL_SIZE))
    cdr_records_count_before = get_postgresql_cdr_count(str(POSTGRES_TABLE_NAME))
    t_text = "Cdr's import started(py_pool_size=%(py_pool_size)s). Total file = %(total_files)s \
    Total files in table before = %(cdr_records_count_before)s \
    Table=%(pg_table_name)s Db=%(pg_db_host)s:%(pg_db_port)s \
    source path %(cdr_files_dir)s. External server ip %(server_ip_external)s \
    " % { \
        "total_files": str(total_files), \
        "pg_db_host": str(POSTGRES_DB_HOST), \
        "pg_db_port": str(POSTGRES_DB_PORT), \
        "cdr_files_dir": str(CDR_FILES_DIR), \
        "server_ip_external": str(get('http://icanhazip.com').text), \
        "py_pool_size": str(POOL_SIZE), \
        "cdr_records_count_before": str(cdr_records_count_before), \
        "pg_table_name": str(POSTGRES_TABLE_NAME)}
    telegram_send(TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_ID, t_text)
    for _ in tqdm.tqdm(pool.imap_unordered(process, cdr_files), total=total_files):
        pass
    cdr_records_count_current = get_postgresql_cdr_count(str(POSTGRES_TABLE_NAME))
    t_text = "Cdr's import finished(py_pool_size=%(py_pool_size)s). Total file  imported = %(total_files)s. \
    Total files in table current = %(cdr_records_count_current)s \
    Table=%(pg_table_name)s Db=%(pg_db_host)s:%(pg_db_port)s \
    source path %(cdr_files_dir)s. External server ip %(server_ip_external)s \
    " % { \
        "total_files": str(total_files), \
        "pg_db_host": str(POSTGRES_DB_HOST), \
        "pg_db_port": str(POSTGRES_DB_PORT), \
        "cdr_files_dir": str(CDR_FILES_DIR), \
        "server_ip_external": str(get('http://icanhazip.com').text), \
        "py_pool_size": str(POOL_SIZE), \
        "cdr_records_count_current": str(cdr_records_count_current), \
        "pg_table_name": str(POSTGRES_TABLE_NAME)}
    telegram_send(TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_ID, t_text)