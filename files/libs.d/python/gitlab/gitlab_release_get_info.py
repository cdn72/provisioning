#!/usr/bin/env python3.11
import os
import time
import json
import argparse
import sys
import re
import subprocess
import jsontree
import datetime
import logging
import psycopg2
from psycopg2 import Error
from flask import Flask
from flask import Flask, request, current_app, g, jsonify, request_finished, make_response, abort
import tornado
import tornado.web
import tornado.options
from ipaddress import IPv4Address
import ipaddr, ipaddress
import tempfile
import uuid
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from urllib.parse import urlencode
from urllib.request import Request, urlopen
import requests
import memorize
from json import dumps
import datetime
from datetime import datetime, timedelta
import sys
import threading
from termcolor import colored, cprint

print_red_on_cyan = lambda x: cprint(x, 'white', 'on_cyan')
print_red_on_green = lambda x: cprint(x, 'white', 'on_green')
print_red_on_blue = lambda x: cprint(x, 'white', 'on_blue')
print_red_on_red = lambda x: cprint(x, 'white', 'on_red')
print_info = lambda x: cprint(x, 'white', 'on_yellow')
print_warn = lambda x: cprint(x, 'red', 'on_yellow')

def gitlab_release_get_info(ci_data,app_version):
    try:
        gitlab_release_get_info_domain = ci_data[0]['GITLAB_RELEASE_GET_INFO_DOMAIN']
    except KeyError:
        gitlab_release_get_info_domain = None
    try:
        gitlab_release_get_info_token = ci_data[0]['GITLAB_RELEASE_GET_INFO_TOKEN']
    except KeyError:
        gitlab_release_get_info_token = None
    try:
        gitlab_release_get_info_project_id = ci_data[0]['GITLAB_RELEASE_GET_INFO_PROJECT_ID']
    except KeyError:
        gitlab_release_get_info_project_id = None

    gitlab_host = ci_data[0]['GITLAB_HOST']
    gitlab_project_id = ci_data[0]['GITLAB_PROJECT_ID']
    gitlab_private_token = ci_data[0]['GITLAB_PRIVATE_TOKEN']

    if gitlab_release_get_info_domain is not None and gitlab_release_get_info_token is not None and  gitlab_release_get_info_project_id is not None:
        try:
            gitlab_project_releases_url = "https://" + gitlab_release_get_info_domain + "/api/v4/projects/" + str(gitlab_release_get_info_project_id) + "/releases/" + str(app_version)
            print_red_on_cyan(f"gitlab_project_releases_url={gitlab_project_releases_url}")
            print_red_on_cyan(f"gitlab_release_get_info_token={gitlab_release_get_info_token}")
            get_gitlab_release_info = json.loads(gitlab_get(gitlab_release_get_info_token,gitlab_project_releases_url))
            print_red_on_cyan("== get_gitlab_release_info from mirror ==")
            print_red_on_cyan(str(get_gitlab_release_info))
            print_red_on_cyan("== get_gitlab_release_info from mirror ==")
            gitlab_release_info_desc = str(get_gitlab_release_info['description']).replace('\n*' , '\n').replace('Bug Fixes' , '*Bug Fixes* ').replace('Features' , '*Features* ')  
            t_text_e = "".join([s for s in gitlab_release_info_desc.splitlines(True) if s.strip("\r\n")])
            print_red_on_red(t_text_e)
        except KeyError:
            t_text_e = "Error get release info with additional gitlab_release_get_info_ vars" 
    else:
        try:
            gitlab_project_releases_url = "https://" + gitlab_host + "/api/v4/projects/" + str(gitlab_project_id) + "/releases/" + str(app_version)
            headers = {'PRIVATE-TOKEN': gitlab_private_token}
            r_gitlab_release_info = requests.get(gitlab_project_releases_url, headers=headers, timeout=(5.05, 20))
            get_gitlab_release_info = json.loads(r_gitlab_release_info.content)
            print_red_on_cyan("==get_gitlab_release_info==")
            print_red_on_cyan(str(get_gitlab_release_info))
            print_red_on_cyan("==get_gitlab_release_info==")
            if "200" in str(r_gitlab_release_info.status_code):
                gitlab_release_info_desc = str(get_gitlab_release_info['description'].replace('\n*' , '\n').replace('Bug Fixes' , '*Bug Fixes* ').replace('Features' , '*Features* '))
                t_text_e = "".join([s for s in gitlab_release_info_desc.splitlines(True) if s.strip("\r\n")])
                print_red_on_green(t_text_e)
            else:
                t_text_e = " ✔ Release info not exist"
        except KeyError:
            t_text_e = " ✔ Release info not exist "
    print_red_on_green(f"t_text_e_returned={t_text_e}")
    return t_text_e


ci_data =
app_version =
print(gitlab_release_get_info(ci_data,app_version))