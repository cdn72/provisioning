#!/usr/bin/env python3.9
from __future__ import print_function
import csv
import json
import os
import sys
import argparse
from elasticsearch import Elasticsearch, RequestsHttpConnection
# auto install module if not exists
import urllib3
import re
# Отключаем warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

index_name="nginxaccess-2021.08.17"
dst_file="/tmp"
eprint = print
ES_HOST = os.getenv('ES_HOST', 'test.local')
ES_PORT = os.getenv('ES_PORT', '443')
ES_HOST_SHCEME = os.getenv('ES_HOST_SHCEME', 'https')
ES_USER = os.getenv('ES_USER', 'sdsdsd')
ES_PASSWORD = os.getenv('ES_PASSWORD', 'ssdsdsdsdnnd')
es_con = ES_HOST_SHCEME + ':' + '//' + ES_HOST + ':' + ES_PORT
es_cred = ES_USER, ES_PASSWORD
csv_filename = dst_file + "/" + index_name + ".csv"
portion=20

eprint("connecting to ES...")
es = Elasticsearch(
    [es_con],
    http_auth=(es_cred),
    scheme=ES_HOST_SHCEME,
    ca_certs=False,
    verify_certs=True,
    maxsize=25,
    connection_class=RequestsHttpConnection
)

doc = {
    "_source": {
        "includes": ["@timestamp", "@timereported", "@log_name", "nginx.remote_addr", "nginx.http_host", "nginx.uri",
                     "nginx.user_agent", "nginx.request_method", "nginx.status", "nginx.request_uri"]
    },
     "size": portion,
     "query" : { "wildcard": {"""nginx.http_host""": "bs.foxcom.lv*"}
            }
        }

###   "query": {
###       "bool": {
###           "must": [
###               {"match": {"nginx.http_host": "bs.foxcom.lv"}},
###               {"wildcard": {"nginx.uri.keyword": "/api/sms/receive/*"}}
###           ],
###
###           "must_not": [
###
###               {"match": {"nginx.status": "400"}}
###
###           ]
###
###       }
###   }
###


def es2csv(index_name, dst_file=None):
    count = 0
    eprint("export index: {} into file: {}".format(index_name, csv_filename))
    try:
        print("Try  export index " +  str(index_name) + " to file " + str(csv_filename))
        res = es.search(index=index_name, body=doc, keep_alive=5d, scroll='10s')
        print("res ok")
        print("Total records " + str(res['total'])) 
    except:
        eprint("ERROR: can not export index: {}".format(index_name, csv_filename))
        return False

    try:
        print("Try open file " + str(csv_filename))
        csvfile = open(csv_filename, 'w')
    except Exception as e:
        eprint("ERROR: Can not open file {} for writing".format(csv_filename))
        eprint(e)
        return False


    total_records = res['hits']['total']


    csv_writer = csv.writer(
        csvfile,
        delimiter=',',
        quotechar='"',
        quoting=csv.QUOTE_MINIMAL
    )
    # print
    eprint('\r{}: {} from {}'.format(index_name, count, total_records), end='')

    # Write to file search result

    while len(res['hits']['hits']) > 0:

        scroll_id = res['_scroll_id']
        rr = res['hits']['hits']

        csv_filename_new = "csv_" + csv_filename
        csv_columns = ["@timestamp", "@timereported", "@log_name", "nginx.remote_addr", "nginx.http_host", "nginx.uri",
                       "nginx.user_agent", "nginx.request_method", "nginx.status", "nginx.request_uri"]
        with open(csv_filename_new, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns, delimiter='|')
            # writer.writeheader()
            for document in [x['_source'] for x in res['hits']['hits']]:
                writer.writerow(document)

        ####for i in rr:
        ####csv_writer.writerow([
        ##### i['_source']['@timestamp'],
        ##### i['_source']['@log_name'],
        ##### i['_id'],
        ##### i['_index'],
        #####json.dumps(i['_source']).replace(r'\u', r'\\u')
        ####json.dumps(i['_source'])
        #####i['_source']
        ####])
        count += len(rr)
        # Print
        eprint('\r{}: {} from {}'.format(index_name, count, total_records), end='')

        res = es.scroll(scroll_id=scroll_id, scroll='1m')

    if not csvfile.closed:
        csvfile.close()
    eprint()
    eprint("Exported: {} records".format(count))
    return True


def main():
    csv_filename = '{}.csv'.format(index_name)
    print("csv_filename " + str(csv_filename))
    try:
        es2csv(index_name, csv_filename)
        # json_to_csv_conv(csv_filename)
    except:
        print("Error")
        eprint("removing {}".format(csv_filename))
        #os.unlink(csv_filename)


if __name__ == "__main__":
    main()
