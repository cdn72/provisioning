#!/usr/bin/env python3.9
#-*- coding: utf-8 -*-
import os

ES_HOST = os.getenv('ES_HOST', 'test.local')
ES_PORT = os.getenv('ES_PORT', '443')
ES_HOST_SHCEME = os.getenv('ES_HOST_SHCEME', 'https')
ES_USER = os.getenv('ES_USER', 'sdsdsd')
ES_PASSWORD = os.getenv('ES_PASSWORD', 'ssdsdsdsdnnd')
es_con = ES_HOST_SHCEME + ':' + '//' + ES_HOST + ':' + ES_PORT
es_cred = ES_USER, ES_PASSWORD
portion=100
index='laravel-2021.08.17'
doc = {
    "size": 300,
    "_source": {
        "includes": ["@timestamp", "@log_name", "message"]
    },

    "query": {"wildcard": {"""level""": "production*"}
              }
}
import sys, time, io
start_time = time.time()

if sys.version[0] != "3":
    print ("\nThis script requires Python 3")
    print ("Please run the script using the 'python3' command.\n")
    quit()

try:
    # import the Elasticsearch low-level client library
    from elasticsearch import Elasticsearch, RequestsHttpConnection

    # import Pandas, JSON, and the NumPy library
    import pandas, json
    import numpy as np

except ImportError as error:
    print ("\nImportError:", error)
    print ("Please use 'pip3' to install the necessary packages.")
    quit()

# create a client instance of the library
print ("\ncreating client instance of Elasticsearch")


elastic_client = Elasticsearch(
    [es_con],
    http_auth=(es_cred),
    scheme=ES_HOST_SHCEME,
    ca_certs=False,
    verify_certs=True,
    #maxsize=25,
    connection_class=RequestsHttpConnection
)

"""
MAKE API CALL TO CLUSTER AND CONVERT
THE RESPONSE OBJECT TO A LIST OF
ELASTICSEARCH DOCUMENTS
"""
# total num of Elasticsearch documents to get with API call
total_docs = 20
print ("\nmaking API call to Elasticsearch for", total_docs, "documents.")
response = elastic_client.search(
    index=index,
    body=doc,
    scroll='10s'  # length of time to keep search context
   # size=total_docs
)

# grab list of docs from nested dictionary response
#print ("putting documents in a list")
elastic_docs = response["hits"]["hits"]


"""
GET ALL OF THE ELASTICSEARCH
INDEX'S FIELDS FROM _SOURCE
"""
#  create an empty Pandas DataFrame object for docs
docs = pandas.DataFrame()

# iterate each Elasticsearch doc in list
print ("\ncreating objects from Elasticsearch data.")
doc_count = 0
for num, doc in enumerate(elastic_docs):

    # get _source data dict from document
    source_data = doc["_source"]

    # get _id from document
    _id = doc["_index"]
    doc_count += 1
    # create a Series object from doc dict object
    doc_data = pandas.Series(source_data, name = doc_count)
    print(doc_data)

    # append the Series object to the DataFrame object
    docs = docs.append(doc_data)


"""
EXPORT THE ELASTICSEARCH DOCUMENTS PUT INTO
PANDAS OBJECTS
"""
print ("\nexporting Pandas objects to different file types.")

# export the Elasticsearch documents as a JSON file
docs.to_json("objectrocket.json")

# have Pandas return a JSON string of the documents
json_export = docs.to_json() # return JSON data
#print ("\nJSON data:", json_export)

# export Elasticsearch documents to a CSV file
docs.to_csv("objectrocket.csv", "|") # CSV delimited by commas

#### export Elasticsearch documents to CSV
###csv_export = docs.to_csv(sep=",") # CSV delimited by commas
###print ("\nCSV data:", csv_export)

# create IO HTML string
import io
html_str = io.StringIO()

# export as HTML
docs.to_html(
    buf=html_str,
    classes='table table-striped'
)

# print out the HTML table
#print (html_str.getvalue())

# save the Elasticsearch documents as an HTML table
docs.to_html("objectrocket.html")

print ("\n\ntime elapsed:", time.time()-start_time)