#!/usr/bin/env python3.11
import os
import shutil
import glob
import logging
import psycopg2
from psycopg2 import OperationalError, errorcodes, errors, sql
from psycopg2.errorcodes import UNIQUE_VIOLATION
import multiprocessing
from multiprocessing import Pool, Process
import re
import gitlab
import json
import time
import datetime
from datetime import datetime, timedelta
from time import gmtime, strftime
from progress.bar import IncrementalBar
import tqdm
from tqdm.contrib.concurrent import process_map
import sys
from requests import get
import pycurl
import csv
import threading
import numpy as np
import pandas
from io import StringIO
from sqlalchemy import create_engine
import traceback
from typing import Iterator, Optional, Dict, Any
from pathlib import Path
import fcntl
import subprocess
import shutil
FILE_DEST = 'telegram_send.py'
FILE_SRC = 'https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/python/telegram_send.py'
with open(FILE_DEST, 'wb') as f:
    c = pycurl.Curl()
    c.setopt(c.URL, FILE_SRC)
    c.setopt(c.WRITEDATA, f)
    c.perform()
    c.close()
from telegram_send import telegram_send

# Vars
POSTGRES_DB_HOST = os.getenv('POSTGRES_DB_HOST', '10.28.0.1')
# POSTGRES_DB_HOST = '10.38.0.1'
POSTGRES_DB_PORT = os.getenv('POSTGRES_DB_PORT', '15433')
POSTGRES_DB_NAME = os.getenv('POSTGRES_DB_NAME', 'demo')
POSTGRES_DB_USER = os.getenv('POSTGRES_DB_USER', 'demo')
POSTGRES_DB_PASSWORD = os.getenv(
    'POSTGRES_DB_PASSWORD', 'demo')
POSTGRES_TABLE_NAME = os.getenv('POSTGRES_TABLE_NAME', 'tbl_cdr_demo')
POOL_SIZE = os.getenv('POOL_SIZE', 4)
# CDR_FILES_DIR = os.getenv('CDR_FILES_DIR', '/mnt/hdd/freeswitch/json_cdr_oh_my/2022-12-28-00-00-01')
CDR_FILES_DIR = os.getenv(
    'CDR_FILES_DIR', '/json_cdr')
WEBHOOK_HTTP_HOST = os.getenv(
    'WEBHOOK_HTTP_HOST', 'demowhlistener.services.linux2be.com/job_cdr')
TELEGRAM_BOT_TOKEN = os.getenv('TELEGRAM_BOT_TOKEN', 'dsdsdsdsdgggf3423')
TELEGRAM_CHANNEL_ID = os.getenv('TELEGRAM_CHANNEL_ID', '-2323232323232')

# Logger
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger('requests').setLevel(logging.CRITICAL)
logger = logging.getLogger(__name__)
json_failed_store_dir = os.getenv(
    'CDR_FILES_FAILED_STORE_DIR', '/tmp/json_failed')
def send_postgresql_csv(file):
    startTime_load = time.time()
    # print("Started load file  " + file + " Time = " + str(startTime_load))
    file_name = re.sub(csv_store_dir + "/", '', str(file),
                       2).replace('.csv', '').replace('-', '_')
    try:
        connection = psycopg2.connect(
            host=POSTGRES_DB_HOST,
            port=POSTGRES_DB_PORT,
            database=POSTGRES_DB_NAME,
            user=POSTGRES_DB_USER,
            password=POSTGRES_DB_PASSWORD,
            connect_timeout=3,
            keepalives=1,
            keepalives_idle=30,
            keepalives_interval=10,
            keepalives_count=5
        )
        connection.autocommit = True
    except:
        print('Cant connect to db ' + str(POSTGRES_DB_NAME), file=sys.stderr)
        logging.exception("Can't connect to database")
        raise
    with connection:
        try:
            cur = connection.cursor()
            copy_sql = """
            DROP TABLE IF EXISTS {temp_table_name};
            CREATE TABLE IF NOT EXISTS {temp_table_name} (
                time TIMESTAMPTZ NOT NULL,
                metric_name text not null,
                data JSONB,
                value double precision
            );
            COPY {temp_table_name} FROM STDIN csv QUOTE E'\\'' DELIMITER E'\\t';
            merge into {table_name} sda   
            using {temp_table_name} sdn
            on sda.metric_name = sdn.metric_name
            when matched then
                DO NOTHING
            when not matched then
                insert (time, metric_name, data,value)
                values (sdn.time,sdn.metric_name,sdn.data,sdn.value );
            DROP TABLE {temp_table_name};
            """.format(
                temp_table_name=file_name,
                table_name=pg_table_name
            )
            with open(file, 'r') as f:
                cur.copy_expert(sql=copy_sql, file=f)
                connection.commit()
                cur.close()
                f.close()
                if os.path.isfile(file):
                    os.remove(file)
                executionTime_load = (time.time() - startTime_load)
                print("Ending loading file " + file +
                      " Loaded was removed. Time spent : " + str(executionTime_load))

        except Exception:
            print('Cant process data to db ' +
                  str(POSTGRES_DB_NAME), file=sys.stderr)
            logging.exception("Can't connect to database")
            raise


def get_postgresql_cdr_count(pg_table_name, limit=1):
    try:
        connection = psycopg2.connect(
            host=POSTGRES_DB_HOST,
            port=POSTGRES_DB_PORT,
            database=POSTGRES_DB_NAME,
            user=POSTGRES_DB_USER,
            password=POSTGRES_DB_PASSWORD,
            connect_timeout=3
        )
        connection.autocommit = True
    except:
        print('Cant connect to db ' + str(POSTGRES_DB_NAME), file=sys.stderr)
        logging.exception("Can't connect to database")
        raise
    with connection:
        try:
            cur = connection.cursor()
            sql = """
            select count(*) from {table_name}
            """.format(
                table_name=pg_table_name
            )
            cur.execute(sql, dict(limit=limit))
            pg_data = cur.fetchone()[0]
            cur.close()
        except Exception:
            logging.exception("select from postgres db failed on {}".format(POSTGRES_DB_NAME))
            print(" error get records from db ", file=sys.stderr)
            pg_data = "error get records from db"
    return pg_data


def app_exec_clean(csv_store_dir):
    for file_d in Path(csv_store_dir).glob('*'):
        try:
            file_d.unlink()
        except OSError as e:
            print("Error: %s : %s" % (file_d, e.strerror))


def csv_create_single(file):

    try:
        file_name = re.sub(relevant_path + "/", '', str(file), 2)
        head, tail = os.path.split(file_name)
        file_name_short = tail 
        with open(file, 'r') as f:
            metric_name = str(tail)
            data = json.load(f)
            value = "3"
            send_time_source = data['variables']['start_epoch']
            send_time = time.strftime(
                "%Y-%m-%d %H:%M:%S", time.gmtime(int(send_time_source)))
            f.close()
            target_file = time.strftime(
                "%Y-%m-%d-%H", time.gmtime(int(send_time_source))) + "00"
            target_file_full_path = csv_store_dir + '/cdr_load_file_' + \
                pg_table_name + '_' + str(target_file) + '.csv'
            with open(target_file_full_path, 'a') as fileObj:
                fcntl.flock(fileObj, fcntl.LOCK_EX)
                writerObj = csv.writer(
                    fileObj, delimiter="\t", quotechar='\'', quoting=csv.QUOTE_MINIMAL)
                writerObj.writerow([send_time, str(metric_name), json.dumps(
                    data, skipkeys=True, sort_keys=True), str(value)])
                fcntl.flock(fileObj, fcntl.LOCK_UN)
    except Exception as e:
        pass
        print(f"Try copy failed file {file} to {json_failed_store_dir}/{file_name_short}")
        shutil.copy2(file, json_failed_store_dir + '/' + str(file_name_short), follow_symlinks=True)
        print("Exception  : {}".format(e) + " File = {}".format(f))
        logger.exception("Logger Exception  : {}".format(e))


def du(path):
    return subprocess.check_output(['du', '-sh', path]).split()[0].decode('utf-8')


def remove_empty_folders(path_abs):
    walk = list(os.walk(path_abs))
    for path, _, _ in walk[::-1]:
        if len(os.listdir(path)) == 0:
            shutil.rmtree(path)


if __name__ == "__main__":

    # BLOCK_0
    # pool = Pool(processes=int(POOL_SIZE))
    startTime_0 = time.time()
    pg_table_name = POSTGRES_TABLE_NAME
    relevant_path = CDR_FILES_DIR
    csv_store_dir = "/tmp/csv_store_dir"
    shutil.rmtree(csv_store_dir, ignore_errors=True, onerror=None)
    shutil.rmtree(json_failed_store_dir, ignore_errors=True, onerror=None)
    os.makedirs(csv_store_dir, exist_ok=True)
    os.makedirs(json_failed_store_dir, exist_ok=True)
    print("Relevant_path: " + relevant_path)
    print("Csv_store_dir: " + csv_store_dir +
          ". csv_store_dir size:" + str(du(csv_store_dir)))
    print("Target table: " + pg_table_name)
    executionTime_0 = (time.time() - startTime_0)

    # BLOCK_1
    startTime_1 = time.time()
    print("Get cdr's files at dir:" + str(CDR_FILES_DIR))
    cdr_files = sorted(glob.glob(CDR_FILES_DIR + "/" + "**/" + "*" + "cdr.json",
                                 recursive=True), key=os.path.getmtime, reverse=False)
    total_files_cdr = len(cdr_files)
    if total_files_cdr > 0:
        app_exec_clean(csv_store_dir)
        cdr_records_count_before = get_postgresql_cdr_count(pg_table_name)
        executionTime_1_1 = (time.time() - startTime_1)
        print("Time for cdr_files init : " + str(executionTime_1_1) + " Total files cdr's for load finded : " +
              str(total_files_cdr) + " Total recodrs cdr's in database before load : " + str(cdr_records_count_before))

        startTime_1_2 = time.time()
        # pool = Pool(processes=int(POOL_SIZE))
        # for _ in tqdm.tqdm(pool.imap_unordered(csv_create_single, cdr_files), total=total_files_cdr):
        #    pass
        print("Start creating csv's. " + str(time.strftime("%Y-%m-%d %H:%M")))
        with Pool(int(POOL_SIZE)) as p:
            p.map(csv_create_single, cdr_files)
        p.close()
        p.join()
        executionTime_1_2 = (time.time() - startTime_1_2)
        print("Time execution for creating csv's: " + str(executionTime_1_2) +
              " Cdr's files in csv's: " + str(len(cdr_files)))

        # BLOCK_2
        startTime_2 = time.time()
        csv_files = sorted(glob.glob(csv_store_dir + "/" + "*" + "csv", recursive=True),
                           key=os.path.getmtime, reverse=True)
        total_files_csv = len(csv_files)
        print("Total files csv's for load generated :" + str(total_files_csv))
        print("Start loading csv's  " + str(total_files_csv) +
              " Pool size: " + str(POOL_SIZE))
        with Pool(int(POOL_SIZE)) as p:
            p.map(send_postgresql_csv, csv_files)
        p.close()
        p.join()
        executionTime_2 = (time.time() - startTime_2)

        # BLOCK_3
        remove_empty_folders(relevant_path)
        cdr_records_count_current = get_postgresql_cdr_count(pg_table_name)
        cdr_files_failed = len(glob.glob(json_failed_store_dir + "/" + "**/" + "*" + "cdr.json",
                            recursive=True))
        cdrs_loaded_to_db = (int(cdr_records_count_current) - int(cdr_records_count_before))
        if cdrs_loaded_to_db < 0:
            cdrs_loaded_to_db = 0
        print("Cdrs in db before load: " + str(cdr_records_count_before))
        print("Cdrs in db after load: " + str(cdr_records_count_current))
        print("Csvs with cdr's generated and loaded to db :" + str(total_files_csv))
        print("Cdrs files failed load to db : " + str(cdr_files_failed))

        print("Cdrs loaded to db : " + str(cdrs_loaded_to_db))
        print("Time execution for prepare: " + str(executionTime_0))
        print("Time execution for create csv's from " +
              str(total_files_cdr) + " cdr's files: " + str(executionTime_1_2))
        print("Time execution for load csv: " + str(executionTime_2))
        print("Time execution total: " + str((time.time() - startTime_0)))
        t_text = 'test'
        #telegram_send(TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_ID, t_text)

    else:
        executionTime_total = (time.time() - startTime_0)
        print("Load skipped : due cdr files not exist in " +
              str(CDR_FILES_DIR) + " Count: " + str(len(cdr_files)))
        print("Total execution time: " + str(executionTime_total))
