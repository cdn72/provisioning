#!/usr/bin/env python3.9
#-*- coding: utf-8 -*-
from elasticsearch import Elasticsearch,RequestsHttpConnection, exceptions
from elasticsearch.serializer import JSONSerializer
import os
import json, time
import pandas, json
import numpy as np
import sqlalchemy

ES_HOST = os.getenv('ES_HOST', 'test.local')
ES_PORT = os.getenv('ES_PORT', '443')
ES_HOST_SHCEME = os.getenv('ES_HOST_SHCEME', 'https')
ES_USER = os.getenv('ES_USER', 'sdsdsd')
ES_PASSWORD = os.getenv('ES_PASSWORD', 'ssdsdsdsdnnd')
ES_INDEX_SCROLL_SIZE = os.getenv('ES_INDEX_SCROLL_SIZE', 2000)
ES_INDEX_SCROLL_TIME = os.getenv('ES_INDEX_SCROLL_TIME', "20s")
ES_INDEX_ALIAS = os.getenv('ES_INDEX_ALIAS', 'testindex')

ES_SEARCH_RANGE_DATE_START = os.getenv('ES_SEARCH_RANGE_DATE_START',"2021-08-16T00:00:00.000000000+00:00")
ES_SEARCH_RANGE_DATE_END = os.getenv('ES_SEARCH_RANGE_DATE_END',"2021-08-16T00:05:00.000000000+00:00")
ES_EXPORT_FILENAME = os.getenv('ES_EXPORT_FILENAME', "/tmp/" + str(ES_INDEX_ALIAS))
PG_HOST = os.getenv('PG_HOST', '10.28.0.1')
PG_PORT = os.getenv('PG_PORT', '15433')
PG_DBNAME = os.getenv('PG_DBNAME', 'infra_logs')
PG_USER = os.getenv('PG_USER', 'infra_logs')
PG_PASSWORD = os.getenv('PG_PASSWORD', 'infra_logs')
PG_TABLE_NAME = os.getenv('PG_TABLE_NAME', ES_INDEX_ALIAS)
es_con = ES_HOST_SHCEME + ':' + '//' + ES_HOST + ':' + ES_PORT
es_cred = ES_USER, ES_PASSWORD
start_time = time.time()


class SetEncoder(JSONSerializer):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        if isinstance(obj, Something):
            return 'CustomSomethingRepresentation'
        return JSONSerializer.default(self, obj)
# declare an instance of the Elasticsearch library
client = Elasticsearch(
    [es_con],
    http_auth=(es_cred),
    scheme=ES_HOST_SHCEME,
    ca_certs=False,
    verify_certs=True,
    # maxsize=25,
    connection_class=RequestsHttpConnection,
    http_compress=True,
    serializer=SetEncoder()
)



try:
    # use the JSON library's dump() method for indentation
    info = json.dumps(client.info(), indent=4)
    es_health=json.dumps(client.cluster.health(), indent=4)

    # pass client object to info() method
    print ("Elasticsearch client info():", info)
    print ("Elasticsearch client es_health():", es_health)


except exceptions.ConnectionError as err:

    # print ConnectionError for Elasticsearch
    print ("\nElasticsearch info() ERROR:", err)
    print ("\nThe client host:", ES_HOST, "is invalid or cluster is not running")

    # change the client's value to 'None' if ConnectionError
    client = None

# valid client instance for Elasticsearch
if client != None:

    # get all of the indices on the Elasticsearch cluster
    all_indices = client.indices.get_alias(index=ES_INDEX_ALIAS)

    # keep track of the number of the documents returned
    doc_count = 0
    #  create an empty Pandas DataFrame object for docs
    docs = pandas.DataFrame()
    # iterate over the list of Elasticsearch indices

    print("Selected indices = " + str(all_indices))
    for num, index in enumerate(all_indices):

        # declare a filter query dict object
        #es_query = {
        #        "range": {
        #            "@timestamp": {
        #                "gte": ES_SEARCH_RANGE_DATE_START,
        #                "lt":  ES_SEARCH_RANGE_DATE_END
        #            }}}

        es_query = {
        			"bool" : {
        				"filter" : {
        					"term" : {
        						"nginx.http_host.keyword" : "rx.foxcom.lv",
                    "range": {
                        "@timestamp": {
                            "gte": ES_SEARCH_RANGE_DATE_START,
                            "lt":  ES_SEARCH_RANGE_DATE_END
                        }

        					}
        				}
        			}
        		},
        		    "sort" : {
        			   "nginx.remote_addr": {"order": "asc"}
        		}}
        # make a search() request to get all docs in the index
        print("\nMake query : ", str(es_query))
        resp = client.search(
        	size = ES_INDEX_SCROLL_SIZE,
            index = index,
            query = es_query,
            scroll = ES_INDEX_SCROLL_TIME,
            request_timeout=45
        )

        # keep track of pass scroll _id
        old_scroll_id = resp['_scroll_id']
        print("\nold_scroll_id = ", old_scroll_id)

        # use a 'while' iterator to loop over document 'hits'
        while len(resp['hits']['hits']):

            # make a request using the Scroll API
            resp = client.scroll(
                scroll_id = old_scroll_id,
                scroll = ES_INDEX_SCROLL_TIME # length of time to keep search context
            )

            # check if there's a new scroll ID
            if old_scroll_id != resp['_scroll_id']:
                print ("NEW SCROLL ID:", resp['_scroll_id'])

            # keep track of pass scroll _id
            old_scroll_id = resp['_scroll_id']

            # print the response results
            print ("\nresponse for index:", index)
            print ("_scroll_id:", resp['_scroll_id'])
            print ('response["hits"]["total"]["value"]:', resp["hits"]["total"]["value"])

            # iterate over the document hits for each 'scroll'
            for doc in resp['hits']['hits']:
                #print ("\n", doc['_source'])
                doc_count += 1
                doc_data = pandas.Series(doc['_source'], name = doc_count)
                #print("doc_data")
                #print(str(doc_data))
                docs = docs.append(doc_data)
                #print ("DOC COUNT:", doc_count)

    # print the total time and document count at the end
    print("\nTOTAL DOC COUNT:", doc_count)
    """
    EXPORT THE ELASTICSEARCH DOCUMENTS PUT INTO
    PANDAS OBJECTS
    """
    print("\nexporting Pandas objects to different file types to files with mask " + str(ES_EXPORT_FILENAME))
    docs.to_json(str(ES_EXPORT_FILENAME) + ".json", orient="records", lines=True, date_format="epoch")
    docs.to_csv(str(ES_EXPORT_FILENAME) + ".csv", sep="|", encoding='utf-8', header=False, line_terminator="\n")
    engine = sqlalchemy.create_engine(
        "postgresql://" + str(PG_USER) + ":" + str(PG_PASSWORD) + "@" + str(PG_HOST) + ":" + str(PG_PORT) + "/" + str(
            PG_DBNAME))
    print("\nImport into db " + str(PG_DBNAME) + "db url " + 'postgresql://' + str(PG_USER) + str(PG_HOST) + ":" + str(
        PG_PORT))
    table_name = "tbl_" + str(PG_TABLE_NAME)
    docs.to_sql(table_name, engine, if_exists="append", chunksize=300)

#json_export = docs.to_json()

# print the elapsed time
print ("TOTAL TIME:", time.time() - start_time, "seconds.")