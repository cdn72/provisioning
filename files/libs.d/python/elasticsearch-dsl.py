#!/usr/bin/env python3.10
#-*- coding: utf-8 -*-
import os
import json
import time
import pandas
import json
import numpy as np
import sqlalchemy
from elasticsearch import Elasticsearch, RequestsHttpConnection, exceptions
from elasticsearch.serializer import JSONSerializer
from elasticsearch_dsl import Q
from elasticsearch_dsl import Search


ES_HOST = os.getenv('ES_HOST', 'test.local')
ES_PORT = os.getenv('ES_PORT', '443')
ES_HOST_SHCEME = os.getenv('ES_HOST_SHCEME', 'https')
ES_USER = os.getenv('ES_USER', 'sdsdsd')
ES_PASSWORD = os.getenv('ES_PASSWORD', 'ssdsdsdsdnnd')
ES_INDEX_SCROLL_SIZE = os.getenv('ES_INDEX_SCROLL_SIZE', 2000)
ES_INDEX_SCROLL_TIME = os.getenv('ES_INDEX_SCROLL_TIME', "20s")
ES_INDEX_ALIAS = os.getenv('ES_INDEX_ALIAS', 'nginxaccess-2021.08.16')

ES_SEARCH_RANGE_DATE_START = os.getenv(
    'ES_SEARCH_RANGE_DATE_START', "2021-08-16T00:00:00.000000000+00:00")
ES_SEARCH_RANGE_DATE_END = os.getenv(
    'ES_SEARCH_RANGE_DATE_END', "2021-08-16T00:05:00.000000000+00:00")
ES_EXPORT_FILENAME = os.getenv(
    'ES_EXPORT_FILENAME', "/tmp/" + str(ES_INDEX_ALIAS))
PG_HOST = os.getenv('PG_HOST', '10.28.0.1')
PG_PORT = os.getenv('PG_PORT', '15433')
PG_DBNAME = os.getenv('PG_DBNAME', 'infra_logs')
PG_USER = os.getenv('PG_USER', 'infra_logs')
PG_PASSWORD = os.getenv('PG_PASSWORD', 'infra_logs')
PG_TABLE_NAME = os.getenv('PG_TABLE_NAME', ES_INDEX_ALIAS)
es_con = ES_HOST_SHCEME + ':' + '//' + ES_HOST + ':' + ES_PORT
es_cred = ES_USER, ES_PASSWORD
start_time = time.time()


class SetEncoder(JSONSerializer):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        if isinstance(obj, Something):
            return 'CustomSomethingRepresentation'
        return JSONSerializer.default(self, obj)


# declare an instance of the Elasticsearch library
es_client = Elasticsearch(
    [es_con],
    http_auth=(es_cred),
    scheme=ES_HOST_SHCEME,
    ca_certs=False,
    verify_certs=True,
    # maxsize=25,
    connection_class=RequestsHttpConnection,
    http_compress=True,
    serializer=SetEncoder()
)


try:
    info = json.dumps(es_client.info(), indent=4)
    es_health = json.dumps(es_client.cluster.health(), indent=4)
    #print ("Elasticsearch es_client info():", info)
    #print ("Elasticsearch es_client es_health():", es_health)


except exceptions.ConnectionError as err:
    print ("\nElasticsearch info() ERROR:", err)
    print ("\nThe es_client host:", ES_HOST,
           "is invalid or cluster is not running")
    es_client = None



#s = Search(using=es_client, index=ES_INDEX_ALIAS, doc_type='doc') \
#    .filter("term", category="search") \
#    .filter('range', @timestamp={'gte': ES_SEARCH_RANGE_DATE_START, 'lte': ES_SEARCH_RANGE_DATE_END}) \
#    .sort({"timestamp": {"order": "asc"}})
#response = s.execute()

# form query for search: match company and age
#print("ES_SEARCH_RANGE_DATE_START",ES_SEARCH_RANGE_DATE_START)
#print("ES_SEARCH_RANGE_DATE_END",ES_SEARCH_RANGE_DATE_END)
#query = Q(Bool(must=[Range(nginx.time=={'gte': ES_SEARCH_RANGE_DATE_START, 'lte': ES_SEARCH_RANGE_DATE_END}), Term(nginx.http_host=='bs.foxcom.lv')]))

s = Search(using=es_client, index=ES_INDEX_ALIAS)

s.query = Q('bool', must=[Q('match', **{'nginx.http_host': 'bs.foxcom.lv'})])
s = s.filter('range', **{'@timestamp': {'gte': ES_SEARCH_RANGE_DATE_START, 'lte': ES_SEARCH_RANGE_DATE_END}})
s = s.filter('match', **{'nginx.status': 200})
#s = s.source(False)
s = s.source(['@timestamp','nginx.time','nginx.http_host','nginx.uri','nginx.status','nginx.remote_addr','nginx.http_x_forwarded_for','nginx.user_agent','nginx.request_body','nginx.request_method','nginx.request'])
s = s.extra(explain=True,track_total_hits=True)
response = s.execute()
print('Total responce took.',response.took)
print('Total hits found.', str(response.hits.total))
print('Total response hits relation.',response.hits.total.relation)
doc_count = 0
docs = pandas.DataFrame()
for doc in s.scan():
    doc_count += 1
    doc_data = pandas.Series(doc.to_dict(), name = doc_count)
    docs = docs.append(doc_data)



docs.to_json(str(ES_EXPORT_FILENAME) + ".json", orient="records", lines=True, date_format="epoch")

engine = sqlalchemy.create_engine(
    "postgresql://" + str(PG_USER) + ":" + str(PG_PASSWORD) + "@" + str(PG_HOST) + ":" + str(PG_PORT) + "/" + str(
        PG_DBNAME))
print("\nImport into db " + str(PG_DBNAME) + "db url " + 'postgresql://' + str(PG_USER) + str(PG_HOST) + ":" + str(
    PG_PORT))
table_name = "tbl_" + str(PG_TABLE_NAME)
docs.to_sql(table_name, engine, if_exists="append", chunksize=300)



#https://github.com/elastic/elasticsearch-dsl-py