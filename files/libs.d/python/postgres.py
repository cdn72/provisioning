#!/usr/bin/env python3
import os
import psycopg2
from psycopg2 import Error
from psycopg2.extras import RealDictCursor
from collections import namedtuple




def ci_data_get_convy(table_name,org_name,app_name,app_http_host):
    connection = None
    try:
        connection = psycopg2.connect(user=POSTGRES_DB_USER, password=POSTGRES_DB_PASSWORD,
                                      host=POSTGRES_DB_HOST, port=POSTGRES_DB_PORT, database=POSTGRES_DB_NAME)
        cursor = connection.cursor(cursor_factory=RealDictCursor)
        cursor.execute(
            f"""
            select 
            data->>'APP_NAME' AS "APP_NAME",
            data->>'ORG_NAME' AS "ORG_NAME",
            data->>'APP_HTTP_HOST' AS "APP_HTTP_HOST",
            data->>'APP_ADDONS' AS "APP_ADDONS",
            data->>'APP_HOST_REMOTE_MYSQL' AS "APP_HOST_REMOTE_MYSQL",
            data->>'APP_HOST_REMOTE_PG' AS "APP_HOST_REMOTE_PG",
            data->>'APP_HOST_REMOTE_DATA' AS "APP_HOST_REMOTE_DATA",
            data->>'APP_CLUSTER_HOSTS_IPS' AS "APP_CLUSTER_HOSTS_IPS",
            data->>'APP_OCHESTATOR_HOST' AS "APP_OCHESTATOR_HOST",
            data->>'SMS_RX_HTTP_HOSTS' AS "SMS_RX_HTTP_HOSTS",
            data->>'ENV_NGINX_BACKEND_SMS_RX' AS "ENV_NGINX_BACKEND_SMS_RX",
            data->>'CONVY_CONSUMER_PORTAL_HTTP_HOSTS' AS "CONVY_CONSUMER_PORTAL_HTTP_HOSTS",
            data->>'CONVY_WEB_ACL_LIST' AS "CONVY_WEB_ACL_LIST",
            data->>'D_C_P_LB_MYSQL' AS "D_C_P_LB_MYSQL",
            data->>'D_C_P_LB_POSTGRES' AS "D_C_P_LB_POSTGRES"
            from {table_name}
            WHERE data->>'ORG_NAME' in ('{org_name}') and data->>'APP_NAME' in ('{app_name}') and  data->>'APP_HTTP_HOST' in ('{app_http_host}')
            ORDER BY time asc limit 1
            ;
            """)
        rows = cursor.fetchone()
        return rows
        cursor.close()
    except (Exception, psycopg2.DatabaseError) as error:
        error_data = "error"
        return error_data
    finally:
        if connection is not None:
            connection.close()


def main() -> None:
    filename = '/tmp/'+ APP_NAME + '-' + APP_HTTP_HOST + '-shared-vars.sh'
    if APP_NAME == 'convy':
        ORG_NAME = os.getenv('ORG_NAME', 'devops inc')
        APP_NAME = os.getenv('APP_NAME', 'demo')
        APP_HTTP_HOST = os.getenv('APP_HTTP_HOST', 'demo.local')
        POSTGRES_DB_HOST = os.getenv('POSTGRES_DB_HOST', '10.151.0.10')
        POSTGRES_DB_PORT = os.getenv('POSTGRES_DB_PORT', '5432')
        POSTGRES_DB_NAME = os.getenv('POSTGRES_DB_NAME', 'infra')
        POSTGRES_DB_USER = os.getenv('POSTGRES_DB_USER', 'infra')
        POSTGRES_DB_PASSWORD = os.getenv(
            'POSTGRES_DB_PASSWORD', 'dsdsdsdsd')
        POSTGRES_TABLE_NAME = os.getenv(
            'POSTGRES_TABLE_NAME', 'tbl_apps_jsonb')
        data = ci_data_get_convy(POSTGRES_TABLE_NAME,ORG_NAME,APP_NAME,APP_HTTP_HOST)
        column_names = []
        data_rows = []
        for row in data:
            with open(filename, 'a') as file:
               file.write("export "+row+'="'+data[row]+'"\n')



if __name__ == '__main__':
    main()

