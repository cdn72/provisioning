#!/usr/bin/env bash

##Example run
##./gluster_cluster_build.sh --ssh_private_key_path=${HOME}/.ssh/id_rsa --glusterfs_servers_ips=10.151.0.145,10.151.0.154 --glusterfs_ppa_ver=9 --glusterfs_role=server --glusterfs_store_disk=/dev/sdb --glusterfs_private_network_mask=10.11.0 --glusterfs_server_data_folder=/glusterfs --glusterfs_volume_name=vg0 --glusterfs_mountpoint=/docker-compose --glusterfs_clients_ips=10.151.0.145,10.151.0.154,10.151.0.144,10.151.0.148,10.151.0.149,10.151.0.150

for i in "$@"
do
case $i in
    --ssh_private_key_path=*) # ssh_private_key_path=deploy/deploy-keys/id_rsa
    SSH_PRIVATE_KEY_PATH="${i#*=}"
    shift # past argument=value
    ;;	
    --glusterfs_servers_ips=*) # glusterfs_servers_ips=10.151.0.145,10.151.0.154
    GLUSTER_SERVERS_IPS="${i#*=}"
    shift # past argument=value
    ;;
    --glusterfs_ppa_ver=*) # glusterfs_ppa_ver=9
    GLUSTER_PPA_VER="${i#*=}"
    shift # past argument=value
    ;;
    --glusterfs_role=*) # glusterfs_role=server
    GLUSTER_ROLE="${i#*=}"
    shift # past argument=value
    ;;
    --glusterfs_store_disk=*) # glusterfs_store_disk=/dev/sdb
    GLUSTER_STORE_DISK="${i#*=}"
    shift # past argument=value
    ;;
    --glusterfs_private_network_mask=*) # glusterfs_private_network_mask=10.11.0
    GLUSTER_PRIVATE_NETWORK_MASK="${i#*=}"
    shift # past argument=value
    ;;
    --glusterfs_server_data_folder=*) # glusterfs_server_data_folder=/glusterfs
    GLUSTER_SERVER_DATA="${i#*=}"
    shift # past argument=value
    ;;
    --glusterfs_volume_name=*) # glusterfs_volume_name=vg0
    GLUSTER_SERVER_VOLUME_NAME="${i#*=}"
    shift # past argument=value
    ;;
    --glusterfs_mountpoint=*) # glusterfs_mountpoint=/docker-compose
    GLUSTER_MOUNTPOINT="${i#*=}"
    shift # past argument=value
    ;;
    --glusterfs_clients_ips=*) # glusterfs_clients_ips=10.151.0.145,10.151.0.154,10.151.0.144,10.151.0.148,10.151.0.149,10.151.0.150
    GLUSTER_CLIENTS="${i#*=}"
    shift # past argument=value
    ;;	
    --default)
    DEFAULT=YES
    shift # past argument with no value
    ;;
    *)
          # unknown option
    ;;
esac
done

if [[ ! -n ${GLUSTER_SERVERS_IPS} ]];then
	echo "Error glusterfs_servers_ips is empty. Exit"
	exit 1
fi

SSH_OPTIONS="-i ${SSH_PRIVATE_KEY_PATH} -o StrictHostKeyChecking=no -t"
GLUSTER_HOSTS_FILE="/tmp/glusterfs_hosts.list"
GLUSTER_VOLUME_STATUS_FILE="/etc/glusterfs_${GLUSTER_SERVER_VOLUME_NAME}"
GLUSTED_PEER_PROBED_SERVER_1=$(echo "${GLUSTER_SERVERS_IPS}" | cut -d\, -f1 )
GLUSTER_PEER_PROBED_SERVER_NAME_1=$(ssh root@${GLUSTED_PEER_PROBED_SERVER_1} hostname)
GLUSTED_PEER_PROBED_SERVER_2=$(echo "${GLUSTER_SERVERS_IPS}" | cut -d\, -f2 )
GLUSTER_PEER_PROBED_SERVER_NAME_2=$(ssh root@${GLUSTED_PEER_PROBED_SERVER_2} hostname)



printf "#####Vars print#####################\n"
printf "####################################\n"
printf "GLUSTER_PRIVATE_NETWORK_MASK=$GLUSTER_PRIVATE_NETWORK_MASK\n"
printf "GLUSTER_PPA_VER=$GLUSTER_PPA_VER\n"

printf "####################################\n"
printf "####################################\n"
printf "###GLUSTER_SERVERS_IPS=${GLUSTER_SERVERS_IPS}\n"
sleep 5;


glusterfs_server_install() {

if [[ -f "${GLUSTER_HOSTS_FILE}" ]];then
	rm -fv ${GLUSTER_HOSTS_FILE}
fi
printf "#GLUSTER_BLOCK_START_${GLUSTER_SERVERS_IPS}\n" >> ${GLUSTER_HOSTS_FILE}

for server in $(echo "${GLUSTER_CLIENTS}" | tr -s "," " ")
do
	echo "Exec ssh-keyscan -t rsa ${server}  >> ${HOME}/.ssh/known_hosts"
	ssh-keyscan -t rsa ${server}  >> ${HOME}/.ssh/known_hosts
done

for server in $(echo "${GLUSTER_SERVERS_IPS}" | tr -s "," " ")
do
	echo "Install glusterfs=v${GLUSTER_PPA_VER} (if not exist) on server=${server}"
	ssh ${SSH_OPTIONS} root@${server} << EOF
			dpkg -s glusterfs-server | grep 'Status' | grep -q "Status: install" || (apt-get install software-properties-common -y ; add-apt-repository ppa:gluster/glusterfs-${GLUSTER_PPA_VER} -y ; apt update && apt install glusterfs-server -y ; apt-mark hold glusterfs*)
EOF
	GLUSTER_SERVER_HOSTNAME=$(ssh ${SSH_OPTIONS} root@${server} hostname)
	GLUSTER_SERVER_PRIVATE_IP=$(ssh ${SSH_OPTIONS} root@${server} ip a | grep "${GLUSTER_PRIVATE_NETWORK_MASK}" | awk '{ print $2 }' | cut -d \/ -f1)
	grep -q "${GLUSTER_SERVER_HOSTNAME}" ${GLUSTER_HOSTS_FILE} || (  printf "${GLUSTER_SERVER_PRIVATE_IP} ${GLUSTER_SERVER_HOSTNAME}\n"  >> ${GLUSTER_HOSTS_FILE})
	
done

printf "#GLUSTER_BLOCK_END\n" >> ${GLUSTER_HOSTS_FILE}


}

glusterfs_server_disk_prepare() {

for server in $(echo "${GLUSTER_SERVERS_IPS}" | tr -s "," " ")
do
	scp ${GLUSTER_HOSTS_FILE} root@${server}:/tmp/
done

for server in $(echo "${GLUSTER_SERVERS_IPS}" | tr -s "," " ")
do
	ssh ${SSH_OPTIONS} root@${server} << EOF
		grep -q 'GLUSTER_BLOCK_START_${GLUSTER_SERVERS_IPS}' /etc/hosts || (cat ${GLUSTER_HOSTS_FILE} >> /etc/hosts)
EOF

done


for server in $(echo "${GLUSTER_SERVERS_IPS}" | tr -s "," " ")
do
	echo "install parted"
	ssh ${SSH_OPTIONS} root@${server} << EOF
		 dpkg -s parted | grep 'Status' | grep -q "Status: install"   || (apt-get install parted -y );
EOF
	sleep 6;
	echo "exec parted -a optimal -s ${GLUSTER_STORE_DISK} mklabel gpt mkpart  primary ext4 0% 100%"
	ssh ${SSH_OPTIONS} root@${server} << EOF
		grep  -q "${GLUSTER_SERVER_DATA}" /etc/fstab || (parted -a optimal -s ${GLUSTER_STORE_DISK} mklabel gpt mkpart  primary ext4 0% 100%)
		
EOF
	sleep 6;
	echo "Exec mkfs.ext4 ${GLUSTER_STORE_DISK}1 on ${server}"
	ssh ${SSH_OPTIONS} root@${server} << EOF
		grep  -q "${GLUSTER_SERVER_DATA}" /etc/fstab || (mkfs.ext4 ${GLUSTER_STORE_DISK}1)
EOF
	sleep 6;
	ssh ${SSH_OPTIONS} root@${server} << EOF
		grep -q "${GLUSTER_SERVER_DATA}" /etc/fstab || (mkdir -p ${GLUSTER_SERVER_DATA})
EOF
	echo "Get BLKID_GLUSTER on ${server}"
	BLKID_GLUSTER=$(ssh ${SSH_OPTIONS} root@${server} blkid | grep "${GLUSTER_STORE_DISK}1" | awk '{ print $2 }' )
	echo  "BLKID_GLUSTER = ${BLKID_GLUSTER} on ${server}"
	echo "Add ${BLKID_GLUSTER} ${GLUSTER_SERVER_DATA} ext4 rw,noatime 0 to /etc/fstab on server ${server}"
	ssh ${SSH_OPTIONS} root@${server} << EOF
		grep -q "${GLUSTER_SERVER_DATA}" /etc/fstab || (echo '${BLKID_GLUSTER} ${GLUSTER_SERVER_DATA} ext4 rw,noatime 0' >> /etc/fstab)
		mount ${GLUSTER_SERVER_DATA}
EOF
done
}

glusterfs_server_init () {
	for server in $(echo "${GLUSTER_SERVERS_IPS}" | tr -s "," " ")
	do
		echo "Exec /usr/bin/systemctl unmask && enable glusterd.service on ${server}"
		ssh ${SSH_OPTIONS} root@${server} "/usr/bin/systemctl unmask glusterd.service;"
		ssh ${SSH_OPTIONS} root@${server} "/usr/bin/systemctl enable glusterd.service;"

		echo "Exec /usr/bin/systemctl start glusterd.service on ${server}"
		ssh ${SSH_OPTIONS} root@${server} "/usr/bin/systemctl start glusterd.service"

	done


# From first server make peer probe second server

	echo "exec gluster peer probe ${GLUSTER_PEER_PROBED_SERVER_NAME_2}"
ssh ${SSH_OPTIONS} root@${GLUSTED_PEER_PROBED_SERVER_1} <<OEF
	gluster peer probe ${GLUSTER_PEER_PROBED_SERVER_NAME_2}
	sleep 5;
	mkdir -p ${GLUSTER_SERVER_DATA}/${GLUSTER_PEER_PROBED_SERVER_NAME_1}/gvol0;
	mkdir -p ${GLUSTER_SERVER_DATA}/gvol0;
OEF

ssh ${SSH_OPTIONS} root@${GLUSTED_PEER_PROBED_SERVER_2} <<OEF
	gluster peer probe ${GLUSTER_PEER_PROBED_SERVER_NAME_1};
	sleep 5;
	mkdir -p ${GLUSTER_SERVER_DATA}/${GLUSTER_PEER_PROBED_SERVER_NAME_2}/gvol0;
	mkdir -p ${GLUSTER_SERVER_DATA}/gvol0;
OEF

ssh ${SSH_OPTIONS} root@${GLUSTED_PEER_PROBED_SERVER_1} <<OEF

		if [ ! -f "${GLUSTER_VOLUME_STATUS_FILE}" ];
		then
			gluster volume create ${GLUSTER_SERVER_VOLUME_NAME} replica 2 ${GLUSTER_PEER_PROBED_SERVER_NAME_1}:${GLUSTER_SERVER_DATA}/gvol0 ${GLUSTER_PEER_PROBED_SERVER_NAME_2}:${GLUSTER_SERVER_DATA}/gvol0 force && touch ${GLUSTER_VOLUME_STATUS_FILE};
			gluster volume start ${GLUSTER_SERVER_VOLUME_NAME}
		else
			echo "Gluster volume name ${GLUSTER_SERVER_VOLUME_NAME} already exist. Trigger file is ${GLUSTER_VOLUME_STATUS_FILE}"
		fi
OEF
	
ssh ${SSH_OPTIONS} root@${GLUSTED_PEER_PROBED_SERVER_1} <<OEF
		echo "Mount ${GLUSTER_MOUNTPOINT}"
		mkdir -p ${GLUSTER_MOUNTPOINT};
		grep -q "${GLUSTER_MOUNTPOINT}" /etc/fstab || ( echo "${GLUSTER_PEER_PROBED_SERVER_NAME_1}:/${GLUSTER_SERVER_VOLUME_NAME} ${GLUSTER_MOUNTPOINT} glusterfs defaults,_netdev 0 0" >> /etc/fstab;)
		sleep 2;
		umount -l ${GLUSTER_MOUNTPOINT} || (echo "Skip unmount ${GLUSTER_MOUNTPOINT}")
		mount ${GLUSTER_MOUNTPOINT}
OEF

ssh root@${GLUSTED_PEER_PROBED_SERVER_2} <<OEF
		mkdir -p ${GLUSTER_MOUNTPOINT};
		grep -q "${GLUSTER_MOUNTPOINT}" /etc/fstab || ( echo "${GLUSTER_PEER_PROBED_SERVER_NAME_2}:/${GLUSTER_SERVER_VOLUME_NAME} ${GLUSTER_MOUNTPOINT} glusterfs defaults,_netdev 0 0" >> /etc/fstab;)
		sleep 2;
		umount -l ${GLUSTER_MOUNTPOINT} || (echo "Skip unmount ${GLUSTER_MOUNTPOINT}")
		mount ${GLUSTER_MOUNTPOINT}
OEF

}

glusterfs_mount_clients() {

	echo "glusterfs_mount_clients on ${GLUSTER_CLIENTS}"

for server in $(echo "${GLUSTER_CLIENTS}" | tr -s "," " ")
do

	scp ${GLUSTER_HOSTS_FILE} root@${server}:/tmp/

	ssh ${SSH_OPTIONS} root@${server} << EOF
		dpkg -s  glusterfs-client | grep 'Status' | grep -q "Status: install" || (apt-get install software-properties-common -y ; add-apt-repository ppa:gluster/glusterfs-${GLUSTER_PPA_VER} -y ; apt update; apt install glusterfs-client -y ; apt-mark hold glusterfs-client* )
		grep -q "${GLUSTER_SERVER_DATA}" /etc/hosts || (cat ${GLUSTER_HOSTS_FILE} >> /etc/hosts)
		mkdir -p ${GLUSTER_MOUNTPOINT};
		grep -q "${GLUSTER_MOUNTPOINT}" /etc/fstab || (echo "${GLUSTER_PEER_PROBED_SERVER_NAME_2}:/${GLUSTER_SERVER_VOLUME_NAME} ${GLUSTER_MOUNTPOINT} glusterfs defaults,_netdev,backupvolfile-server=${GLUSTER_PEER_PROBED_SERVER_NAME_1} 0 0" >> /etc/fstab;)
		sleep 5;
		umount -l ${GLUSTER_MOUNTPOINT} || (echo "Skip unmount ${GLUSTER_MOUNTPOINT}")
		mount ${GLUSTER_MOUNTPOINT}

EOF
done
}


snippets() {
	echo ""
	#apt remove glusterfs-server -y --allow-change-held-packages
}


main() {
	glusterfs_server_install
	glusterfs_server_disk_prepare
	glusterfs_server_init
	glusterfs_mount_clients
}

main