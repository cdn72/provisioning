telegram_send_message_post(){
	local TEXT=$1
	local USERID=$2
	local KEY=$3
	local TIMEOUT="10"
	local URL="https://api.telegram.org/bot$KEY/sendMessage"
	local DATE_EXEC="$(date "+%d %b %Y %H:%M")"
	local TMPFILE='/tmp/ipinfo-$DATE_EXEC.txt'
	curl  -s --max-time $TIMEOUT -d "chat_id=$USERID&disable_web_page_preview=1&text=${TEXT}" $URL > /dev/null 2>&1
}

telegram_send_message_get(){
	local T_TEXT=$1
	local WEBHOOK_URL=$2
	local T_TEXT_URL_ENCODED=$(echo -ne "${T_TEXT}" | hexdump -v -e '/1 "%02x"' | sed 's/\(..\)/%\1/g')
	curl  -X GET -s --max-time ${TIMEOUT:-2} "${WEBHOOK_URL}&disable_web_page_preview=1&parse_mode=markdown&disable_notification=true&text=${T_TEXT_URL_ENCODED}" > /dev/null 2>&1
}

telegram_send_file () {
	local TEXT=$1
	local USERID=$2
	local KEY=$3
	local log_files=$4
	local notify_url="https://api.telegram.org/bot${KEY}/sendMessage?chat_id=${USERID}"

for log_file in ${log_files};do
	echo "Try send ${log_file}"
	if [[ -f "${log_file}" ]];then
		curl -F document=@"${log_file}" "$(echo "${notify_url}" | sed 's/sendMessage/sendDocument/')" > /dev/null
	else
		echo "Skip send ${log_file}"
	fi
done

}

install_dbeaver(){
	cd /tmp
	sudo apt install "./$(
		curl -O "$(
			curl -sL -I -w '%{url_effective}' \
			https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb \
			| tail -1 )" -w '%{filename_effective}'
		)"
	rm -v  dbeaver*.deb
}


system_upgrade(){
	sudo apt-get update ; sudo apt-get dist-upgrade -y && sudo apt-get upgrade -y
	install_dbeaver
	sudo curl -L https://raw.githubusercontent.com/docker/compose/1.29.2/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
}

kill_dbeaver() {
	kill -9 $(ps aux | grep 'dbeaver' | awk '{ print $2}')
}

kill_workrave() {
	kill -9 $(ps aux | grep 'workrave' | awk '{ print $2}')
}


kill_vlc() {
	kill -9 $(ps aux | grep '/usr/bin/vlc' | awk '{ print $2}')
}


ssh_agent_start(){
	ssh-add -D ; eval `ssh-agent` ; ssh-add  ~/.ssh/id_rsa ; ssh-add -l
}

youtube_video_get(){
	if [[ -z "$@" ]];then
		echo "Error. Arg1 is empty"
	else
		cd ${youtube_download_dir} && python3 /usr/local/bin/youtube-dl --cookies ${youtube_com_cookies} -f best "$@"
		#youtube-dl -f bestvideo+bestaudio --merge-output-format mkv --all-subs --cookies /git/linux2be.com/itc-life/files/pcs/asus_tuf_gaming_a17/youtube.com_cookies.txt [YouTube URL]
	fi
}

urldecode() {
    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
}


shell_colors () {
	# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Bold
BBlack='\033[1;30m'       # Black
BRed='\033[1;31m'         # Red
BGreen='\033[1;32m'       # Green
BYellow='\033[1;33m'      # Yellow
BBlue='\033[1;34m'        # Blue
BPurple='\033[1;35m'      # Purple
BCyan='\033[1;36m'        # Cyan
BWhite='\033[1;37m'       # White

# Underline
UBlack='\033[4;30m'       # Black
URed='\033[4;31m'         # Red
UGreen='\033[4;32m'       # Green
UYellow='\033[4;33m'      # Yellow
UBlue='\033[4;34m'        # Blue
UPurple='\033[4;35m'      # Purple
UCyan='\033[4;36m'        # Cyan
UWhite='\033[4;37m'       # White

# Background
On_Black='\033[40m'       # Black
On_Red='\033[41m'         # Red
On_Green='\033[42m'       # Green
On_Yellow='\033[43m'      # Yellow
On_Blue='\033[44m'        # Blue
On_Purple='\033[45m'      # Purple
On_Cyan='\033[46m'        # Cyan
On_White='\033[47m'       # White

# High Intensity
IBlack='\033[0;90m'       # Black
IRed='\033[0;91m'         # Red
IGreen='\033[0;92m'       # Green
IYellow='\033[0;93m'      # Yellow
IBlue='\033[0;94m'        # Blue
IPurple='\033[0;95m'      # Purple
ICyan='\033[0;96m'        # Cyan
IWhite='\033[0;97m'       # White

# Bold High Intensity
BIBlack='\033[1;90m'      # Black
BIRed='\033[1;91m'        # Red
BIGreen='\033[1;92m'      # Green
BIYellow='\033[1;93m'     # Yellow
BIBlue='\033[1;94m'       # Blue
BIPurple='\033[1;95m'     # Purple
BICyan='\033[1;96m'       # Cyan
BIWhite='\033[1;97m'      # White

# High Intensity backgrounds
On_IBlack='\033[0;100m'   # Black
On_IRed='\033[0;101m'     # Red
On_IGreen='\033[0;102m'   # Green
On_IYellow='\033[0;103m'  # Yellow
On_IBlue='\033[0;104m'    # Blue
On_IPurple='\033[0;105m'  # Purple
On_ICyan='\033[0;106m'    # Cyan
On_IWhite='\033[0;107m'   # White

}