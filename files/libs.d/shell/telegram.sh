telegram_send_message_post(){
	local TEXT=$1
	local USERID=$2
	local KEY=$3
	local TIMEOUT="10"
	local URL="https://api.telegram.org/bot$KEY/sendMessage"
	curl  -s -q --max-time $TIMEOUT -d "chat_id=$USERID&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null
}

telegram_send_message_get(){
	local T_TEXT=$1
	local WEBHOOK_URL=$2
	local T_TEXT_URL_ENCODED=$(echo -ne "${T_TEXT}" | hexdump -v -e '/1 "%02x"' | sed 's/\(..\)/%\1/g')
	curl  -X GET -s --max-time ${TIMEOUT:-2} "${WEBHOOK_URL}&disable_web_page_preview=1&parse_mode=markdown&disable_notification=true&text=${T_TEXT_URL_ENCODED}" > /dev/null 2>&1
}