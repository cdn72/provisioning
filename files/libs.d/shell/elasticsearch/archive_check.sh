#!/bin/sh -x
es_on_host=$(netstat -tln|grep "[12]9200"|awk '{print $4}'|sort -r|head -n1)
if [ "x$es_on_host" == "x" ];
then
    echo es not running
    exit -1;
fi
master_node=$(curl "http://$es_on_host/_cluster/state/master_node" 2>/dev/null|jq ".master_node"|tr -d '"')
master_node_transport=$(curl "http://$es_on_host/_nodes/_all" 2>/dev/null|jq '.nodes."'$master_node'".http.publish_address'|tr -d '"')
__tmp=$(netstat -tln|grep $master_node_transport|awk '{print $4}')
if [ "x$master_node_transport" != "x$__tmp" ];
then
    echo there is no master on this node;
    exit -1;
fi
echo we are on master node
echo run so
rm -f /data/archive/scripts/archivecontrol/archive_status.txt
INDICES="/data/archive/scripts/indices.txt"
for indice in $(cat $INDICES|cut -d',' -f1); do
    curl "http://x.x.x.x:9200/_cat/snapshots/$indice" 2>/dev/null | tail -1 | while read c1 c2 c3 c4 c5; do echo -e 'indice Name: '$c1' \nBackup Status: '$c2'';echo Last Backup Date:; date -d @$c3;echo -e '\n'; done
done >> /data/archive/scripts/archivecontrol/archive_status.txt
from="from@example.com"
to="to@example.com"
mailserver="example.com"
mailtext="/data/archive/scripts/archivecontrol/archive_status.txt"
if [ ! -f $mailtext ]
then
    echo "Cannot find your mail text file.  Exiting..."
    exit 1
fi
(
    sleep 1
    echo "ehlo"
    sleep 1
    echo "mail from:$from"
    sleep 1
    OLDIFS=$IFS
    IFS=';'
    for i in $to; do
        echo "rcpt to:$i"
        sleep 1
    done
    IFS=$OLDIFS
    echo "data"
    sleep 1
    echo "subject: elasticsearch archive"
    sleep 1
    echo "from:$from"
    sleep 1
    echo "to:$to"
    sleep 1
    echo " "
    sleep 1
    echo $HOST
    cat $mailtext
    echo "."
    sleep 1
    echo "QUIT"
) | /usr/bin/telnet $mailserver 25