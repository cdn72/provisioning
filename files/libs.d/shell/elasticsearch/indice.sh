#!/bin/bash

es_on_host=$(netstat -tln|grep "9200"|awk '{print $4}'|sort -r|head -n1)

if [ "x$es_on_host" == "x" ];
then
    echo es not running
    exit -1;
fi
master_node=$(curl "http://$es_on_host/_cluster/state/master_node" 2>/dev/null|jq ".master_node"|tr -d '"')
master_node_transport=$(curl "http://$es_on_host/_nodes/_all" 2>/dev/null|jq '.nodes."'$master_node'".http.publish_address'|tr -d '"')
__tmp=$(netstat -tln|grep $master_node_transport|awk '{print $4}')
if [ "x$master_node_transport" != "x$__tmp" ];
then
    echo there is no master on this node;
    exit -1;
fi
echo we are on master node
echo run so
from="from@example.com"
to="to@example.com"
mailserver="example.com"
indicetext=indices.txt
arcfile=indices.txt
flag=0
echo " " >$indicetext
indicelist=(`curl "http://x.x.x.x:9200/_cat/indices?format=json&pretty=true" | jq -r '.[].index' | sort | cut -d '_' -f1 | uniq`)
for i in ${indicelist[@]}
do
    while IFS=',' read appname shorret longret
    do
        #echo  $i
        if [ "$i" == "$appname" ]
        then
            echo -e "$i exist in archive \n" >>$indicetext
            flag=1
            break
        else
            flag=0
            continue
        fi
    done < $arcfile
    if [ "$flag" -eq 0 ]
    then
        echo -e "$i not existing in archive \n" >>$indicetext
    fi
done
if [ ! -f $indicetext ]
then
    echo "Cannot find your mail text file.  Exiting..."
    exit 1
fi
(
    sleep 1
    echo "ehlo"
    sleep 1
    echo "mail from:$from"
    sleep 1
    OLDIFS=$IFS
    IFS=';'
    for i in $to; do
        echo "rcpt to:$i"
        sleep 1
    done
    IFS=$OLDIFS
    echo "data"
    sleep 1
    echo "subject: elasticsearch indice check"
    sleep 1
    echo "from:$from"
    sleep 1
    echo "to:$to"
    sleep 1
    echo " "
    sleep 1
    echo $HOST
    cat $indicetext
    echo "."
    sleep 1
    echo "QUIT"
) | /usr/bin/telnet $mailserver 25
