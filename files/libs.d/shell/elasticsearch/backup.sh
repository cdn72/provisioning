#/bin/bash
echo -e "Enter Index Name"
read indexname
echo -e "Index: "$indexname""
echo "recent indices of $indexname"
curl -X GET "x.x.x.x:9200/_cat/indices/"$indexname"*?v&s=index"
count=$(curl -X GET "x.x.x.x:9200/_cat/indices/"$indexname"*?v&s=index" | wc -l)
let "count-=1"
echo $count
echo "Start backup process for:"
read no
echo "Processing..."
curl -XPUT -H 'Content-Type: application/json' 'http://x.x.x.x:9200/_snapshot/'$indexname'/'$indexname'_'$no'?wait_for_completion=true&pretty' -d '{
  "indices": "'$indexname'_'$no'",
  "ignore_unavailable": true,
  "include_global_state": false
}'
a=1
while [ $a -eq 1 ]
do
    read -r -p "Delete Index '$indexname'_'$no'? [Y/n] " input
    case $input in
        [yY][eE][sS]|[yY])
            curl -X DELETE "x.x.x.x:9200/"$indexname"_"$no""
            echo /n "Index '$indexname'_'$no' deleted."
            a=0
            ;;
        [nN][oO]|[nN])
            echo "Index '$indexname'_'$no'not deleted."
            a=0
            ;;
        *)
            echo "Invalid input..."
            a=1
            ;;
    esac
done
