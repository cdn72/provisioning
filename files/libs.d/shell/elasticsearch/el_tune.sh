#!/usr/bin/env bash

ELASTIC_URI="${ELASTIC_URI:-http://elasticsearch-master-headless:9200}"
ELASTIC_AUTH_USER="${ELASTIC_AUTH_USER:-elasticuser}"
ELASTIC_AUTH_PASSWORD="${ELASTIC_AUTH_PASSWORD:-elasticpassword}"
KIBANA_URI="${KIBANA_URI:-http://kibana-kibana:5601}"
CURL_OPTIONS=(--silent --user ${ELASTIC_AUTH_USER}:${ELASTIC_AUTH_PASSWORD}  --header 'Content-Type: application/json' --insecure)


if [[ $(curl -o /dev/null "${CURL_OPTIONS[@]}"  "${ELASTIC_URI}/_cat/health" -XGET -w "%{http_code}" | tail -n 1 ) != '200'  ]];then
  echo "Error. Elasticsearch not ready"
  exit 1
else
  echo "Pass check"
fi


ELK_TUNE(){
#########
echo "Run update cluster.routing.allocation.disk.watermark.flood_stage"
curl "${CURL_OPTIONS[@]}"  "${ELASTIC_URI}/_cluster/settings" -XPUT -d '{
  "transient": {
    "cluster.routing.allocation.disk.watermark.flood_stage" : "96%"
  }
}'

##########
##########
echo "Run update cluster.routing.allocation.disk.watermark.low"
curl "${CURL_OPTIONS[@]}"  "${ELASTIC_URI}/_cluster/settings" -XPUT -d '{
  "transient": {
    "cluster.routing.allocation.disk.watermark.low": "95%",
    "cluster.routing.allocation.disk.watermark.high": "96%",
    "cluster.info.update.interval": "5m"
  }
}'

echo "Run update refresh_interval"
curl "${CURL_OPTIONS[@]}" "${ELASTIC_URI}/_template/all_indices" -XPUT -d '{
  "template": "*",
  "settings": {
    "refresh_interval": "10s"
  }
}'

echo "Update cluster.routing.allocation.disk.threshold_enabled"
curl "${CURL_OPTIONS[@]}"  ${ELASTIC_URI}/_cluster/settings -XPUT -d '{ "transient": { "cluster.routing.allocation.disk.threshold_enabled": false } }'


}


ELK_ASSIGN_SHARD(){
  echo "Update number_of_replicas"
  curl "${CURL_OPTIONS[@]}"  "${ELASTIC_URI}/_all/_settings?pretty" -XPUT -d' { "number_of_replicas": 0 }';

  echo "Update index.unassigned.node_left.delayed_timeout"  
  curl "${CURL_OPTIONS[@]}" "${ELASTIC_URI}/_all/_settings?pretty" -XPUT  -d'
  {
    "settings": {
      "index.unassigned.node_left.delayed_timeout": "5m"
  }
}'

}


ELK_INDEX_MAPPINGS_CREATE(){
########	
echo "create nginxaccess_1 mappings"
curl "${CURL_OPTIONS[@]}" "${ELASTIC_URI}/_template/nginxaccess_1" -XPUT  -d ' {

  "index_patterns": "nginxaccess-*",
  "settings": {
    "number_of_replicas" : "0"
  },
  "mappings": {
    "_source": {
      "enabled": true
    },
        "properties" : {
                  "nginx.request_time": {"type": "integer","ignore_malformed": true},
                  "nginx.msec": {"type": "integer","ignore_malformed": true},
                  "nginx.upstream_response_time": {"type": "integer","ignore_malformed": true},
                  "nginx.http_x_forwarded_for": {"type": "ip","ignore_malformed": true},
                  "nginx.remote_addr": {"type": "ip","ignore_malformed": true},
                  "nginx.status": {"type": "integer","ignore_malformed": true},
                  "nginx.upstream_status": {"type": "integer","ignore_malformed": true},
                  "nginx.body_bytes_sent": {"type": "integer","ignore_malformed": true},
                  "nginx.upstream_response_length": {"type": "integer","ignore_malformed": true},
                  "nginx.request_uri": {"type": "text","fields": {"keyword": {"type": "keyword"}
                                                           }
                                                                       }
                                                                                          }
    }        
}'
############
echo "Create index template workeraccess"
curl "${CURL_OPTIONS[@]}"  "${ELASTIC_URI}/_template/workeraccess_1" -XPUT -d ' {

  "index_patterns": "workeraccess-*",
  "settings": {
    "number_of_replicas" : "0"
  },
  "mappings": {
    "_source": {
      "enabled": true
    },
        "properties" : {
                  "nginx.request_time": {"type": "integer","ignore_malformed": true},
                  "nginx.msec": {"type": "integer","ignore_malformed": true},
                  "nginx.upstream_response_time": {"type": "integer","ignore_malformed": true},
                  "nginx.http_x_forwarded_for": {"type": "ip","ignore_malformed": true},
                  "nginx.remote_addr": {"type": "ip","ignore_malformed": true},
                  "nginx.status": {"type": "integer","ignore_malformed": true},
                  "nginx.upstream_status": {"type": "integer","ignore_malformed": true},
                  "nginx.body_bytes_sent": {"type": "integer","ignore_malformed": true},
                  "nginx.upstream_response_length": {"type": "integer","ignore_malformed": true},
                  "nginx.request_uri": {"type": "text","fields": {"keyword": {"type": "keyword"}
                                                           }
                                                                       }
                                                                                          }
    }        
}'
echo "Create index template remote-syslog"

curl "${CURL_OPTIONS[@]}" "${ELASTIC_URI}/_template/remote-syslog_1" -XPUT  -d ' {
  "index_patterns": "remote-syslog-*",
  "settings": {
    "number_of_replicas" : "0"
  }
 }
}'

}


KIBANA_CREATE_INDEX(){

echo "Create kibana index"
curl "${CURL_OPTIONS[@]}" "${KIBANA_URI}/api/saved_objects/index-pattern/nginxaccess-*" -H "kbn-version: 7.17.8" -H "kbn-xsrf: true"  -XPUT -d '
{
  "attributes":
    {
      "title":"nginxaccess-*",
      "timeFieldName":"@timestamp",
      "fields":"[]"
    }
}';
}

ELK_INDEX_DELETE(){
  echo "delete index nginx"
  curl "${CURL_OPTIONS[@]}"  "${ELASTIC_URI}/nginx*" -XDELETE
  echo "delete index other"
  curl "${CURL_OPTIONS[@]}" "${ELASTIC_URI}/other*" -XDELETE
  echo "delete index remote-syslog"
  curl "${CURL_OPTIONS[@]}" "${ELASTIC_URI}/remote-syslog*" -XDELETE
}

ELK_INDEX_DELETE_ELASTALERT(){
  echo "delete index elastalert"
  curl "${CURL_OPTIONS[@]}" "${ELASTIC_URI}/elastalert*" -XDELETE
}

ELK_INDEX_SIZE_SHOW(){
  echo "Shoe index sizes."
  echo "exec curl "${CURL_OPTIONS[@]}" \"${ELASTIC_URI}/_cat/indices?v\"  | awk '{print \$3 \" \" \$9}' | sort -r -k2 -n"
  curl "${CURL_OPTIONS[@]}" "${ELASTIC_URI}/_cat/indices?v"  | awk '{print $3 " " $9}' | sort -r -k2 -n
}

ELK_KIBANA_DELETE_PATTERN(){
  for I in $(ELK_KIBANA_GET_PATTERN)
  do
    echo "Delete pattern $I"
    curl "${CURL_OPTIONS[@]}" --header 'kbn-xsrf: this_is_required_header'  "${KIBANA_URI}/api/saved_objects/index-pattern/${I}" -X DELETE
  done
}

ELK_KIBANA_GET_PATTERN(){
  echo "Kibana pattern ${KIBANA_URI}"
  curl "${CURL_OPTIONS[@]}" --header 'kbn-xsrf: this_is_required_header' -X GET "${KIBANA_URI}/api/saved_objects/_find?type=index-pattern&fields=title&fields=type&per_page=1000&search_fields=title&search=nginxaccess*" | jq '.saved_objects' | jq .[].id | tr -d '"'
}

main(){
  #ELK_INDEX_DELETE
  #ELK_INDEX_DELETE_ELASTALERT
  ELK_INDEX_SIZE_SHOW
  #ELK_KIBANA_GET_PATTERN
  #ELK_KIBANA_DELETE_PATTERN
  ELK_TUNE
  ELK_ASSIGN_SHARD
  ELK_INDEX_MAPPINGS_CREATE
  ELK_INDEX_SIZE_SHOW
  #KIBANA_CREATE_INDEX
}

main
