#!/bin/bash
echo -e "Enter Index Name"
read indexname
echo -e "Index name "$indexname""
echo "recent indice's number $indexname"
recent=$(curl -s -X GET "x.x.x.x:9200/_cat/indices/"$indexname"*?v&s=index" | tail -1 | while read c1 c2 c3 c4 c5; do echo $c3 | cut -d "_" -f2 ; done)
echo $recent
echo -e "Year? [2022-2021-2020-2019-2018-2017]"
read yyear
echo -e "Month? [1-2-3-4-5-6-7-8-9-10-11-12]"
read mmonth
echo -e "Day? [1-2-3....29-30-31]"
read dday
day=$(date +%-d)
month=$(date +%-m)
year=$(date +%Y)
minus=$(python -c "from datetime import date; print (date($year,$month,$day)-date($yyear,$mmonth,$dday)).days")
echo -e "Time gap: $minus"
number=$((recent - minus))
echo -e "number of restore index: $number"
status=$(curl -s -X GET "x.x.x.x:9200/_snapshot/"$indexname"/"$indexname"_"$number"" | jq '.snapshots' | sed '1,1d' | sed '$d' | jq .state)
echo -e "Status of restore index:$status"
if [ -z "$status" ]
then
    echo -e "There is no backup for given day"
    exit -1;
elif [ $status == '"SUCCESS"' ]
then
    curl -s -X POST "x.x.x.x:9200/_snapshot/"$indexname"/"$indexname"_"$number"/_restore" -H 'Content-Type: application/json' -d' {"indices": "'$indexname'_'$number'","rename_pattern": "'$indexname'_(.+)","rename_replacement": "restored_$1"}'
    echo -e "Waiting..."
else
    echo error
    exit -1;
fi
echo -e "Admin user?"
read admin
while true ; do
    health=$(curl -s -XGET "http://x.x.x.x:9200/_cluster/health?pretty" | jq .status)
    if [ $health == '"green"' ]
    then
        echo -e "Password?"
        curl -s -i -u $admin -X POST "https://<GRAYLOGHOST>:443/api/system/indices/ranges/restored_"$number"/rebuild"
        sleep 10
        echo -e "Password again"
        curl -s -i -u $admin -X POST "https://<GRAYLOGHOST>:443/api/system/indices/ranges/restored_"$number"/rebuild"
        echo -e "Time range calculated"
        break
    else
        sleep 40
        echo -e "Waiting for cluster health to be green..."
    fi
done
