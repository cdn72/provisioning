#!/usr/bin/env bash
DATE=$(date +%Y.%m.%d)
TIMESTAMP=$(date +"%Y-%m-%dT%T.%6N%z")

ELASTICSEARCH_URL="${ELASTICSEARCH_URL:-http://localhost:9200}"
ELASTICSEATCH_USER="${ELASTICSEATCH_USER:-elastic_username}"
ELASTICSEATCH_PASSWORD="${ELASTICSEATCH_PASSWORD:-elastic_password}"
ELASTICSEARCH_INDEX_NAME="${ELASTICSEARCH_INDEX_NAME:-test}"
ELASTICSEATCH_INDEX_SETTINGS_FILE="${ELASTICSEATCH_INDEX_SETTINGS_FILE:-}"
ELASTICSEATCH_FILE_TO_LOAD="${ELASTICSEATCH_FILE_TO_LOAD:-/tmp/file.txt}"
ELASTICSEATCH_INDEX_URL_ROOT="${ELASTICSEARCH_URL}/${ELASTICSEARCH_INDEX_NAME}"
ELASTICSEATCH_INDEX_URL="${ELASTICSEARCH_URL}/${ELASTICSEARCH_INDEX_NAME}-${DATE}"
ELASTICSEATCH_INDEX_WILDCARD_URL="${ELASTICSEARCH_URL}/${ELASTICSEARCH_INDEX_NAME}-*"
ELASTICSEATCH_RECORD_URL="$ELASTICSEATCH_INDEX_URL/_doc/"


CURL_OPTIONS=(--silent --user ${ELASTICSEATCH_USER}:${ELASTICSEATCH_PASSWORD}  --header 'Content-Type: application/json' -H 'Cache-Control: no-cache' --insecure)

echo "ELASTICSEARCH_URL=${ELASTICSEARCH_URL}"

if [[ -z "${ELASTICSEATCH_INDEX_SETTINGS_FILE}" ]];then
    echo "Get example mappings file for elasticsearhc index ${ELASTICSEARCH_INDEX_NAME}"
    curl -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/apps/elasticsearch/mapping_example.json -o /tmp/ELASTICSEATCH_INDEX_SETTINGS_FILE.json
    ELASTICSEATCH_INDEX_SETTINGS_FILE="/tmp/ELASTICSEATCH_INDEX_SETTINGS_FILE.json"
    envsubst < /tmp/ELASTICSEATCH_INDEX_SETTINGS_FILE.json > /tmp/ELASTICSEATCH_INDEX_SETTINGS_FILE_V.json
    cat /tmp/ELASTICSEATCH_INDEX_SETTINGS_FILE_V.json > /tmp/ELASTICSEATCH_INDEX_SETTINGS_FILE.json

fi

elasticsearch_send_data() {

cat ${ELASTICSEATCH_FILE_TO_LOAD} | while IFS= read -r line; 
do
    echo "$line"
    jq -n --arg var "$line"  '.field = $var'| echo 
done;
#jq -n --arg var "$line"  '.field = $var'| curl --data @- -H "Content-Type: application/json" -POST "${ELASTICSEARCH_URL}/${ELASTICSEARCH_INDEX_NAME}/doc";
}


elasticsearch_index_create() {

    echo "Try Creating index ${ELASTICSEARCH_INDEX_NAME}"
    STATUS=$(curl -s -o /dev/null -w '%{http_code}' $ELASTICSEARCH_URL -u "${ELASTICSEATCH_USER}":"${ELASTICSEATCH_PASSWORD}")
    if [[ "${STATUS}" -eq 200 ]]; then
        printf "index ${ELASTICSEARCH_INDEX_NAME}  already exists\n"
    else
        printf "index ${ELASTICSEARCH_INDEX_NAME} not exist. Try to create with file ${ELASTICSEATCH_INDEX_SETTINGS_FILE}\n"
        printf "$(cat ${ELASTICSEATCH_INDEX_SETTINGS_FILE})\n"
        curl  -XPUT "${CURL_OPTIONS[@]}" "${ELASTICSEARCH_URL}/_template/${ELASTICSEARCH_INDEX_NAME}_1" \
        --data-binary @${ELASTICSEATCH_INDEX_SETTINGS_FILE}
        echo ""
        echo "Done"
        echo "Create index template ${ELASTICSEARCH_INDEX_NAME}"
    fi
}

elasticsearch_index_delete() {
    echo "Try Delete index ${ELASTICSEATCH_INDEX_WILDCARD_URL}"
    STATUS=$(curl -XDELETE -u "${ELASTICSEATCH_USER}":"${ELASTICSEATCH_PASSWORD}" -s -o /dev/null -w '%{http_code}' ${ELASTICSEATCH_INDEX_WILDCARD_URL} )
    echo "STATUS=${STATUS}"
    if [[ "${STATUS}" -eq 200 ]]; then
        echo "Ok.Index ${ELASTICSEATCH_INDEX_WILDCARD_URL} deleted."
    elif [[ "${STATUS}" -eq 404 ]]; then
        echo "Warning.Index ${ELASTICSEATCH_INDEX_WILDCARD_URL} not exist"
    else
        echo "Error. Status code =${STATUS}"
    fi
}


elasticsearch_index_data_send () {
    local TIMESTAMP_L=$(date +"%Y-%m-%dT%T.%6N%z")
    echo "Send data with TIMESTAMP_L=$TIMESTAMP_L to ${ELASTICSEATCH_RECORD_URL}"
    curl \
    -X POST \
    ${ELASTICSEATCH_RECORD_URL} \
    -u "${ELASTICSEATCH_USER}":"${ELASTICSEATCH_PASSWORD}" \
    -H 'Cache-Control: no-cache' \
    -H 'Content-Type: application/json' \
    -d "{ 
        \"@timestamp\": \"$TIMESTAMP_L\",
        \"app_name\": \"$1\", 
        \"app_http_host\": \"$2\", 
        \"app_version\":\"$3\",
        \"tags\": \"$4\", 
        \"app_stat_value\": \"$5\"
    }"
}


elasticsearch_data_get_all () {
    echo "Get data from $ELASTICSEATCH_INDEX_URL"
    curl "${CURL_OPTIONS[@]}" -XGET "${ELASTICSEARCH_URL}/${ELASTICSEATCH_INDEX_URL}/_search?scroll=24h" -d '
{
    "size": 100,
    "query": {
        "match_all": {}
    }
}'


}

elasticsearch_index_delete
elasticsearch_index_create
for i in {1..20};do
    elasticsearch_index_data_send "test-app-$(echo $(( $RANDOM % 150 + 1 )))" "test-app-$(echo $(( $RANDOM % 150 + 1 ))).example.local" "v1.0.$(echo $(( $RANDOM % 150 + 1 )))" "tag_3,tesg_d$(echo $(( $RANDOM % 150 + 1 )))" $(echo $(( $RANDOM % 150 + 1 )))
done
elasticsearch_data_get_all

