#!/bin/sh -ex

es_on_host=$(netstat -tln|grep "9200"|awk '{print $4}'|sort -r|head -n1)

if [ "x$es_on_host" == "x" ];
then
    echo es not running
    exit -1;
fi
master_node=$(curl "http://$es_on_host/_cluster/state/master_node" 2>/dev/null|jq ".master_node"|tr -d '"')
master_node_transport=$(curl "http://$es_on_host/_nodes/_all" 2>/dev/null|jq '.nodes."'$master_node'".http.publish_address'|tr -d '"')
__tmp=$(netstat -tln|grep $master_node_transport|awk '{print $4}')
if [ "x$master_node_transport" != "x$__tmp" ];
then
    echo there is no master on this node;
    exit -1;
fi
echo we are on master node
echo archive so
INDICES="/data/archive/scripts/indices.txt"
for indice in $(cat $INDICES|cut -d',' -f1); do
    echo $indice
    mkdir -p /data/archive/snapshots/$indice
    curl "http://$master_node_transport/_cat/repositories?v" 2>/dev/null|grep $indice >/dev/null 2>&1 || {
        curl -XPUT "http://$master_node_transport/_snapshot/$indice" -H 'Content-Type: application/json' -d'
        {
            "type": "fs",
            "settings": {
                "location": "/data/archive/snapshots/'$indice'",
                "compress": true,
                "chunk_size": "512m",
                "max_snapshot_bytes_per_sec": "128m",
                "max_restore_bytes_per_sec": "128m"
            }
        }
        ' 2>/dev/null
} |grep '{"acknowledged":true}' >/dev/null 2>&1 || { echo "can not create repo"; exit -1; }
retention_cnt=$(grep $indice  $INDICES|cut -d"," -f2)
indice_2_snap=$(curl "http://$master_node_transport/_cat/indices/${indice}_*?s=index&h=index"  2>/dev/null|tr '_' ' '|sort -n -k2|tr ' ' '_'|head -n $((0-retention_cnt)))
if [ "x$indice_2_snap" != "x" ];
then
for indice_2_snap_item in "$indice_2_snap";
do
snapstatus=$(curl "http://$master_node_transport/_snapshot/$indice/$indice_2_snap_item" 2>/dev/null|jq ".status")
if [ "x$snapstatus" != "x404" ];
then
echo "snapshot already taken";
continue;
else
echo the indice $indice_2_snap will be archived;
        curl -XPUT -H 'Content-Type: application/json' "http://$master_node_transport/_snapshot/$indice/$indice_2_snap_item?wait_for_completion=true&pretty" -d '{
            "indices": "'$indice_2_snap'",
            "ignore_unavailable": true,
            "include_global_state": false
        }' 2>/dev/null
        echo deleting the indice
        curl -XDELETE "http://$master_node_transport/$indice_2_snap_item" 2>/dev/null
    fi
done
else
echo no archive required
fi
archive_retention_cnt=$(grep $indice  $INDICES|cut -d"," -f3)
archive_2_del=$(curl "http://$master_node_transport/_cat/snapshots/$indice?h=id&s=id" 2>/dev/null|tr '_' ' '|sort -n -k2|tr ' ' '_'|head -n $((0-archive_retention_cnt))| tr  '\n' ' ')
if [ "x$archive_2_del" != "x" ];
then
for archive_2_del_item in $archive_2_del;
do
    curl -XDELETE "http://$master_node_transport/_snapshot/$indice/$archive_2_del_item" 2>/dev/null
done
fi
done