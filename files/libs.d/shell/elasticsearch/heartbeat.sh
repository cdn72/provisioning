#!/bin/sh

# Inputs:
# $1 = cluster
# $2 = node
# $3 = service
# $4 = tags
# $5 = uuid
# $6 = heartbeat_frequency_seconds

# Example Usage:
# heartbeat.sh 'cluster-id' 'node-id' 'my-fancy-app' 'tag-a,tag-b,tag-c' 'cluster-id-node-id-heartbeat-my-fancy-app' 30

DATE=`date +%Y.%m.%d`
TIMESTAMP=`date +"%Y-%m-%dT%T.%3N"`

ES_INDEX_URL="https://example.com:9200/heartbeat-$DATE"
ES_RECORD_URL="$ES_INDEX_URL/doc/"

STATUS=$(curl -s -o /dev/null -w '%{http_code}' $ES_INDEX_URL -u 'elastic_username':'elastic_password')
if [ $STATUS -eq 200 ]; then
    #Index already exists...
    printf "Heartbeat index exists"

else
    #Create index and mapping if doesnt exist
    curl -X PUT \
      $ES_INDEX_URL \
      -u 'elastic_username':'elastic_password' \
      -H 'Cache-Control: no-cache' \
      -H 'Content-Type: application/json' \
      -d '{
        "mappings" : {
            "doc": {
                "properties": {
                    "@timestamp": {
                        "type": "date"
                    },
                    "cluster": {
                        "type": "keyword",
                        "ignore_above": 256
                    },
                    "node": {
                        "type": "keyword",
                        "ignore_above": 256
                    },
                    "service": {
                        "type": "keyword",
                        "ignore_above": 256
                    },
                    "tags": {
                        "type": "keyword",
                        "ignore_above": 256
                    },
                    "uuid": {
                        "type": "keyword",
                        "ignore_above": 256
                    },
                    "heartbeat_frequency_seconds": {
                        "type": "integer"
                    }
                }
            }
        }
    }'       
fi


#Second post data to it
/usr/bin/curl \
    -X POST \
    $ES_RECORD_URL \
    -u 'elastic_username':'elastic_password' \
    -H 'Cache-Control: no-cache' \
    -H 'Content-Type: application/json' \
    -d "{ 
        \"@timestamp\": \"$TIMESTAMP\",
        \"cluster\": \"$1\", 
        \"node\": \"$2\", 
        \"service\":\"$3\",
        \"tags\": \"$4\", 
        \"uuid\": \"$5\", 
        \"heartbeat_frequency_seconds\": \"$6\"
    }"


exit 0 #success