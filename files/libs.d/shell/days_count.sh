example_days_count(){
d="2021-08-01"
d_end="2021-08-31"
until [[ $d > $d_end ]]; do 
    echo "Cdr compare for data = $d"
    docker exec --env d=$d -i -u 0 $(docker ps --filter name=convy_worker --filter status=running -q) sh -c 'cd /var/www/html/${CONVY_HTTP_HOST}/current && php artisan convy:cdr-load-db $d $d'
    d=$(date -I -d "$d + 1 day")
done
}