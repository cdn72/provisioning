#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail
echo "APP_VERSION=${APP_VERSION}"
if [[ "${JOBS_LIST[*]}" =~ "elk_tune" ]]; then
    echo "Run job elk_tune"
    curl -L --silent ${ELK_SCRIPT_TUNE:-https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/apps/elasticsearch/el_tune.sh} -o /tmp/el_tune.sh && bash /tmp/el_tune.sh
fi


echo "Jobs finished"
