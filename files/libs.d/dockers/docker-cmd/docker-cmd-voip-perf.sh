#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

flags=("-f" "/dev/null")
cmd=$(command -v tail)

if [[ -n "${VOIP_PERF_EXTRA_FLAGS:-}" ]]; then
    read -r -a extra_flags <<< "$VOIP_PERF_EXTRA_FLAGS"
    flags+=("${extra_flags[@]}")
fi

flags+=("$@")
printf "** Starting Voip perf **\n"
exec "$cmd" "${flags[@]}"