#!/usr/bin/env bash
set -o errexit

cmd=$(command -v sshd)

exec "$cmd" -D -g 20 -r  -E /var/log/sshd.log