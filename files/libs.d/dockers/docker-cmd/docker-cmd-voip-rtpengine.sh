#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

flags=("--config-file" "${RTPENGINE_CONFIG_FILE:-/opt/rtpengine.prod.conf}"  "--log-stderr")
cmd=$(command -v rtpengine)

if [[ -n "${RTPENGINE_EXTRA_FLAGS:-}" ]]; then
    read -r -a extra_flags <<< "$RTPENGINE_EXTRA_FLAGS"
    flags+=("${extra_flags[@]}")
fi

flags+=("$@")
printf "** Starting Rtpengine **\n"
exec "$cmd" "${flags[@]}"