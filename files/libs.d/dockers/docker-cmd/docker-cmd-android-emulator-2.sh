#!/usr/bin/env bash

export ANDROID_ADB_FILE="${ANDROID_ADB_FILE:-/android/sdk/platform-tools/adb}"

adb_connect () {

echo "Try exec ${ANDROID_ADB_FILE} connect ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555}"
until ${ANDROID_ADB_FILE} connect ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555}
do
    echo "Try reexec ${ANDROID_ADB_FILE} connect ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555}"
    sleep 10
done
echo "DONE. ${ANDROID_ADB_FILE} connect ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555} "

}

adb_push_frida () {

if [[ -f "/android/sdk/frida-server-${FRIDA_SERVER_VERSION:-16.1.0}-android-x86_64" ]];then
    echo "Try exec ${ANDROID_ADB_FILE} -s ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555} push "${FILE_FRIDA_SERVER:-/android/sdk/frida-server-${FRIDA_SERVER_VERSION:-16.1.0}-android-x86_64}" /data/local/tmp/frida-server"
    until ${ANDROID_ADB_FILE} -s ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555} push "${FILE_FRIDA_SERVER:-/android/sdk/frida-server-${FRIDA_SERVER_VERSION:-16.1.0}-android-x86_64}" /data/local/tmp/frida-server
    do
        echo "Try reexec ${ANDROID_ADB_FILE} -s ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555} push "${FILE_FRIDA_SERVER:-/android/sdk/frida-server-${FRIDA_SERVER_VERSION:-16.1.0}-android-x86_64}" /data/local/tmp/frida-server"
        sleep 10
    done
    echo "DONE. ${ANDROID_ADB_FILE} -s ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555} push "${FILE_FRIDA_SERVER:-/android/sdk/frida-server-${FRIDA_SERVER_VERSION:-16.1.0}-android-x86_64}" /data/local/tmp/frida-server"
fi

}

adb_install_apks () {

if [[ -d "${ANDROID_APK_FILES_DIR:-/android/apk}" ]];then
    echo "Try install files in ${ANDROID_APK_FILES_DIR:-/android/apk}"
    cd ${ANDROID_APK_FILES_DIR:-/android/apk}
    if ls -F *.apk 2>&1>> /dev/null;then
        echo "Try install files in ${ANDROID_APK_FILES_DIR:-/android/apk}"
        for apk_file in $(ls -F *.apk) ;do
            echo "Try install apk file ${apk_file}"
            until ${ANDROID_ADB_FILE} -s ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555} install "${apk_file}"
            do
                echo "Try reexec ${ANDROID_ADB_FILE} -s ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555} install "${apk_file}""
                sleep 10
            done
            echo "Success. ${apk_file} installed"
        done
    else
        echo "Files in ${ANDROID_APK_FILES_DIR:-/android/apk} with extension *.apk not finded."
    fi
else
    echo "Skip install apk's files. Dir ${ANDROID_APK_FILES_DIR:-/android/apk} not exist"
fi

}

run_frida () {

echo "Try exec ${ANDROID_ADB_FILE} -s  ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555} shell su 0 \"/data/local/tmp/frida-server\" -D"
until ${ANDROID_ADB_FILE} -s  ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555} shell su 0 "/data/local/tmp/frida-server" -D
do
    echo "Try reexec ${ANDROID_ADB_FILE} -s shell su 0 "/data/local/tmp/frida-server" -D"
    sleep 10
done
}

main () {
echo "/docker-cmd.sh started at $(date)"
adb_connect
until ${ANDROID_ADB_FILE} -s  ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555} shell getprop dev.bootcomplete | grep "1"
do
    echo "Wait for getprop dev.bootcomplete == 1. Sleep "
    sleep 15
done

adb_push_frida
adb_install_apks
run_frida
echo "/docker-cmd.sh finished at $(date)"
}

main

