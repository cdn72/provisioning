#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

flags=("-nc" "-nf" "-nonat")
cmd=$(command -v freeswitch)

if [[ -n "${FRESWITCH_EXTRA_FLAGS:-}" ]]; then
    read -r -a extra_flags <<< "$FRESWITCH_EXTRA_FLAGS"
    flags+=("${extra_flags[@]}")
fi

flags+=("$@")
printf "** Starting Freeswitch **\n"
exec "$cmd" "${flags[@]}"