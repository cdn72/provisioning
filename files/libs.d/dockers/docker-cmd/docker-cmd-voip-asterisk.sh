#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

flags=("-f" "-U" "${DOCKER_IMAGE_RUN_USER:-asterisk}" "-p" "-vvvdddf")
cmd=$(command -v asterisk)

if [[ -n "${ASTERISK_EXTRA_FLAGS:-}" ]]; then
    read -r -a extra_flags <<< "$ASTERISK_EXTRA_FLAGS"
    flags+=("${extra_flags[@]}")
fi

flags+=("$@")
printf "** Starting Asterisk **\n"
printf "** Exec \"$cmd\" \"${flags[@]}\" **\n"
exec "$cmd" "${flags[@]}"