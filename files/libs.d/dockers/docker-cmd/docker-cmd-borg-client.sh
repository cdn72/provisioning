#!/usr/bin/env bash
#set -o errexit
#set -o nounset
#set -o pipefail

if [[ -f "/opt/public_libs/files/libs.d/apps/main.sh" ]];then
    . /opt/public_libs/files/libs.d/apps/main.sh
fi

export BORG_CLIENT_BACKUP_EXCLUDE_DEFAULT="/borg_client/borg_exclude_default.list"
export BORG_CLIENT_PRUNE_OPTIONS_DEFAULTS="--keep-daily=2 --keep-weekly=2 --keep-monthly=12 --keep-yearly=2 --glob-archives={{ borg_arhive }}-*"
export BORG_CLIENT_PRUNE_OPTIONS="${BORG_CLIENT_PRUNE_OPTIONS/--prefix/--glob-archives}"
export BORG_CLIENT_PRUNE_OPTIONS_DEFAULTS="${BORG_CLIENT_PRUNE_OPTIONS_DEFAULTS/--prefix/--glob-archives}"
error_message="error"


if [[ -z "${BORG_ARCHIVE_POSTFIX}" ]];then
    BORG_ARCHIVE_POSTFIX=$(date +%Y_%m_%d__%H_%M_%S)
fi
if [[ -z "${BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK}" ]];then
    BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes
fi
if [[ -z "${BORG_RELOCATED_REPO_ACCESS_IS_OK}" ]];then
    BORG_RELOCATED_REPO_ACCESS_IS_OK=yes
fi

cmd=$(command -v borg)

borg_create(){
    local flags=("create" "--stats" "--list" "--progress" "--exclude-from" "${BORG_CLIENT_BACKUP_EXCLUDE:-${BORG_CLIENT_BACKUP_EXCLUDE_DEFAULT}}" "${BORG_CLIENT_REMOTE_REPO_URL}/./${BORG_CLIENT_REPO_NAME}::${BORG_CLIENT_ARCHIVE_NAME}-${BORG_CLIENT_ARCHIVE_DATE}" ""${BORG_CLIENT_BACKUP_DIRS}"")
    flags+=("$@")
    if [[ -n "${BORG_CLIENT_EXTRA_FLAGS:-}" ]]; then
        read -r -a extra_flags <<< "$BORG_CLIENT_EXTRA_FLAGS"
        local flags+=("${extra_flags[@]}")
    fi
    printf "** --Starting Borg create --**\n"
    echo "** Exec $cmd ${flags[@]}"
    eval "$cmd" "${flags[@]}"
}

borg_prune(){
    local flags=("prune" "--stats" "--list" "--force" ""${BORG_CLIENT_PRUNE_OPTIONS:-${BORG_CLIENT_PRUNE_OPTIONS_DEFAULTS}}"" "${BORG_CLIENT_REMOTE_REPO_URL}/./${BORG_CLIENT_REPO_NAME}")
    flags+=("$@")
    printf "** Starting Borg prune **\n"
    echo "** Exec $cmd ${flags[@]}"
    eval "$cmd" "${flags[@]}"
}

borg_list(){
    local flags=("list" "${BORG_CLIENT_REMOTE_REPO_URL}/./${BORG_CLIENT_REPO_NAME}")
    flags+=("$@")
    printf "** Starting Borg list **\n"
    echo "** Exec $cmd ${flags[@]}"
    eval "$cmd" "${flags[@]}"
}

borg_init () {
    local flags=("init" "--encryption=none" "${BORG_CLIENT_REMOTE_REPO_URL}/./${BORG_CLIENT_REPO_NAME}")
    flags+=("$@")
    printf "** Starting Borg init **\n"
    echo "** Exec $cmd ${flags[@]}"
    eval "$cmd" "${flags[@]}"
}


borg_postscript () {
    local script_code=$1 
    local script_desc=$2
    printf "script_code=${script_code} script_desc=${script_desc}\n"

    if [[ ${script_code} != "0" ]];then
        printf "Borg ${script_desc} finished with non zero code. Send message with ${WEBHOOK_URL}\n"
        send_message_chat "Borg ${script_desc} finished with non zero code ${BORG_CLIENT_REPO_NAME}" "Was executed ${script_desc} on  ${BORG_CLIENT_REMOTE_REPO_URL}/./${BORG_CLIENT_REPO_NAME}::${BORG_CLIENT_ARCHIVE_NAME}-${BORG_CLIENT_ARCHIVE_DATE}"
    fi

    if [[ "${BORG_CLIENT_VERBOSE:-false}" == "true" ]];then
        printf "Borg ${script_desc} finished success on ${BORG_CLIENT_REPO_NAME}. Send message with ${WEBHOOK_URL}\n"
        send_message_chat "Borg ${script_desc} finished success on ${BORG_CLIENT_REPO_NAME}" "Was executed ${script_desc} on  ${BORG_CLIENT_REMOTE_REPO_URL}/./${BORG_CLIENT_REPO_NAME}::${BORG_CLIENT_ARCHIVE_NAME}-${BORG_CLIENT_ARCHIVE_DATE}"
    else
        printf "Skip send message to ${WEBHOOK_URL} . BORG_CLIENT_VERBOSE=${BORG_CLIENT_VERBOSE}\n"
    fi
}

main () {

    if [[ ${BORG_CLIENT_ACTION} == "init" ]];then 
        borg_init "$@"
    fi

    if [[ ${BORG_CLIENT_ACTION} == "create" ]];then 
        borg_create "$@"
    fi

    if [[ ${BORG_CLIENT_ACTION} == "prune" ]];then 
        borg_prune "$@" 
    fi

    if [[ $? != "0" ]];then
        borg_postscript "1" "${BORG_CLIENT_ACTION}"
    else
        borg_postscript "0" "${BORG_CLIENT_ACTION}"
    fi
    
}
main