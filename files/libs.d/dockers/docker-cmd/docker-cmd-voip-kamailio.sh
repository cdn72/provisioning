#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

flags=("-f" "${KAMAILIO_CFGFILE:-/etc/kamailio/kamailio.cfg}" "-DD" "-E")
cmd=$(command -v kamailio)

if [[ -n "${KAMAILIO_EXTRA_FLAGS:-}" ]]; then
    read -r -a extra_flags <<< "$KAMAILIO_EXTRA_FLAGS"
    flags+=("${extra_flags[@]}")
fi

flags+=("$@")
printf "** Starting Kamailio **\n"
exec "$cmd" "${flags[@]}"