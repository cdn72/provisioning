#!/usr/bin/env bash

####-----------####
####-VARIABLES-####
####-----------####
export PASSWORD=
export HTTP_PASSWORD=
export ANDROID_AVD_HOME="${ANDROID_AVD_HOME:-/home/${DOCKER_IMAGE_RUN_USER:-user}/.android/avd}"
export ANDROID_ADV_NAME="${ANDROID_ADV_NAME:-Pixel_2_API_29}"
export ANDROID_FRIDA_SOURCE_FILE="${ANDROID_FRIDA_SOURCE_FILE:-/android/sdk/frida-server-16.1.3-android-x86_64}"
export ANDROID_FRIDA_TARGET_FILE="${ANDROID_FRIDA_TARGET_FILE:-/data/local/tmp/frida-server}"
export ANDROID_FRIDA_ARGS="${ANDROID_FRIDA_ARGS:---listen=0.0.0.0 -P}"
export ANDROID_EMULATOR_DIR="${ANDROID_EMULATOR_DIR:-/home/${DOCKER_IMAGE_RUN_USER:-user}/Android/Sdk/emulator}"
export ANDROID_ADB_FILE="${ANDROID_ADB_FILE:-/home/${DOCKER_IMAGE_RUN_USER}/Android/Sdk/platform-tools/adb}"
export ANDROID_EMULATOR_EXTRA_ARGS="${ANDROID_EMULATOR_EXTRA_ARGS}"
export ANDROID_EMULATOR_ARGS="${ANDROID_EMULATOR_ARGS:--avd ${ANDROID_ADV_NAME} -verbose -grpc 8554 -skip-adb-auth  -no-snapshot -feature AllowSnapshotMigration -gpu swiftshader_indirect}"
export ANDROID_APK_FILES_DIR="${ANDROID_APK_FILES_DIR:-/android/apk/${ANDROID_ADV_NAME}}"
export CHECK_DIRS="${CHECK_DIRS:-/data/log /opt/public_libs/Files/libs.d/apps/android}"
export CHECK_FILES="${CHECK_FILES:-/home/${DOCKER_IMAGE_RUN_USER:-user}/.android/avd/${ANDROID_ADV_NAME}.avd/multiinstance.lock /home/${DOCKER_IMAGE_RUN_USER:-user}/.android/avd/${ANDROID_ADV_NAME}.avd/hardware-qemu.ini.lock}"
export FRIDA_LOG_DIR="/data/log"
export FRIDA_LOG_FILE="${FRIDA_LOG_FILE:-${FRIDA_LOG_DIR}/frida-server-x86_64.log}"
export FRIDA_RESTART_LOG_FILE="${FRIDA_LOG_DIR}/frida-server-restart.log"
export OPENVPN_ENABLED="${OPENVPN_ENABLED:-0}"
export OPENVPN_FILES_DIR="${OPENVPN_FILES_DIR:-/data/openvpn/current}"
export OPENVPN_CUSTOM_ROUTES="${OPENVPN_CUSTOM_ROUTES:-10.50.0.0/24 10.151.0.0/24 10.48.0.0/24 10.49.0.0/24 10.42.0.0/16 10.43.0.0/16 172.16.0.0/12 192.168.0.0/16}"
export IP_ADDR_CONTAINER_DNS_SERVER_FALLBACK=${IP_ADDR_CONTAINER_DNS_SERVER_FALLBACK:-10.28.0.1}
export TINI_SUBREAPER=1
export DOCKER_IMAGE_RUN_USER="user"
export HOME="/home/${DOCKER_IMAGE_RUN_USER:-user}"



run () {
    if [[ "$VERBOSE" -lt 0 ]]; then
        VERBOSE=0
    fi
    if [[ "$VERBOSE" -gt 1 ]]; then
        echo "COMMAND: $@"
    fi
    case $VERBOSE in
        0|1)
             eval "$@" >/dev/null 2>&1
             ;;
        2)
            eval "$@" >/dev/null
            ;;
        *)
            eval "$@"
            ;;
    esac
}

var_append () {
    local _var_append_varname
    _var_append_varname=$1
    shift
    if test "$(var_value $_var_append_varname)"; then
        eval $_var_append_varname=\$$_var_append_varname\'\ $(_var_quote_value "$*")\'
    else
        eval $_var_append_varname=\'$(_var_quote_value "$*")\'
    fi
}
forward_loggers() {
  run mkdir /tmp/android-unknown
  run mkfifo /tmp/android-unknown/kernel.log
  run mkfifo /tmp/android-unknown/logcat.log
  #echo "emulator: It is safe to ignore the warnings from tail. The files will come into existence soon."
  #tail --retry -f /tmp/android-unknown/goldfish_rtc_0 | sed -u 's/^/video: /g' &
  #cat /tmp/android-unknown/kernel.log | sed -u 's/^/kernel: /g' &
  #cat /tmp/android-unknown/logcat.log | sed -u 's/^/logcat: /g' &
}

get_container_ip () {
    ip  addr show  | grep 'inet' | grep -ve inet6 | grep -ve "127.0.0.1" |  grep "${CONTAINER_NETWORK:-172}" | awk  '{ print $2 }' | cut -d'/' -f1 | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b"
    return
}

if [[ -z ${IP_ADDR_SOCAT_DEFAULT} ]];then
    export IP_ADDR_SOCAT_DEFAULT=$(get_container_ip)
fi
if [[ -z ${IP_ADDR_SOCAT_5554} ]];then
    export IP_ADDR_SOCAT_5554="0.0.0.0"
fi
if [[ -z ${IP_ADDR_SOCAT_5555} ]];then
    export IP_ADDR_SOCAT_5555="0.0.0.0"
fi
if [[ -z ${IP_ADDR_SOCAT_27042} ]];then
    export IP_ADDR_SOCAT_27042="0.0.0.0"
fi

if [[ -z ${IP_ADDR_SOCAT_18888} ]];then
    export IP_ADDR_SOCAT_18888="0.0.0.0"
fi

export ip_addr_get=$(nslookup ${ROUTE_GET_DOMAIN:-google.com} | grep Address | grep -ve ':53\|#53' | awk '{ print $2 }' | head -n 1)
export ipaddr_gateway_default=$(ip route get "${ip_addr_get}" | head -n 1 | awk  '{ print $3 }'  | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" )
export ipaddr_nameserver_default=$(grep nameserver /etc/resolv.conf | head -n 1 | awk '{print $NF}')

####-----------####
####-FUNCTIONS-####
####-----------####

adb_connect () {
echo "Exec ${FUNCNAME[0]}"
until ${ANDROID_ADB_FILE:-adb} connect  ${ADB_LISTER_IP:-127.0.0.1}:${ADB_LISTER_PORT:-5555}
do
    echo "Retry ${FUNCNAME[0]}"
    sleep 10
done
echo "DONE. ${FUNCNAME[0]} "
}

adb_forward() {
    echo "Exec ${FUNCNAME[0]}"
    ${ANDROID_ADB_FILE:-adb} shell "su 0 setenforce 0"
    ${ANDROID_ADB_FILE:-adb} shell setprop service.adb.tcp.port 5555
    ${ANDROID_ADB_FILE:-adb} forward tcp:27042 tcp:27042
    ${ANDROID_ADB_FILE:-adb} forward tcp:8888 tcp:8888
}

adb_push_frida () {
if [[ -f "${ANDROID_FRIDA_SOURCE_FILE}" ]];then
    echo "Try exec ${FUNCNAME[0]}"
    until ${ANDROID_ADB_FILE:-adb} push "${FILE_FRIDA_SERVER:-${ANDROID_FRIDA_SOURCE_FILE}}" ${ANDROID_FRIDA_TARGET_FILE}
    do
        echo "Retry ${FUNCNAME[0]}"
        sleep 5
    done
    echo "DONE. ${FUNCNAME[0]}"
fi
}

adb_set_apk_perms () {
    local apk_file="${1}"
    local packagename=$(aapt d permissions ${apk_file}  | grep package | awk  '{ print $2 }' )
    echo "set app perms packagename=${packagename} from file apk_file=${apk_file}"
    adb_set_package_perms "${packagename}"
    adb_set_package_settings "${packagename}"
}

adb_shell_exec() {
    local command=$1
    ${ANDROID_ADB_FILE:-adb} shell ${command}
    return
}

adb_set_package_perms() {
    local packagename="${1}"
    echo "Get android_permission for ${packagename}"
    adb_shell_exec "dumpsys package ${packagename}" 2&>1 > /tmp/${packagename}.list
    cat /tmp/${packagename}.list | grep 'android.permission.*granted.*false' | sed -e 's/^[ \t]*//' -e "/^name=android.permission\./s/^[:]//p"  | awk -F":" '{ print $1 }' > /tmp/${packagename}.filtered
    for app_perm in $(cat /tmp/${packagename}.filtered);do
        echo "Set perms app_perm=${app_perm} for packagename=${packagename}"
        ${ANDROID_ADB_FILE:-adb} shell "su 0 pm grant ${packagename} ${app_perm}" || echo "Error Set perms app_perm=${app_perm} for  packagename=${packagename}"
    done
    rm /tmp/${packagename}* -fv
}

adb_manage_apks () {
    local action="${1}"
      if [[ "${action}" == "install" ]];then
        echo "Try install apk's file from ANDROID_APK_FILES_DIR=${ANDROID_APK_FILES_DIR}"
        if [[ -d "${ANDROID_APK_FILES_DIR}" ]];then
            cd ${ANDROID_APK_FILES_DIR}
            if ls -F *.apk 2>&1>> /dev/null;then
                echo "Try install files in ${ANDROID_APK_FILES_DIR}"
                for apk_file in $(ls -F *.apk) ;do
                    echo "Try install apk file ${apk_file}. adb=${ANDROID_ADB_FILE:-adb}"
                    until ${ANDROID_ADB_FILE:-adb}  install "${apk_file}"
                    do
                        echo "Retry ${FUNCNAME[0]}"
                        sleep 10
                    done
                    echo "OK. ${action} for ${apk_file}"
                done
            else
                echo "Files in ${ANDROID_APK_FILES_DIR} with extension *.apk not finded."
            fi
        else
            echo "Skip ${action} apk's files. Dir ${ANDROID_APK_FILES_DIR} not exist"
        fi
     fi
     if [[ "${action}" == "perms_set" ]];then
        if [[ -d "${ANDROID_APK_FILES_DIR}" ]];then
            cd ${ANDROID_APK_FILES_DIR}
            if ls -F ${ANDROID_APK_FILES_DIR}/*.apk 2>&1>> /dev/null;then
                for apk_file in $(ls -F ${ANDROID_APK_FILES_DIR}/*.apk) ;do
                    adb_set_apk_perms ${apk_file}
                done
            else
                echo "Files in ${ANDROID_APK_FILES_DIR} with extension *.apk not finded."
            fi
        else
            echo "Skip ${action} apk's files. Dir ${ANDROID_APK_FILES_DIR} not exist"
        fi
     fi
}

adb_list_apks () {
    echo "Get installed app's list"
    ${ANDROID_ADB_FILE:-adb} shell pm list packages -f | grep "/data/app" > ${HOME}/${ANDROID_ADV_NAME}_packages.list
    echo "------------${FUNCNAME[0]}----------------------"
    echo "------------------------------------------------"
    cat   ${HOME}/${ANDROID_ADV_NAME}_packages.list
    echo "------------${FUNCNAME[0]}----------------------"    
    echo "------------------------------------------------"
}

adb_set_package_settings() {
    local package_name="${1}"
    case $package_name in
        "com.google.android.gms")
             eval package_settings_com_google_android_gms $package_name
             ;;
        "com.google.android.apps.tachyon")
            eval package_settings_com_google_android_apps_tachyon $package_name
            ;;
        *)
            echo "Unknown package_name"
            ;;
    esac
}

package_settings_com_google_android_apps_tachyon () {
    local package_name="${1}"
    echo "work with ${package_name}"
}

package_settings_com_google_android_gms () {
    local package_name="${1}"
    local need_dirs="/data/data/$package_name/shared_prefs"
    local device_profile_serial=""
    local securityToken=""
    local androidId=""
    local lastCheckin=""
    local versionInfo=""
    local digest=""
    local deviceDataVersionInfo=""
    mkdir -p /home/user/temp/${package_name}
cat <<OEF> /home/user/temp/${package_name}/com.google.android.gms_preferences.xml
<?xml version='1.0' encoding='utf-8' standalone='yes' ?>
<map>
    <string name="gcm_last_persistent_id"></string>
    <string name="gcm_network_mobile">0</string>
    <string name="device_profile_serial">${device_profile_serial}</string>
    <string name="gcm_network_roaming">0</string>
    <boolean name="checkin_enable_service" value="true" />
    <string name="gcm_network_wifi">0</string>
    <boolean name="gcm_enable_mcs_service" value="true" />
    <boolean name="safetynet_enabled" value="true" />
    <boolean name="droidguard_enabled" value="true" />
    <boolean name="gcm_confirm_new_apps" value="false" />
    <string name="gcm_network_other">0</string>
</map>
OEF

cat <<OEF> /home/user/temp/${package_name}/checkin.xml
<?xml version='1.0' encoding='utf-8' standalone='yes' ?>
<map>
    <long name="securityToken" value="${securityToken}" />
    <string name="digest">${digest}/string>
    <long name="lastCheckin" value="${lastCheckin}" />
    <string name="deviceDataVersionInfo">${deviceDataVersionInfo}</string>
    <string name="versionInfo">${versionInfo}</string>
    <long name="androidId" value="${androidId}" />
</map>
OEF

}

adb_bootstrap_wait() {
until ${ANDROID_ADB_FILE:-adb}  shell getprop dev.bootcomplete | grep "1"
do
    echo "Wait for getprop dev.bootcomplete == 1. Sleep 3"
    sleep 3
done
    echo "Command getprop dev.bootcomplete . Success"
}

script_create_frida_run() {

    cat <<OEF> /home/${USER:-user}/run_frida.sh
#!/usr/bin/env bash
export FRIDA_LOG_DIR="/data/log"
export FRIDA_LOG_FILE="${FRIDA_LOG_FILE:-${FRIDA_LOG_DIR}/frida-server-x86_64.log}"
export FRIDA_RESTART_LOG_FILE="${FRIDA_LOG_DIR}/frida-server-restart.log"
export DATE=\$(date +%Y-%m-%d__%H_%M_%S)

if [[ -f "/docker-cmd.sh" ]];then
    . /docker-cmd.sh
    adb_bootstrap_wait
    ${ANDROID_ADB_FILE:-adb} shell "su 0 killall frida-server" || echo "Skipped illall frida-server"
    ${ANDROID_ADB_FILE:-adb} shell "su 0 killall re.frida.helper" || echo "Skipped illall re.frida.helper"
    #${ANDROID_ADB_FILE:-adb} shell "pm clear com.google.android.apps.tachyon" || echo "skip clean app com.google.android.apps.tachyon"
    frida_pids_host=\$(ps aux | grep "adb.*frida" | grep -ve grep | awk '{ print \$2 }' )
    if [[ -n \${frida_pids_host} ]];then
        for frida_pid_host in \${frida_pids_host};do
            kill -9 \$frida_pid_host
        done
    fi
    frida_pids=\$(${ANDROID_ADB_FILE:-adb} shell "su 0 ps -A | grep frida" | awk '{ print \$2}')
    if [[ -n \${frida_pids} ]];then
        for frida_pid in \${frida_pids};do
            message="Frida pid's finded. Force kill proc with pid \${frida_pid}"
            echo "\${message}"
            echo "\${message}" >> ${FRIDA_LOG_DIR}/frida_restart_log.log
            ${ANDROID_ADB_FILE:-adb} shell "su 0 kill -9 \${frida_pid}"
            message="Frida health bad = Frida killed && restarted at \${DATE}"
        done
    else
        message="Frida killer successed"
    fi
    if [[ "\${tachyon_kill:-0}" == "1" ]];then
        tachyon_pids=\$(${ANDROID_ADB_FILE:-adb} shell "su 0 ps -A | grep com.google.android.apps.tachyon" | awk '{ print \$2}')
        if [[ -n "\${tachyon_pids}" ]];then
            for tachyon_pid in \${tachyon_pids};do
                message="tachyon pid's finded. Force kill proc with pid \${tachyon_pid}"
                echo "\${message}"
                echo "\${message}" >> /data/log/tachyon_restart_log.log
                ${ANDROID_ADB_FILE:-adb} shell "su 0 kill -9 \${tachyon_pid}"
                message="tachyon health bad = tachyon killed && restarted at \${DATE}"
            done
        else
            message="tachyon_pids is empty"
        fi
    fi
fi
echo "Start frida via  ${ANDROID_ADB_FILE:-adb} shell \"su 0 \${ANDROID_FRIDA_TARGET_FILE} \${ANDROID_FRIDA_ARGS} &\""
${ANDROID_ADB_FILE:-adb} shell "su 0 \${ANDROID_FRIDA_TARGET_FILE} \${ANDROID_FRIDA_ARGS} &"

OEF
chmod +x /home/${USER:-user}/run_frida.sh
}

supervisor_add_frida () {
    
if [[ -s ${FRIDA_LOG_FILE} ]]; then
    truncate -s 0 ${FRIDA_LOG_FILE}
fi

if [[ "${SUPERVISOR_FRIDA_ON:-1}" == "1" ]];then
    echo "Create /etc/supervisor/conf.d/frida.conf"
    cat <<OEF> /etc/supervisor/conf.d/frida.conf
[supervisord]
nodaemon=true

[program:frida]
environment=HOME="/home/${USER:-user}",USER="user"
priority=300
directory=/home/${USER:-user}/
command=/home/${USER:-user}/run_frida.sh
autostart=true
autorestart=true
stdout_logFile=${FRIDA_LOG_FILE}
stdout_logFile_maxbytes=0
stderr_logFile=${FRIDA_LOG_FILE}
stderr_logFile_maxbytes=0
startretries=500
stopwaitsecs=2
startsecs=2
user=${USER:-user}
OEF
fi

/usr/bin/supervisorctl --configuration /etc/supervisor/supervisord.conf reread
/usr/bin/supervisorctl --configuration /etc/supervisor/supervisord.conf update
}

supervisor_permisision_watcher () {

if [[ "${ADB_PERMISSION_WATCHER:-1}" == "1" ]];then
    echo "Create /etc/supervisor/conf.d/permisision_watcher.conf"
    cat <<OEF> /etc/supervisor/conf.d/permisision_watcher.conf
[supervisord]
nodaemon=true

[program:permisision_watcher]
priority=300
command=sh -c "while true; do bash /docker-cmd.sh adb_perms_set && sleep ${ADB_PERMISSION_WATCHER_TIMER:-30};done"
autostart=true
autorestart=true
stdout_logFile=/var/log/permisision_watcher.log
stdout_logFile_maxbytes=0
stderr_logFile=/var/log/permisision_watcher.log
stderr_logFile_maxbytes=0
startretries=0
stopwaitsecs=2
startsecs=2
user=root
OEF
fi
: > /var/log/permisision_watcher.log
/usr/bin/supervisorctl --configuration /etc/supervisor/supervisord.conf reread
/usr/bin/supervisorctl --configuration /etc/supervisor/supervisord.conf update
}


supervisor_add_openvpn () {
    echo "Try create openvpn /etc/supervisor/conf.d/openvpn.conf supervisor unit"
    if [[ -f "${HOME}/openvpn/openvpn.ovpn" ]];then
        cat <<OEF> /etc/supervisor/conf.d/openvpn.conf
[supervisord]
nodaemon=true

[program:openvpn]
directory=${HOME}/openvpn
command=/usr/sbin/openvpn --config openvpn.ovpn
autostart=true
autorestart=true
startretries=400
stderr_logFile=/var/log/openvpn.err
stdout_logFile=/var/log/openvpn.log
priority=999
OEF
        /usr/bin/supervisorctl --configuration /etc/supervisor/supervisord.conf reread
        /usr/bin/supervisorctl --configuration /etc/supervisor/supervisord.conf update
    else
        echo "Warning. Skip run openvpn via supervisor. File ${HOME}/openvpn/openvpn.ovpn not exist."
    fi
}

openvpn_files_prepare() {

if [[ ! -f "${HOME}/openvpn/client-connect.sh" && ! -f "${HOME}/openvpn/client-disconnect.sh" ]];then
    rm -rfv ${OPENVPN_FILES_DIR}/*.sh
    if [[ ! -d "${HOME}/openvpn" ]];then 
        mkdir -p ${HOME}/openvpn
    fi

cat <<OEF> ${HOME}/openvpn/client-connect.sh
#!/usr/bin/env bash
ip route delete default via ${ipaddr_gateway_default} dev eth0
OEF
   
cat <<OEF> ${HOME}/openvpn/client-disconnect.sh
#!/usr/bin/env bash
ip route add default via  ${ipaddr_gateway_default} dev eth0
cp /etc/resolv.conf.default /etc/resolv.conf
if [[ -f "/etc/resolv.conf.default" ]];then 
    cp /etc/resolv.conf.default /etc/resolv.conf 
fi
OEF
   if [[ ! -f "/etc/resolv.conf.default" ]];then 
       cp /etc/resolv.conf /etc/resolv.conf.default
   fi
   container_nameserver=$(grep nameserver /etc/resolv.conf | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b")
   route_dns_server=$(ip route get ${container_nameserver} | head -n 1 | awk -F 'uid' '{print $1}' | awk -F 'via' '{print $NF}')
   for openvpn_custom_route in ${OPENVPN_CUSTOM_ROUTES};do
       echo "ip route add ${openvpn_custom_route} via ${route_dns_server}" >> ${HOME}/openvpn/client-connect.sh
   done
   for openvpn_custom_route in ${OPENVPN_CUSTOM_ROUTES};do
       echo "ip route delete ${openvpn_custom_route} via ${route_dns_server}" >> ${HOME}/openvpn/client-disconnect.sh
   done
   
   if [[ "${RUNTIME:-docker}" == "docker" ]];then
       local container_ip=$(ip a l ${DOCKER_CONTAINER_INTERFACE:-eth0} | awk '/inet/ {print $2}' | cut -d/ -f1)
       sed -i "s|local 127.0.0.11 dev lo src 127.0.0.1|${ipaddr_gateway_default} dev ${DOCKER_CONTAINER_INTERFACE:-eth0} src ${container_ip}|g"  ${HOME}/openvpn/client-connect.sh
       sed -i "s|local 127.0.0.11 dev lo src 127.0.0.1|${ipaddr_gateway_default} dev ${DOCKER_CONTAINER_INTERFACE:-eth0} src ${container_ip}|g"  ${HOME}/openvpn/client-disconnect.sh
       echo "ip route add ${DOCKER_DNS_SERVER_EXTERNAL:-10.28.0.1} via ${ipaddr_gateway_default} dev ${DOCKER_CONTAINER_INTERFACE:-eth0} src ${container_ip} || echo skip " >> ${HOME}/openvpn/client-connect.sh
       echo "ip route delete ${DOCKER_DNS_SERVER_EXTERNAL:-10.28.0.1} via ${ipaddr_gateway_default} dev ${DOCKER_CONTAINER_INTERFACE:-eth0} src ${container_ip} || echo skip" >> ${HOME}/openvpn/client-disconnect.sh
   fi
   
   cp -v ${OPENVPN_FILES_DIR}/* ${HOME}/openvpn/
   chmod +rwxrwxrwx  ${HOME}/openvpn/*.sh
else
    cp -v ${OPENVPN_FILES_DIR}/* ${HOME}/openvpn/
fi
}

adb_perms_set() {
    adb_manage_apks "perms_set"
}
adb_apks_install() {
    adb_manage_apks "install"
}

run_frida() {
    /home/${USER:-user}/run_frida.sh
}

####-----------####
####---MAIN----####
####-----------####
main () {
    echo "/docker-cmd.sh started at $(date)"
    adb_bootstrap_wait
    adb_forward
    adb_push_frida
    adb_apks_install
    adb_perms_set
    adb_list_apks
    script_create_frida_run
    if [[ "${OPENVPN_ENABLED}" == "1" ]];then
        openvpn_files_prepare
        supervisor_add_openvpn
    fi
    #supervisor_add_frida
    supervisor_permisision_watcher
    echo "/docker-cmd.sh finished at $(date)"
    run_frida
    
}

if [[ -n "$1" ]];then
    echo "Try exec $1"
    eval $1
fi