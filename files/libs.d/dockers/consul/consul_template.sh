consul_template_prepare(){
  ${exec_prefix} echo "Exec funct ${FUNCNAME[0]}"
	if [[ ! -d "/opt/consul-template/config" ]];then
		mkdir -p /opt/consul-template/config
	fi
	if [[ ! -d "/opt/consul-template/templates" ]];then
		mkdir -p /opt/consul-template/templates
	fi
	
}


consul_template_gen_nginx_mysql () {

	${exec_prefix} cat <<OEF> /opt/consul-template/config/consul-template-${APP_ROLE:-${DOCKER_ROLE}}.cfg
consul { 
  auth {
    enabled = false
  }

  address = "localhost:8500"

  retry {
    enabled = true
    attempts = 12
    backoff = "250ms"
    max_backoff = "1m"
  }

  ssl {
    enabled = false
  }
}

reload_signal = "SIGHUP"
kill_signal = "SIGINT"
max_stale = "10m"
log_level = "info"

wait {
  min = "5s"
  max = "10s"
}

template {
  source = "/opt/consul-template/templates/consul-template-${APP_ROLE:-${DOCKER_ROLE}}.ctmpl"
  destination = "/etc/nginx/stream.d/${APP_ROLE:-${DOCKER_ROLE}}.conf"
  command = "/usr/sbin/nginx -s reload || true"
  command_timeout = "60s"
  perms = 0600
  backup = true 
  wait = "2s:6s"
}
OEF

	${exec_prefix} cat <<OEF> /opt/consul-template/templates/consul-template-${APP_ROLE:-${DOCKER_ROLE}}.ctmpl

upstream mysql-write {
{{range \$index, \$element := service "${DISCOVERY_SERVICE_NAME_MYSQL_WRITER:-pxc}"}}
{{if eq \$index 0}}
	server {{.Address}}:{{.Port}};#{{.Node}}{{end}}
{{end}}
}
upstream mysql-read {
{{range \$index, \$element := service "${DISCOVERY_SERVICE_NAME_MYSQL_READER:-pxc}"}}
{{if ne \$index 0}}
	server  {{.Address}}:{{.Port}}; #{{.Node}}{{end}}
{{end}}
{{range \$index, \$element := service "${DISCOVERY_SERVICE_NAME_MYSQL_READER:-pxc}"}}
{{if eq \$index 0}}
	server  {{.Address}}:{{.Port}}; #{{.Node}}{{end}} 
{{end}}

}
    server {
        listen 3306 so_keepalive=on;
        proxy_pass mysql-write;
        include ${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf;
    }
    server {
        listen 3307 so_keepalive=on;
        proxy_pass mysql-write;
        include ${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf;
    }
OEF
}


consul_template_gen_nginx_postgres () {
  ${exec_prefix} echo "Exec funct ${FUNCNAME[0]}"

  ${exec_prefix} cat <<OEF> "/opt/consul-template/config/consul-template-${APP_ROLE:-${DOCKER_ROLE}}.cfg"
consul { 
  auth {
    enabled = false
  }

  address = "localhost:8500"

  retry {
    enabled = true
    attempts = 22
    backoff = "250ms"
    max_backoff = "1m"
  }

  ssl {
    enabled = false
  }
}

reload_signal = "SIGHUP"
kill_signal = "SIGINT"
max_stale = "10m"
log_level = "info"

wait {
  min = "5s"
  max = "10s"
}

template {
  source = "/opt/consul-template/templates/consul-template-${APP_ROLE:-${DOCKER_ROLE}}.ctmpl"
  destination = "/etc/nginx/stream.d/${APP_ROLE:-${DOCKER_ROLE}}.conf"
  command = "/usr/sbin/nginx -s reload || true"
  command_timeout = "60s"
  perms = 0600
  backup = true 
  wait = "2s:6s"
}
OEF

  ${exec_prefix} cat <<OEF> "/opt/consul-template/templates/consul-template-${APP_ROLE:-${DOCKER_ROLE}}.ctmpl"

upstream postgres-write {
{{range \$index, \$element := service "${DISCOVERY_SERVICE_NAME_POSTGRES_WRITER:-postgres}"}}
{{if eq \$index 0}}
  server {{.Address}}:{{.Port}};#{{.Node}}{{end}}
{{end}}
}

upstream postgres-read {
{{range \$index, \$element := service "${DISCOVERY_SERVICE_NAME_POSTGRES_READER:-pxc}"}}
{{if ne \$index 0}}
  server  {{.Address}}:{{.Port}}; #{{.Node}}{{end}}
{{end}}
{{range \$index, \$element := service "${DISCOVERY_SERVICE_NAME_POSTGRES_READER:-pxc}"}}
{{if eq \$index 0}}
  server  {{.Address}}:{{.Port}}; #{{.Node}}{{end}} 
{{end}}

}

server {
  listen 5432 so_keepalive=on;
  proxy_connect_timeout ${NGINX_TIMEOUT_CLIENT:-6h};
  proxy_socket_keepalive on;
  proxy_pass postgres-write;
  include ${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_postgres.conf;
}
server {
  listen 5433 so_keepalive=on;
  proxy_connect_timeout ${NGINX_TIMEOUT_CLIENT:-6h};
  proxy_socket_keepalive on;
  proxy_pass postgres-read;
  include ${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_postgres.conf;
}
OEF

}
