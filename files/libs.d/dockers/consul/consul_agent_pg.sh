#!/usr/bin/env bash
start_arg="${1}"

consul_agent_start_pg(){
    echo "start_arg=${start_arg}"
    count=1;
    while [[ "$(curl -s -L -o /dev/null -w "%{http_code}" http://"${DISCOVERY_SERVICE_HOST:-consul.server}":${DISCOVERY_SERVICE_PORT:-8500})" != "200" ]];do echo "wait for http://"${DISCOVERY_SERVICE_HOST:-consul.server}":${DISCOVERY_SERVICE_PORT:-8500}. Sleep $count" && sleep $count ;done

    consul_net=$(nslookup ${DISCOVERY_SERVICE_HOST:-consul.server} | grep Address | grep -ve ':53\|#53' | awk '{ print $2 }' | head -n 1)
    ipaddr=$(ip route get "${consul_net}" | head -n 1 | awk  '{ print $5 }'  | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" )
    hostname=$(hostname)
    version_postgres=$(psql --version | awk '{print $NF}')
    name_postfix=$(uuidgen -t  | cut -b 1-12 || date "+%Y-%m-%d-%H-%S" )


    if grep -o -q "^primary_conninfo = " ${POSTGRESQL_CONF_FILE:-/opt/bitnami/postgresql/conf/postgresql.conf} ;then
        echo "primary_conninfo in config is configured .Become standby";
        server_role="standby";
    else
        echo "primary_conninfo in config not configured. Become master";
        server_role="master";
    fi

    tags="[\"${server_role}\"]";

    echo "Begin func ${FUNCNAME[0]}" 
    echo "{ \"services\": [ { \"name\": \"${CONSUL_SERVICE_NAME}\", \"id\": \"${hostname}-${name_postfix}-${server_role}\", \"address\": \"${ipaddr}\", \"port\": 5432, \"Tags\": $tags, \"checks\": [ { \"name\": \"check if postgres port is open\", \"tcp\": \"${ipaddr}:5432\", \"interval\": \"7s\", \"timeout\": \"3s\" } ] } ] }" > /tmp/${CONSUL_SERVICE_NAME}_${ipaddr}.json
    sleep 1;
    echo "show /tmp/${CONSUL_SERVICE_NAME}_${ipaddr}.json"
    echo "------------------------------------"
    cat /tmp/${CONSUL_SERVICE_NAME}_${ipaddr}.json
    echo "------------------------------------"
    sleep 1;
    start_consul_agent_command="consul agent -retry-join \"${DISCOVERY_SERVICE_HOST:-consul.server}\" -client 0.0.0.0 -bind ${ipaddr} -node=${hostname}-${name_postfix}-${server_role} -node-id=$(uuidgen -t) -log-level err -data-dir "${CONSUL_AGENT_DIR:-/tmp}" -config-file /tmp/${CONSUL_SERVICE_NAME}_${ipaddr}.json"
    if [[ $(ps aux | grep "consul.*agent.*join" | grep -ve 'grep') == "" ]];then
        if [[ "${start_arg}" == "wait" ]];then
            echo "Start sleep 15"
            sleep 15;
        fi
        echo "Consul agent not running. Try start"
        echo ${start_consul_agent_command} && eval "${start_consul_agent_command}"
        echo "sleep 5" && sleep 5
    else
        echo "consul agent already running. Make sigint and start"
        local pid=$(ps aux | grep "consul.*agent.*join" | grep -ve 'grep' | awk '{print $2}')
        kill -s SIGINT $pid && sleep 7;
        echo "${start_consul_agent_command}" && eval "${start_consul_agent_command}"
        
    fi
    echo "End func ${FUNCNAME[0]}" 
}

if [[ "${CONSUL_AGENT}" != "false" ]];then 
    consul_agent_start_pg 2>&1 & disown
fi