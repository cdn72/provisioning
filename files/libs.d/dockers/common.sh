common_init(){
    if [[ "${exec_prefix}" == "root" ]];then
        export exec_prefix=""
    else
        export exec_prefix="sudo"
    fi
}

docker_get_container_ip(){
   ## help
   ###docker_get_container_ip redis_redis.1.snqrgv1usfb32uutsgae32a23"
   local container_prefix=$1
   dpkg -s jq | grep 'Status' | grep -q "Status: install" || (apt update && apt install jq -y )
   export docker_container_name=$(docker ps --filter name=${container_prefix} --filter status=running -q)
   export docker_container_id=$(docker inspect ${docker_container_name} | jq .[].Id)
   export docker_container_ip=$(docker network inspect docker_gwbridge | jq  ".[].Containers."$docker_container_id".IPv4Address" | sed 's~[^\.[:alnum:]/]\+~~g' | cut -d'/' -f1)
   printf "%s$docker_container_ip\n"
}

docker_image_check(){
    if [ ! -d "${HOME}/.docker" ];then
        mkdir -p ${HOME}/.docker
    fi
    if [ ! -f " ${HOME}/.docker/config.json" ];then
        touch  ${HOME}/.docker/config.json
    fi
    echo '{"experimental": "enabled"}' | tee $HOME/.docker/config.json
    echo "Show config {HOME}/.docker/config.json" && cat ${HOME}/.docker/config.json 
    docker login -u "$DOCKER_REGISTRY_USER" -p "$DOCKER_REGISTRY_PASSWORD" "$CI_REGISTRY"
    echo "Check for ${DOCKER_IMAGE_WORKER_CI}"
    docker manifest inspect "${DOCKER_IMAGE_WORKER_CI}" > /dev/null
}

docker_cleanup(){
    docker rm -v $(docker ps --filter status=exited -q 2>/dev/null) 2>/dev/null
    docker rmi $(docker images --filter dangling=true -q 2>/dev/null) 2>/dev/null
}

docker_status_postres_repmgr_bitnami() {
	docker exec -ti -u 33 $(docker ps | grep "post.*repmg.*" | awk '{print $NF}' | head -n 1)  sh -c 'repmgr -f /opt/bitnami/repmgr/conf/repmgr.conf cluster show'
}

crane_image_check() {
    echo "Check for image ${DOCKER_IMAGE_WORKER_CI} with crane util"
    crane manifest "${DOCKER_IMAGE_WORKER_CI}"  > /dev/null
}

kaniko_helper() {
    echo "Begin exec ${FUNCNAME[ 0 ]}"
    if [ ! -d "/kaniko/.docker" ] ;then mkdir -p /kaniko/.docker;fi
    echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$DOCKER_REGISTRY_USER\",\"password\":\"$DOCKER_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    echo "Finish exec ${FUNCNAME[ 0 ]}"
}