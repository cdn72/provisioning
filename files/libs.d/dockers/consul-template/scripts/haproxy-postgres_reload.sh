#!/usr/bin/env bash

service_reload_haproxy_supervisord(){
    haproxy_user="${HAPROXY_USER:-haproxy}"
    chown -R ${haproxy_user}:${haproxy_user} /etc/haproxy

    code_check_config_haproxy=$(/usr/local/sbin/haproxy -c -V -f /etc/haproxy/haproxy.conf > /dev/null ; echo "return_code=$?" | awk -F= '{print $NF}')
    if [[ "${code_check_config_haproxy}" == "0" ]];then
        echo "Reload haproxy with supervisord"
        sup_haproxy_status=$(supervisorctl -c /etc/supervisor/supervisord.conf status haproxy | awk '{ print $2}' )
        if [[ "${sup_haproxy_status}" == "RUNNING" ]];then
            supervisorctl -c /etc/supervisor/supervisord.conf restart haproxy;
            export haproxy_pids=$(ps aux | grep '/etc/haproxy/haproxy.conf' | grep -ve 'grep\|consul\|template' | grep 'Ssl' | awk '{print $2}')
            if [[ ! -z $haproxy_pids ]];then
                for haproxy_pid in $haproxy_pids
                do
                    echo "Kill process with pid $haproxy_pid";
                    kill -9 $haproxy_pid;
                done
                echo "Restart haproxy via supervisor comamnd"
                supervisorctl -c /etc/supervisor/supervisord.conf restart haproxy || (echo "Skip restart")
            fi
        else
            if [[ ! -z $haproxy_pids ]];then
                for haproxy_pid in $haproxy_pids
                do
                    echo "Kill process with pid $haproxy_pid"
                    kill -9 $haproxy_pid;
done
            fi


            echo "sup_haproxy_status=$sup_haproxy_status. Skip supervisorctl -c /etc/supervisor/supervisord.conf restart haproxy"
            sleep 1;
        fi
    else
        echo "Error during code_check_config_haproxy. $(/usr/local/sbin/haproxy -c -V -f /etc/haproxy/haproxy.conf). Status_Code = ${code_check_config_haproxy}"
    fi

}

service_reload_haproxy(){
    haproxy_user="${HAPROXY_USER:-haproxy}"
    chown -R ${haproxy_user}:${haproxy_user} /etc/haproxy
    sleep 2;
    code_check_config_haproxy=$(/usr/local/sbin/haproxy -c -V -f /etc/haproxy/haproxy.conf > /dev/null ; echo "return_code=$?" | awk -F= '{print $NF}')
    if [[ "${code_check_config_haproxy}" == "0" ]];then
        haproxy -f /etc/haproxy/haproxy.conf -D -p /usr/share/haproxy/haproxy.pid -sf $(cat /usr/share/haproxy/haproxy.pid)
    else
        echo "error haproxy config"
    fi
}



main(){
    if [[ "${DOCKER_ROLE:-hapxoxy}" ]];then
        service_reload_haproxy_supervisord
        #service_reload_haproxy
    fi
}
main