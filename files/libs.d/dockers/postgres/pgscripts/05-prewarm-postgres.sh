#!/usr/bin/env bash

PG_DB_NAME="${1}"
DATETIME=$(date +%F_+%T)
RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
BLUE='\033[0;34m'
NC='\033[0m'


if [[ -n "${POSTGRES_POSTGRES_PASSWORD}" ]];then
    export pg_password=$(export | grep "POSTGRES_POSTGRES_PASSWORD.*$" | head -n1  | awk -F "=" '{print $2}' | sed 's/^.//' | sed 's/.$//' )
    export pg_user=postgres
else
    export pg_password=$(export | grep "POS.*_PAS.*$" | head -n1  | awk -F "=" '{print $2}' | sed 's/^.//' | sed 's/.$//' )
    export pg_user=$(export | grep "POS.*_USER.*$"  | awk 'END{print}' | awk -F "=" '{print $2}' | sed 's|\"||g'   | head -n 1)
fi)

echo "pg_password=${pg_password}"
echo "pg_user=${pg_user:-postgres}"

precheck () {
    if [[ "${PG_DB_NAME}" == "" ]];
    then
        echo -e "${RED} Arg 1 - \"PG_DB_NAME\" is empty. Stop script ${DATETIME} ${NC}"
        exit 1
    fi
}

prewarm_postgres() {

    echo -e "${CYAN} Начинаем разогрев базы ${PG_DB_NAME}. Время начала  ${DATETIME} ${NC}"

    echo -e "${CYAN} Creating extension prewarm_postgres if not exist in ${PG_DB_NAME} ${NC}"

    PGPASSWORD=${pg_password} psql --username=${pg_user:-postgres} psql  -qAt --dbname=${PG_DB_NAME} -c 'CREATE EXTENSION IF NOT EXISTS pg_prewarm;'

    for tbl in $(PGPASSWORD=${pg_password} psql --username=${pg_user:-postgres} -qAt --dbname=${PG_DB_NAME} -c "select tablename from pg_tables where schemaname = 'public';")
    do
        echo -e "${CYAN} MAKE WARM UP TABLE $tbl in DATABASE ${PG_DB_NAME} begin at ${DATETIME}${NC}"
        PGPASSWORD=${pg_password} psql  --username=${pg_user:-postgres} -qAt --dbname=${PG_DB_NAME} -c "SELECT pg_prewarm('${tbl}')";
        echo -e "${GREEN} MAKE WARM UP TABLE $tbl in DATABASE ${PG_DB_NAME} finished at ${DATETIME}${NC}"

done
    echo -e "${BLUE} Разогрев базы ${PG_DB_NAME} закончен в ${DATETIME} ${NC}"
}

main(){
    precheck
    prewarm_postgres
}
main




