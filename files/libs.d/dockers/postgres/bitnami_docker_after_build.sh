echo "----"
echo "----"
echo "PG_EXTENSION_SKIPPED=${PG_EXTENSION_SKIPPED}"
echo "----"
echo "APP_VERSION_PG_TIMESCALEDB=${APP_VERSION_PG_TIMESCALEDB}"
echo "----"
echo "APP_VERSION_PG_TIMESCALEDB_TOOLKIT=${APP_VERSION_PG_TIMESCALEDB_TOOLKIT}"
echo "----"
echo "----"
install_packages nano curl wget iproute2 net-tools iputils-ping dnsutils uuid-runtime  gcc make build-essential libxml2-dev libgeos-dev libproj-dev libgdal-dev libssl-dev cmake git unzip zlib1g-dev liblz4-dev libreadline-dev wget gnupg
mkdir -p /docker-info.d/ /pgscripts /docker-entrypoint.d /home/${DOCKER_IMAGE_RUN_USER:-app-user} 
chown -Rv ${DOCKER_IMAGE_RUN_USER:-app-user}:${DOCKER_IMAGE_RUN_USER:-app-user} /home/${DOCKER_IMAGE_RUN_USER:-app-user}
if [[ ${PG_EXTENSION_SKIPPED} =~ timescaledb.* ]] ;then
    echo "Skip install timescale_db ${APP_VERSION_PG_TIMESCALEDB:-2.14.0}";
else
    if [[ "${APP_VERSION_PG_TIMESCALEDB_EXISTS_COMPILED}" =~ .*rue ]];then
        TIMESCALEDB_DB_FILE="timescaledb-${APP_VERSION_PG_TIMESCALEDB:-2.14.0}-pg-${APP_VERSION:-15.3}-${APP_VENDOR_MAIN:-bitnami}.tar.gz"
        TIMESCALEDB_FILE_REMOTE="https://gitlab.com/cdn72/extra-files/-/raw/${EXTRA_FILES_VERSION:-main}/files/postgresql/compiled_extensions/${TIMESCALEDB_DB_FILE}"

        echo "GET compiled timescaled extension APP_VERSION_PG_TIMESCALEDB=${APP_VERSION_PG_TIMESCALEDB:-2.14.0} from ${TIMESCALEDB_FILE_REMOTE}"
        curl -L ${TIMESCALEDB_FILE_REMOTE} -o /tmp/${TIMESCALEDB_DB_FILE}
        cd /tmp && tar -xvf ${TIMESCALEDB_DB_FILE}
        cp timescaledb*.so /opt/bitnami/postgresql/lib/
        cp timescaledb.control /opt/bitnami/postgresql/share/extension/timescaledb.control
        cp timescaledb--*.sql /opt/bitnami/postgresql/share/extension/
    else
        echo "BEgin install timescale_db ${APP_VERSION_PG_TIMESCALEDB:-2.14.0}";
        git config --global advice.detachedHead false;git clone ${GIT_REPO_TIMESCALEDB:-https://github.com/timescale/timescaledb.git} /tmp/${APP_VERSION_PG_TIMESCALEDB:-2.14.0} \
        && export C_INCLUDE_PATH=/opt/bitnami/postgresql/include/:/opt/bitnami/common/include/ \
        && export LIBRARY_PATH=/opt/bitnami/postgresql/lib/:/opt/bitnami/common/lib/ \
        && export LD_LIBRARY_PATH=/opt/bitnami/postgresql/lib/:/opt/bitnami/common/lib/ \
        && cd /tmp/${APP_VERSION_PG_TIMESCALEDB:-2.14.0} && git checkout ${APP_VERSION_PG_TIMESCALEDB:-2.14.0} \
        && ./bootstrap && cd build && make && make install
    fi
fi

if [[ ${PG_EXTENSION_SKIPPED} =~ .*timescaledb_toolkit.* ]] ;then
    echo "Skip install timescaledb_toolkit with version ${APP_VERSION_PG_TIMESCALEDB_TOOLKIT:-1.18.0}"
else
    if [[ "${APP_VERSION_PG_TIMESCALEDB_TOOLKIT_EXISTS_COMPILED}" =~ .*rue ]];then
        TIMESCALEDB_DB_TOOLKIT_FILE="timescaledb_toolkit-${APP_VERSION_PG_TIMESCALEDB_TOOLKIT:-1.18.0}-pg-${APP_VERSION:-15.3}-${APP_VENDOR_MAIN:-bitnami}.tar.gz"
        TIMESCALEDB_DB_TOOLKIT_FILE_REMOTE="https://gitlab.com/cdn72/extra-files/-/raw/${EXTRA_FILES_VERSION:-main}/files/postgresql/compiled_extensions/${TIMESCALEDB_DB_TOOLKIT_FILE}"
        echo "GET compiled timescaledb_toolkit extension APP_VERSION_PG_TIMESCALEDB_TOOLKIT=${APP_VERSION_PG_TIMESCALEDB_TOOLKIT:-1.18.0} from ${TIMESCALEDB_DB_TOOLKIT_FILE_REMOTE}"
        curl -L ${TIMESCALEDB_DB_TOOLKIT_FILE_REMOTE} -o /tmp/${TIMESCALEDB_DB_TOOLKIT_FILE}
        cd /tmp && tar -xvf ${TIMESCALEDB_DB_TOOLKIT_FILE}
        cp timescaledb_toolkit-${APP_VERSION_PG_TIMESCALEDB_TOOLKIT:-1.18.0}.so /opt/bitnami/postgresql/lib/timescaledb_toolkit-${APP_VERSION_PG_TIMESCALEDB_TOOLKIT:-1.18.0}.so
        cp timescaledb_toolkit.control /opt/bitnami/postgresql/share/extension/timescaledb_toolkit.control
        cp timescaledb_toolkit--*.sql /opt/bitnami/postgresql/share/extension/
    else
        echo "Begin compile timescaledb_toolkit from source APP_VERSION_PG_TIMESCALEDB_TOOLKIT=${APP_VERSION_PG_TIMESCALEDB_TOOLKIT:-1.18.0}" \
        && sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt bullseye-pgdg main" > /etc/apt/sources.list.d/pgdg.list' \
        && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
        && apt update ; apt-get install make gcc pkg-config clang  libssl-dev postgresql-server-dev-${APP_VERSION/.*/} -y \
        && export C_INCLUDE_PATH=/opt/bitnami/postgresql/include/:/opt/bitnami/common/include/ \
        && export LIBRARY_PATH=/opt/bitnami/postgresql/lib/:/opt/bitnami/common/lib/ \
        && export LD_LIBRARY_PATH=/opt/bitnami/postgresql/lib/:/opt/bitnami/common/lib/ \
        && su -s /bin/bash -c "cd /home/${DOCKER_IMAGE_RUN_USER:-app-user}; curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs -o /tmp/sh.rustup.rs && chmod +x /tmp/sh.rustup.rs && sh /tmp/sh.rustup.rs -y" ${DOCKER_IMAGE_RUN_USER:-app-user} \
        && su -s /bin/bash -c "echo \"Cargo install to user ${DOCKER_IMAGE_RUN_USER:-app-user}. Source file /home/${DOCKER_IMAGE_RUN_USER:-app-user}/.cargo/env\"" ${DOCKER_IMAGE_RUN_USER:-app-user} \
        && su -s /bin/bash -c "cd /home/${DOCKER_IMAGE_RUN_USER:-app-user}; source /home/${DOCKER_IMAGE_RUN_USER:-app-user}/.cargo/env; cargo install --version '=0.10.2' --force cargo-pgrx;" ${DOCKER_IMAGE_RUN_USER:-app-user} \
        && su -s /bin/bash -c "echo \"Cargo cargo pgrx init --pg${APP_VERSION/.*/} pg_config. Source file /home/${DOCKER_IMAGE_RUN_USER:-app-user}/.cargo/env\"" ${DOCKER_IMAGE_RUN_USER:-app-user} \
        && su -s /bin/bash -c "cd /home/${DOCKER_IMAGE_RUN_USER:-app-user}; source /home/${DOCKER_IMAGE_RUN_USER:-app-user}/.cargo/env; cargo pgrx init --pg${APP_VERSION/.*/} pg_config; git config --global advice.detachedHead false;git clone -b ${APP_VERSION_PG_TIMESCALEDB_TOOLKIT:-1.18.0} https://github.com/timescale/timescaledb-toolkit /home/${DOCKER_IMAGE_RUN_USER:-app-user}/timescaledb-toolkit;cd /home/${DOCKER_IMAGE_RUN_USER:-app-user}/timescaledb-toolkit/extension && cargo pgrx install --release; cargo run --manifest-path ../tools/post-install/Cargo.toml -- pg_config" ${DOCKER_IMAGE_RUN_USER:-app-user}
    fi
fi

if [[ ${PG_EXTENSION_SKIPPED} =~ .*ip4r.* ]] ;then
    echo "Skip install ip4r with version ${APP_VERSION_PG_IP4R:-master}"
else
    if [[ "${APP_VERSION_PG_IP4R_EXISTS_COMPILED}" =~ .*rue ]];then
        echo "GET compiled timescaled extension APP_VERSION_PG_IP4R=${APP_VERSION_PG_IP4R:-master}"
        curl -L https://gitlab.com/cdn72/extra-files/-/raw/${EXTRA_FILES_VERSION:-main}/files/postgresql/compiled_extensions/ip4r-${APP_VERSION_PG_IP4R:-2.4}-pg-${APP_VERSION:-15.3}-${APP_VENDOR_MAIN:-bitnami}.tar.gz -o /tmp/ip4r-${APP_VERSION_PG_IP4R:-2.4}-pg-${APP_VERSION:-15.3}-${APP_VENDOR_MAIN:-bitnami}.tar.gz
        cd /tmp && tar -xvf ip4r-${APP_VERSION_PG_IP4R:-2.4}-pg-${APP_VERSION:-15.3}-${APP_VENDOR_MAIN:-bitnami}.tar.gz
        cp ip4r.so /opt/bitnami/postgresql/lib/ip4r.so
        cp ip4r.control /opt/bitnami/postgresql/share/extension/ip4r.control
        cp ip4r--*.sql /opt/bitnami/postgresql/share/extension/
    else
        echo "Begin compile ip4r from source APP_VERSION_PG_IP4R=${APP_VERSION_PG_IP4R:-master}"
        apt update --fix-missing; apt install -y wget gnupg gcc \
        && sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt bullseye-pgdg main" > /etc/apt/sources.list.d/pgdg.list' \
        && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
        && apt update ; apt-get install make gcc pkg-config clang  libssl-dev postgresql-server-dev-${APP_VERSION/.*/} -y \
        && export C_INCLUDE_PATH=/opt/bitnami/postgresql/include/:/opt/bitnami/common/include/ \
        && export LIBRARY_PATH=/opt/bitnami/postgresql/lib/:/opt/bitnami/common/lib/ \
        && export LD_LIBRARY_PATH=/opt/bitnami/postgresql/lib/:/opt/bitnami/common/lib/ \
        && su -s /bin/bash -c "cd /home/${DOCKER_IMAGE_RUN_USER:-app-user}; mkdir -p  /home/${DOCKER_IMAGE_RUN_USER:-app-user}/ip4r ;" ${DOCKER_IMAGE_RUN_USER:-app-user} \
        && su -s /bin/bash -c "cd /home/${DOCKER_IMAGE_RUN_USER:-app-user};git config --global advice.detachedHead false;git clone -b ${APP_VERSION_PG_IP4R:-master} https://github.com/devsadds/ip4r.git /home/${DOCKER_IMAGE_RUN_USER:-app-user}/ip4r;cd /home/${DOCKER_IMAGE_RUN_USER:-app-user}/ip4r && make && make install" ${DOCKER_IMAGE_RUN_USER:-app-user} \
        && rm -rf /home/${DOCKER_IMAGE_RUN_USER:-app-user}/ip4r 
    fi
fi

if [[ ${PG_EXTENSION_SKIPPED} =~ .*wal-g-pg.* ]] ;then
    echo "Skip install wal-g-pg with version ${APP_VERSION_WALG_PG:-3.0.0}"
else
    echo "Install https://github.com/wal-g/wal-g/releases/download/v${APP_VERSION_WALG_PG:-3.0.0}/wal-g-pg-ubuntu-20.04-amd64" \
    && curl -v -L "https://github.com/wal-g/wal-g/releases/download/v${APP_VERSION_WALG_PG:-3.0.0}/wal-g-pg-ubuntu-20.04-amd64" -o /usr/bin/wal-g-pg \
    && chmod +x /usr/bin/wal-g-pg \
    && ln -sf /usr/bin/wal-g-pg /usr/bin/wal-g \
    && wal-g -v || (echo "wal-g error" && exit 1)
fi

echo "Install pg_repack=${APP_VERSION_PG_REPACK:-ver_1.4.8}"
git clone -b ${APP_VERSION_PG_REPACK:-ver_1.4.8} https://github.com/reorg/pg_repack.git /tmp/${APP_VERSION_PG_REPACK:-ver_1.4.8} \
    && cd /tmp/${APP_VERSION_PG_REPACK:-ver_1.4.8} \
    && make && make install \
    && git clone -b ${APP_VERSION_PG_HINT_PLAN:-REL14_1_4_0} https://github.com/ossc-db/pg_hint_plan.git /tmp/${APP_VERSION_PG_HINT_PLAN:-REL14_1_4_0} \
    && cd /tmp/${APP_VERSION_PG_HINT_PLAN:-REL14_1_4_0} \
    && make && make install

echo "Install pg_partman=${APP_VERSION_PG_PARTMAN:-v4.7.1}"
git clone -b ${APP_VERSION_PG_PARTMAN:-v4.7.1} https://github.com/pgpartman/pg_partman.git /tmp/${APP_VERSION_PG_PARTMAN:-v4.7.1} \
    && cd /tmp/${APP_VERSION_PG_PARTMAN:-v4.7.1} \
    && make NO_BGW=1 install \
    && git clone https://github.com/citusdata/pg_cron.git /tmp/pg_cron_${APP_VERSION_PG_CRON:-v1.4.2} \
    && cd /tmp/pg_cron_${APP_VERSION_PG_CRON:-v1.4.2}  && PATH=$PATH make install

echo "Install extra files"
git clone -b ${PROVISIONING_VERSION:-master} https://gitlab.com/cdn72/provisioning.git /tmp/provisioning-${PROVISIONING_VERSION:-master} \
    && cd /tmp/provisioning-${PROVISIONING_VERSION:-master}/files/libs.d/dockers/postgres  \
    && cp bashrc_alias.sh  initbash_profile.sh /usr/sbin/ \
    && cp -a /tmp/provisioning-${PROVISIONING_VERSION:-master}/files/libs.d/dockers/postgres/pgscripts / \
    && cp -a /tmp/provisioning-${PROVISIONING_VERSION:-master}/files/libs.d/dockers/postgres/configs/bitnami/postgres14/conf.d/override_ts.conf /opt/bitnami/postgresql/conf/conf.d/ \
    && cp -a /tmp/provisioning-${PROVISIONING_VERSION:-master}/files/libs.d/dockers/consul/consul_agent_pg.sh /docker-entrypoint.d/consul_agent.sh \
    && curl -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/apps/postgres/pg_repack.sh -o /pgscripts/pg_repack.sh \
    && chmod +x  -v /usr/sbin/bashrc_alias.sh /usr/sbin/initbash_profile.sh /docker-entrypoint.d/consul_agent.sh \
    && chmod +x  -v -R /pgscripts \
    && curl -L  https://gitlab.com/cdn72/extra-files/-/raw/main/files/consul/consul_${CONSUL_VERSION:-1.11.5}_linux_amd64.zip  -o consul_${CONSUL_VERSION:-1.11.5}_linux_amd64.zip \
    && unzip -d /bin consul_${CONSUL_VERSION:-1.11.5}_linux_amd64.zip \
    && rm consul_${CONSUL_VERSION:-1.11.5}_linux_amd64.zip \
    && ln -sf /opt/bitnami/postgresql/bin/pg_dump /usr/bin/pg_dump \
    && ln -sf /opt/bitnami/postgresql/bin/psql /usr/bin/psql \
    && ln -sf /opt/bitnami/postgresql/bin/pg_restore /usr/bin/pg_restore \
    && sed -i '/#set -o xtrace/a bash /docker-entrypoint.d/consul_agent.sh' /opt/bitnami/scripts/postgresql/entrypoint.sh
echo "Clean packages"
    apt remove -y wget git unzip gcc make build-essential libxml2-dev libgeos-dev libproj-dev libgdal-dev libssl-dev cmake zlib1g-dev liblz4-dev libreadline-dev
    apt remove -y postgresql-server-dev-${APP_VERSION/.*/} || (echo "postgresql-server-dev-${APP_VERSION/.*/} not installed.Skip")
    apt autoremove -y
    cd  /opt/bitnami/postgresql/lib && chmod +x * \
    && chown -R ${DOCKER_IMAGE_RUN_USER:-app-user}:${DOCKER_IMAGE_RUN_USER:-app-user} /opt/bitnami /bitnami /home/${DOCKER_IMAGE_RUN_USER:-app-user} /opt/bitnami/postgresql \
    && rm -rf /tmp/* \
    && su -s /bin/bash -c "rm -rf /home/${DOCKER_IMAGE_RUN_USER:-app-user}/.cargo /home/${DOCKER_IMAGE_RUN_USER:-app-user}/.pgrx ${HOME}/.pgrx /home/${DOCKER_IMAGE_RUN_USER:-app-user}/.rustup /home/${DOCKER_IMAGE_RUN_USER:-app-user}/timescaledb-toolkit" ${DOCKER_IMAGE_RUN_USER:-app-user}