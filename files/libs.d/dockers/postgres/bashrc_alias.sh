postgresroot() {
	echo "Exec func ${FUNCNAME}"
	if [[ -n "${POSTGRES_POSTGRES_PASSWORD}" ]];then
		export pg_password=$(export | grep "POSTGRES_POSTGRES_PASSWORD.*$" | head -n1  | awk -F "=" '{print $2}' | sed 's/^.//' | sed 's/.$//' )
		export pg_user=postgres
	else
		export pg_password=$(export | grep "POS.*_PAS.*$" | head -n1  | awk -F "=" '{print $2}' | sed 's/^.//' | sed 's/.$//' )
		export pg_user=$(export | grep "POS.*_USER.*$"  | awk 'END{print}' | awk -F "=" '{print $2}' | sed 's|\"||g'   | head -n 1)
	fi
	PGPASSWORD="${pg_password}" psql  --username "${pg_user}"
}


repmgr_cluster_status () {
	echo "Exec ${FUNCNAME}"
	su  -s /bin/bash -c  "repmgr -f /opt/bitnami/repmgr/conf/repmgr.conf cluster show"  "${DOCKER_IMAGE_RUN_USER:-www-data}"
}