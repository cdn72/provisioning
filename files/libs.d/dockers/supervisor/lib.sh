supervisor_init_first(){
    if [[ ! -d "/etc/supervisor/conf.d" ]];then
        ${exec_prefix} mkdir -p /etc/supervisor/conf.d;
    fi
${exec_prefix} cat <<OEF> /etc/supervisor/supervisord.conf
[unix_http_server]
file=/run/supervisord.sock  ; the path to the socket file
[supervisord]
logfile=/var/log/supervisord.log ; main log file; default $CWD/supervisord.log
[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface
[supervisorctl]
serverurl=unix:///run/supervisord.sock ; use a unix:// URL for a unix socket
[include]
files = /etc/supervisor/conf.d/*.conf
[supervisord]
nodaemon=true
logfile=/dev/null
OEF
    export supervisor_config_dir="/etc/supervisor/conf.d"
    export supervisor_config_ext="conf"
}

supervisor_run(){
    ${exec_prefix} /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
}

supervisor_init_consul_template(){
    ${exec_prefix} echo "Exec funct  ${FUNCNAME[0]}"
    ${exec_prefix} cat <<OEF> ${supervisor_config_dir}/${FUNCNAME[0]}.${supervisor_config_ext}
[program:${FUNCNAME[0]}]
command=/usr/local/bin/consul-template -config=/opt/consul-template/config/consul-template-${APP_ROLE:-${DOCKER_ROLE}}.cfg
exitcodes=0,2
stopsignal=SIGTERM
stopwaitsecs=5
stopasgroup=false
killasgroup=false
autostart=true
autorestart=true
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
startretries=100
priority=1
OEF
}


supervisor_init_nginx(){
    ${exec_prefix} echo "Exec funct  ${FUNCNAME[0]}"
    ${exec_prefix} cat <<OEF> ${supervisor_config_dir}/${FUNCNAME[0]}.${supervisor_config_ext}
[program:${FUNCNAME[0]}]
command=nginx -g 'daemon off;'
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
autorestart=false
startretries=0
stopwaitsecs=10
stopsignal=SIGINT
priority=999
OEF
}


supervisor_init_harpoxy(){
    ${exec_prefix} cat << OEF > /etc/supervisor/conf.d/haproxy.conf

[supervisord]
nodaemon=true
logfile=/dev/null


[program:haproxy]
command=/usr/local/sbin/haproxy  -f /etc/haproxy/haproxy.conf  -p /usr/share/haproxy/haproxy.pid
exitcodes=0,2
stopsignal=SIGUSR1
stopwaitsecs=2
stopasgroup=false
killasgroup=false
autostart=true
autorestart=true
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
startretries=100
OEF
}


supervisor_init_exporter_haproxy(){
    ${exec_prefix} echo "Exec funct echo ${FUNCNAME[0]}"
    ${exec_prefix} cat << OEF > /etc/supervisor/conf.d/${FUNCNAME[0]}.conf

[supervisord]
nodaemon=true
logfile=/dev/null


[program:${FUNCNAME[0]}]
command=/etc/prometheus/exporters/haproxy_exporter --web.listen-address=":19101" --haproxy.scrape-uri=unix:/usr/share/haproxy/hapee-lb.sock
exitcodes=0,2
stopsignal=SIGTERM
stopwaitsecs=5
stopasgroup=false
killasgroup=false
autostart=true
autorestart=true
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
startretries=100
OEF
}