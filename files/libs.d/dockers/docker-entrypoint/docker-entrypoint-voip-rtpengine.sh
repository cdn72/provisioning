#!/usr/bin/env bash
set -x
app_prepare_voip-rtpengine(){

  if [[ ! -f "${RTPENGINE_CONFIG_FILE:-/opt/rtpengine.prod.conf}" ]];then
    echo "File ${RTPENGINE_CONFIG_FILE:-/opt/rtpengine.prod.conf} not exist. exit" ;
    exit 125;
  fi


  if lsmod | grep xt_RTPENGINE || modprobe xt_RTPENGINE; then 
      echo "rtpengine kernel module loaded." ;
  else
      echo "rtpengine kernel module not loaded. exit" ;
      exit 125;
  fi

  [ -z "$RTPENGINE_INTERFACES" ] || sed -i -e "s/INTERFACES/$RTPENGINE_INTERFACES/g" ${RTPENGINE_CONFIG_FILE:-/opt/rtpengine.prod.conf}
  [ -z "$RTPENGINE_LISTEN_NG_INTERFACE" ] || sed -i -e "s/LISTEN_NG_INTERFACE/$RTPENGINE_LISTEN_NG_INTERFACE/g" ${RTPENGINE_CONFIG_FILE:-/opt/rtpengine.prod.conf}
  [ -z "$RTPENGINE_LISTEN_NG_PORT" ] || sed -i -e "s/LISTEN_NG_PORT/$RTPENGINE_LISTEN_NG_PORT/g" ${RTPENGINE_CONFIG_FILE:-/opt/rtpengine.prod.conf}
  [ -z "$RTPENGINE_PORT_MIN" ] || sed -i -e "s/PORT_MIN/$RTPENGINE_PORT_MIN/g" ${RTPENGINE_CONFIG_FILE:-/opt/rtpengine.prod.conf}
  [ -z "$RTPENGINE_PORT_MAX" ] || sed -i -e "s/PORT_MAX/$RTPENGINE_PORT_MAX/g" ${RTPENGINE_CONFIG_FILE:-/opt/rtpengine.prod.conf}
}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    if [[ "${APP_NAME}" == "voip-rtpengine" ]];then
        app_prepare_voip-rtpengine
        #/usr/sbin/rtpengine --config-file  ${RTPENGINE_CONFIG_FILE:-/opt/rtpengine.prod.conf}  --log-stderr
    fi
}

main

echo ""
exec "$@"






