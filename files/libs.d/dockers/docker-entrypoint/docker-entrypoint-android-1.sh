#!/usr/bin/env bash

. /docker-cmd.sh

####-----------####
####-FUNCTIONS-####
####-----------####

app_prepare() {

if [[ -f "${ANDROID_ADB_FILE}" ]];then
    echo "Replace /usr/bin/adb with ${ANDROID_ADB_FILE}"
    if [[ -f "/usr/bin/adb" ]];then mv /usr/bin/adb /usr/bin/adb_old;fi
    sudo ln -sf ${ANDROID_ADB_FILE} /usr/bin/adb
fi

echo "Check for fils ${CHECK_FILES}"
for check_file in ${CHECK_FILES};do
if [[ -f "${check_file}" ]] ;then
  echo "Remove check File $check_file"  
  rm -v "${check_file}"
fi
done

for DIR_CHECK in ${CHECK_DIRS};do
    if [[ ! -d "${DIR_CHECK}" ]];then 
        mkdir -p "${DIR_CHECK}"
    else
        echo "Dir ${DIR_CHECK} already exist"
    fi
done

cat <<OEF> /usr/local/bin/xvfb.sh
#!/usr/bin/env sh
exec /usr/bin/Xvfb :1 -screen 0 ${RESOLUTION:-1920x1080}x24
OEF
chmod +rwx /usr/local/bin/xvfb.sh


check_system_groups="adm kvm sudo"

for check_system_group in ${check_system_groups};do
    if [ $(getent group ${check_system_group}) ]; then
        echo "group ${check_system_group} exists."
    else
        echo "group ${check_system_group} does not exist. Try create..."
        groupadd -r ${check_system_group}
    fi
done

if [[ "${DOCKER_IMAGE_RUN_USER:-user}" != "root" ]]; then

    echo "* enable custom user: ${DOCKER_IMAGE_RUN_USER:-user}"

    if ! id -u ${DOCKER_IMAGE_RUN_USER:-user} &>/dev/null ;then
        useradd --create-home --shell /bin/bash --user-group --groups adm,sudo,kvm ${DOCKER_IMAGE_RUN_USER:-user}
    fi
    if [[ "$(id -u ${DOCKER_IMAGE_RUN_USER:-user})" != "${USER_UID:-1001}" ]];then
        usermod --uid ${USER_UID:-1001} ${DOCKER_IMAGE_RUN_USER:-user}
    fi
    if [[ "$(id -g  ${DOCKER_IMAGE_RUN_USER:-user})" != "${USER_GID:-1001}" ]];then
        groupmod --gid ${USER_GID:-1001} ${DOCKER_IMAGE_RUN_USER:-user}
    fi

    for check_system_group in ${check_system_groups};do
        echo "Check user ${DOCKER_IMAGE_RUN_USER:-user} for group ${check_system_group}"
        if [[ ! "$(groups ${DOCKER_IMAGE_RUN_USER:-user})" =~ "${check_system_group}" ]]; then
          echo "Try add user  ${DOCKER_IMAGE_RUN_USER:-user} to group ${check_system_group}";       
          usermod -a -G ${check_system_group} ${DOCKER_IMAGE_RUN_USER:-user};
        else
          echo "User ${DOCKER_IMAGE_RUN_USER:-user} in group ${check_system_group}";
        fi
done
    if [ -z "$DOCKER_IMAGE_RUN_USER_PASSWORD" ]; then
        echo "  set default password to \"ubuntu\""
        DOCKER_IMAGE_RUN_USER_PASSWORD=ubuntu
    fi
    echo "Change user=${DOCKER_IMAGE_RUN_USER:-user} password to ${DOCKER_IMAGE_RUN_USER_PASSWORD}"
    echo "${DOCKER_IMAGE_RUN_USER:-user}:${DOCKER_IMAGE_RUN_USER_PASSWORD}" | chpasswd
    chown -R ${DOCKER_IMAGE_RUN_USER:-user} /dev/kvm


    if [[ -d "/dev/snd" ]];then
       chgrp -R adm /dev/snd
    fi
else
    echo "* Skip enable custom user: ${DOCKER_IMAGE_RUN_USER:-user}"
fi

if [[ ! -f "/data/log/kernel-${ANDROID_ADV_NAME}.log" ]];then
    touch /data/log/kernel-${ANDROID_ADV_NAME}.log
fi

if [[ ! -f "/data/log/logcat-${ANDROID_ADV_NAME}.log" ]];then
    touch /data/log/logcat-${ANDROID_ADV_NAME}.log
fi

cat <<OEF> /home/${DOCKER_IMAGE_RUN_USER:-user}/run_${ANDROID_ADV_NAME}.sh
#!/usr/bin/env bash
export DISPLAY=":1"
export HOME="${HOME}"
sudo ${ANDROID_EMULATOR_DIR}/emulator ${ANDROID_EMULATOR_ARGS}
OEF

chown ${DOCKER_IMAGE_RUN_USER:-user}  /home/user/run_${ANDROID_ADV_NAME}.sh
chmod +rwxrwxrwx  /home/user/run_${ANDROID_ADV_NAME}.sh 
chmod +rwxrwxrwx /android/sdk/frida-server-16.1.3-android-x86_64
if [ ! -x "$HOME/.config/pcmanfm/LXDE/" ]; then
    mkdir -p $HOME/.config/pcmanfm/LXDE/
    ln -sf /usr/local/share/doro-lxde-wallpapers/desktop-items-0.conf $HOME/.config/pcmanfm/LXDE/
fi
touch "/home/${DOCKER_IMAGE_RUN_USER:-user}/Desktop/${HOSTNAME}-${IP_ADDR_SOCAT_DEFAULT}-${ANDROID_ADV_NAME}.info"
echo "${DOCKER_IMAGE_RUN_USER:-user} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
for p_dir in "/home/user /android /data"
do
   find ${p_dir}  \! -user "${DOCKER_IMAGE_RUN_USER:-user}" -exec chown -h  ${DOCKER_IMAGE_RUN_USER:-user}:"${DOCKER_IMAGE_RUN_USER:-user}" {} +;
   find ${p_dir}  \! -group "${DOCKER_IMAGE_RUN_USER:-user}" -exec chown -h  ${DOCKER_IMAGE_RUN_USER:-user}:"${DOCKER_IMAGE_RUN_USER:-user}" {} +;
done
rm -rf /root && ln -sf /home/user /root

}

adv_prepare_Pixel_2 () {
if [[ -d "${ANDROID_AVD_HOME}/Pixel_2.avd" ]];then
    rm -rfv ${ANDROID_AVD_HOME}/Pixel_2.avd
    mkdir -p ${ANDROID_AVD_HOME}/Pixel_2.avd
else
    mkdir -p ${ANDROID_AVD_HOME}/Pixel_2.avd
fi

cat <<OEF> ${ANDROID_AVD_HOME}/Pixel_2.ini
avd.ini.encoding=UTF-8
path=/home/user/.android/avd/Pixel_2.avd
path.rel=avd/Pixel_2.avd
target=android-29
OEF


cat <<OEF> ${ANDROID_AVD_HOME}/Pixel_2.avd/config.ini
AvdId = Pixel_2
PlayStore.enabled = False
avd.ini.displayname = Pixel_2
avd.ini.encoding = UTF-8
# Real Pixel2 ships with 32GB
disk.dataPartition.size = 6442450944
fastboot.forceColdBoot = no
hw.accelerometer = yes
hw.audioInput = yes
hw.battery = yes
hw.camera.back = emulated
hw.camera.front = emulated
hw.cpu.ncore = 4
hw.dPad = no
hw.device.hash2 = MD5:bc5032b2a871da511332401af3ac6bb0
hw.device.manufacturer = Google
hw.gps = yes
hw.gpu.enabled = yes
hw.gpu.mode = auto
hw.initialOrientation = Portrait
hw.keyboard = yes
hw.mainKeys = no
hw.ramSize = 4096
hw.sensors.orientation = yes
hw.sensors.proximity = yes
hw.trackBall = no
runtime.network.latency = none
runtime.network.speed = full
vm.heapSize = 1024
tag.display = Google APIs
hw.lcd.density = 440
hw.lcd.height = 1920
hw.lcd.width = 1080
tag.id = google_apis
abi.type = x86_64
hw.cpu.arch = x86_64
image.sysdir.1 = system-images/android-30/default/x86_64/
OEF

chown -R ${DOCKER_IMAGE_RUN_USER:-user}:${DOCKER_IMAGE_RUN_USER:-user} ${ANDROID_AVD_HOME}
}

adv_prepare_Pixel_2_API_29 () {

if [[ -d "${ANDROID_AVD_HOME}/Pixel_2_API_29.avd" ]];then
    echo "Clean dir ${ANDROID_AVD_HOME}/Pixel_2_API_29.avd"
    rm -rfv ${ANDROID_AVD_HOME}/Pixel_2_API_29.avd
    mkdir -p ${ANDROID_AVD_HOME}/Pixel_2_API_29.avd
else
    mkdir -p ${ANDROID_AVD_HOME}/Pixel_2_API_29.avd
fi

cat <<OEF> ${ANDROID_AVD_HOME}/Pixel_2_API_29.ini
avd.ini.encoding=UTF-8
path=/home/user/.android/avd/Pixel_2_API_29.avd
path.rel=avd/Pixel_2_API_29.avd
target=android-29
OEF

cat <<OEF> ${ANDROID_AVD_HOME}/Pixel_2_API_29.avd/config.ini
AvdId = Pixel_2_API_29
PlayStore.enabled = false
abi.type = x86_64
avd.ini.displayname = Pixel_2_API_29
avd.ini.encoding = UTF-8
disk.dataPartition.size = 6442450944
fastboot.chosenSnapshotFile = 
fastboot.forceChosenSnapshotBoot = no
fastboot.forceColdBoot = yes
fastboot.forceFastBoot = no
hw.accelerometer = yes
hw.arc = false
hw.audioInput = yes
hw.battery = yes
hw.camera.back = virtualscene
hw.camera.front = emulated
hw.cpu.arch = x86_64
hw.cpu.ncore = 4
hw.dPad = no
hw.device.hash2 = MD5:55acbc835978f326788ed66a5cd4c9a7
hw.device.manufacturer = Google
hw.device.name = pixel_2
hw.gps = yes
hw.gpu.enabled = yes
hw.gpu.mode = auto
hw.initialOrientation = Portrait
hw.keyboard = yes
hw.lcd.density = 420
hw.lcd.height = 1920
hw.lcd.width = 1080
hw.mainKeys = no
hw.ramSize = 4096
hw.sdCard = yes
hw.sensors.orientation = yes
hw.sensors.proximity = yes
hw.trackBall = no
image.sysdir.1 = system-images/android-29/microg/x86_64/
runtime.network.latency = none
runtime.network.speed = full
sdcard.size = 512M
showDeviceFrame = yes
skin.dynamic = yes
skin.name = pixel_2
skin.path = /home/user/Android/Sdk/skins/pixel_2
tag.display = microG
tag.id = microg
vm.heapSize = 1024
OEF

chown -R ${DOCKER_IMAGE_RUN_USER:-user}:${DOCKER_IMAGE_RUN_USER:-user} ${ANDROID_AVD_HOME}
}


adv_prepare_Pixel_3_AP_29 () {

if [[ -d "${ANDROID_AVD_HOME}/Pixel_3_AP_29.avd" ]];then
    rm -rfv ${ANDROID_AVD_HOME}/Pixel_3_AP_29.avd
    mkdir -p ${ANDROID_AVD_HOME}/Pixel_3_AP_29.avd
else
    mkdir -p ${ANDROID_AVD_HOME}/Pixel_3_AP_29.avd
fi

cat <<OEF> ${ANDROID_AVD_HOME}/Pixel_3_AP_29.ini
avd.ini.encoding=UTF-8
path=/home/user/.android/avd/Pixel_3_AP_29.avd
path.rel=avd/Pixel_3_AP_29.avd
target=android-29
OEF


cat <<OEF> ${ANDROID_AVD_HOME}/Pixel_3_AP_29.avd/config.ini
AvdId=Pixel_3_AP_29
PlayStore.enabled=false
abi.type=x86_64
avd.ini.displayname=Pixel_3_AP_ 29
avd.ini.encoding=UTF-8
disk.dataPartition.size=16G
fastboot.chosenSnapshotFile=
fastboot.forceChosenSnapshotBoot=no
fastboot.forceColdBoot=yes
fastboot.forceFastBoot=no
hw.accelerometer=yes
hw.arc=false
hw.audioInput=yes
hw.battery=yes
hw.camera.back=virtualscene
hw.camera.front=emulated
hw.cpu.arch=x86_64
hw.cpu.ncore=6
hw.dPad=no
hw.device.hash2=MD5:8a60718609e0741c7c0cc225f49c5590
hw.device.manufacturer=Google
hw.device.name=pixel_3
hw.gps=yes
hw.gpu.enabled=no
hw.gpu.mode=off
hw.initialOrientation=Portrait
hw.keyboard=yes
hw.lcd.density=440
hw.lcd.height=2160
hw.lcd.width=1080
hw.mainKeys=no
hw.ramSize=4096
hw.sdCard=yes
hw.sensors.orientation=yes
hw.sensors.proximity=yes
hw.trackBall=no
image.sysdir.1=system-images/android-29/microg/x86_64/
runtime.network.latency=none
runtime.network.speed=full
sdcard.size=512M
showDeviceFrame=yes
skin.dynamic=yes
skin.name=pixel_3
skin.path=/home/${DOCKER_IMAGE_RUN_USER:-user}/Android/Sdk/skins/pixel_3
tag.display=microG
tag.id=microg
vm.heapSize=1024

OEF

chown -R ${DOCKER_IMAGE_RUN_USER:-user}:${DOCKER_IMAGE_RUN_USER:-user} ${ANDROID_AVD_HOME}
}


configure_nginx () {
    mkdir -p /etc/nginx/stream.d
    rm -rf /etc/nginx/{sites-available,sites-available}
cat <<'OEF'> /etc/nginx/nginx.conf
user www-data;
worker_processes 2;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;
events {
	worker_connections 1024;
}
http {
	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;
	include /etc/nginx/mime.types;
	default_type application/octet-stream;
	ssl_prefer_server_ciphers on;
	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;
	gzip on;
	include /etc/nginx/conf.d/*.conf;
}

stream {

    log_format json_combined escape=json
    '{'
    '"nginx.time":"$time_local",'
    '"nginx.host":"",'
    '"nginx.http_host":"",'
    '"nginx.remote_addr":"$remote_addr",'
    '"nginx.http_x_forwarded_for":"",'
    '"nginx.request_method":"$protocol",'
    '"nginx.request":"",'
    '"nginx.status":"$status",'
    '"nginx.upstream_status":"",'
    '"nginx.body_bytes_sent":"$bytes_sent, $bytes_received",'
    '"nginx.http_referer":"",'
    '"nginx.request_time":"$session_time",'
    '"nginx.upstream_response_time":"$upstream_connect_time",'
    '"nginx.upstream_http_x_cache":"",'
    '"nginx.uri":"",'
    '"nginx.upstream_addr":"$upstream_addr",'
    '"nginx.upstream_response_length":"$upstream_bytes_sent, $upstream_bytes_received",'
    '"nginx.server_name":"",'
    '"nginx.upstream_cache_status":"",'
    '"nginx.user_agent":"",'
    '"nginx.request_uri":"",'
    '"nginx.request_body":""'
    '}';
	access_log /var/log/nginx/access.log json_combined;
	error_log /var/log/nginx/error.log;
    include /etc/nginx/stream.d/*.conf;
}
OEF

cat <<'OEF'> /etc/nginx/conf.d/main.cof
server {
	listen 80 default_server;
	root /usr/local/lib/web/frontend/;
	index index.html index.htm;
	location ~ .*/(api/.*|websockify) {
		try_files $uri @api$http_upgrade;
	}
	location / {
		rewrite /approot/(.*) /$1 break;
		root /usr/local/lib/web/frontend/;
	}
	location @apiwebsocket {
		proxy_connect_timeout       7d;
		proxy_send_timeout          7d;
		proxy_read_timeout          7d;
		proxy_buffering                         off;
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
		proxy_pass http://127.0.0.1:6081;
	}
	location @api {
		proxy_set_header X-Real-IP  $remote_addr;
		proxy_set_header X-Forwarded-For $remote_addr;
		proxy_set_header Host $host;
		max_ranges 0;
		proxy_pass http://127.0.0.1:6079;
	}
}
OEF

}


configure_supervisor () {
    rm -rf /etc/supervisor/conf.d/*
cat <<OEF> /etc/supervisor/supervisord.conf

[unix_http_server]
file=/var/run/supervisor.sock   ; (the path to the socket file)
chmod=0770                       ; sockef file mode (default 0700)

[supervisord]
logfile=/var/log/supervisor/supervisord.log ; (main log file;default $CWD/supervisord.log)
pidfile=/var/run/supervisord.pid ; (supervisord pidfile;default supervisord.pid)
childlogdir=/var/log/supervisor            ; ('AUTO' child log dir, default $TEMP)
user=root

[inet_http_server]
port=${SUPERVISOR_SERVER_LISTEN_ADDR:-0.0.0.0:9001}

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl=unix:///var/run/supervisor.sock ; use a unix:// URL  for a unix socket

[include]
files = /etc/supervisor/conf.d/*.conf
OEF

cat <<OEF> /etc/supervisor/conf.d/main.conf

[supervisord]
redirect_stderr=true
stopsignal=QUIT
autorestart=true
directory=/home/${DOCKER_IMAGE_RUN_USER}

[program:nginx]
priority=10
command=nginx -c /etc/nginx/nginx.conf -g 'daemon off;'
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0

[program:web]
priority=10
directory=/usr/local/lib/web/backend
command=/usr/local/lib/web/backend/run.py
stderr_logFile=/var/log/web.err
stdout_logFile=/var/log/web.log

[group:x]
programs=xvfb,wm,lxpanel,pcmanfm,x11vnc,novnc

[program:wm]
environment=DISPLAY=":1",HOME="${HOME}",USER="${DOCKER_IMAGE_RUN_USER:-user}"
priority=15
command=/usr/bin/openbox
stderr_logFile=/var/log/openbox.err
stdout_logFile=/var/log/openbox.log

[program:lxpanel]
environment=DISPLAY=":1",HOME="${HOME}",USER="${DOCKER_IMAGE_RUN_USER:-user}"
priority=15
directory=${HOME}
command=/usr/bin/lxpanel --profile LXDE
user=${DOCKER_IMAGE_RUN_USER:-user}
stderr_logFile=/var/log/lxpanel.err
stdout_logFile=/var/log/lxpanel.log

[program:pcmanfm]
environment=DISPLAY=":1",HOME="${HOME}",USER="${DOCKER_IMAGE_RUN_USER:-user}"
priority=15
directory=${HOME}
command=/usr/bin/pcmanfm --desktop --profile LXDE
user=${DOCKER_IMAGE_RUN_USER:-user}
stopwaitsecs=3
stderr_logFile=/var/log/pcmanfm.err
stdout_logFile=/var/log/pcmanfm.log

[program:xvfb]
priority=10

command=/usr/bin/Xvfb :1 -screen 0 1920x1080x24
stopsignal=KILL
stderr_logFile=/var/log/xvfb.err
stdout_logFile=/var/log/xvfb.log


[program:x11vnc]
priority=30
command=x11vnc -display :1 -xkb -forever -shared -repeat -capslock
stderr_logFile=/var/log/x11vnc.err
stdout_logFile=/var/log/x11vnc.log


[program:novnc]
priority=35
directory=/usr/local/lib/web/frontend/static/novnc
command=bash /usr/local/lib/web/frontend/static/novnc/utils/launch.sh --listen 6081
stopasgroup=true
stderr_logFile=/var/log/novnc.err
stdout_logFile=/var/log/novnc.log

OEF

sudo chown -R ${DOCKER_IMAGE_RUN_USER}  /var/log/supervisor
if [ -n "$VNC_PASSWORD" ]; then
    echo -n "$VNC_PASSWORD" > /.password1
    x11vnc -storepasswd $(cat /.password1) /.password2
    chmod 400 /.password*
    sed -i 's/^command=x11vnc.*/& -rfbauth \/.password2/' /etc/supervisor/conf.d/main.conf
    export VNC_PASSWORD=
fi

if [ -n "$X11VNC_ARGS" ]; then
    sed -i "s/^command=x11vnc.*/& ${X11VNC_ARGS}/" /etc/supervisor/conf.d/main.conf
fi

if [ -n "$OPENBOX_ARGS" ]; then
    sed -i "s#^command=/usr/bin/openbox\$#& ${OPENBOX_ARGS}#" /etc/supervisor/conf.d/main.conf
fi

if [[ "${SOCAT_ENABLED}" != "False" ]];then
    cat <<OEF> /etc/supervisor/conf.d/socat.conf

[supervisord]
nodaemon=true

[program:socat5554]
priority=45
command=socat -ls -lh tcp-listen:${PORT_SOCAT_5554:-25554},bind=${IP_ADDR_SOCAT_5554:-0.0.0.0},fork,reuseaddr tcp:127.0.0.1:5554
autostart=true
autorestart=true
startretries=0
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
[program:socat5555]
priority=45
command=socat -ls -lh tcp-listen:${PORT_SOCAT_5555:-25555},bind=${IP_ADDR_SOCAT_5555:-0.0.0.0},fork,reuseaddr tcp:127.0.0.1:5555 
autostart=true
autorestart=true
startretries=0
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
[program:socat27042]
priority=45
command=socat -ls -lh tcp-listen:${PORT_SOCAT_27042:-47042},bind=${IP_ADDR_SOCAT_27042:-0.0.0.0},fork,reuseaddr tcp:127.0.0.1:27042 
autostart=true
autorestart=true
startretries=0
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
[program:socat18888]
priority=45
command=socat -ls -lh tcp-listen:${PORT_SOCAT_18888:-18888},bind=${IP_ADDR_SOCAT_18888:-0.0.0.0},fork,reuseaddr tcp:127.0.0.1:8888 
autostart=true
autorestart=true
startretries=0
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
OEF
else
    echo "Skip socat create supervisord"
fi

#if su -s /bin/bash -c "${ANDROID_EMULATOR_DIR}/emulator -list-avds" "${DOCKER_IMAGE_RUN_USER:-user}" | grep ${ANDROID_ADV_NAME}
#then
    cat <<OEF> /etc/supervisor/conf.d/emulator_${ANDROID_ADV_NAME}.conf
[supervisord]
nodaemon=true

[program:emulator_${ANDROID_ADV_NAME}]
environment=DISPLAY=":1",HOME="${HOME}",USER="${DOCKER_IMAGE_RUN_USER:-user}"
priority=999
directory=${ANDROID_EMULATOR_DIR}
command=/home/user/run_${ANDROID_ADV_NAME}.sh
startwaitsecs=15
stopwaitsecs=15
user=${DOCKER_IMAGE_RUN_USER:-user}
stderr_logFile=/var/log/emulator_${ANDROID_ADV_NAME}.err
stdout_logFile=/var/log/emulator_${ANDROID_ADV_NAME}.log
OEF
#else
##    echo "Warning. Skip run emulator_${ANDROID_ADV_NAME} via supervisor. ANDROID_ADV_NAME=${ANDROID_ADV_NAME}  Unknown AVD name."
#fi

}

run_after_start () {
if [[ -f "/docker-cmd.sh" ]];then
    /docker-cmd.sh "main" > /data/log/docker-cmd.log 2>&1 &
fi
}

app_run () {
    echo "3. Run  --supervisord -n -c /etc/supervisor/supervisord.conf"
    exec  supervisord --nodaemon --configuration /etc/supervisor/supervisord.conf
}

main () {
    forward_loggers
    openvpn_files_prepare
    configure_nginx
    configure_supervisor
    app_prepare
    adv_prepare_Pixel_2
    adv_prepare_Pixel_2_API_29
    adv_prepare_Pixel_3_AP_29
    run_after_start
    app_run
}

main


