#!/usr/bin/env bash

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    if [[ "${APP_NAME}" == "sms-http-rx" ]];then
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_sms_http_rx
        fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main





