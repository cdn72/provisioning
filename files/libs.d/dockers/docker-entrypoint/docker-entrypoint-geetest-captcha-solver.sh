#!/usr/bin/env bash
app_prepare_geetest-captcha-solver (){
    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    if [[ -d "${APP_DIR_CURRENT}/logs" && ! -L "${APP_DIR_CURRENT}/logs" ]];then
        rm -r ${APP_DIR_CURRENT}/logs
        ln -sfr ${APP_DIR_SHARED}/logs ${APP_DIR_CURRENT}/logs
    fi
    LOG_DIRS=$(cat ${APP_DIR_SHARED}/.env  | grep LOGS_ | awk -F"=" '{print $2}')
    for LOG_DIR in ${LOG_DIRS}
    do
        if [[ ! -d "${LOG_DIR}" ]];then 
            mkdir -pv ${LOG_DIR};
            chmod 775 -R -v ${LOG_DIR}
            chmod -x -R -v ${LOG_DIR}
        else
            chmod 775 -R -v ${LOG_DIR}
            chmod -x -R -v ${LOG_DIR}
        fi
    done
    chmod 777 -R ${APP_DIR_SHARED}/logs
    cat <<OEF> /etc/proxychains4.conf
    strict_chain
    #proxy_dns
    remote_dns_subnet 224
    tcp_read_time_out 15000
    tcp_connect_time_out 8000
    localnet 127.0.0.0/255.0.0.0
    localnet 10.0.0.0/255.0.0.0
    localnet 172.16.0.0/255.255.0.0
    localnet 192.168.0.0/255.255.0.0
    [ProxyList]
    http ${HTTP_PROXY_HOST:-10.50.0.119} ${HTTP_PROXY_PORT:-3128}
OEF
    echo "Crons add for user ${DOCKER_IMAGE_RUN_USER:-www-data}"
    if [[ -f "/var/spool/cron/crontabs/${DOCKER_IMAGE_RUN_USER:-www-data}" ]]; then
        rm /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    fi
    if [[ ! -d "${APP_DIR_SHARED}/storage/logs/crons" ]];then
        mkdir -p ${APP_DIR_SHARED}/storage/logs/crons
    fi
    cat <<OEF> /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
##Crons
OEF

    cat <<OEF> /tmp/crontabs
*/30 * * * * cd ${APP_DIR_CURRENT} && python3.9 utils/refresh_tasklist.py >> ${APP_DIR_SHARED}/storage/logs/crons/refresh_tasklist.log 2>&1
00 */12 * * * find ${APP_DIR_SHARED}/logs/resources* -type f -mtime +7 -exec rm -v {} \; >> ${APP_DIR_SHARED}/storage/logs/crons/logs_clean.log 2>&1
OEF
    crontab -l -u ${DOCKER_IMAGE_RUN_USER:-www-data} | cat - /tmp/crontabs | crontab -u ${DOCKER_IMAGE_RUN_USER:-www-data} -
    echo "Cronlist"
    cat /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    echo "Fix rights"
    chown -v "${DOCKER_IMAGE_RUN_USER:-www-data}":crontab /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/consul_agent.sh
    source /opt/public_libs/files/libs.d/apps/consul_template.sh
    if [[ "${APP_NAME}" == "geetest-captcha-solver" ]];then
        app_prepare_geetest-captcha-solver
        consul_agent_start
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_geetest-captcha-solver
        #supervisor_init_refresh_google_tokens
        supervisor_init_cron
        app_fix_perms || (echo "skip app_fix_perms")
        app_prepare_run_adb
        supervisor_run
    fi
}

main






