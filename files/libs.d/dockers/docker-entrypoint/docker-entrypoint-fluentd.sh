#!/usr/bin/env bash

GEO_DB_FILE_URL_PUBLIC="https://gitlab.com/zabbix.etagi72/staticfiles/-/raw/master/files/GeoLite2-City_20210518/GeoLite2-City.mmdb"
CHECK_DIRS="${FLUENTD_LOG_DIR:-/fluentdlogs} /home/${DOCKER_IMAGE_RUN_USER:-fluentd} /fluentd /etc/fluentd ${FLUENTD_BUFFER_DIR:-/fluentdbuffer}"

app_prepare () {

cat /etc/passwd | grep ${DOCKER_IMAGE_RUN_USER:-fluentd}

if [ $? -eq 0 ]; then
    deluser ${DOCKER_IMAGE_RUN_USER:-fluentd}
fi

adduser -D -g '' -u ${FLUENTD_UID:-1000} -h /home/${DOCKER_IMAGE_RUN_USER:-fluentd} ${DOCKER_IMAGE_RUN_USER:-fluentd}

for CHECK_DIR in ${CHECK_DIRS}
do
    if [ ! -d "${CHECK_DIR}" ];then
        mkdir -p ${CHECK_DIR}
        chown -R ${DOCKER_IMAGE_RUN_USER:-fluentd} ${CHECK_DIR}
    else
        chown -R ${DOCKER_IMAGE_RUN_USER:-fluentd} ${CHECK_DIR}
    fi
done

if [ "${GEO_GET}" == "1" ];then
	curl -L "${GEO_DB_FILE_URL_PUBLIC}" -o "${GEO_FILE_FOLDER:-/home/${DOCKER_IMAGE_RUN_USER:-fluentd}}"/GeoLite2-City.mmdb || echo "Error during get GeoLite2-City.mmdb from ${GEO_DB_FILE_URL_PUBLIC}"
fi

if [[ "${ARG_VERBOSE}" == "true" ]];then
    VERBOSE="--verbose"
fi

cat <<OEF > /etc/supervisor/supervisord.conf
[unix_http_server]
file=/run/supervisord.sock  ; the path to the socket file
[supervisord]
logfile=/var/log/supervisord.log ; main log file; default /supervisord.log
[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface
[supervisorctl]
serverurl=unix:///run/supervisord.sock ; use a unix:// URL for a unix socket
[include]
files = /etc/supervisor/conf.d/*.conf
[supervisord]
nodaemon=true
logfile=/dev/null
[inet_http_server]
port=0.0.0.0:9009
username=${SUPERVISOR_AUTH_USER:-zzzz}
password=${SUPERVISOR_AUTH_PASSWORD:-zzzz}
OEF

cat <<OEF> /etc/supervisor/conf.d/fluentd.conf
[supervisord]
nodaemon=true

[program:fluentd]
command=fluentd -c ${FLUENTD_CONFIG_FILE:-/etc/fluentd/fluentd.conf} ${VERBOSE:-}
autostart=true
autorestart=unexpected
stdout_logfile=/proc/self/fd/2
stdout_logfile_maxbytes=0
stderr_logfile=/proc/self/fd/2
stderr_logfile_maxbytes=0
exitcodes=0
user=${DOCKER_IMAGE_RUN_USER:-fluentd}

OEF
    if [ -r "${FLUENTD_DEFAULT:-/etc/default/fluentd}" ]; then
        set -o allexport
        source ${FLUENTD_DEFAULT:-/etc/default/fluentd}
        set +o allexport
    fi
}

app_run_supervisord() {
    /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
}


main () {
    app_prepare
    app_run_supervisord
}

main


