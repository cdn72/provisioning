#!/usr/bin/env bash

flags=("--web.listen-address=${EXPORTER_FREESWITCH_WEB_LISTEN_ADDRESS:-:9282}"  "--web.telemetry-path=/metrics" "--freeswitch.scrape-uri=${EXPORTER_FREESWITCH_SCRAPE_URI:-tcp://localhost:8021}" "--freeswitch.password=${EXPORTER_FREESWITCH_PASSWORD:-ClueCon}")
cmd="/usr/local/bin/freeswitch_exporter"

if [[ -n "${EXPORTER_FREESWITCH_EXTRA_FLAGS:-}" ]]; then
    read -r -a extra_flags <<< "$EXPORTER_FREESWITCH_EXTRA_FLAGS"
    flags+=("${extra_flags[@]}")
fi

flags+=("$@")
printf "** Starting exporter_freeswitch **\n"
exec "$cmd" "${flags[@]}"


