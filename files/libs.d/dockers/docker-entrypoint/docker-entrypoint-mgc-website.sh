#!/usr/bin/env bash
app_prepare_mgc-website(){
    mkdir -p \
    ${APP_DIR_SHARED}/storage/logs/{api,resources,resources_tests,crons,sessions}  \
    ${APP_DIR_SHARED}/storage/{phones,sessions,views,cache,bootstrap/cache} \
    ${APP_DIR_SHARED}/storage/framework/{cache,sessions,views} \
    ${APP_DIR_SHARED}/app \
    ${APP_DIR_SHARED}/bootstrap/cache
    if [[ -f "${APP_DIR_SHARED}/.env" ]];then
        ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    fi
    if [[  ! -L "${APP_DIR_CURRENT}/storage" ]];then
        mv ${APP_DIR_CURRENT}/storage ${APP_DIR_CURRENT}/storage.old
        ln -sf ${APP_DIR_SHARED}/storage ${APP_DIR_CURRENT}/storage
    fi
    if [[ ! -d "${APP_DIR_SHARED}/bootstrap/cache" ]];then
        mkdir -p ${APP_DIR_SHARED}/bootstrap/cache
        chown -R ${DOCKER_IMAGE_RUN_USER:-www-data} ${APP_DIR_SHARED}/bootstrap/cache
    fi

    if [[  ! -L "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        if [[ -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
            mv ${APP_DIR_CURRENT}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache.old
        fi
        ln -sf ${APP_DIR_SHARED}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache
    fi
    su -s /bin/bash -c "cd ${APP_DIR_CURRENT} && php artisan cache:clear; php artisan route:clear;php artisan route:cache;php artisan config:cache; php artisan optimize; php artisan storage:link" ${DOCKER_IMAGE_RUN_USER:-www-data} 
}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/supercronic.sh

    if [[ "${APP_NAME}" == "mgc-website" ]];then
        app_fix_perms || (echo "skip perms fix")
        app_prepare_mgc-website
        supervisor_init_first

        if [[ "${APP_FUNCTION}" == "backend" ]];then
            supervisor_init_php_fpm
            supervisor_init_nginx
            supervisor_init_queue_laravel
            nginx_config_gen_http_backend_mgc-website
            if  grep -q 'OCTANE_SERVER=roadrunner' "${APP_DIR_CURRENT}/.env" ; then 
                supervisor_init_octane_mgc-website
            fi            
        fi
        app_fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main






