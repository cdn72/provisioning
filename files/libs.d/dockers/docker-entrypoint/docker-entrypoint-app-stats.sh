#!/usr/bin/env bash



main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/consul_agent.sh
    source /opt/public_libs/files/libs.d/apps/consul_template.sh
    ##libs app
    source /opt/public_libs/files/libs.d/apps/app-stats.sh

    supervisor_init_first
    supervisor_init_dumb
    app_prepare_app-stats
    sleep 3;
    consul_agent_start

    app_fix_perms || (echo "skip perms fix")


    if [[ "${APP_FUNCTION}" == "backend" ]];then
        supervisor_init_octane_app-stats
    fi
    if [[ "${APP_FUNCTION}" == "dispatcher" ]];then
        supervisor_init_dispatcher_app-stats
    fi
    if [[ "${APP_FUNCTION}" == "cron" ]];then
        supervisor_init_cron_app-stats
    fi
    if [[ "${APP_FUNCTION}" == "db-listen" ]];then
        supervisor_init_db-listen_app-stats
    fi
    
    supervisor_run


}

main








