#!/usr/bin/env bash

app_prepare_captcha-solver () {
    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    if [[ -d "${APP_DIR_CURRENT}/logs" && ! -L "${APP_DIR_CURRENT}/logs" ]];then
        rm -r ${APP_DIR_CURRENT}/logs
        ln -sfr ${APP_DIR_SHARED}/logs ${APP_DIR_CURRENT}/logs
    fi
    LOG_DIRS=$(cat ${APP_DIR_SHARED}/.env  | grep LOGS_ | awk -F"=" '{print $2}')
    for LOG_DIR in ${LOG_DIRS}
    do
        if [[ ! -d "${LOG_DIR}" ]];then 
            mkdir -pv ${LOG_DIR};
            chmod 775 -R -v ${LOG_DIR}
            chmod -x -R -v ${LOG_DIR}
        else
            chmod 775 -R -v ${LOG_DIR}
            chmod -x -R -v ${LOG_DIR}
        fi
    done
    chmod 777 -R ${APP_DIR_SHARED}/logs
    cat <<OEF> /etc/proxychains4.conf
    strict_chain
    #proxy_dns
    remote_dns_subnet 224
    tcp_read_time_out 15000
    tcp_connect_time_out 8000
    localnet 127.0.0.0/255.0.0.0
    localnet 10.0.0.0/255.0.0.0
    localnet 172.16.0.0/255.240.0.0
    localnet 192.168.0.0/255.255.0.0
    [ProxyList]
    http ${HTTP_PROXY_HOST:-10.50.0.119} ${HTTP_PROXY_PORT:-3128}
OEF
    echo "Crons add for user ${DOCKER_IMAGE_RUN_USER:-app-user}"
    if [[ -f "/var/spool/cron/crontabs/${DOCKER_IMAGE_RUN_USER:-app-user}" ]]; then
        rm /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-app-user}"
    fi
    cat <<OEF> /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-app-user}"
##Crons
OEF
    cat <<OEF> /tmp/crontabs
*/10 * * * * cd ${APP_DIR_CURRENT} && /usr/local/bin/python3.9 ${APP_DIR_CURRENT}/utils/refresh_tasklist.py  >> ${APP_DIR_CURRENT}/logs/crons/refresh_tasklist.log 2>&1
00 */12 * * * find ${APP_DIR_CURRENT}/logs/resources* -type f -mtime +7 -exec rm -v {} \; >> ${APP_DIR_SHARED}/logs/crons/logs_clean.log 2>&1
OEF
    crontab -l -u ${DOCKER_IMAGE_RUN_USER:-app-user} | cat - /tmp/crontabs | crontab -u ${DOCKER_IMAGE_RUN_USER:-app-user} -
    echo "Cronlist"
    cat /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-app-user}"
    echo "Fix rights"
    chmod 777 -R -v ${APP_DIR_CURRENT}/logs
    chown -v "${DOCKER_IMAGE_RUN_USER:-app-user}":crontab /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-app-user}"

}

main_docker (){
    echo "Start as main_docker"
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/consul_agent.sh
    source /opt/public_libs/files/libs.d/apps/consul_template.sh
    if [[ "${APP_NAME}" == "captcha-solver" ]];then
        app_prepare_captcha-solver
        consul_agent_start
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_captcha-solver
        supervisor_init_cron
        app_fix_perms || (echo "skip app_fix_perms")
        app_prepare_run_adb
        supervisor_run
    fi

}
main_k8s () {
    echo "Start as main_k8s"
    source /opt/public_libs/files/libs.d/apps/main.sh
    cd ${APP_DIR_CURRENT}
    if [[ ! -d "${APP_DIR_SHARED}/storage/logs/crons" ]];then
        mkdir -p ${APP_DIR_SHARED}/storage/logs/crons
    fi
    if [[ ! -f "${APP_DIR_CURRENT}/.env" ]];then
        ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    fi
    if [[ -d "${APP_DIR_CURRENT}/logs" && ! -L "${APP_DIR_CURRENT}/logs" ]];then
        rm -r ${APP_DIR_CURRENT}/logs
        ln -sfr ${APP_DIR_SHARED}/storage/logs ${APP_DIR_CURRENT}/logs
    fi
    LOG_DIRS=$(cat ${APP_DIR_CURRENT}/.env  | grep LOGS_ | awk -F"=" '{print $2}')
    for LOG_DIR in ${LOG_DIRS}
    do
        if [[ ! -d "${LOG_DIR}" ]];then 
            mkdir -pv ${LOG_DIR};
            chmod 775 -R -v ${LOG_DIR}
            chmod -x -R -v ${LOG_DIR}
        else
            chmod 775 -R -v ${LOG_DIR}
            chmod -x -R -v ${LOG_DIR}
        fi
    done
    chmod 777 -Rv ${APP_DIR_CURRENT}/logs

if [[ "${DOCKER_ROLE}" == "cron" ]];then
    echo "DOCKER_ROLE=cron"

    cat <<OEF> /tmp/refresh_tasklist.sh
#!/usr/bin/env bash
cd ${APP_DIR_CURRENT} && /usr/local/bin/python3.9 ${APP_DIR_CURRENT}/utils/refresh_tasklist.py  >> ${APP_DIR_CURRENT}/logs/crons/refresh_tasklist.log 2>&1
OEF
    cat <<OEF> /tmp/logs_clean.sh
#!/usr/bin/env bash
find ${APP_DIR_CURRENT}/logs/resources* -type f -mtime +7 -exec rm -v {} \; >> ${APP_DIR_CURRENT}/logs/crons/logs_clean.log 2>&1
OEF

    cat <<OEF> /tmp/test_cron.sh
#!/usr/bin/env bash
echo "\$(date)" >> ${APP_DIR_CURRENT}/logs/crons/test_log.log 2>&1
OEF

    cat <<OEF> /tmp/crontabs
*/10 * * * * /tmp/refresh_tasklist.sh
00 */12 * * * /tmp/logs_clean.sh
* * * * * /tmp/test_cron.sh
OEF
    chmod +x -x /tmp/*.sh
    exec /usr/local/bin/supercronic --debug /tmp/crontabs
else
    echo "DOCKER_ROLE=worker"
    app_prepare_run_adb  
    exec /usr/local/bin/uwsgi --chdir=${APP_DIR_CURRENT} --module=web.run:application --socket=0.0.0.0:8000 --uid=${DOCKER_IMAGE_RUN_USER:-app-user} --gid=${DOCKER_IMAGE_RUN_USER:-app-user} --vacuum --die-on-term --processes=5 --enable-threads --master -l 2048
fi
}

if [[ -n "${POD_NAME}" ]];then
    main_k8s
else
    main_docker
fi







