#!/usr/bin/env bash
main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    if [[ "${APP_NAME}" == "nordic-website" ]];then
        nginx_config_gen_http_backend_nordic-website
        fix_perms || (echo "skip perms fix")
        nginx_run
    fi
}

main






