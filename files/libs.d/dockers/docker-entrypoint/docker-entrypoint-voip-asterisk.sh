#!/usr/bin/env bash

app_prepare_voip-asterisk(){
  get_function_name
  echo "--------------------"
  chown -R ${DOCKER_IMAGE_RUN_USER:-asterisk}:${DOCKER_IMAGE_RUN_USER:-asterisk} /etc/asterisk /var/*/asterisk /usr/*/asterisk 
}

main(){
    set -e
    source /opt/public_libs/files/libs.d/apps/main.sh
    set +e
    if [[ "${APP_NAME}" == "voip-asterisk" ]];then
        app_prepare_voip-asterisk
    fi
}

main

echo ""
exec "$@"




