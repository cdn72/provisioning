#!/usr/bin/env bash
app_prepare_whitesparrow(){
    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    ln -sf ${APP_DIR_SHARED}/.env.local ${APP_DIR_CURRENT}/.env.local
    if [[ ! -d "${APP_DIR_CURRENT}/bootstrap" ]];then
        mkdir -p ${APP_DIR_CURRENT}/bootstrap
    fi
    if [[  ! -L "${APP_DIR_CURRENT}/storage" ]];then
        if [[ -d "${APP_DIR_CURRENT}/storage" ]];then
            mv ${APP_DIR_CURRENT}/storage ${APP_DIR_CURRENT}/storage.old
        fi
        ln -sf ${APP_DIR_SHARED}/storage ${APP_DIR_CURRENT}/storage
    fi
    if [[ ! -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        mkdir -p ${APP_DIR_CURRENT}/bootstrap/cache
    fi
    if [[  ! -L "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        if [[ -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
            mv ${APP_DIR_CURRENT}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache.old
        fi
        ln -sf ${APP_DIR_SHARED}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache
    fi

}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    if [[ "${APP_NAME}" == "whitesparrow" ]];then
        app_prepare_whitesparrow
        supervisor_init_first
        supervisor_init_nginx
        nginx_config_gen_http_backend_whitesparrow
        fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main






