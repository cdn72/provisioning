#!/usr/bin/env bash
app_prepare_iprn(){
    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    if [[  ! -L "${APP_DIR_CURRENT}/storage" ]];then
        mv ${APP_DIR_CURRENT}/storage ${APP_DIR_CURRENT}/storage.old
        ln -sf ${APP_DIR_SHARED}/storage ${APP_DIR_CURRENT}/storage
    fi
    if [[ ! -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        mkdir -p ${APP_DIR_CURRENT}/bootstrap/cache
    fi
    if [[  ! -L "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        if [[ -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
            mv ${APP_DIR_CURRENT}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache.old
        fi
        ln -sf ${APP_DIR_SHARED}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache
    fi

}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    if [[ "${APP_NAME}" == "iprn" ]];then
        app_prepare_iprn
        nginx_config_gen_http_backend_iprn
        fix_perms || (echo "skip perms fix")
        nginx_run
    fi
}

main






