#!/usr/bin/env bash
app_prepare_smsgic-backend(){

    mkdir -p \
    ${APP_DIR_SHARED}/storage/logs/{api,resources,resources_tests,crons,sessions}  \
    ${APP_DIR_SHARED}/storage/{phones,sessions,views,cache,bootstrap/cache} \
    ${APP_DIR_SHARED}/storage/framework/{cache,sessions,views} \
    ${APP_DIR_SHARED}/app \
    ${APP_DIR_SHARED}/bootstrap/cache
    if [[ -f "${APP_DIR_SHARED}/.env" ]];then
        ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    fi
    if [[  ! -L "${APP_DIR_CURRENT}/storage" ]];then
        mv ${APP_DIR_CURRENT}/storage ${APP_DIR_CURRENT}/storage.old
        ln -sf ${APP_DIR_SHARED}/storage ${APP_DIR_CURRENT}/storage
    fi
    if [[ ! -d "${APP_DIR_SHARED}/bootstrap/cache" ]];then
        mkdir -p ${APP_DIR_SHARED}/bootstrap/cache
        chown -R ${DOCKER_IMAGE_RUN_USER:-www-data} ${APP_DIR_SHARED}/bootstrap/cache
    fi

    if [[  ! -L "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        if [[ -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
            mv ${APP_DIR_CURRENT}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache.old
        fi
        ln -sf ${APP_DIR_SHARED}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache
    fi
    cd ${APP_DIR_CURRENT} 
    php artisan config:clear
    php artisan clear-compiled
    php artisan config:cache
    php artisan cache:clear 
    php artisan optimize
    php artisan "docs:generate:collection" || echo "Error artisan docs:generate:collection command"
}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/supercronic.sh

    if [[ "${APP_NAME}" == "smsgic-backend" ]];then
        app_prepare_smsgic-backend
        supervisor_init_first

        if [[ "${APP_FUNCTION}" == "cron" ]];then
            supercronic_minute_laravel
            supervisor_init_supercronic_laravel
            supervisor_init_queue_laravel
        fi

        if [[ "${APP_FUNCTION}" == "backend" ]];then
            supervisor_init_php_fpm
            supervisor_init_laravel_ws_listener
            supervisor_init_nginx
            nginx_config_gen_http_backend_smsgic-backend
            if  grep -q 'OCTANE_SERVER=roadrunner' "${APP_DIR_CURRENT}/.env" ; then 
                supervisor_init_octane_smsgic-backend
            fi            
        fi

        app_fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main






