#!/usr/bin/env bash
set -e

HOSTNAME=$(hostname)

app_prepare_borg-client () {
  get_function_name
  ssh-keyscan -t rsa $(echo ${BORG_CLIENT_REMOTE_REPO_URL#*@} | awk -F":" '{ print $1}') > ${HOME}/.ssh/known_hosts
  grep -q "^ClientAliveInterval 20" /etc/ssh/sshd_config || sed -i "\$aClientAliveInterval 20" /etc/ssh/sshd_config
  grep -q "^ClientAliveCountMax 3" /etc/ssh/sshd_config  || sed -i "\$aClientAliveCountMax 3" /etc/ssh/sshd_config
  if [[ -f "${HOME}/.ssh/known_hosts" ]] ;then rm -v ${HOME}/.ssh/known_hosts;fi
  /usr/bin/find "${HOME}"/.cache/borg -type d  -iname 'lock.exclusive'  | xargs rm -rf -v  || (echo "File lock.exclusive not exist. skip delete")
}

main(){
    set -e
    source /opt/public_libs/files/libs.d/apps/main.sh
    set +e
    if [[ "${APP_NAME}" == "borg-client" ]];then
        app_prepare_borg-client
    fi
}

main

echo ""
exec "$@"





