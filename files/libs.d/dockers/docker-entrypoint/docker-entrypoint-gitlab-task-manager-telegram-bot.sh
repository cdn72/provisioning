#!/usr/bin/env bash

app_prepare_gitlab-task-manager-telegram-bot(){
  get_function_name
  echo "--------------------"
  if [[ -f ${APP_DIR_SHARED}/.env ]];then
     ln -sf "${APP_DIR_SHARED}"/.env "${APP_DIR_CURRENT:-/var/www/html}"/.env
  fi
  find ${APP_DIR_SHARED}  \! -group ${DOCKER_IMAGE_RUN_USER:-www-data} -exec chown -h -v www-data:${DOCKER_IMAGE_RUN_USER:-www-data} {} +;
  find ${APP_DIR_CURRENT}  \! -group ${DOCKER_IMAGE_RUN_USER:-www-data} -exec chown -h -v www-data:${DOCKER_IMAGE_RUN_USER:-www-data} {} +;
}

main(){
    set -e
    source /opt/public_libs/files/libs.d/apps/main.sh
    set +e
    if [[ "${APP_NAME}" == "gitlab-task-manager-telegram-bot" ]];then
        app_prepare_gitlab-task-manager-telegram-bot
    fi
}

main

echo ""
exec "apache2-foreground"




