#!/usr/bin/env bash

app_prepare_voip-perf(){
  get_function_name
  echo "--------------------"
}

main(){
    set -e
    source /opt/public_libs/files/libs.d/apps/main.sh
    set +e
    if [[ "${APP_NAME}" == "voip-perf" ]];then
        app_prepare_voip-perf
    fi
}

main

echo ""
exec "$@"




