#!/usr/bin/env bash
app_prepare_website-foxcom(){
    if [[ -f "${APP_DIR_SHARED}/.env " ]];then ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env;fi
}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    if [[ "${APP_NAME}" == "website-foxcom" ]];then
        app_prepare_website-foxcom
        nginx_config_gen_http_backend_website-foxcom
        nginx_run
    fi
}

main






