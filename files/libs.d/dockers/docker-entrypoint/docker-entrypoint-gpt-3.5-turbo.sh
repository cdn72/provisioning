#!/usr/bin/env bash

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    if [[ "${APP_NAME}" == "gpt-3.5-turbo" ]];then
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_gpt_3_5_turbo
        fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main





