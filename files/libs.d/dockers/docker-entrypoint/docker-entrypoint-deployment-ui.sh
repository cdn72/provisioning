#!/usr/bin/env bash



main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/consul_agent.sh
    source /opt/public_libs/files/libs.d/apps/consul_template.sh
    ##libs app
    source /opt/public_libs/files/libs.d/apps/deployment-ui.sh

    supervisor_init_first
    supervisor_init_dumb
    supervisor_init_nginx
    supervisor_init_php_fpm
    #supervisor_init_laravel_ws_listener
    nginx_config_generate_backend_deployment-ui

    app_prepare_deployment-ui
    app_fix_perms || (echo "skip perms fix")
    supervisor_run_exec
}

main








