#!/usr/bin/env bash
set -e

HOSTNAME=$(hostname)

app_configtest_voip-kamailio () {
  get_function_name
  /usr/sbin/kamailio -f ${KAMAILIO_CFGFILE:-/etc/kamailio/kamailio.cfg}  -c
}

app_prepare_voip-kamailio () {
  get_function_name
  /usr/bin/wait-for -t 20 $DB_HOST:$DB_PORT
}

main(){
    set -e
    source /opt/public_libs/files/libs.d/apps/main.sh
    set +e
    if [[ "${APP_NAME}" == "voip-kamailio" ]];then
        app_configtest_voip-kamailio
        app_prepare_voip-kamailio
        #/usr/sbin/kamailio -f ${KAMAILIO_CFGFILE:-/etc/kamailio/kamailio.cfg} -DD -E
    fi
}

main

echo ""
exec "$@"





