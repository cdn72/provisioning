#!/usr/bin/env bash
app_prepare_smsgic-website(){
    if [[ -f "${APP_DIR_SHARED}/.env" ]];then ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env;fi
    if [[ -d "${APP_DIR_SHARED}/storage" ]];then
        if [[  ! -L "${APP_DIR_CURRENT}/storage" ]];then
            if [[ -d "${APP_DIR_CURRENT}/storage" && ! -L "${APP_DIR_CURRENT}/storage" ]];then
                mv ${APP_DIR_CURRENT}/storage ${APP_DIR_CURRENT}/storage.old
            fi
            ln -sf ${APP_DIR_SHARED}/storage ${APP_DIR_CURRENT}/storage
        fi
    fi
    if [[ ! -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        mkdir -p ${APP_DIR_CURRENT}/bootstrap/cache
    fi
    if [[  ! -L "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        if [[ -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
            mv ${APP_DIR_CURRENT}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache.old
        fi
        ln -sf ${APP_DIR_SHARED}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache
    fi

}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    source /opt/public_libs/files/libs.d/apps/nodejs/main.sh
    if [[ "${APP_NAME}" == "smsgic-website" ]];then
        app_prepare_smsgic-website
        nginx_config_gen_http_backend_smsgic-website
        app_fix_perms || (echo "skip perms fix")
        npm_run_server_prod
        #supervisor_init_first
        #supervisor_init_nginx
        #supervisor_run
    fi
}

main






