#!/usr/bin/env bash
set -e

app_prepare_borg-server () {
  get_function_name
if [[ ! -f "/etc/ssh/ssh_host_rsa_key" ]]; then
    # generate fresh rsa key
    ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
fi
if [[ ! -f "/etc/ssh/ssh_host_dsa_key" ]]; then
    ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
fi
sleep 5;
if [[ ! -d "${BORG_HOME_FOLDER:-/home/borg}/.ssh" ]];then
    echo "Exec mkdir -p  ${BORG_HOME_FOLDER:-/home/borg}/.ssh"
    mkdir -pv  ${BORG_HOME_FOLDER:-/home/borg}/.ssh
fi

if [[ ! -f "${BORG_HOME_FOLDER:-/home/borg}/.ssh/authorized_keys" ]];then
    touch ${BORG_HOME_FOLDER:-/home/borg}/.ssh/authorized_keys
fi


if [[ "${BORG_SERVER_BACKUP_FOLDER}" == "" ]];then
    echo "var BORG_SERVER_BACKUP_FOLDER is empty"
else
    echo "Prepare ${BORG_SERVER_BACKUP_FOLDER}"
    if [[ ! -d "${BORG_SERVER_BACKUP_FOLDER}" ]];then
        echo "Creating ${BORG_SERVER_BACKUP_FOLDER}"
        mkdir -pv ${BORG_SERVER_BACKUP_FOLDER}
    else
        echo "change user to  borg:borg for directory ${BORG_SERVER_BACKUP_FOLDER}"
    fi

fi


if [[ ! -d "/var/run/sshd" ]]; then
    mkdir -p /var/run/sshd
fi
if [[ ! -d "/var/log" ]]; then
    mkdir -p /var/log
fi
if [[ ! -f "/var/log/sshd.log" ]]; then
    touch /var/log/sshd.log
fi

if [[ -d ${BORG_HOME_FOLDER:-/home/borg}/.ssh ]];then
    chmod  700 -v ${BORG_HOME_FOLDER:-/home/borg}/.ssh
    chmod  700 -v ${BORG_HOME_FOLDER:-/home/borg}/.ssh/authorized_keys
fi

echo "Remove stailed backups"

    /usr/bin/find ${BORG_SERVER_BACKUP_FOLDER} -maxdepth 4 -type d -iname "lock.exclusive"  | xargs rm -v -rf || (echo "Noting to do")
    if [[ -d "${BORG_HOME_FOLDER:-/home/borg}" ]];then
        screen -dmSL ${FUNCNAME}_borg sh -c "find ${BORG_HOME_FOLDER:-/home/borg} \! -user borg -exec chown -h -v borg:borg {} +;find ${BORG_HOME_FOLDER:-/home/borg} \! -group borg -exec chown -h -v borg:borg {} +;"
    fi
    if [[ -d "${BORG_SERVER_DATA:-/borg_server_v2/data}" ]];then
        screen -dmSL ${FUNCNAME}_bup sh -c "find ${BORG_SERVER_DATA:-/borg_server_v2/data} \! -user borg -exec chown -h -v borg:borg {} +;find ${BORG_SERVER_DATA:-/borg_server_v2/data} \! -group borg -exec chown -h -v borg:borg {} +;"
    fi 
}

main(){
    set -e
    source /opt/public_libs/files/libs.d/apps/main.sh
    set +e
    if [[ "${APP_NAME}" == "borg-server" ]];then
        app_prepare_borg-server
    fi
}

main

echo ""
exec "$@"





