#!/usr/bin/env bash

#colors
RED='\033[0;31m'
CYAN='\033[1;36m'
NC='\033[0m' # No Color

ENV_NGINX_DOCKER_FUNCTION_NAME_NGINX_PROXY="nginx-proxy"
ENV_NGINX_CONF_DIR_PROXY="/etc/nginx/proxy.d"

ENV_NGINX_PROXY_CONFIG_GLOBAL_REWRITES="convy_global_rewrites_proxy.conf"
ENV_NGINX_DIR="/etc/nginx"
ENV_ACME_DIR_WWW="/var/www/letsencrypt"
ENV_ACME_DIR="/etc/letsencrypt"




if [[ -z "${ENV_NGINX_BACKEND_SMS_RX}" ]];then
    NGINX_BACKEND_SMS_RX="sms-http-rx:8000"
    NGINX_BACKEND_SMS_RX_HOST=$(echo "${NGINX_BACKEND_SMS_RX}" | cut -d\: -f1 )
else
    NGINX_BACKEND_SMS_RX="${ENV_NGINX_BACKEND_SMS_RX}"
    NGINX_BACKEND_SMS_RX_HOST=$(echo "${NGINX_BACKEND_SMS_RX}" | cut -d\: -f1 )
fi

if [[ -z "${ENV_NGINX_BACKEND_CONSUMER_PORTAL}" ]];then
    NGINX_BACKEND_CONSUMER_PORTAL="consumer-portal:8100"
    NGINX_BACKEND_CONSUMER_PORTAL_HOST=$(echo "${NGINX_BACKEND_CONSUMER_PORTAL}" | cut -d\: -f1 )
else
    NGINX_BACKEND_CONSUMER_PORTAL="${ENV_NGINX_BACKEND_CONSUMER_PORTAL}"
    NGINX_BACKEND_CONSUMER_PORTAL_HOST=$(echo "${ENV_NGINX_BACKEND_CONSUMER_PORTAL}" | cut -d\: -f1 )
fi


nginx_acl_set(){

    if [ ! -d "${ENV_NGINX_DIR}/upstream.d" ];then
        mkdir -p ${ENV_NGINX_DIR}/upstream.d
    fi

    if [ ! -f "${ENV_NGINX_DIR}/upstream.d/convy.conf" ];then
        touch ${ENV_NGINX_DIR}/upstream.d/convy.conf;
        /bin/cat <<OEF> "${ENV_NGINX_DIR}/upstream.d/convy.conf"
upstream backend_convy_worker {
    server worker:80;
}
upstream backend_convy_octane {
    server worker:8000;
}
upstream backend_convy_ws {
    server dispatcher:6001;
}
OEF
    fi    

    if [ -n "${CONVY_WEB_ACL_LIST}" ];then 
        echo "Set acl list.CONVY_WEB_ACL_LIST = ${CONVY_WEB_ACL_LIST}"
    else
        CONVY_WEB_ACL_LIST="0.0.0.0/0"
        echo "Empty acl. CONVY_WEB_ACL_LIST = ${CONVY_WEB_ACL_LIST}"
    fi

    if [ ! -f "${ENV_NGINX_DIR}/convy_acl.conf" ];then 
        touch "${ENV_NGINX_DIR}/convy_acl.conf"
    else
        : > "${ENV_NGINX_DIR}/convy_acl.conf"
    fi

    for IP in $(echo "${CONVY_WEB_ACL_LIST}" | tr -s " " "\012")
    do
        echo "allow ${IP};" >> "${ENV_NGINX_DIR}/convy_acl.conf"
done
    echo "deny all;" >> ${ENV_NGINX_DIR}/convy_acl.conf

    cat ${ENV_NGINX_DIR}/convy_acl.conf
}


nginx_config_dirs_create () {

    if [[  ! -f "${ENV_NGINX_DIR}/${ENV_NGINX_PROXY_CONFIG_GLOBAL_REWRITES}" ]]
    then
        touch  "${ENV_NGINX_DIR}/${ENV_NGINX_PROXY_CONFIG_GLOBAL_REWRITES}"
    fi

    if [[ ! -d "${ENV_ACME_DIR_WWW}" ]]
    then
        mkdir -p "${ENV_ACME_DIR_WWW}"
        chown --recursive www-data:www-data "${ENV_ACME_DIR_WWW}"
    else
        chown --recursive www-data:www-data "${ENV_ACME_DIR_WWW}"    
    fi
}

nginx_proxy_generate_config_convy_http () {
echo "exec ${FUNCNAME[0]} func"
echo "NGINX_CONVY_HTTP_HOST is ${NGINX_CONVY_HTTP_HOST}"
http_version_check=$(2>&1 nginx -V | tr -- - '\n'  | grep -q 'http_v2\|http_v3' && echo "http_2" || echo "http_1_1")

if [[ "${http_version_check}" == "http_2" ]];then
    echo "http_proto=http2"
    http_proto="http2"
else
    echo "http_proto=http_1"
    http_proto=""
fi
for NGINX_CONVY_HTTP_HOST in $(echo "${CONVY_HTTP_HOST}" | tr "," "\n")
do

        #if [[ "${NGINX_CONVY_HTTP_HOST}" != "" ]]
        if [[ -n "${NGINX_CONVY_HTTP_HOST/[ ]*\n/}" ]]
        then
        echo -e "${CYAN} Генерируем nginx config ${NGINX_CONVY_HTTP_HOST} для ${ENV_NGINX_DOCKER_FUNCTION} ${NC}"
        CONVY_DIR_NAME="/var/www/html/${NGINX_CONVY_HTTP_HOST}/current"
        SSL_CONFIG="${ENV_ACME_DIR}/ssl.d/${NGINX_CONVY_HTTP_HOST}/ssl.conf"
            if [[ ! -f "${ENV_NGINX_CONF_DIR_PROXY}/${NGINX_CONVY_HTTP_HOST}.conf" ]]
            then
                touch ${ENV_NGINX_CONF_DIR_PROXY}/"${NGINX_CONVY_HTTP_HOST}".conf
            fi
                if [[ ! -f "${ENV_ACME_DIR}/ssl.d/${NGINX_CONVY_HTTP_HOST}/ssl.conf" ]];then
                    echo "ssl config ${SSL_CONFIG} not exist - generate nginx conf without ssl cert"
                    grep -q "${NGINX_CONVY_HOST_HTTP}" ${ENV_NGINX_CONF_DIR_PROXY}/"${NGINX_CONVY_HTTP_HOST}".conf || /bin/cat <<OEF> ${ENV_NGINX_CONF_DIR_PROXY}/"${NGINX_CONVY_HTTP_HOST}".conf
###NGINX PROXY CONFIG BEGIN for ${NGINX_CONVY_HTTP_HOST}
include ${ENV_NGINX_DIR}/upstream.d/convy.conf;
server {
    listen 80 ${NGINX_HTTP_LISTEN_OPTIONS:-rcvbuf=64000 sndbuf=200000 backlog=2048 fastopen=500 reuseport};
    server_name
    ${NGINX_CONVY_HTTP_HOST}
    www.${NGINX_CONVY_HTTP_HOST}
    ;
    proxy_max_temp_file_size 0;
    lingering_time 86400;
    client_max_body_size 400m;
    include nginx-logs-headers.conf;
    include ${ENV_NGINX_PROXY_CONFIG_GLOBAL_REWRITES};
    location ^~ /.well-known/acme-challenge/ {
        alias /var/www/letsencrypt/.well-known/acme-challenge/; 
        default_type "text/plain";
    }
    location /ws {
        proxy_pass             http://backend_convy_ws;
        proxy_read_timeout     60;
        proxy_connect_timeout  60;
        proxy_redirect         off;
        # Allow the use of websockets
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host \$host;
        proxy_cache_bypass \$http_upgrade;
    }
    location /api/sms/receive {
        proxy_pass   http://backend_convy_octane/api/sms/receive;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
    }
    
    location /api/ {
        proxy_pass   http://backend_convy_worker/api/;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
    }  
    location / {
        proxy_pass   http://backend_convy_worker/;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
        include ${ENV_NGINX_DIR}/convy_acl.conf;
    }
    
}                   
###NGINX PROXY CONFIG END for ${NGINX_CONVY_HTTP_HOST}
OEF
else
    if [[ -f "${ENV_NGINX_DIR}/conf.d/redirect_force_https_${NGINX_CONVY_HTTP_HOST}" ]];then
        echo "ssl config ${SSL_CONFIG} exist - generate nginx conf without ssl cert. File /etc/nginx/conf.d/redirect_${NGINX_CONVY_HTTP_HOST} exist. Force https"
        grep -q "${NGINX_CONVY_HOST_HTTP}" ${ENV_NGINX_CONF_DIR_PROXY}/"${NGINX_CONVY_HTTP_HOST}".conf || /bin/cat <<OEF> ${ENV_NGINX_CONF_DIR_PROXY}/"${NGINX_CONVY_HTTP_HOST}".conf
###NGINX PROXY CONFIG BEGIN for ${NGINX_CONVY_HTTP_HOST}
include ${ENV_NGINX_DIR}/upstream.d/convy.conf;
server {
    listen 80 ${NGINX_HTTP_LISTEN_OPTIONS:-rcvbuf=64000 sndbuf=200000 backlog=2048 fastopen=500 reuseport};
    server_name
    ${NGINX_CONVY_HTTP_HOST}
    www.${NGINX_CONVY_HTTP_HOST}
    ;  
    proxy_max_temp_file_size 0;
    lingering_time 86400;
    client_max_body_size 400m;
    include ${ENV_NGINX_PROXY_CONFIG_GLOBAL_REWRITES};
    include nginx-logs-headers.conf;
    location ^~ /.well-known/acme-challenge/ {
        alias /var/www/letsencrypt/.well-known/acme-challenge/; 
        default_type "text/plain";
    }
    location / {
      return 307 https://${NGINX_CONVY_HTTP_HOST}\$request_uri;
    }
    

}          
server {
    listen 443 ssl ${http_proto} ${NGINX_TLS_LISTEN_OPTIONS:-rcvbuf=64000 sndbuf=200000 backlog=2048 fastopen=500 reuseport};
    server_name 
    ${NGINX_CONVY_HTTP_HOST}
    ;
    #Include ssl config
    include ${SSL_CONFIG};

    proxy_max_temp_file_size 0;
    lingering_time 86400;
    client_max_body_size 400m;
    include ${ENV_NGINX_PROXY_CONFIG_GLOBAL_REWRITES};
    location ^~ /.well-known/acme-challenge/ {
        alias /var/www/letsencrypt/.well-known/acme-challenge/; 
        default_type "text/plain";
    }
    
    location /ws {
        proxy_pass            http://backend_convy_ws;
        proxy_read_timeout     60;
        proxy_connect_timeout  60;
        proxy_redirect         off;
    
        # Allow the use of websockets
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host \$host;
        proxy_cache_bypass \$http_upgrade;
    }
    location /api/sms/receive {
        proxy_pass   http://backend_convy_octane/api/sms/receive;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
    }

    location /api/ {
        proxy_pass   http://backend_convy_worker/api/;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
        proxy_pass_request_body on;
        #proxy_set_header        Content-Length "";
        #proxy_set_header        X-Original-URI \$request_uri;
    }               
    location / {
        proxy_pass   http://backend_convy_worker/;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
        include ${ENV_NGINX_DIR}/convy_acl.conf;
    }
}           
server {
    listen 6001 ssl ${http_proto} ${NGINX_TLS_LISTEN_OPTIONS:-rcvbuf=64000 sndbuf=200000 backlog=2048 fastopen=500};
    server_name 
    ${NGINX_CONVY_HTTP_HOST}
    ;
    #Include ssl config
    include ${SSL_CONFIG};
    client_max_body_size 400m;       
    location / {
        proxy_pass             http://backend_convy_ws;
        proxy_read_timeout     60;
        proxy_connect_timeout  60;
        proxy_redirect         off;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host \$host;
        proxy_cache_bypass \$http_upgrade;
    }               

}        
###NGINX PROXY CONFIG END for ${NGINX_CONVY_HTTP_HOST}
OEF
    else
        echo "ssl config ${SSL_CONFIG} exist - generate nginx conf without ssl cert. File /etc/nginx/conf.d/redirect_${NGINX_CONVY_HTTP_HOST} not exist. Skip Force https"
        grep -q "${NGINX_CONVY_HOST_HTTP}" ${ENV_NGINX_CONF_DIR_PROXY}/"${NGINX_CONVY_HTTP_HOST}".conf || /bin/cat <<OEF> ${ENV_NGINX_CONF_DIR_PROXY}/"${NGINX_CONVY_HTTP_HOST}".conf
###NGINX PROXY CONFIG BEGIN for ${NGINX_CONVY_HTTP_HOST}
include ${ENV_NGINX_DIR}/upstream.d/convy.conf;
server {
    listen 80 ${NGINX_HTTP_LISTEN_OPTIONS:-rcvbuf=64000 sndbuf=200000 backlog=2048 fastopen=500 reuseport};
    server_name
    ${NGINX_CONVY_HTTP_HOST}
    www.${NGINX_CONVY_HTTP_HOST}
    ;  
    proxy_max_temp_file_size 0;
    lingering_time 86400;
    client_max_body_size 400m;
    include ${ENV_NGINX_PROXY_CONFIG_GLOBAL_REWRITES};
    include nginx-logs-headers.conf;
    location ^~ /.well-known/acme-challenge/ {
        alias /var/www/letsencrypt/.well-known/acme-challenge/; 
        default_type "text/plain";
    }
    location /ws {
        proxy_pass             http://backend_convy_ws;
        proxy_read_timeout     60;
        proxy_connect_timeout  60;
        proxy_redirect         off;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host \$host;
        proxy_cache_bypass \$http_upgrade;
    }

    location /api/sms/receive {
        proxy_pass   http://backend_convy_octane/api/sms/receive;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
    }

    location /api/ {
        proxy_pass   http://backend_convy_worker/api/;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
        proxy_pass_request_body on;
        #proxy_set_header        Content-Length "";
        #proxy_set_header        X-Original-URI \$request_uri;
    }               
    location / {
        proxy_pass   http://backend_convy_worker/;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
        include ${ENV_NGINX_DIR}/convy_acl.conf;
    }
    

}          
server {
    listen 443 ssl ${http_proto} ${NGINX_TLS_LISTEN_OPTIONS:-rcvbuf=64000 sndbuf=200000 backlog=2048 fastopen=500 reuseport};
    server_name 
    ${NGINX_CONVY_HTTP_HOST}
    ;
    #Include ssl config
    include ${SSL_CONFIG};

    proxy_max_temp_file_size 0;
    lingering_time 86400;
    client_max_body_size 400m;
    include ${ENV_NGINX_PROXY_CONFIG_GLOBAL_REWRITES};
    location ^~ /.well-known/acme-challenge/ {
        alias /var/www/letsencrypt/.well-known/acme-challenge/; 
        default_type "text/plain";
    }
    
    location /ws {
        proxy_pass            http://backend_convy_ws;
        proxy_read_timeout     60;
        proxy_connect_timeout  60;
        proxy_redirect         off;
    
        # Allow the use of websockets
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host \$host;
        proxy_cache_bypass \$http_upgrade;
    }
    location /api/sms/receive {
        proxy_pass   http://backend_convy_octane/api/sms/receive;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
    }

    location /api/ {
        proxy_pass   http://backend_convy_worker/api/;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
        proxy_pass_request_body on;
        #proxy_set_header        Content-Length "";
        #proxy_set_header        X-Original-URI \$request_uri;
    }               
    location / {
        proxy_pass   http://backend_convy_worker/;
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-Server \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_connect_timeout 120;
        proxy_send_timeout 120;
        proxy_read_timeout 180;
        proxy_pass_request_headers on;
        proxy_set_header Accept-Encoding "" ;
        include ${ENV_NGINX_DIR}/convy_acl.conf;
    }
}           
server {
    listen 6001 ssl ${http_proto} ${NGINX_TLS_LISTEN_OPTIONS:-rcvbuf=64000 sndbuf=200000 backlog=2048 fastopen=500};
    server_name 
    ${NGINX_CONVY_HTTP_HOST}
    ;
    #Include ssl config
    include ${SSL_CONFIG};
    client_max_body_size 400m;       
    location / {
        proxy_pass             http://backend_convy_ws;
        proxy_read_timeout     60;
        proxy_connect_timeout  60;
        proxy_redirect         off;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host \$host;
        proxy_cache_bypass \$http_upgrade;
    }               

}        
###NGINX PROXY CONFIG END for ${NGINX_CONVY_HTTP_HOST}
OEF
    fi

        fi
    fi 
done

}

nginx_proxy_generate_config_sms_rx_http () {
    echo "exec ${FUNCNAME[0]} func"

for SMS_RX_HTTP_HOST in $(echo "${SMS_RX_HTTP_HOSTS}" | tr "," "\n")
do
        if [[ -n "${SMS_RX_HTTP_HOST/[ ]*\n/}" ]]
        then
            echo -e "${CYAN} Генерируем nginx config ${SMS_RX_HTTP_HOST} ${NC}"
            SSL_CONFIG="${ENV_ACME_DIR}/ssl.d/${SMS_RX_HTTP_HOST}/ssl.${SMS_RX_HTTP_HOST}.conf"
            if [[ ! -f "${ENV_NGINX_CONF_DIR_PROXY}/${SMS_RX_HTTP_HOST}.conf" ]];
            then
                touch "${ENV_NGINX_CONF_DIR_PROXY}"/"${SMS_RX_HTTP_HOST}".conf
            fi
            if [[ -f "${SSL_CONFIG}" ]];then
                    echo "ssl config ${SSL_CONFIG} exist - generate nginx conf with ssl"
                    grep -q "${SMS_RX_HTTP_HOST}" "${ENV_NGINX_CONF_DIR_PROXY}"/"${SMS_RX_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${ENV_NGINX_CONF_DIR_PROXY}"/"${SMS_RX_HTTP_HOST}".conf
###NGINX PROXY CONFIG BEGIN for ${SMS_RX_HTTP_HOST}
server {
    listen 80;
    server_name
    ${SMS_RX_HTTP_HOST}
    ;
    location ^~ /.well-known/acme-challenge/ {
        alias /var/www/letsencrypt/.well-known/acme-challenge/; 
        default_type "text/plain";
    }

    location / {
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header REMOTE_ADDR \$remote_addr;
        proxy_pass_request_headers on;
        proxy_pass http://${NGINX_BACKEND_SMS_RX}/;
        proxy_set_header Accept-Encoding "" ;
    }

}

server {
    listen 443 ssl ${http_proto};
    server_name 
    ${SMS_RX_HTTP_HOST}
    ;
    include ${SSL_CONFIG};
    location ^~ /.well-known/acme-challenge/ {
        alias /var/www/letsencrypt/.well-known/acme-challenge/; 
        default_type "text/plain";
    }
    location / {
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header REMOTE_ADDR \$remote_addr;
        proxy_pass_request_headers on;
        proxy_pass http://${NGINX_BACKEND_SMS_RX}/;
        proxy_set_header Accept-Encoding "" ;
    }
}

###NGINX PROXY CONFIG END for ${SMS_RX_HTTP_HOST}
OEF
            else
                echo "${SSL_CONFIG} not exist. Gen only http config."
                grep -q "${SMS_RX_HTTP_HOST}" "${ENV_NGINX_CONF_DIR_PROXY}"/"${SMS_RX_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${ENV_NGINX_CONF_DIR_PROXY}"/"${SMS_RX_HTTP_HOST}".conf
###NGINX PROXY CONFIG BEGIN for ${SMS_RX_HTTP_HOST}
server {
    listen 80;
    server_name
    ${SMS_RX_HTTP_HOST}
    ;
    location ^~ /.well-known/acme-challenge/ {
        alias /var/www/letsencrypt/.well-known/acme-challenge/; 
        default_type "text/plain";
    }

    location / {
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header REMOTE_ADDR \$remote_addr;
        proxy_pass_request_headers on;
        proxy_pass http://${NGINX_BACKEND_SMS_RX}/;
        proxy_set_header Accept-Encoding "" ;
    }

}
###NGINX PROXY CONFIG END for ${SMS_RX_HTTP_HOST}
OEF

            fi
        fi

done

}


nginx_proxy_generate_config_consumer_portal_http () {

echo "exec ${FUNCNAME[0]} func"

for CONVY_CONSUMER_PORTAL_HTTP_HOST in $(echo "${CONVY_CONSUMER_PORTAL_HTTP_HOSTS}" | tr "," "\n")
do
        if [[ -n "${CONVY_CONSUMER_PORTAL_HTTP_HOST/[ ]*\n/}" ]]
        then
            echo -e "${CYAN} Генерируем nginx config ${CONVY_CONSUMER_PORTAL_HTTP_HOST} ${NC}"
            SSL_CONFIG="${ENV_ACME_DIR}/ssl.d/${CONVY_CONSUMER_PORTAL_HTTP_HOST}/ssl.${CONVY_CONSUMER_PORTAL_HTTP_HOST}.conf"
            if [[ ! -f "${ENV_NGINX_CONF_DIR_PROXY}/${CONVY_CONSUMER_PORTAL_HTTP_HOST}.conf" ]];
            then
                touch "${ENV_NGINX_CONF_DIR_PROXY}"/"${CONVY_CONSUMER_PORTAL_HTTP_HOST}".conf
            fi
            if [[ -f "${SSL_CONFIG}" ]];then
                    echo "ssl config ${SSL_CONFIG} exist - generate nginx conf with ssl"
                    grep -q "${CONVY_CONSUMER_PORTAL_HTTP_HOST}" "${ENV_NGINX_CONF_DIR_PROXY}"/"${CONVY_CONSUMER_PORTAL_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${ENV_NGINX_CONF_DIR_PROXY}"/"${CONVY_CONSUMER_PORTAL_HTTP_HOST}".conf
###NGINX PROXY CONFIG BEGIN for ${CONVY_CONSUMER_PORTAL_HTTP_HOST}
server {
    listen 80;
    server_name
    ${CONVY_CONSUMER_PORTAL_HTTP_HOST}
    ;
    location ^~ /.well-known/acme-challenge/ {
        alias /var/www/letsencrypt/.well-known/acme-challenge/; 
        default_type "text/plain";
    }
    
    location / {
        return 301 https://${CONVY_CONSUMER_PORTAL_HTTP_HOST}\$request_uri;
    }
}

server {
    listen 443 ssl ${http_proto};
    server_name 
    ${CONVY_CONSUMER_PORTAL_HTTP_HOST}
    ;
    include ${SSL_CONFIG};
    location ^~ /.well-known/acme-challenge/ {
        alias /var/www/letsencrypt/.well-known/acme-challenge/; 
        default_type "text/plain";
    }
    location / {
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header REMOTE_ADDR \$remote_addr;
        proxy_pass_request_headers on;
        proxy_pass http://${NGINX_BACKEND_CONSUMER_PORTAL}/;
        proxy_set_header Accept-Encoding "" ;
    }
    location /webdavserverprivate2/ {
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header REMOTE_ADDR \$remote_addr;
        proxy_pass_request_headers on;
        proxy_pass http://consumer-portal:80/webdavserverprivate2/;
    }


}

###NGINX PROXY CONFIG END for ${CONVY_CONSUMER_PORTAL_HTTP_HOST}
OEF
            else
                echo "${SSL_CONFIG} not exist. Gen only http config."
                grep -q "${CONVY_CONSUMER_PORTAL_HTTP_HOST}" "${ENV_NGINX_CONF_DIR_PROXY}"/"${CONVY_CONSUMER_PORTAL_HTTP_HOST}".conf ||  /bin/cat <<OEF> "${ENV_NGINX_CONF_DIR_PROXY}"/"${CONVY_CONSUMER_PORTAL_HTTP_HOST}".conf
###NGINX PROXY CONFIG BEGIN for ${CONVY_CONSUMER_PORTAL_HTTP_HOST}
server {
    listen 80;
    server_name
    ${CONVY_CONSUMER_PORTAL_HTTP_HOST}
    ;
    location ^~ /.well-known/acme-challenge/ {
        alias /var/www/letsencrypt/.well-known/acme-challenge/; 
        default_type "text/plain";
    }

    location / {
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header REMOTE_ADDR \$remote_addr;
        proxy_pass_request_headers on;
        proxy_pass http://${NGINX_BACKEND_CONSUMER_PORTAL}/;
        proxy_set_header Accept-Encoding "" ;
    }
    location /webdavserverprivate2/ {
        proxy_set_header Host \$host;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header REMOTE_ADDR \$remote_addr;
        proxy_pass_request_headers on;
        proxy_pass http://consumer-portal:80/webdavserverprivate2/;
    }
}
###NGINX PROXY CONFIG END for ${CONVY_CONSUMER_PORTAL_HTTP_HOST}
OEF
            fi
        fi

done

}


##ФУНКЦИЯ ГЕНЕРАЦИЯ КОНФИГОВ NGINX ДЛЯ PROXY
nginx_generate_config_proxy_convy () {
    nginx_config_dirs_create
    nginx_acl_set
    nginx_proxy_generate_config_convy_http

    if [[ -z "${SMS_RX_HTTP_HOSTS}" ]];then
        echo "Var SMS_RX_HTTP_HOSTS is empty.Skip run nginx_proxy_generate_config_sms_rx_http"
    else
        ping -c 2 -q "${NGINX_BACKEND_SMS_RX_HOST}" && nginx_proxy_generate_config_sms_rx_http || echo "${NGINX_BACKEND_SMS_RX_HOST} host not pinged. Skip generate config via nginx_proxy_generate_config_sms_rx_http"
    fi

    if [[ -z "${CONVY_CONSUMER_PORTAL_HTTP_HOSTS}" ]];then
        echo "Var CONVY_CONSUMER_PORTAL_HTTP_HOSTS is empty.Skip run nginx_proxy_generate_config_consumer_portal_http"
    else
        ping -c 2 -q "${NGINX_BACKEND_CONSUMER_PORTAL_HOST}" && nginx_proxy_generate_config_consumer_portal_http || echo "${NGINX_BACKEND_CONSUMER_PORTAL_HOST} host not pinged. Skip generate config via nginx_proxy_generate_config_consumer_portal_http"
    fi

}

nginx_run () {
    nginx -g 'daemon off;'
}

main() {
    nginx_generate_config_proxy_convy
    nginx_run
}

main
