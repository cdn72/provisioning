#!/usr/bin/env bash

app_prepare_django(){
    echo "Run prepare"
}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    source /opt/public_libs/files/libs.d/apps/consul_agent.sh
    source /opt/public_libs/files/libs.d/apps/consul_template.sh
    if [[ "${APP_NAME}" == "django" ]];then
        app_prepare_django
        nginx_prepare
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_uwsgi_django
        supervisor_init_nginx
        fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main






