#!/usr/bin/env bash
if [[ ! -d "/tmp" ]];then mkdir /tmp;fi
cd /tmp && curl -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/python/load_files_to_db.py -o /tmp/load_files_to_db.py
cd /tmp && python3 /tmp/load_files_to_db.py || (echo "skip exit codes with error")
