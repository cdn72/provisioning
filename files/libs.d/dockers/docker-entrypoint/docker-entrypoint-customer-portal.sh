#!/usr/bin/env bash

app_prepare_customer-portal(){
	s_files="app/theme.scss app/config.js app/assets/logo.png app/assets/favicon.ico "
	for s_file in ${s_files};do

		if [[ -L "${APP_DIR_CURRENT}/${s_files}" ]];then
			echo "file ${APP_DIR_CURRENT}/${s_files} is symlink. Skip"
		else
			echo "Try creating symlink";
			if [[ -f "${APP_DIR_SHARED}/${s_file}" ]];then
				if [[ -f  "${APP_DIR_CURRENT}/${s_file}" ]];then
					rm ${APP_DIR_CURRENT}/${s_file}
				fi
				echo "Create symlink ${APP_DIR_CURRENT}";
				ln -sfn ${APP_DIR_SHARED}/${s_file} ${APP_DIR_CURRENT}/${s_file};
				sleep 1;
				if [[ -L "${APP_DIR_CURRENT}/${s_file}" ]];then
					echo "symlink to ${APP_DIR_CURRENT}/${s_file} creare successful"
				else
					echo "Error create symlink to ${APP_DIR_CURRENT}/${s_file}"
				fi
			fi
		fi
done

}


main(){
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
	if [[ "${APP_NAME}" == "customer-portal" ]];then
		app_prepare_customer-portal
		supervisor_init_first
		supervisord_init_customer-portal
		supervisor_run
	fi
}

main






