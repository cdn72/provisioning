#!/usr/bin/env bash

export PASSWORD=
export HTTP_PASSWORD=
export DIRS_CHECK="/data/log /opt/public_libs/files/libs.d/apps/android"
export check_files="/home/user/.android/avd/Pixel_2_API_29.avd/multiinstance.lock /home/user/.android/avd/Pixel_2_API_29.avd/hardware-qemu.ini.lock"
export ipaddr_socat=$(ip  addr show | grep "${CONTAINER_NETWORK:-172}" | grep 'inet' | awk  '{ print $2 }' | cut -d'/' -f1 | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" )

if [ -n "$VNC_PASSWORD" ]; then
    echo -n "$VNC_PASSWORD" > /.password1
    x11vnc -storepasswd $(cat /.password1) /.password2
    chmod 400 /.password*
    sed -i 's/^command=x11vnc.*/& -rfbauth \/.password2/' /etc/supervisor/conf.d/supervisord.conf
    export VNC_PASSWORD=
fi

if [ -n "$X11VNC_ARGS" ]; then
    sed -i "s/^command=x11vnc.*/& ${X11VNC_ARGS}/" /etc/supervisor/conf.d/supervisord.conf
fi

if [ -n "$OPENBOX_ARGS" ]; then
    sed -i "s#^command=/usr/bin/openbox\$#& ${OPENBOX_ARGS}#" /etc/supervisor/conf.d/supervisord.conf
fi

if [ -n "$RESOLUTION" ]; then
    sed -i "s/1024x768/$RESOLUTION/" /usr/local/bin/xvfb.sh
fi

USER=${USER:-root}
HOME=/root
if [ "$USER" != "root" ]; then
    echo "* enable custom user: $USER"
    useradd --create-home --shell /bin/bash --user-group --groups adm,sudo $USER
    if [ -z "$PASSWORD" ]; then
        echo "  set default password to \"ubuntu\""
        PASSWORD=ubuntu
    fi
    HOME=/home/$USER
    echo "$USER:$PASSWORD" | chpasswd
    cp -r /root/{.config,.gtkrc-2.0,.asoundrc} ${HOME}
    chown -R $USER:$USER ${HOME}
    [ -d "/dev/snd" ] && chgrp -R adm /dev/snd
fi
sed -i -e "s|%USER%|$USER|" -e "s|%HOME%|$HOME|" /etc/supervisor/conf.d/supervisord.conf

# home folder
if [ ! -x "$HOME/.config/pcmanfm/LXDE/" ]; then
    mkdir -p $HOME/.config/pcmanfm/LXDE/
    ln -sf /usr/local/share/doro-lxde-wallpapers/desktop-items-0.conf $HOME/.config/pcmanfm/LXDE/
    chown -R $USER:$USER $HOME
fi

# nginx workers
sed -i 's|worker_processes .*|worker_processes 1;|' /etc/nginx/nginx.conf

# nginx ssl
if [ -n "$SSL_PORT" ] && [ -e "/etc/nginx/ssl/nginx.key" ]; then
    echo "* enable SSL"
	sed -i 's|#_SSL_PORT_#\(.*\)443\(.*\)|\1'$SSL_PORT'\2|' /etc/nginx/sites-enabled/default
	sed -i 's|#_SSL_PORT_#||' /etc/nginx/sites-enabled/default
fi

# nginx http base authentication
if [ -n "$HTTP_PASSWORD" ]; then
    echo "* enable HTTP base authentication"
    htpasswd -bc /etc/nginx/.htpasswd $USER $HTTP_PASSWORD
	sed -i 's|#_HTTP_PASSWORD_#||' /etc/nginx/sites-enabled/default
fi

# dynamic prefix path renaming
if [ -n "$RELATIVE_URL_ROOT" ]; then
    echo "* enable RELATIVE_URL_ROOT: $RELATIVE_URL_ROOT"
	sed -i 's|#_RELATIVE_URL_ROOT_||' /etc/nginx/sites-enabled/default
	sed -i 's|_RELATIVE_URL_ROOT_|'$RELATIVE_URL_ROOT'|' /etc/nginx/sites-enabled/default
fi

cat <<OEF> /etc/supervisor/conf.d/socat.conf

[supervisord]
nodaemon=true

[program:socat5554]
priority=35
command=socat tcp-listen:5554,bind=${ipaddr_socat},fork,reuseaddr tcp:127.0.0.1:5554
autostart=true
autorestart=true
startretries=0

[program:socat5555]
priority=35
command=socat tcp-listen:5555,bind=${ipaddr_socat},fork,reuseaddr tcp:127.0.0.1:5555
autostart=true
autorestart=true
startretries=0

[program:socat27042]
priority=35
command=socat tcp-listen:27042,bind=${ipaddr_socat},fork,reuseaddr tcp:127.0.0.1:27042
autostart=true
autorestart=true
startretries=0
OEF

cat <<'OEF'> /etc/supervisor/conf.d/emulator_Pixel_2_API_29.conf
[supervisord]
nodaemon=true

[program:emulator_Pixel_2_API_29]
environment=DISPLAY=":1",HOME="/home/user",USER="user"
priority=55
directory=/home/user/Android/Sdk/emulator
command=sudo ./emulator -avd Pixel_2_API_29
stopwaitsecs=3
user=user
OEF


cat <<OEF> /etc/supervisor/conf.d/frida.conf
[supervisord]
nodaemon=true

[program:frida]
#user=${DOCKER_IMAGE_RUN_USER:-user}
environment=DISPLAY=":1",HOME="/home/user",USER="user"
priority=999
directory=/home/${DOCKER_IMAGE_RUN_USER:-user}/
command=adb shell /data/local/tmp/frida-server-x86_64

autostart=true
autorestart=true
stdout_logfile=/data/log/frida-server-x86_64.log
stdout_logfile_maxbytes=0
stderr_logfile=/data/log/frida-server-x86_64.log
stderr_logfile_maxbytes=0
startretries=0
stopwaitsecs=10
user=root
OEF

cat <<'OEF'> /etc/supervisor/conf.d/monitor_check_frida.conf

[supervisord]
nodaemon=true

[program:monitor_check_frida]
command=sh -c "while true;do bash /opt/public_libs/files/libs.d/apps/android/monitor_check_frida.sh;done"
autostart=true
autorestart=true
priority=999
stdout_logfile=/var/log/monitor_check_frida.log
stdout_logfile_maxbytes=0
stderr_logfile=/var/log/monitor_check_frida.log
stderr_logfile_maxbytes=0
startretries=0
stopwaitsecs=10
user=root

OEF


cat <<'OEF'> /etc/supervisor/conf.d/openvpn-zenmate.conf
[supervisord]
nodaemon=true

[program:openvpn-zenmate]
directory=/data/openvpn/zenmate
command=/usr/sbin/openvpn --config openvpn.ovpn
autostart=true
autorestart=true
startretries=400
stderr_logfile=/var/log/openvpn.err
stdout_logfile=/var/log/openvpn.log
priority=70
OEF

for check_file in ${check_files};do
if [[ -f ${check_file} ]] ;then
  echo "Remove check file $check_file"	
  rm -v ${check_file}
fi
done

for DIR_CHECK in ${DIRS_CHECK};do
	if [[ ! -d "${DIR_CHECK}" ]];then 
		mkdir -p $"{DIR_CHECK}"
	else
		echo "Dir ${DIR_CHECK} already exist"
	fi
done

echo "${USER:-root} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

echo "1. Run adb shell setprop service.adb.tcp.port 5555"
su -s /bin/bash -c 'adb shell setprop service.adb.tcp.port 5555' ${DOCKER_IMAGE_RUN_USER:-user}
echo "1. -----------------"

echo "2. Run adb forward tcp:27042 tcp:27042"
su -s /bin/bash -c 'adb forward tcp:27042 tcp:27042' ${DOCKER_IMAGE_RUN_USER:-user}
echo "2. -----------------"

exec /bin/tini -- supervisord -n -c /etc/supervisor/supervisord.conf