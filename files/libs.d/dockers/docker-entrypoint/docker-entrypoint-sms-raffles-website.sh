#!/usr/bin/env bash
app_prepare_sms-raffles-website(){
    if [[ -f "${APP_DIR_SHARED}/.env" ]];then ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env;fi
    if [[ ! -f "${APP_DIR_CURRENT}/public/static/numbers.json" ]];then
        if [[ -f "${APP_DIR_SHARED}/public/static/numbers.json" ]];then ln -sf ${APP_DIR_SHARED}/public/static/numbers.json ${APP_DIR_CURRENT}/public/static/numbers.json;fi
    fi
    if [[ ! -f "${APP_DIR_CURRENT}/public/static/text.json" ]];then
        if [[ -f "${APP_DIR_SHARED}/public/static/text.json" ]];then ln -sf ${APP_DIR_SHARED}/public/static/text.json ${APP_DIR_CURRENT}/public/static/text.json;fi
    fi

    if [[ -d "${APP_DIR_SHARED}/storage" ]];then
        if [[  ! -L "${APP_DIR_CURRENT}/storage" ]];then
            if [[ -d "${APP_DIR_CURRENT}/storage" && ! -L "${APP_DIR_CURRENT}/storage" ]];then
                mv ${APP_DIR_CURRENT}/storage ${APP_DIR_CURRENT}/storage.old
            fi
            ln -sf ${APP_DIR_SHARED}/storage ${APP_DIR_CURRENT}/storage
        fi
    fi
}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    if [[ "${APP_NAME}" == "sms-raffles-website" ]];then
        app_prepare_sms-raffles-website
        nginx_config_gen_http_backend_sms-raffles-website
        app_fix_perms || (echo "skip perms fix")
        nginx_run
    fi
}

main






