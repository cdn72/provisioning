#!/usr/bin/env bash

app_prepare_voip-freeswitch(){
  get_function_name
  echo "--------------------"
}

main(){
    set -e
    source /opt/public_libs/files/libs.d/apps/main.sh
    set +e
    if [[ "${APP_NAME}" == "voip-freeswitch" ]];then
        app_prepare_voip-freeswitch
    fi
}

main

echo ""
exec "$@"




