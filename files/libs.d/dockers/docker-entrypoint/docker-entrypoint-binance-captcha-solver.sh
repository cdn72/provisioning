#!/usr/bin/env bash
app_prepare_binance-captcha-solver (){
    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env

    cat <<OEF> /etc/proxychains4.conf
    strict_chain
    #proxy_dns
    remote_dns_subnet 224
    tcp_read_time_out 15000
    tcp_connect_time_out 8000
    localnet 127.0.0.0/255.0.0.0
    localnet 10.0.0.0/255.0.0.0
    localnet 172.16.0.0/255.255.0.0
    localnet 192.168.0.0/255.255.0.0
    [ProxyList]
    http ${HTTP_PROXY_HOST:-192.168.100.111} ${HTTP_PROXY_PORT:-3128}
OEF


    echo "Crons add for user ${DOCKER_IMAGE_RUN_USER:-www-data}"
    if [[ -f "/var/spool/cron/crontabs/${DOCKER_IMAGE_RUN_USER:-www-data}" ]]; then
        rm /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    fi
    if [[ ! -d "${APP_DIR_SHARED}/storage/logs/crons" ]];then
        mkdir -p ${APP_DIR_SHARED}/storage/logs/crons
    fi
    cat <<OEF> /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
##Crons
OEF

    cat <<OEF> /tmp/crontabs
00 00 * * find ${APP_DIR_SHARED}/logs/resources* -type f -mtime +7 -exec rm -v {} \; >> ${APP_DIR_SHARED}/storage/logs/crons/logs_clean.log 2>&1
*/30 * * * cd ${APP_DIR_CURRENT} && python3.7 utils/refresh_tasklist.py >> ${APP_DIR_SHARED}/storage/logs/crons/refresh_tasklist.log 2>&1
OEF
    crontab -l -u ${DOCKER_IMAGE_RUN_USER:-www-data} | cat - /tmp/crontabs | crontab -u ${DOCKER_IMAGE_RUN_USER:-www-data} -
    echo "Cronlist"
    cat /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    echo "Fix rights"
    chown -v "${DOCKER_IMAGE_RUN_USER:-www-data}":crontab /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    
    if [[ ! -f "${APP_DIR_SHARED}/storage/${APP_VERSION}_redis_flushed" ]];then
        export REDIS_HOST=$(grep -Po "^REDIS_HOST=.*" ${APP_DIR_SHARED}/.env | awk -F\= '{print $2}')
        export REDIS_PORT=$(grep -Po "^REDIS_PORT=.*" ${APP_DIR_SHARED}/.env | awk -F\= '{print $2}')
        export REDIS_DB=$(grep -Po "^REDIS_DB=.*" ${APP_DIR_SHARED}/.env | awk -F\= '{print $2}')
        echo "Try Clean redis database ${REDIS_DB:-0}" >> ${APP_DIR_SHARED}/storage/${APP_VERSION}_redis_flushed 2>&1
        echo "exec redis-cli -h ${REDIS_HOST:-redis} -p ${REDIS_PORT:-6379} -n ${REDIS_DB:-0} flushdb"
        echo "exec redis-cli -h ${REDIS_HOST:-redis} -p ${REDIS_PORT:-6379} -n ${REDIS_DB:-0} flushdb" >> ${APP_DIR_SHARED}/storage/${APP_VERSION}_redis_flushed 2>&1
        redis-cli -h ${REDIS_HOST:-redis} -p ${REDIS_PORT:-6379} -n ${REDIS_DB:-0} flushdb >> ${APP_DIR_SHARED}/storage/${APP_VERSION}_redis_flushed 2>&1
        if [ $? == 0 ];then
            echo "Redis db ${REDIS_DB:-0}  flushed success at $(date)" >> ${APP_DIR_SHARED}/storage/${APP_VERSION}_redis_flushed
        else
            echo "Redis db ${REDIS_DB:-0}  flushed with error's at $(date)" > ${APP_DIR_SHARED}/storage/${APP_VERSION}_redis_flushed
        fi
    else
        echo "Skip clean redis database. File ${APP_DIR_SHARED}/storage/${APP_VERSION}_redis_flushed exist"
    fi

}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/consul_agent.sh
    source /opt/public_libs/files/libs.d/apps/consul_template.sh
    if [[ "${APP_NAME}" == "binance-captcha-solver" ]];then
        app_prepare_binance-captcha-solver
        consul_agent_start
        sleep 10;
        supervisor_init_nginx
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_binance-captcha-solver
        supervisor_init_pm2_binance-captcha-solver
        supervisor_init_cron
        app_fix_perms || (echo "skip perms fix")
        app_prepare_run_adb
        supervisor_run
    fi
}

main






