#!/usr/bin/env bash

app_prepare_sms-registration-bot(){

    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    if grep -q "^PROXYCHAINS=1" "${APP_DIR_CURRENT}"/.env;then
        export APP_EXEC_PREARG="/usr/bin/proxychains4 -q"
    else
        export APP_EXEC_PREARG=
    fi

if grep -q "^PROXYCHAINS=1" "${APP_DIR_CURRENT}"/.env;then
    PROXYCHAINS_HOST=$(grep -Po '^PROXYCHAINS_HOST=.*' ${APP_DIR_CURRENT}/.env  | awk -F\= '{print $2}' )
    PROXYCHAINS_PORT=$(grep -Po '^PROXYCHAINS_PORT=.*' ${APP_DIR_CURRENT}/.env | awk -F\= '{print $2}' )
    if [[ -n ${PROXYCHAINS_HOST} && ${PROXYCHAINS_PORT} ]];then
    cat <<OEF> /etc/proxychains4.conf
    strict_chain
    #proxy_dns
    remote_dns_subnet 224
    tcp_read_time_out 15000
    tcp_connect_time_out 8000
    localnet 127.0.0.0/255.0.0.0
    localnet 10.0.0.0/255.0.0.0
    localnet 172.16.0.0/255.240.0.0
    localnet 192.168.0.0/255.255.0.0
    [ProxyList]
    http ${PROXYCHAINS_HOST:-10.50.0.119} ${PROXYCHAINS_PORT:-3128}
OEF
    else
        echo "PROXYCHAINS_HOST or PROXYCHAINS_PORT not exist in ${APP_DIR_CURRENT}/.env. Skip generate /etc/proxychains4.conf"
    fi
else
    echo "PROXYCHAINS not enabled in ${APP_DIR_CURRENT}/.env Skip generate /etc/proxychains4.conf" 
fi

    echo "Crons add for user ${DOCKER_IMAGE_RUN_USER:-www-data}"
    if [[ -f "/var/spool/cron/crontabs/${DOCKER_IMAGE_RUN_USER:-www-data}" ]]; then
        rm /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    fi
    cat <<OEF> /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
##Crons
OEF

    cat <<OEF> /tmp/crontabs

*/15 * * * * cd ${APP_DIR_CURRENT} &&  python3.7 utils/captcha_refresh.py  >> ${APP_DIR_SHARED}/storage/logs/crons/captcha_refresh.log 2>&1
0 0 * * * find ${APP_DIR_SHARED}/storage/logs/resources* -mtime +7 -exec rm {} \; >> ${APP_DIR_SHARED}/storage/logs/crons/old_log_remove.log 2>&1
OEF

if grep -q "^CRON_PROXY_REFRESH_ENABLED=.*rue" "${APP_DIR_CURRENT}"/.env;then
    cat <<OEF>> /tmp/crontabs
*/10 * * * * cd ${APP_DIR_CURRENT} &&  python3.7 utils/proxy_refresh.py >> ${APP_DIR_SHARED}/storage/logs/crons/proxy_refresh.log 2>&1
OEF
fi

if [[ -f "${APP_DIR_CURRENT}/utils/healthcheck.py" ]];then
    cat <<OEF>> /tmp/crontabs
0 0 * * 1 cd ${APP_DIR_CURRENT} &&  python3.7 utils/healthcheck.py >> ${APP_DIR_SHARED}/storage/logs/crons/healthcheck.log 2>&1
OEF
fi



if grep -q "^MONITORING_PROM_ENABLED=.*rue" "${APP_DIR_CURRENT}"/.env;then
    cat <<OEF>> /tmp/crontabs
00 */1 * * * cd ${APP_DIR_CURRENT} &&  python3.7 utils/main_resources.py >> ${APP_DIR_SHARED}/storage/logs/crons/main_resources.log 2>&1
OEF
fi

cat <<OEF> /tmp/scripts_update.sh
if grep -q "^CRON_SCRIPT_UPDATE_ENABLED=.*rue" "${APP_DIR_CURRENT}"/.env;then
    if [[ -f "${APP_DIR_CURRENT}/utils/update.py" ]];then
        cd ${APP_DIR_CURRENT} &&  python3.7 utils/update.py
    else
        echo "Error run scripts_update. File ${APP_DIR_CURRENT}/utils/update.py not exist"
    fi
else
    echo "Variable 'CRON_SCRIPT_UPDATE_ENABLED' not equal true or not exist. skip run ${APP_DIR_CURRENT}/utils/update.py"
fi
OEF

chmod 777 -v /tmp/scripts_update.sh

    cat <<OEF>> /tmp/crontabs
*/5 * * * * bash /tmp/scripts_update.sh  >> ${APP_DIR_SHARED}/storage/logs/crons/scripts_update.log 2>&1
OEF

    crontab -l -u ${DOCKER_IMAGE_RUN_USER:-www-data} | cat - /tmp/crontabs | crontab -u ${DOCKER_IMAGE_RUN_USER:-www-data} -
    echo "Cronlist"
    cat /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    echo "Fix rights"
    chown -v "${DOCKER_IMAGE_RUN_USER:-www-data}":crontab /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"

    echo "Create file ${APP_DIR_CURRENT}/sms-sender.ini"
    cat <<OEF> "${APP_DIR_CURRENT}"/sms-sender.ini
[uwsgi]
module = web.api:app
master = true
processes = 5
chdir = ${APP_DIR_CURRENT}
socket = 0.0.0.0:8000
uid =${DOCKER_IMAGE_RUN_USER:-www-data}
gid = ${DOCKER_IMAGE_RUN_USER:-www-data}
vacuum = true
die-on-term = true
plugin=python3
OEF



}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    source /opt/public_libs/files/libs.d/apps/consul_agent.sh
    source /opt/public_libs/files/libs.d/apps/consul_template.sh
    if [[ "${APP_NAME}" == "sms-registration-bot" ]];then
        app_prepare_sms-registration-bot
        consul_agent_start
        nginx_prepare
        nginx_init_redis_proxy
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_uwsgi_sms-registration-bot
        supervisor_init_cron
        supervisor_init_nginx

        fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main






