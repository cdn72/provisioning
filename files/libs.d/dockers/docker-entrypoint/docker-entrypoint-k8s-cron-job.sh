#!/usr/bin/env bash

app_prepare_k8s-cron-job(){
  get_function_name
  echo "--------------------"
}

main(){
    set -e
    source /opt/public_libs/files/libs.d/apps/main.sh
    set +e
    if [[ "${APP_NAME}" == "k8s-cron-job" ]];then
        app_prepare_k8s-cron-job
    fi
}

main

echo ""
exec "$@"




