#!/usr/bin/env bash

app_prepare_playwright-service (){
    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env

    cat <<OEF> /etc/proxychains4.conf
    strict_chain
    #proxy_dns
    remote_dns_subnet 224
    tcp_read_time_out 15000
    tcp_connect_time_out 8000
    localnet 127.0.0.0/255.0.0.0
    localnet 10.0.0.0/255.0.0.0
    localnet 172.16.0.0/255.240.0.0
    localnet 192.168.0.0/255.255.0.0
    [ProxyList]
    http ${HTTP_PROXY_HOST:-192.168.100.111} ${HTTP_PROXY_PORT:-3128}
OEF


if [[ -f "/opt/public_libs/files/libs.d/apps/playwright-service/monitor_check_playwright-service.sh" ]];then
    if [[ ! -d "${APP_DIR_SHARED}/storage/logs" ]];then mkdir -p ${APP_DIR_SHARED}/storage/logs;fi
cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/monitor_check_playwright-service.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:monitor_check_playwright-service]
command=sh -c "while true;do bash /opt/public_libs/files/libs.d/apps/playwright-service/monitor_check_playwright-service.sh;done"
autostart=true
autorestart=true
stdout_logfile=${APP_DIR_SHARED}/storage/logs/monitor_check_playwright-service.log
stdout_logfile_maxbytes=0
stderr_logfile=${APP_DIR_SHARED}/storage/logs/monitor_check_playwright-service.log
stderr_logfile_maxbytes=0
startretries=140
startretries=140
stopwaitsecs=5
user=${DOCKER_IMAGE_RUN_USER:-www-data}
OEF
fi


    echo "Crons add for user ${DOCKER_IMAGE_RUN_USER:-www-data}"
    if [[ -f "/var/spool/cron/crontabs/${DOCKER_IMAGE_RUN_USER:-www-data}" ]]; then
        rm /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    fi
    if [[ ! -d "${APP_DIR_SHARED}/storage/logs/crons" ]];then
        mkdir -p ${APP_DIR_SHARED}/storage/logs/crons
    fi
    cat <<OEF> /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
##Crons
OEF

    crontab -l -u ${DOCKER_IMAGE_RUN_USER:-www-data} | cat - /tmp/crontabs | crontab -u ${DOCKER_IMAGE_RUN_USER:-www-data} -
    echo "Cronlist"
    cat /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    echo "Fix rights"
    chown -v "${DOCKER_IMAGE_RUN_USER:-www-data}":crontab /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    if [[ -f "${APP_DIR_CURRENT}/utils/health_check.py" ]];then chmod +x ${APP_DIR_CURRENT}/utils/health_check.py;fi

}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/consul_agent.sh
    source /opt/public_libs/files/libs.d/apps/consul_template.sh
    if [[ "${APP_NAME}" == "playwright-service" ]];then
        app_prepare_playwright-service
        consul_agent_start
        sleep 10;
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_pm2_playwright-service
        app_fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main






