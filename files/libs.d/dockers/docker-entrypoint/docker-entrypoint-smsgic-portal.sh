#!/usr/bin/env bash
app_prepare_smsgic-portal(){
    if [[ -f "${APP_DIR_SHARED}/.env" ]];then
        ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    fi
    if [[ ! -d "${APP_DIR_SHARED}/storage" ]];then
        mkdir -p ${APP_DIR_SHARED}/storage
    fi
    if [[  ! -L "${APP_DIR_CURRENT}/storage" ]];then
        if [[ -d "${APP_DIR_SHARED}/storage" ]];then mv ${APP_DIR_CURRENT}/storage ${APP_DIR_CURRENT}/storage.old;fi
        ln -sf ${APP_DIR_SHARED}/storage ${APP_DIR_CURRENT}/storage
    fi
}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    if [[ "${APP_NAME}" == "smsgic-portal" ]];then
        app_prepare_smsgic-portal
        nginx_config_gen_http_backend_smsgic-portal
        fix_perms || (echo "skip perms fix")
        #supervisor_init_first
        #supervisor_init_nginx
        #supervisor_run
        nginx_run
    fi
}

main






