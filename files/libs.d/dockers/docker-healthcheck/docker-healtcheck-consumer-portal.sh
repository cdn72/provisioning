#!/usr/bin/env bash


hc_convy_consumer-portal(){
	curl -o /dev/null -s -w "%{http_code}\n" http://127.0.0.1:8100 && echo "Alive"
}

main(){
	if [[  "${APP_NAME}" == "consumer-portal" ]];then
		hc_convy_consumer-portal
	fi

}
main