#!/usr/bin/env python3
import time
from selenium import webdriver
import os
import dotenv
import sys
import subprocess
import requests
from requests.auth import HTTPDigestAuth
import time
import datetime
from datetime import datetime, timedelta
from time import gmtime, strftime

dotenv.load_dotenv()

TELEGRAM_BOT_TOKEN = os.getenv("TELEGRAM_BOT_TOKEN","1801387352:О323оаввыа")
TELEGRAM_CHANNEL_ID = os.getenv("TELEGRAM_CHANNEL_ID","-1001802164071323232")
SELENIUM_HOST = os.getenv("SELENIUM_HOST", "127.0.0.1")
SELENIUM_PORT = os.getenv("SELENIUM_PORT", "4444")
SELENIUM_TARGET_TEST_HOST = os.getenv("SELENIUM_TARGET_TEST_HOST", "https://google.com")
SELENIUM_HEALTHCHECK_ATTEMPTS = os.getenv("SELENIUM_HEALTHCHECK_ATTEMPTS", 1)
APP_NAME = os.getenv("APP_HTTP_HOST", "selenoid-chrome")
APP_HTTP_HOST = os.getenv("APP_HTTP_HOST", "undefined")
APP_VERSION = os.getenv("APP_VERSION", "undefined")
DEBUG_KILL_ON = os.getenv("DEBUG_KILL_ON", "0")
DEBUG_VERBOSE_LEVEL = os.getenv("DEBUG_VERBOSE_LEVEL", "0")

def telegram_send(t_bot_token, t_group_id, t_text):
    url = 'https://api.telegram.org/bot' + t_bot_token + '/sendMessage'
    payload = {'chat_id': t_group_id,
               'disable_web_page_preview': 1,
               'disable_notification': 0,
               'text': t_text}
    print('Send to telegram')
    print("url= %(r_url)s payload %(r_payload)s \
        " % {"r_url": url, "r_payload": payload})
    r = requests.post(url, data=payload)


def selenoid_healthcheck_run():
    attempts = 0
    while attempts < int(SELENIUM_HEALTHCHECK_ATTEMPTS):
        try:
            print(f"Check endpoint {SELENIUM_HOST}:{SELENIUM_PORT} ")
            options = webdriver.ChromeOptions()
            options.add_argument("--host-resolver-rules=MAP *google* 127.0.0.1")
            browser = webdriver.Remote(
                options=options,
                command_executor="http://%s:%s" % (SELENIUM_HOST, SELENIUM_PORT),
            )
            browser.close()
            time.sleep(1)
            return "pass" 
            #exit(0)
            if DEBUG_KILL_ON  == "1":
                try:
                    print(f"DEBUG_KILL_ON activated. Try SELENIUM_TARGET_TEST_HOST={SELENIUM_TARGET_TEST_HOST}")
                    browser.set_page_load_timeout(2)
                    browser.get(
                        SELENIUM_TARGET_TEST_HOST
                        )
                    html = browser.page_source
                    print(html)
                    #browser.close()
                except Exception as error:
                    print(f"Warning. error during browser.get")
                    browser.close()
                    time_now = str(datetime.now().strftime("%Y-%d-%H %H:%M:%S"))
                    t_text = f"{APP_NAME} {APP_HTTP_HOST} restarted. error during browser.get at {time_now}"
                    telegram_send(TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_ID, t_text)
                    p2 = subprocess.Popen("kill 1", stdout=subprocess.PIPE, shell=True)
                    print(p2.communicate())
        except Exception as error:
            attempts += 1
            time_now = str(datetime.now().strftime("%Y-%d-%H %H:%M:%S"))
            print(error)
            time.sleep(2)
            #exit(1)
            #t_text = f"{APP_NAME} {APP_HTTP_HOST} restarted at {time_now}"
            #telegram_send(TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_ID, t_text)
            #p2 = subprocess.Popen("kill 1", stdout=subprocess.PIPE, shell=True)
            #print(p2.communicate())
    return "error"    


response_val = str(selenoid_healthcheck_run())

if response_val == "error":
    print("ERROR")
    time_now = str(datetime.now().strftime("%Y-%d-%H %H:%M:%S"))
    t_text = f"{APP_NAME} {APP_HTTP_HOST} healthcheck bad. Container restarted. Time {time_now}. "
    print(t_text)
    if DEBUG_VERBOSE_LEVEL  == "1":
        telegram_send(TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_ID, t_text)
    p2 = subprocess.Popen("kill 1", stdout=subprocess.PIPE, shell=True)
    print(p2.communicate())
elif response_val == "pass":
    print("PASS")
    print("Healthcheck finished good in " + str(datetime.now().strftime("%Y-%d-%H %H:%M:%S")), file=sys.stdout)
    if DEBUG_VERBOSE_LEVEL  == "3":
        time_now = str(datetime.now().strftime("%Y-%d-%H %H:%M:%S"))
        t_text = f"{APP_NAME} {APP_HTTP_HOST} healthcheck ok . TIme  {time_now}"
        telegram_send(TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_ID, t_text)
else:
    print("UNKNOWN STATE")