#!/usr/bin/env bash
export REDIS_HOST=$(grep -Po "^REDIS_HOST=.*" ${APP_DIR_SHARED}/.env | awk -F\= '{print $2}')
export REDIS_PORT=$(grep -Po "^REDIS_PORT=.*" ${APP_DIR_SHARED}/.env | awk -F\= '{print $2}')
export REDIS_DB=$(grep -Po "^REDIS_DB=.*" ${APP_DIR_SHARED}/.env | awk -F\= '{print $2}')
local_endpoint=$(curl -q -s  localhost:8500/v1/agent/services | jq .[].Address | tr -d '"')
browser_endpoints=$(redis-cli -h ${REDIS_HOST:-redis} -p ${REDIS_PORT:-6379} -n ${REDIS_DB:-0} scard browser_endpoints_${local_endpoint} | awk '{print $NF}')
min_endpoint="10"
if [[ "${browser_endpoints}" -gt "${min_endpoint}" ]];then
	echo "Health ok. browser_endpoints count ${browser_endpoints} for endpoint address ${local_endpoint}"
else
	echo "Healthcheck error. browser_endpoints unavailabled ${browser_endpoints} for endpoint address ${local_endpoint}"
	exit 1
fi