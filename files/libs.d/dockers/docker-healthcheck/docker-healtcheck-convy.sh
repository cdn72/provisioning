#!/usr/bin/env bash

hc_convy_dispatcher(){
    curl -f http://127.0.0.1:81/health
}

hc_convy_worker(){
    if ! grep -q "roadrunner" "/var/www/html/${CONVY_HTTP_HOST:-${APP_HTTP_HOST}}/current/composer.json"; then
        echo "healthcheck swoole"
        curl -f http://127.0.0.1:8000/api/octane/healthcheck/swoole;
    elif grep -q "roadrunner" "/var/www/html/${CONVY_HTTP_HOST:-${APP_HTTP_HOST}}/current/composer.json"; then
        echo "healthcheck roadrunner"
        curl -f http://127.0.0.1:8000/api/octane/healthcheck/roadrunner;
    else
        echo "healthcheck simple"
        curl -f http://127.0.0.1:81/health
    fi
}
main(){
    if [[  "${ENV_WORKER_FUNCTION}" == "dispatcher" ]] ;then
        hc_convy_dispatcher
    fi
    if [[  "${ENV_WORKER_FUNCTION}" == "worker" ]] ;then
        if grep -q "DOCKER_CONTAINER_HEALTHCHECK_ENABLED=.*alse"  ${APP_DIR_CURRENT}/.env ;then
            echo "hc_convy_worker skipped"
        else
            hc_convy_worker
        fi
    fi
}
main