#!/usr/bin/env python3.7
#python3.7 -m pip install  psutil
import psutil
from playwright.sync_api import sync_playwright

process_tag = "brows.serv"

playwright = sync_playwright().start()
for proc in psutil.process_iter():
    try:
        name = proc.cmdline()[0] if len(proc.cmdline()) > 0 else ""
        if process_tag.lower() in name.lower():
            browser_endpoint = name[len(process_tag) + 1:]
            print(f"Check browser_endpoint {browser_endpoint}")
            if browser_endpoint:
                try:
                    browser = playwright.firefox.connect(browser_endpoint)
                except Exception as e:
                    print(f"Kill {browser_endpoint}")
                    proc.kill()
            else:
            	print(str(browser_endpoint) + "ok")
    except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
        pass