<?php
return [
    "namespace" => 'app',
    "enabled" => env('HORIZON_PROMETHEUS_EXPORTER_ENABLED', true),
    "url" => env('HORIZON_PROMETHEUS_EXPORTER_URL', 'metrics'),
    "exporters" => [
        \LKDevelopment\HorizonPrometheusExporter\Exporter\CurrentMasterSupervisors::class,
        \LKDevelopment\HorizonPrometheusExporter\Exporter\JobsPerMinute::class,
        \LKDevelopment\HorizonPrometheusExporter\Exporter\CurrentWorkload::class,
        \LKDevelopment\HorizonPrometheusExporter\Exporter\CurrentProccesesPerQueue::class,
        \LKDevelopment\HorizonPrometheusExporter\Exporter\FailedJobsPerHour::class,
        \LKDevelopment\HorizonPrometheusExporter\Exporter\HorizonStatus::class,
        \LKDevelopment\HorizonPrometheusExporter\Exporter\RecentJobs::class
    ],
    "ip_whitelist" => [
    ],
];
