#!/usr/bin/env bash
export GITLAB_HTTP_HOST="git.linux2be.com"
export GITLAB_DOCKER_DIR="/docker-compose/SWARM/services/gitlab/${GITLAB_HTTP_HOST}"
export GITLAB_DATA_DIR="${GITLAB_DOCKER_DIR}/data.backup.14/gitlab/data"
export GITLAB_CONFIG_DIR="${GITLAB_DOCKER_DIR}/data.backup.14/gitlab/config"
export GITLAB_BACKUP_DIR="${GITLAB_DATA_DIR}/backups"

export BACKUP_OPTIONS="SKIP="
export BACKUP_DIR="/docker-compose-local/bup/app/gitlab/${GITLAB_HTTP_HOST}"
export DATE=$(date "+%Y_%m_%d")


backup_gitlab () {
    echo "Begin backup gitlab ${GITLAB_HTTP_HOST} at $(date)"
    docker exec -i $(docker ps | grep 'gitlab' | grep 'assets' | awk '{print $NF}') gitlab-rake gitlab:backup:create CRON=1 ${BACKUP_OPTIONS};
    echo "Finished backup gitlab ${GITLAB_HTTP_HOST} at $(date)"
    if [[ ! -d "${BACKUP_DIR}/${DATE}" ]];then
        mkdir -p "${BACKUP_DIR}/${DATE}"
    fi
    rsync -aPzv ${GITLAB_BACKUP_DIR}/ ${BACKUP_DIR}/${DATE}/
    rsync -aPzv ${GITLAB_CONFIG_DIR}/gitlab-secrets.json ${BACKUP_DIR}/${DATE}/
    rsync -aPzv ${GITLAB_CONFIG_DIR}/gitlab.rb ${BACKUP_DIR}/${DATE}/

}

delete_old_backups () {
    if [[ -d "${GITLAB_BACKUP_DIR}" ]];then
        find "${GITLAB_BACKUP_DIR}" -maxdepth 0 -type d -exec rm -rv {} \;
    fi
    if [[ ! -d "${GITLAB_BACKUP_DIR}" ]];then
        mkdir -p ${GITLAB_BACKUP_DIR} -m 777
    fi
    if [[ -d "${BACKUP_DIR}" ]];then
        find "${BACKUP_DIR}" -maxdepth 1 -type d -mtime +1 -exec rm -rv {} \;
    fi
}




main () {
    delete_old_backups
    backup_gitlab
    delete_old_backups
}

main