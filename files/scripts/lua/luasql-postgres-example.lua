#!/usr/bin/env lua

luasql = require "luasql.postgres"
env = assert (luasql.postgres())
con = assert (env:connect("db_edr_dev" ,'postgres', 'phahMMMddd999h7uutheePighMMMdsdsdss','10.28.0.1', 25432))
cur = assert (con:execute"SELECT name, email from users")

row = cur:fetch ({}, "a")	-- the rows will be indexed by field names
while row do
  print(string.format("Name: %s, E-mail: %s", row.name, row.email))
  row = cur:fetch (row, "a")	-- reusing the table of results
endc

cur:close()
con:close()
env:close()