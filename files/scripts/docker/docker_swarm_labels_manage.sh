#!/usr/bin/env bash

export docker_swarm_nodes=$(docker node ls | grep 'febo-vm-node-02' | grep -ve "HOSTNAME" | awk '{print $1}')


export docker_swarm_labels_add_old="haproxy-postgres=true \
consul=true \
pgloader=true \
haproxy-postgres=14  \
pgbouncer-swarm=true \
postgres-14-1-bitnami=true \
postgres-15-1-bitnami=true \
worker_redis_node=true \
redis-swarm-node-1=true  \
meilisearch=true \
redis-swarm-node-2=true"
export docker_swarm_labels_add="worker_cluster_node=true nginx_proxy_cluster_node=true"
#febo-vm-node-02-convy-db
#febo-vm-node-03-convy-worker worker_cluster_node=true nginx_proxy_cluster_node=true

swarm_label_add() {
	for docker_swarm_node in ${docker_swarm_nodes}
	do
		local docker_swarm_node_name=$(docker node  inspect ${docker_swarm_node} | jq '.[0].Description.Hostname' | sed 's/^.\(.*\).$/\1/')
		local label_add_key=${1%=*}
		local label_add_value=${1#*=}
		echo "Try add label ${label_add_key}=${label_add_value}  to node ${docker_swarm_node_name}"
			
		docker_swarm_node_labels_all=$(docker node inspect ${docker_swarm_node_name}  | jq '.[0].Spec.Labels')
		printf "docker_swarm_node_labels_all ${docker_swarm_node_name}\n"
		printf "${docker_swarm_node_labels_all}\n"
		printf "\n"
		label_current_value=$(docker node inspect ${docker_swarm_node_name}  | jq   '.[0].Spec.Labels  | ."'"${label_add_key}"'"' | sed 's/^.\(.*\).$/\1/')
		#if [[ ! -n "${label_current_value}" && ${label_current_value} != "${label_add_value}" ]];then

		echo Compare "${label_add_key}=${label_current_value}" with "${label_add_key}=${label_add_value}"
		#sleep 1;
		if [[ -n "${label_current_value}" && "${label_add_key}=${label_current_value}" = "${label_add_key}=${label_add_value}" ]] ;then
			echo "Skip add label ${label_add_key}=${label_current_value} to node ${docker_swarm_node_name}"
		else
			echo "Adding label ${label_add_key}=${label_current_value}  to node ${docker_swarm_node_name}"
			docker node update --label-add ${label_add_key}=${label_add_value} ${docker_swarm_node_name}
		fi
done

}


main() {
	echo "docker_swarm_labels_add=${docker_swarm_labels_add}"
	echo "docker_swarm_nodes=${docker_swarm_nodes}"
	for docker_swarm_label_add in ${docker_swarm_labels_add}
	do
		swarm_label_add ${docker_swarm_label_add}
done

}
main