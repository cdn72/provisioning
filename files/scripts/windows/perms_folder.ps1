 $path = "C:\folder"
 $acl = Get-Acl $path
 $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("YourUsers","Modify", "ContainerInherit,ObjectInherit", "None", "Allow")
 $acl.SetAccessRule($AccessRule)
 $acl | Set-Acl $path