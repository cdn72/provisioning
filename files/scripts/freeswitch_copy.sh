#!/usr/bin/env bash


current_date=$(date +%Y_%m_%d__%H:%M)


export source_fs_logs="/docker-compose-local/logs/freeswitch"
export source_fs_json_cdr="${source_fs_logs}/json_cdr"
export target_fs_logs="/docker-compose-local/bup/SWARM/deploy-voip/logs/freeswitch"
export target_fs_json_cdr="${target_fs_logs}/json_cdr"


precheck(){
	if [[ ! -d "${target_fs_logs}" ]];then
		mkdir -p "${target_fs_logs}"
	fi
	if [[ ! -d "${target_fs_json_cdr}" ]];then
		mkdir -p "${target_fs_json_cdr}"
	fi
    cd /docker-compose-local/SWARM/python_esl ;docker-compose down ; docker-compose up -d
}

copy_files(){
	nice -n19 ionice -c3 -n7  /usr/bin/find ${source_fs_json_cdr}/ -mindepth 1 -maxdepth 3 -type f -iname '*.cdr.json' | xargs cp -v --preserve=timestamps --update -t ${target_fs_json_cdr}/
	nice -n19 ionice -c3 -n7  /usr/bin/find ${source_fs_logs}/  -maxdepth 1  -type f -iname 'freeswitch.log*' | xargs cp --update --preserve=timestamps -v -t ${target_fs_logs}/

}
import_files_to_db(){
	cd /docker-compose/SWARM/services/python && /usr/local/bin/docker-compose pull && /usr/local/bin/docker-compose up
}

remove_old_files(){

	nice -n19 ionice -c3 -n7  /usr/bin/find ${target_fs_json_cdr}/ -mindepth 1 -maxdepth 3 -type f  -iname '*.cdr.json' -mtime +4 | xargs rm -v -f || (echo "Noting to do")
	nice -n19 ionice -c3 -n7  /usr/bin/find ${target_fs_logs}/ -mindepth 1 -maxdepth 1 -type f  -iname 'freeswitch.log*' -mtime +5 | xargs rm -v -f || (echo "Noting to do")

	nice -n19 ionice -c3 -n7  /usr/bin/find ${source_fs_json_cdr}/ -mindepth 1 -maxdepth 3 -type f  -iname '*.cdr.json' -mtime +2 | xargs rm -v -f || (echo "Noting to do")
	nice -n19 ionice -c3 -n7  /usr/bin/find ${source_fs_logs}/ -mindepth 1 -maxdepth 1 -type f  -iname 'freeswitch.log*' -mtime +10 | xargs rm -v -f || (echo "Noting to do")
}

logrotate_run(){
if [[ -f "${source_fs_logs}/freeswitch.log" ]];then
	truncate -s 0 ${source_fs_logs}/freeswitch.log
    chmod 777 ${source_fs_logs}/freeswitch.log
fi

if [[ ! -f "${source_fs_logs}/freeswitch.log" ]];then 
	touch ${source_fs_logs}/freeswitch.log 
	chmod 777 ${source_fs_logs}/freeswitch.log

fi

docker exec -i deploy-voip_freeswitch_1 sh -c "fs_cli -x 'reload mod_logfile'"

}


snippets_(){
	echo "Fs reload mod_logfile"
	fs_cli -x 'module_exists mod_logfile'
	echo "Crons"
	#00 08 * * * bash /docker-compose/scripts/fs_copy_cdr.sh > /var/log/fs_copy_cdr.log 2>&1

}

main(){
	precheck
	import_files_to_db
	copy_files
    #import_files_to_db
    remove_old_files
    #logrotate_run
}

main