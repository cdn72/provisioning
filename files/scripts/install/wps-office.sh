#!/usr/bin/env bash

echo "Installing wps office 64"
WPS_OFFICE_PACKAGE="wps-office_11.1.0.11664.XA_amd64.deb"

wps_office_install(){
if [[ ! -d " /tmp/wps" ]];then mkdir -p  /tmp/wps && cd  /tmp/wps;else cd  /tmp/wps;fi
if [[ ! -f "${WPS_OFFICE_PACKAGE}" ]];then
    curl "https://wdl1.pcfg.cache.wpscdn.com/wpsdl/wpsoffice/download/linux/11664/wps-office_11.1.0.11664.XA_amd64.deb" -o "${WPS_OFFICE_PACKAGE}"
    sudo dpkg -i "${WPS_OFFICE_PACKAGE}"
    sudo apt-get install -f -y
    sudo apt-get install zip p7zip rsync -y
    rm ${WPS_OFFICE_PACKAGE}
else
    echo "File ${WPS_OFFICE_PACKAGE} exist. Exit"
fi
}
wps_office_install_mui(){
if [[ ! -d " /tmp/wps" ]];then mkdir -p  /tmp/wps && cd  /tmp/wps;else cd  /tmp/wps;fi
curl "https://gitlab.com/cdn72/extra-files/-/raw/main/files/wps-office/dict.zip" -o dict.zip
curl "https://gitlab.com/cdn72/extra-files/-/raw/main/files/wps-office/mui.zip" -o mui.zip
unzip  dict.zip  
unzip mui.zip
sudo rsync -r /tmp/wps/mui/ /opt/kingsoft/wps-office/office6/mui/
sudo rsync -r /tmp/wps/dict/ /opt/kingsoft/wps-office/office6/dicts/spellcheck/
rm -r /tmp/wps
}

main(){
    wps_office_install
    wps_office_install_mui
}
main