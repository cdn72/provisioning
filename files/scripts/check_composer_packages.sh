#!/usr/bin/env bash
export HACKED_PACKS="mephp/acmephp \
acmephp/core \
acmephp/ssl \
doctrine/doctrine-cache-bundle \
doctrine/doctrine-module \
doctrine/doctrine-mongo-odm-module \
doctrine/doctrine-orm-module \
doctrine/instantiator \
growthbook/growthbook \
jdorn/file-system-cache \
jdorn/sql-formatter \
khanamiryan/qrcode-detector-decoder \
object-calisthenics/phpcs-calisthenics-rules \
tga/simhash-php"

if [[ -d "${APP_DIR_CURRENT}/vendor" ]];then
	echo "" >  ${APP_DIR_CURRENT}/HACKED_PACK.log
	
for HACKED_PACK in $HACKED_PACKS
do  
	if [[ -f "${APP_DIR_CURRENT}/vendor/composer/installed.json" ]];then
		grep -q "${HACKED_PACK}" ${APP_DIR_CURRENT}/vendor/composer/installed.json && echo "Finded ${HACKED_PACK}. APP_HTTP_HOST=${APP_HTTP_HOST}" >> ${APP_DIR_CURRENT}/HACKED_PACK.log
	else
		grep -rlq "${HACKED_PACK}" ${APP_DIR_CURRENT}/vendor && echo "Finded ${HACKED_PACK}. APP_HTTP_HOST=${APP_HTTP_HOST}" >> ${APP_DIR_CURRENT}/HACKED_PACK.log
	fi
done
	cat  ${APP_DIR_CURRENT}/HACKED_PACK.log
fi