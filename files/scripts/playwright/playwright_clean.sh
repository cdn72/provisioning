nice -n19 ionice -c2 -n7  find ./tmp -type d  -mmin +120 | xargs rm -rfv



#!/usr/bin/env bash
export PLAYWRIGHT_WORKERS=$(docker ps --filter name=playwri --filter status=running -q)
for PLAYWRIGHT_WORKER in $PLAYWRIGHT_WORKERS;do
	MergedDir="/var/lib/docker/overlay2/7103a8d268afa0d1f6321b71a2e58b9e365ef75dcf78c5973eb00ee6b06c3793/merged"
	UpperDir="/var/lib/docker/overlay2/7103a8d268afa0d1f6321b71a2e58b9e365ef75dcf78c5973eb00ee6b06c3793/diff"
	nice -n19 ionice -c2 -n7  find ${MergedDir}/tmp -type d  -mmin +220 | xargs rm -rfv
	nice -n19 ionice -c2 -n7  find ${UpperDir}/tmp -type d  -mmin +220 | xargs rm -rfv
done


