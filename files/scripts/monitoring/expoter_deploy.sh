#!/usr/bin/env bash

exporter_prepare () {
   echo "Get common lib file"
   curl -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/dockers/common.sh -o /tmp/files/libs.d/dockers/common.sh
   dirs_req="/etc/prometheus/exporters/bin /tmp/files/libs.d/dockers"
   for dir_req   in ${dirs_req}
   do
    if [[ ! -d ${dir_req} ]];then mkdir -pv ${dir_req};fi
   done
}

exporter_get_ip () {
    . /tmp/files/libs.d/dockers/common.sh
}

exporter_get_redis () {
    curl -L https://github.com/oliver006/redis_exporter/releases/download/v1.44.0/redis_exporter-v1.44.0.linux-amd64.tar.gz -o /tmp/redis_exporter-v1.44.0.linux-amd64.tar.gz
    cd /tmp && tar -xvf /tmp/redis_exporter-v1.44.0.linux-amd64.tar.gz && mv redis_exporter-v1.44.0.linux-amd64/redis_exporter /etc/prometheus/exporters/bin/ && chmod +x /etc/prometheus/exporters/bin/redis_exporter
}

exporter_deploy_redis_supervisor() {
    exporter_get_redis
    exporter_get_ip
cat <<OEF > /etc/supervisor/conf.d/exporter_redis.conf
#<!-- BEGIN exporter_redis -->
[supervisord]
nodaemon=true
[program:exporter_redis]
environment=HOME="/usr/bin/"
command=/etc/prometheus/exporters/bin/redis_exporter -redis.addr "redis://172.18.0.4:6379" -web.listen-address "10.50.0.119:9121" -export-client-list
autostart=true
autorestart=true
startretries=300
stderr_logfile=/var/log/exporter_redis.err
stdout_logfile=/var/log/exporter_redis.log
#<!-- END exporter_redis -->
OEF

}


main() {
    exporter_prepare
    exporter_deploy_redis_supervisor
   


}



main