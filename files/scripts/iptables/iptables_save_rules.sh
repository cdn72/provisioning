#!/usr/bin/env bash

filename=$(hostname)-$(curl -q ifconfig.io)
rules_save() {
	iptables-save > rules.txt
	cat rules.txt | grep -ve 'DOCKER\|172.\|docker\|br-' > rules_filtred_${filename/\./-}.txt
}



rules_load() {
	iptables-restore < rules_filtred_${filename/\./-}.txt
}


#rules_save
#rules_load