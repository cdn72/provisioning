#!/usr/bin/env bash
#bash /tmp/iptables_manage.sh --ip_list='${ip_allow}' --ip_ports='${ip_ports}'"
for i in "$@"
do
case $i in
    --ip_list=*) #ip_list="167.235.14.88/32 185.209.146.74/32"
    ip_list="${i#*=}"
    shift # past argument=value
    ;;
    --ip_ports=*) # ip_ports="8088 8021 2223 5060 5061 5068 5080 5081"
    ip_ports="${i#*=}"
    shift # past argument=value
    ;;
    --default)
    DEFAULT=YES
    shift 
    ;;
    *)
          # unknown option
    ;;
esac
done
clean_iptables_rules() {
  local ip_proto=$1
  local ip_port=$2
  local ip_action=$3
  echo "Filter iptables -L INPUT  --line-numbers -n | grep ${ip_port} | grep ${ip_action} | grep ${ip_proto}"
  echo "exec \"iptables -L INPUT  --line-numbers -n | grep "${ip_port}" | grep ${ip_action} | grep ${ip_proto}  | awk '{ print \$1 }' | sed -z 's/\n/ /g'"
  iptables_rules_list=$(iptables -L INPUT  --line-numbers -n | grep "${ip_port}" | grep ${ip_action} | grep ${ip_proto}  | awk '{ print $1 }' | sed -z 's/\n/ /g' )
  iptables_rules_count=$(echo $iptables_rules_list | wc -w )
  echo "iptables_rules_count=$iptables_rules_count"

  if [[ ${iptables_rules_count} > 0 ]];then
    local counter=0
    echo "Finded ${iptables_rules_count} rules ip_port=${ip_port} | ip_action=${ip_action} | ip_proto=${ip_proto}"
    until false
    do
      echo "Counter: $counter ip_port=${ip_port} | ip_action=${ip_action} | ip_proto=${ip_proto}"
      local rune_id_current=$(iptables -L INPUT  --line-numbers -n | grep "${ip_port}" | grep ${ip_action} | grep ${ip_proto}  | awk '{ print $1 }' | head -n 1)
      if [[ -n ${rune_id_current} ]];then
        printf "Exec shell iptables -D INPUT ${rune_id_current}"\n
        iptables -D INPUT ${rune_id_current}
        ((counter++))
      else
        printf "Rule not exist for"
        printf "Filter iptables -L INPUT  --line-numbers -n | grep ${ip_port} | grep ${ip_action} | grep ${ip_proto}\n"

        break
      fi

      
  done
  else
    echo "Not finded rules for rules ip_port=${ip_port} | ip_action=${ip_action} | ip_proto=${ip_proto}"
  fi
  local counter=0

}

iptables_rules_add() {
for ip_proto in ${ip_protos};do
  for ip_addr in ${ip_list};do
    for ip_port in ${ip_ports};do
      echo "Add rule iptables -A INPUT -p ${ip_proto} --dport ${ip_port} -s ${ip_addr} -j ACCEPT"
      iptables -A INPUT -p ${ip_proto} --dport ${ip_port} -s ${ip_addr} -j ACCEPT
done
done
done

}

iptables_rules_add_drop() {
for ip_proto in ${ip_protos};do
    for ip_port in ${ip_ports};do
      echo "Add rule iptables -A INPUT -p ${ip_proto} --dport ${ip_port} -j DROP"
      iptables -A INPUT -p ${ip_proto} --dport ${ip_port} -j DROP
done
done
}

iptables_rules_add_ddos(){
  iptables -t mangle --list --line-numbers -n | grep -q 'tcp flags:!0x17/0x02 ctstate NEW' ||  iptables -t mangle -A PREROUTING -p tcp ! --syn -m conntrack --ctstate NEW -j DROP
  iptables -t mangle --list --line-numbers -n | grep -q 'ctstate INVALID' || iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP
}


snips() {
  iptables -t mangle --list --line-numbers -n
  iptables --list --line-numbers -n
  sudo iptables --list --line-numbers -n | grep 8088
}

iptables_rules_drop () {

iptables --flush  INPUT

#for IPT_ACTION in $(echo ${IPT_ACTIONS})
#do
  #for ip_proto in ${ip_protos};
  #do
    #for ip_port in ${ip_ports};
    #do
      #echo "Exec clean_iptables_rules ${ip_proto} ${ip_port} ${IPT_ACTION}"
      #clean_iptables_rules ${ip_proto} ${ip_port} ${IPT_ACTION}
    #done
  #done
#done

}

main() {


  echo ip_list$ip_list
  if [[ -z $ip_list ]] ;then echo "Error --ip_list= empty." && exit 1 ;fi
  if [[ -z $ip_ports ]] ;then echo "Error --ip_ports= empty." && exit 1 ;fi

  ip_protos="udp tcp"
  IPT_ACTIONS="DROP ACCEPT"

  iptables_rules_drop
  iptables_rules_add
  iptables_rules_add_drop
  iptables_rules_add_ddos
  sleep 2;
  iptables --list --line-numbers -n
}


main