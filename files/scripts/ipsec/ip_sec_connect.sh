#!/usr/bin/env bash

for i in "$@"
do
case $i in

    --server=*) # --server=ipsec.linux2be.com
    SERVER="${i#*=}"
    shift # past argument=value
    ;;  
    --psk_secret=*) # --server="ang$\$hNcs1iLAno1"
    PSK_SECRET="${i#*=}"
    shift # past argument=value
    ;;  
    --user=*) # --user="jkh-std2"
    USER="${i#*=}"
    shift # past argument=value
    ;;
    --password=*) # --password="fXD~Z\$LtLMg7"
    PASSWORD="${i#*=}"
    shift # past argument=value
    ;;
    --ip_route_add=*) # --ip_route_add=10.111.0.0/24
    IP_ROUTE_ADD="${i#*=}"
    shift # past argument=value
    ;;  

    --ip_sec_run_arg=*) # --ip_sec_run_arg=start|stop
    IP_SEC_RUN_ARG="${i#*=}"
    shift # past argument=value
    ;;  
    --default)
    DEFAULT=YES
    shift # past argument with no value
    ;;
    *)
          # unknown option
    ;;
esac
done

if grep -qe '^Ubuntu.*22\.04' /etc/issue.net;then 
    apt-get update --fix-missing && apt-get -y install dnsutils strongswan xl2tpd libcharon-extra-plugins libstrongswan-extra-plugins  kmod -y
    modprobe af_key
    modprobe l2tp_ppp
fi

CONNECT_NAME=${SERVER//./-}
GATEWAY_L2TP=$(nslookup ${SERVER} 1.1.1.1 | grep Address | grep -ve '1.1.1.1' | awk '{print $2}')



ipsec_connect_1(){
    systemctl stop strongswan-starter
cat<<OEF>/etc/ipsec.secrets
${PASSWORD}
OEF

cat > /etc/ipsec.secrets <<EOF
: PSK "${PSK_SECRET}"
#%any ${SERVER} : PSK "${PSK_SECRET}"
EOF
chmod 600 /etc/ipsec.secrets

cat <<EOF> /etc/ipsec.conf
config setup
conn ${CONNECT_NAME}
    right=${SERVER}
    rightid=${SERVER}
    rightsubnet=0.0.0.0/0
    rightauth=pubkey
    leftsourceip=%config
    leftid=${USER}
    leftauth=eap-mschapv2
    eap_identity=%identity
    auto=start


EOF
systemctl status strongswan-starter
systemctl start strongswan-starter
ipsec status
}


part1() {

cat <<EOF> /etc/ipsec.conf
config setup

conn %default
        ikelifetime=60m
        keylife=20m
        rekeymargin=3m
        keyingtries=1
        authby=secret
        ike=3des-sha1-modp1024
        esp=aes128-sha1 
conn ${CONNECT_NAME}
        keyexchange=ikev1
        left=%defaultroute
        auto=add
        keyingtries=3
        auth=esp
        authby=secret
        type=transport
        leftprotoport=17/1701
        rightprotoport=17/1701
        right=${GATEWAY_L2TP}

EOF
}

part2() {
cat > /etc/ipsec.secrets <<EOF
: PSK "${PSK_SECRET}"
#%any ${SERVER} : PSK "${PSK_SECRET}"
EOF
chmod 600 /etc/ipsec.secrets
}

part3() {


cat > /etc/xl2tpd/xl2tpd.conf <<EOF
[global]
access control = yes
ipsec saref = no
[lac ${CONNECT_NAME}]
lns = ${GATEWAY_L2TP}
ppp debug = yes
pppoptfile = /etc/ppp/options.l2tpd.client
length bit = yes
EOF

cat > /etc/ppp/options.l2tpd.client <<EOF
ipcp-accept-local
ipcp-accept-remote
refuse-eap
require-mschap-v2
noccp
noauth
idle 1800
mtu 1410
mru 1410
noipdefault
persist
usepeerdns
debug
connect-delay 5000
name $USER
password $PASSWORD
EOF
chmod 600 /etc/ppp/options.l2tpd.client
}

part4(){
    mkdir -p /var/run/xl2tpd
    touch /var/run/xl2tpd/l2tp-control
}


part5(){
    systemctl start strongswan-starter;systemctl enable strongswan-starter
    systemctl start xl2tpd;systemctl enable xl2tpd
}


part6(){
    echo "Exec ipsec up ${CONNECT_NAME}"
    ipsec up ${CONNECT_NAME}
    ipsec status ${CONNECT_NAME}
}

part7(){
    echo "c ${CONNECT_NAME} ${USER} ${PASSWORD}"
    echo "c ${CONNECT_NAME} ${USER} ${PASSWORD}" > /var/run/xl2tpd/l2tp-control
}

part8(){
    sleep 20;
    ip route add ${IP_ROUTE_ADD} dev ppp0
}

ip_sec_down () {
     ipsec down ${CONNECT_NAME}
     sleep 5;
     exit
}

main() {
if [[ "${IP_SEC_RUN_ARG}" == "stop" ]];then
    echo "Stop tunnel ${CONNECT_NAME}"
    ip_sec_down
fi
if [[ "${IP_SEC_RUN_ARG}" == "start" ]];then
    echo "Start ip tunnel ${CONNECT_NAME}"
    part1
    part2
    part3
    part4
    part5
    part6
    part7
    part8
fi




}

main


##DEBUG

##ipsec setup --start
##ipsec verify
## ipsec auto --readys
##/usr/sbin/ipsec  --add SPBVPN
##/usr/sbin/ipsec  --add SPBVPN
## route add YOUR_LOCAL_PC_PUBLIC_IP gw X.X.X.X
## start ip sec
#ipsec up spbclient
#echo "c spbclient jkh-std1 ang$$hNcs1iLAno1" > /var/run/xl2tpd/l2tp-control

##MANS
##https://www.cisco.com/c/ru_ru/support/docs/ip/internet-key-exchange-ike/117258-config-l2l.html
##https://www.kerkeni.net/configure-l2tp-ipsec-vpn-on-ubuntu-15-10-16-04-to-fortigate-forti-os-5-2.htm