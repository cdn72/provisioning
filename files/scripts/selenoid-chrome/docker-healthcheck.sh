#!/usr/bin/env bash

if [[ -f "./selenium-check.py" ]];then
        python3.8 ./selenium-check.py
        if [[ $? == "0" ]];then
                echo "Alive date=$(date +%Y_%m_%d__%T)"
                echo "Sleep 5" && sleep 5
        else
                echo "Bad health - kill main process date=$(date +%Y_%m_%d__%T)"
                /usr/local/bin/docker-compose down || (echo "skip stop")
                /usr/local/bin/docker-compose up -d
                echo "Sleep 50" && sleep 50
        fi
fi
